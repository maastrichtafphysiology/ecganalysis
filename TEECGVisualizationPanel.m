classdef TEECGVisualizationPanel < NonInvasiveECGVisualizationPanel
    properties
        ValidTEChannels
    end
    
    properties (Access = protected)
        TEAxesPanel
        TEAxesHandles
      
        TEPanel
        TEListbox
        SelectedTEChannels
        
        ValidTEChannelPanel
        ValidTEChannelsListbox
    end
    
    methods
        function self = TEECGVisualizationPanel(position)
            self = self@NonInvasiveECGVisualizationPanel(position);
        end
        
        function SetECGData(self, ecgData)
            self.SelectedTEChannels = 1;
            self.ValidTEChannels = 1:ecgData.GetNumberOfTEChannels;
            
            SetECGData@NonInvasiveECGVisualizationPanel(self, ecgData);
        end
        
        function ecgData = GetECGDataToAnalyze(self)
            if ~isempty(self.ValidChannels)
                validChannels = {self.ValidChannels, self.ValidTEChannels};
                ecgData = self.EcgData.Copy(validChannels, self.ValidTime);
            else
                ecgData = TEECGData.empty(0);
            end
        end
    end
    
    methods (Access = protected)
        function  SetChannelList(self)
            SetChannelList@NonInvasiveECGVisualizationPanel(self);
           
            set(self.TEListbox, 'string', self.EcgData.TELabels);
            set(self.TEListbox, 'value', find(self.SelectedTEChannels));
            
            set(self.ValidTEChannelsListbox, 'string', self.EcgData.TELabels);
            set(self.ValidTEChannelsListbox, 'value', find(self.ValidTEChannels));
        end
        
        function ShowECGData(self)
            ShowECGData@NonInvasiveECGVisualizationPanel(self);
            
            self.ShowTEData();
            
            if numel(self.TEAxesHandles) >= 1 && numel(self.AxesHandles) >= 1
                validTEHandles = ishandle(self.TEAxesHandles);
                validECGAxesHandles = ishandle(self.AxesHandles);
                if any(validTEHandles) && any(validECGAxesHandles)
                    linkaxes([self.AxesHandles(validECGAxesHandles);...
                        self.TEAxesHandles(validTEHandles)], 'x');
                end
            end
        end
        
        function ShowTEData(self)
            time = self.EcgData.GetTimeRange();
            validHandles = ishandle(self.TEAxesHandles);
            if any(validHandles)
                delete(self.TEAxesHandles(validHandles));
            end
            self.TEAxesHandles = [];

            lineData = self.EcgData.TEData(:, self.SelectedTEChannels);
            lineLabels = self.EcgData.TELabels(self.SelectedTEChannels);
            
            self.TEAxesHandles = ECGChannelPanel.TightSubplot(numel(lineLabels), 1, self.TEAxesPanel,...
                0.0025, [0.05, 0], [0, 0.05]);
            
            for lineIndex = 1:numel(lineLabels)
                lineColor = [0 0 0];
                line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'parent', self.TEAxesHandles(lineIndex),...
                    'lineSmoothing', 'on');
                text('parent', self.TEAxesHandles(lineIndex), 'string', lineLabels{lineIndex},...
                    'fontWeight', 'bold',...
                    'fontName', 'Helvetica',...
                    'fontSize', 11,...
                    'units', 'normalized', 'position', [0, 1, 0],...
                    'horizontalAlignment', 'left',...
                    'verticalAlignment', 'top',...
                    'interpreter', 'none');
                set(self.TEAxesHandles(lineIndex), 'xTick', [],...
                    'YAxisLocation', 'right', 'xLim', [time(1), time(end)]);
                axis(self.TEAxesHandles(lineIndex), 'tight');
            end
            set(self.TEAxesHandles(end),'xTickMode', 'auto');
            xlabel(self.TEAxesHandles(end), 'time (seconds)');
            
            if numel(self.TEAxesHandles) > 1
                validHandles = ishandle(self.TEAxesHandles);
                if any(validHandles)
                    linkaxes(self.TEAxesHandles(validHandles), 'xy');
                end
            end
            
            channelIsValid = ismember(self.SelectedTEChannels, self.ValidTEChannels);
            for axesIndex = 1:numel(self.TEAxesHandles)
                if channelIsValid(axesIndex)
                    axesColor = [1 1 1];
                else
                    axesColor = [.8 .8 .8];
                end
                set(self.TEAxesHandles(axesIndex), 'color', axesColor);
            end
        end
        
        function CreateECGAxes(self)
            CreateECGAxes@NonInvasiveECGVisualizationPanel(self);
            
            set(self.AxesPanel, 'position', [.2 .5 .8 .5]);
            
            self.TEAxesPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 .5]);
        end
        
        function CreateChannelSelection(self)
            CreateChannelSelection@NonInvasiveECGVisualizationPanel(self);
            
            set(self.VisibleChannelPanel, 'position', [0 .5 .5 .5]);
            set(self.ValidChannelPanel, 'position', [.5 .5 .5 .5]);
            
            self.TEPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [0 .2 .5 .3],...
                'title', 'TE-ECG',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.TEListbox = uicontrol(...
                'parent', self.TEPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetTEChannels,...
                'busyAction', 'cancel');
            
            self.ValidTEChannelPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [.5 .2 .5 .3],...
                'title', 'Channels to analyze',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.ValidTEChannelsListbox = uicontrol(...
                'parent', self.ValidTEChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetValidTEChannels,...
                'busyAction', 'cancel');
        end
        
        function SetTEChannels(self, varargin)
            self.SelectedTEChannels = get(self.TEListbox, 'value');
            self.ShowECGData();
        end
        
        function SetValidTEChannels(self, varargin)
            self.ValidTEChannels = get(self.ValidTEChannelsListbox, 'value');
            self.ShowECGData();
        end
    end
end