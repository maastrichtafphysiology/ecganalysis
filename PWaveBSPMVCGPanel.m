classdef PWaveBSPMVCGPanel < PWaveVCGPanel
    
    properties (SetObservable)
        XYZIndices
    end
    
    properties (Access = protected)
        XLead1Popup
        XLead2Popup
        YLead1Popup
        YLead2Popup
        ZLead1Popup
        ZLead2Popup
    end
    
    methods
        function self = PWaveBSPMVCGPanel(position)
            self = self@PWaveVCGPanel(position);
            self.XYZIndices = NaN(3, 2);
        end
        
        function Create(self, parentHandle)
            Create@PWaveVCGPanel(self, parentHandle);
        end
        
        function SetPWaveData(self, pWaveData, map)
            SetPWaveData@PWaveVCGPanel(self, pWaveData, map);
            
            self.InitializeLeadSelection();
            self.SetXYZIndices();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@PWaveVCGPanel(self);
            
            % X
            xLeadsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .8 1 .15],...
                'title', 'Lead X',...
                'titlePosition', 'centertop');
            
            self.XLead1Popup = uicontrol('parent', xLeadsPanel,...
                'units', 'normalized',...
                'position', [.1, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
            
            self.XLead2Popup = uicontrol('parent', xLeadsPanel,...
                'units', 'normalized',...
                'position', [.6, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
            
            % Y
            yLeadsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .65 1 .15],...
                'title', 'Lead Y',...
                'titlePosition', 'centertop');
            
            self.YLead1Popup = uicontrol('parent', yLeadsPanel,...
                'units', 'normalized',...
                'position', [.1, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
            
            self.YLead2Popup = uicontrol('parent', yLeadsPanel,...
                'units', 'normalized',...
                'position', [.6, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
            
            % Z
            zLeadsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .50 1 .15],...
                'title', 'Lead Z',...
                'titlePosition', 'centertop');
            
            self.ZLead1Popup = uicontrol('parent', zLeadsPanel,...
                'units', 'normalized',...
                'position', [.1, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
            
            self.ZLead2Popup = uicontrol('parent', zLeadsPanel,...
                'units', 'normalized',...
                'position', [.6, 0, .3, 2/3],...
                'style', 'popupmenu',...
                'string', 'Select lead',...
                'callback', @self.SetXYZIndices);
        end
        
        function InitializeLeadSelection(self)
            electrodeLabels = self.PWaveData.ElectrodeLabels;
            set(self.XLead1Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(1,1))
                set(self.XLead1Popup, 'value', self.XYZIndices(1,1));
            end
            set(self.XLead2Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(1,2))
                set(self.XLead2Popup, 'value', self.XYZIndices(1,2));
            end
            set(self.YLead1Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(2,1))
                set(self.YLead1Popup, 'value', self.XYZIndices(2,1));
            end
            set(self.YLead2Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(2,2))
                set(self.YLead2Popup, 'value', self.XYZIndices(2,2));
            end
            set(self.ZLead1Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(3,1))
                set(self.ZLead1Popup, 'value', self.XYZIndices(3,1));
            end
            set(self.ZLead2Popup, 'string', electrodeLabels);
            if ~isnan(self.XYZIndices(3,2))
                set(self.ZLead2Popup, 'value', self.XYZIndices(3,2));
            end
        end
        
        function ShowXYZPWaves(self)
            delete(get(self.VCGView, 'Children'));
            
            axesHandles = ECGChannelPanel.TightSubplot(3, 1, self.VCGView,...
                0.01, [0.05, 0.025], [0.1, 0.05]);
            
            averagePWaves = self.PWaveData.AveragePWaves;
            % X
            xPwave = averagePWaves(:, self.XYZIndices(1, 2)) - averagePWaves(:, self.XYZIndices(1, 1));
            self.ShowPWave(axesHandles(1), xPwave);
            text(0.05, 0.95, 'X',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(1));
            set(axesHandles(1), 'xTickLabel', {});
            
            % Y
            yPwave = averagePWaves(:, self.XYZIndices(2, 2)) - averagePWaves(:, self.XYZIndices(2, 1));
            self.ShowPWave(axesHandles(2), yPwave);
            text(0.05, 0.95, 'Y',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(2));
            set(axesHandles(2), 'xTickLabel', {});
            
            % Z
            zPwave = averagePWaves(:, self.XYZIndices(3, 2)) - averagePWaves(:, self.XYZIndices(3, 1));
            self.ShowPWave(axesHandles(3), zPwave);
            text(0.05, 0.95, 'Z',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(3));
            
            xlabel(axesHandles(3), 'Time (ms)');            
        end
        
        function ShowXYZLoop(self)
            delete(get(self.VCGLoopView, 'Children'));
            
            axesHandle = subplot(1,1,1, 'parent', self.VCGLoopView);
            
            intervalTime = self.PWaveData.AveragingInterval;
            
            averagePWaves = self.PWaveData.AveragePWaves;
            xPwave = averagePWaves(:, self.XYZIndices(1, 2)) - averagePWaves(:, self.XYZIndices(1, 1));
            yPwave = averagePWaves(:, self.XYZIndices(2, 2)) - averagePWaves(:, self.XYZIndices(2, 1));
            zPwave = averagePWaves(:, self.XYZIndices(3, 2)) - averagePWaves(:, self.XYZIndices(3, 1));
            
            [~, maximumPosition] = max(sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2));
            
            scatter3(xPwave, yPwave, min(zPwave) * ones(size(zPwave)), 5, [.7, .7, .7], 'parent', axesHandle);
            hold(axesHandle, 'on');
            scatter3(max(xPwave) * ones(size(xPwave)), yPwave, zPwave, 5, [.7, .7, .7], 'parent', axesHandle);
            scatter3(xPwave, max(yPwave) * ones(size(yPwave)), zPwave, 5, [.7, .7, .7], 'parent', axesHandle);
            scatter3(xPwave, yPwave, zPwave, 20, intervalTime, 'parent', axesHandle);
            quiver3(0,0,0, xPwave(maximumPosition), yPwave(maximumPosition), zPwave(maximumPosition),...
                'lineWidth', 2, 'color', [0,0,0], 'maxHeadSize', 0.5);
            
            hold(axesHandle, 'off');
            axis(axesHandle, 'tight');
            xlabel(axesHandle, 'X');
            ylabel(axesHandle, 'Y');
            zlabel(axesHandle, 'Z');
            title(axesHandle, 'XYZ loop');
            colorbar;
        end
        
        function ShowXYZData(self)
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            % scale time to seconds
            intervalTime = intervalTime / 1000;
            
            % scale signals to micro volt
            averagePWaves = self.PWaveData.AveragePWaves * 1000;
            xPwave = averagePWaves(:, self.XYZIndices(1, 2)) - averagePWaves(:, self.XYZIndices(1, 1));
            yPwave = averagePWaves(:, self.XYZIndices(2, 2)) - averagePWaves(:, self.XYZIndices(2, 1));
            zPwave = averagePWaves(:, self.XYZIndices(3, 2)) - averagePWaves(:, self.XYZIndices(3, 1));
            xyzWave = sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2);
            
            xArea = trapz(intervalTime(validTime), xPwave(validTime));
            yArea = trapz(intervalTime(validTime), yPwave(validTime));
            zArea = trapz(intervalTime(validTime), zPwave(validTime));
            
            xAreaAbs = trapz(intervalTime(validTime), abs(xPwave(validTime)));
            yAreaAbs = trapz(intervalTime(validTime), abs(yPwave(validTime)));
            zAreaAbs = trapz(intervalTime(validTime), abs(zPwave(validTime)));
            xyzArea = trapz(intervalTime(validTime), abs(xyzWave(validTime)));
            
            % azimuth (transversal XZ plane)& elevation (frontal Y-XZ plane)
            [maximumDistance, maximumPosition] = max(sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2));
            [azimuth, elevation] = cart2sph(xPwave(maximumPosition), zPwave(maximumPosition), yPwave(maximumPosition));
            
            
            resultString = {...
                'Uncorrected absolute | signed XYZ area';...
                '';...
                ['X area: ', num2str(xAreaAbs, '%.2f'), 'muVs', ' | ', num2str(xArea, '%.2f'), 'muVs'];...
                ['Y area: ', num2str(yAreaAbs, '%.2f'), 'muVs', ' | ', num2str(yArea, '%.2f'), 'muVs'];...
                ['Z area: ', num2str(zAreaAbs, '%.2f'), 'muVs', ' | ', num2str(zArea, '%.2f'), 'muVs'];...
                ['XYZ area: ', num2str(xyzArea, '%.2f'), 'muVs'];...
                '';...
                ['Maxmimum distance from origin: ', num2str(maximumDistance, '%.2f'), 'muV'];...
                ['Azimuth (XZ): ', num2str(180 * azimuth /pi, '%.2f'), ' degrees'];...
                ['Elevation (Y-XZ): ', num2str(180 * elevation /pi, '%.2f'), ' degrees']};
            
            set(self.ResultEdit, 'string', resultString, 'horizontalAlignment', 'left', 'fontSize', 12);
        end
    end
    
    methods(Access = private)
        function SetXYZIndices(self, varargin)
            if isempty(self.PWaveData), return; end
            
            newXYZIndices = NaN(3, 2);
            
            newXYZIndices(1, 1) = get(self.XLead1Popup, 'value');
            newXYZIndices(1, 2) = get(self.XLead2Popup, 'value');
            newXYZIndices(2, 1) = get(self.YLead1Popup, 'value');
            newXYZIndices(2, 2) = get(self.YLead2Popup, 'value');
            newXYZIndices(3, 1) = get(self.ZLead1Popup, 'value');
            newXYZIndices(3, 2) = get(self.ZLead2Popup, 'value');
            
            self.XYZIndices = newXYZIndices;
            
            self.ShowXYZPWaves();
            self.ShowXYZLoop();
            self.ShowXYZData();
        end
    end
end