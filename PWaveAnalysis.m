classdef PWaveAnalysis < UserInterfacePkg.CustomFigure
    properties (Access = private)
        EcgData
        
        TabGroup
        
        ChannelSelectionView
        PreProcessingView
        QRSTDetectionView
        PWaveMatchingView
        PWaveDurationView
        PWaveAnalysisView
        
        QRSTDetectionTab
        PWaveMatchingTab
        PWaveDurationTab
        
        ApplicationSettings
        ParallelProcessing
        
        ZoomToggleButton
        PanToggleButton
        
        AnalysisStatusBar
        
        MappedFileTypeIndices
    end
    
    properties (Constant)
        FILETYPEFILTER = {...
            '*.bdf', 'BioSemi TE-ECG Measurement (*.bdf)';...
            '*.bdf', 'BioSemi BSPM Measurement (*.bdf)';...
            '*.xml', 'Schiller XML data (*.xml)';...
            '*.xml', 'CardioSoft XML data (*.xml)';...
            '*.xml', 'MUSE XML data (*.xml)';...
            '*.txt', 'BARD ECG (*.txt)';
            '*.csv', 'Brugada CSV data (*.csv)';...
            '*.csv', '8 lead CSV data (*.csv)';...
            '*.dat', 'Amedtec ECG data (*.dat)';...
            '*.asc', 'KORA S4 data (*.asc)';...
            '*.rhy', 'KORA S4 data (*.rhy)';...
            '*.csv', 'MyDiagnostic data (*.csv)';...
            '*.txt', 'YourRhythmics data (*.txt)';...
            '*.DATA', 'YourRhythmics DATA (*.DATA)';...
            '*.csv', 'YourRhythmics csv (*.csv)';...
            '*.240', 'EP Solutions 240 BSPM data (*.240)';...
            '*.mat', 'LabChart MATLAB export (*.mat)';...
            '*.dcm', 'DICOM ECG (*.dcm)'};
    end
    
    methods
        function self = PWaveAnalysis(position)
            if nargin < 1
                InitializeNoninvasiveAnalysis;
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'P-Wave Analysis';
            self.ParallelProcessing = false;
            self.MappedFileTypeIndices = 1:size(PWaveAnalysis.FILETYPEFILTER, 1);
            
            if nargin < 1
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', jet(256));
            
            self.Hide();
            
            self.InitializeApplicationSettings();
            
            self.CreateFileMenu();
            
            self.CreateSettingsMenu();
            
            self.CreateToolbar();
            
            self.CreateStatusBar();
            
            self.InitializeEvents();
            
            self.InitializeApplicationSettings();
            
            self.Show();
        end
        
        function delete(self)
            
        end
    end
    
    methods (Access = private)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end
            
            switch class(self.EcgData)
                case 'BDFData'
                    self.InitializeBDFTabs();
                case {'ECGData', 'DCMData'}
                    self.InitializeECGTabs();
            end
        end
        
        function ShowEmptyECGTabs(self, varargin)
            self.EcgData = ECGData('', [], NaN);
            self.InitializeTabs();
        end
        
        function InitializeECGTabs(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
            self.ChannelSelectionView = NonInvasiveECGVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            preProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = ECGPreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.EcgPreProcessor.ApplyBandpassFilter = 1;
            self.PreProcessingView.EcgPreProcessor.ApplyMedianBaselineFilter = 1;
            self.PreProcessingView.EcgPreProcessor.ApplyCombFilterPerLead = 1;
            self.PreProcessingView.EcgPreProcessor.ApplyBandpassFilter = 1;
            self.PreProcessingView.EcgPreProcessor.BandpassFrequencies = [0.1 150];
            self.PreProcessingView.EcgPreProcessor.BandpassOrder = 2;
            
            self.PreProcessingView.Create(preProcessingTab);
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            self.QRSTDetectionTab = uitab(self.TabGroup, 'title','QRS-T detection');
            self.QRSTDetectionView = QRSTDetectionPanel([0 0 1 1]);
            self.QRSTDetectionView.Create(self.QRSTDetectionTab);
            self.QRSTDetectionView.Show();
            
            addlistener(self.QRSTDetectionView, 'QRSTDetectionCompleted',...
                @self.HandleQRSTDetectionEvent);
            
            self.PWaveMatchingTab = uitab(self.TabGroup, 'title','P-Wave Matching');
            self.PWaveMatchingView = PWaveMatchingPanel([0 0 1 1]);
            self.PWaveMatchingView.Create(self.PWaveMatchingTab);
            self.PWaveMatchingView.Show();
            
            addlistener(self.PWaveMatchingView, 'PWaveMatchingCompleted',...
                @self.HandlePWaveMatchingEvent);
            
            self.PWaveDurationTab = uitab(self.TabGroup, 'title','P-Wave Duration');
            self.PWaveDurationView = PWaveDurationPanel([0 0 1 1]);
            self.PWaveDurationView.Create(self.PWaveDurationTab);
            self.PWaveDurationView.Show();
            
            addlistener(self.PWaveDurationView, 'PWaveDurationChecked',...
                @self.HandlePWaveDurationEvent);
            addlistener(self.PWaveDurationView, 'NewPWaveMatchingLoaded',...
                @self.HandleNewPWaveMatchingLoadedEvent);
            
            pWaveAnalysisTab = uitab(self.TabGroup, 'title','P-Wave Analysis');
            self.PWaveAnalysisView = PWaveAnalysisPanel([0 0 1 1]);
            self.PWaveAnalysisView.Create(pWaveAnalysisTab);
            self.PWaveAnalysisView.Show();
        end
        
        function InitializeBDFTabs(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
            self.ChannelSelectionView = BDFVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            preProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(preProcessingTab);
%             self.PreProcessingView.DisableBandpassFilter();
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            self.QRSTDetectionTab = uitab(self.TabGroup, 'title','QRS-T Detection');
            self.QRSTDetectionView = QRSTDetectionPanel([0 0 1 1]);
            self.QRSTDetectionView.Create(self.QRSTDetectionTab);
            self.QRSTDetectionView.Show();
            
            addlistener(self.QRSTDetectionView, 'QRSTDetectionCompleted',...
                @self.HandleQRSTDetectionEvent);
            
            self.PWaveMatchingTab = uitab(self.TabGroup, 'title','P-Wave Matching');
            self.PWaveMatchingView = PWaveMatchingPanel([0 0 1 1]);
            self.PWaveMatchingView.Create(self.PWaveMatchingTab);
            self.PWaveMatchingView.Show();
            
            addlistener(self.PWaveMatchingView, 'PWaveMatchingCompleted',...
                @self.HandlePWaveMatchingEvent);
            
            pWaveAnalysisTab = uitab(self.TabGroup, 'title','P-Wave Analysis');
            self.PWaveAnalysisView = PWaveAnalysisPanel([0 0 1 1]);
            self.PWaveAnalysisView.Create(pWaveAnalysisTab);
            self.PWaveAnalysisView.Show();
        end
        
        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end
            
            if isobject(self.PreProcessingView)
                delete(self.PreProcessingView);
            end
            
            if isobject(self.QRSTDetectionView)
                delete(self.QRSTDetectionView);
            end
            
            if isobject(self.PWaveMatchingView)
                delete(self.PWaveMatchingView);
            end
            
            if isobject(self.PWaveDurationView)
                delete(self.PWaveDurationView);
            end
            
            if isobject(self.PWaveAnalysisView)
                delete(self.PWaveAnalysisView);
            end
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Load signal',...
                'callback', @self.LoadSignal);
        end
        
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Algorithm settings',...
                'callback', @self.ShowApplicationSettings);
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load signal',...
                'clickedCallback', @self.LoadSignal);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.DCMButtonData,...
                'tooltipString', 'Show empty ECG tabs',...
                'clickedCallback', @self.ShowEmptyECGTabs);
        end
        
        function CreateStatusBar(self)
            self.AnalysisStatusBar = UserInterfacePkg.StatusBar([0 0 1 .025]);
            self.AddCustomControl(self.AnalysisStatusBar);
            self.AnalysisStatusBar.AddListener(UserInterfacePkg.StatusEventClass.Instance(), 'StatusChange');
            self.AnalysisStatusBar.Show();
        end
        
        function SetFilename(self)
            if isempty(self.EcgData), return; end
            
            self.Name = ['P-Wave Analysis - ', self.EcgData.Filename];
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);
            
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function LoadSignal(self, varargin)
            fileTypeFilters = PWaveAnalysis.FILETYPEFILTER;
            mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);
            
            % MAC OS (until 11.4) is not compatible with the uigetfile
            % filter option. Workaround for mac users: 
            if ismac
                filterIndex = menu('Select file type',mappedFiletypeFilters(:,2));
                [fileName, pathName, ~] = HelperFunctions.customUigetfile(...
                    '*.*',...
                    'Select a signal file');
            elseif ispc 
                [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                    mappedFiletypeFilters,...
                    'Select a signal file');
            end
            
            if isequal(fileName, 0), return; end
            
            mappedFilterIndex = self.MappedFileTypeIndices(filterIndex);
            if filterIndex > 1
                fileTypeIndices = 1:size(PWaveAnalysis.FILETYPEFILTER, 1);
                fileTypeIndices(mappedFilterIndex) = [];
                self.MappedFileTypeIndices = [mappedFilterIndex, fileTypeIndices];
            end
                
            switch mappedFilterIndex
                case {1, 2}
                    ecgData = InputOutputPkg.ECGReader.ReadBDF(fullfile(pathName, fileName));
                    ecgData.Organize();
                    switch mappedFilterIndex
                        case 2
                            ecgData.Type = 'BSPM';
                    end
                case 3
                    ecgData = InputOutputPkg.ECGReader.ReadSchillerXML(fullfile(pathName, fileName));
                case 4
                    ecgData = InputOutputPkg.ECGReader.ReadCardioSoftFullDisclosure(fullfile(pathName, fileName));
                case 5
                    ecgData = InputOutputPkg.ECGReader.ReadMUMCBase64XML(fullfile(pathName, fileName));
                case 6
                    ecgData = InputOutputPkg.ECGReader.ReadBARDECG(fullfile(pathName, fileName));
                case 7
                    ecgData = InputOutputPkg.ECGReader.ReadBrugadaCSV(fullfile(pathName, fileName));
                case 8
                    ecgData = InputOutputPkg.ECGReader.Read8LeadCSV(fullfile(pathName, fileName));
                case 9
                    ecgData = InputOutputPkg.ECGReader.ReadAmedtecData(fullfile(pathName, fileName));
                case 10
                    ecgData = InputOutputPkg.ECGReader.ReadKORAS4(fullfile(pathName, fileName));
                case 11
                    ecgData = InputOutputPkg.ECGReader.ReadKORAS4RHY(fullfile(pathName, fileName));
                case 12
                    ecgData = InputOutputPkg.ECGReader.ReadMyDiagnostic(fullfile(pathName, fileName));
                case 13
                    ecgData = InputOutputPkg.ECGReader.ReadYRTXT(fullfile(pathName, fileName));
                case 14
                    ecgData = InputOutputPkg.ECGReader.ReadYourRhythmicsData(fullfile(pathName, fileName));
                case 15
                    ecgData = InputOutputPkg.ECGReader.ReadYourRhythmicsCSV(fullfile(pathName, fileName));
                case 16
                    ecgData = InputOutputPkg.ECGReader.ReadEPSolutionsECG240(fullfile(pathName, fileName));
                case 17
                    ecgData = InputOutputPkg.ECGReader.ReadLabChart(fullfile(pathName, fileName));
                case 18
                    ecgData = InputOutputPkg.ECGReader.ReadDICOM(fullfile(pathName, fileName));
                otherwise
                    errordlg('File Error', 'Unkown file type');
                    return;
            end
            
            self.EcgData = ecgData;
            self.SetFilename();
            self.InitializeTabs();
            
            self.ChannelSelectionView.SetECGData(self.EcgData);
            
            switch mappedFilterIndex
                case {4, 5, 7, 8, 9, 10, 11, 13, 14}
%                     self.AutomaticStandardECG();
                case {12}
                    self.AutomaticHandHeld();
                otherwise
                    return;
            end
        end
        
        function AutomaticStandardECG(self)
            ecgPreProcessor = ECGPreProcessor();
            ecgPreProcessor.BandpassFrequencies = [0.1, 250];
            ecgPreProcessor.BandpassOrder = 2;
            ecgPreProcessor.ApplyBandpassFilter = true;
            bandpassFrequencies = [0.1, 250];
            
            ecgPreProcessor.NotchFrequency = 49.15;
            ecgPreProcessor.ApplyNotchFilter = true;
            
            self.PreProcessingView.SetECGData(self.EcgData, ecgPreProcessor);
            preProcessedData = self.PreProcessingView.PreProcessData(bandpassFrequencies);
            self.QRSTDetectionView.SetECGData(preProcessedData);
            
            leadIIindex = find(ismember(preProcessedData.ElectrodeLabels, 'II'));
            
            qrstDetectionElectrodeIndex = leadIIindex;
            qrstDetector = QRSTDetector();
            qrstDetector.OriginalPanTompkins = false;
            qrstDetector.VentricularRefractoryPeriod = 0.5;
            qrstDetector.IntegrationWindowLength = 0.3;
            qrstDetector.SelectProximalPeak = true;
            qrstDetector.SelectedPeakSign = 1;
            self.QRSTDetectionView.DetectRWavesOnLead(qrstDetectionElectrodeIndex,...
                qrstDetector, 1:preProcessedData.GetNumberOfChannels);
            
            rWaveIndices = self.QRSTDetectionView.RWaveIndices;
            ecgData = self.QRSTDetectionView.EcgData;
            
            self.PWaveMatchingView.RWaveIndices = rWaveIndices;
            self.PWaveMatchingView.SetECGData(ecgData);
            
            pWaveMatching = PWaveMatcher();
            pWaveMatching.CorrelationThreshold = 0.9;
            pWaveMatching.TemplateShift = 0.030;
            pWaveMatching.PWaveRange = [-0.300, -0.050];
            
            self.PWaveMatchingView.ComputeAndShowPWaveMatching(pWaveMatching);
            
            set(self.TabGroup, 'selectedTab', self.PWaveMatchingTab);
        end
        
        function AutomaticHandHeld(self)
            ecgPreProcessor = ECGPreProcessor();
            ecgPreProcessor.BandpassFrequencies = [0.1, 100];
            ecgPreProcessor.BandpassOrder = 2;
            ecgPreProcessor.ApplyBandpassFilter = true;
            bandpassFrequencies = [0.1, 100];
            
            self.PreProcessingView.SetECGData(self.EcgData, ecgPreProcessor);
            preProcessedData = self.PreProcessingView.PreProcessData(bandpassFrequencies);            
            self.QRSTDetectionView.SetECGData(preProcessedData);
            
            qrstDetectionElectrodeIndex = 1;
            qrstDetector = QRSTDetector();
            qrstDetector.OriginalPanTompkins = false;
            qrstDetector.VentricularRefractoryPeriod = 0.2;
            qrstDetector.IntegrationWindowLength = 0.3;
            qrstDetector.SelectProximalPeak = true;
            qrstDetector.SelectedPeakSign = -1;
            self.QRSTDetectionView.DetectRWavesOnLead(qrstDetectionElectrodeIndex,...
                qrstDetector, 1:preProcessedData.GetNumberOfChannels);
            
            rWaveIndices = self.QRSTDetectionView.RWaveIndices;
            ecgData = self.QRSTDetectionView.EcgData;
            
            self.PWaveMatchingView.RWaveIndices = rWaveIndices;
            self.PWaveMatchingView.SetECGData(ecgData);
            
            pWaveMatching = PWaveMatcher();
            pWaveMatching.PWaveRange = [-0.300, -0.050];
            pWaveMatching.BaselineBandwidth = 0.050;
            pWaveMatching.CorrelationThreshold = 0.5;
            pWaveMatching.TemplateShift = 0.0;
            
            self.PWaveMatchingView.ComputeAndShowPWaveMatching(pWaveMatching);
            
            set(self.TabGroup, 'selectedTab', self.PWaveMatchingTab);
        end
        
        function InitializeApplicationSettings(self)
            self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
            self.ApplicationSettings.Create();
            self.ApplicationSettings.Hide();
        end
        
        function ShowApplicationSettings(self, varargin)
            if ~isobject(self.ApplicationSettings)
                self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
                self.ApplicationSettings.Create();
            end
            self.ApplicationSettings.Show();
        end
        
        function SetParallelProcessing(self, source, varargin)
           if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                if parpool('size') > 0
                   parpool('close'); 
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                if parpool('size') == 0
                    parpool('open');
                end
            end
        end
        
        function PanOn(self, varargin)
            pan on;
            set(self.ZoomToggleButton, 'state', 'off');
        end
        
        function ZoomOn(self, varargin)
            zoom on;
            set(self.PanToggleButton, 'state', 'off');
        end
        
        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');
            
            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();
            
%             ecgPreProcessor = self.PreProcessingView.EcgPreProcessor();
%             self.PreProcessingView.SetECGData(ecgData,ecgPreProcessor);
            
            self.PreProcessingView.SetECGData(ecgData)
            % Reset bandpass frequencty because SetECGData sets BandpassStop to Nyquist freq
            self.PreProcessingView.EcgPreProcessor.BandpassFrequencies = [0.1 150];
            set(self.PreProcessingView.BandpassStopEdit,'string',num2str(150))
        end
        
        function HandlePreProcessingEvent(self, varargin)
            disp('Pre-processing completed');
            
            ecgData = self.PreProcessingView.GetPreProcessedECGData();
            
            self.QRSTDetectionView.SetECGData(ecgData);
        end
        
        function HandleQRSTDetectionEvent(self, varargin)
            disp('QRST cancellation completed');
            
            rWaveIndices = self.QRSTDetectionView.RWaveIndices;
            qOnsetIndex = self.QRSTDetectionView.QOnsetIndex;
            rIndex = self.QRSTDetectionView.RIndex;
            ecgData = self.QRSTDetectionView.EcgData;
            
            self.PWaveMatchingView.RWaveIndices = rWaveIndices;
            self.PWaveMatchingView.QOnsetIndex = qOnsetIndex;
            self.PWaveMatchingView.RIndex = rIndex;
            self.PWaveMatchingView.SetECGData(ecgData);
        end
        
        function HandlePWaveMatchingEvent(self, varargin)
            disp('P Wave matching completed');
            
            pWaveData = self.PWaveMatchingView.GetPWaveData();
            if isempty(pWaveData), return; end
            self.PWaveAnalysisView.SetPWaveData(pWaveData);
            
            pWaveMatchingData = self.PWaveMatchingView.GetPWaveMatchingData();
            if isempty(pWaveMatchingData), return; end
            self.PWaveDurationView.SetPWaveDurationData(pWaveMatchingData);
        end
        
        function HandlePWaveDurationEvent(self, varargin)
            disp('P Wave duration checked');
        end
        
        function HandleNewPWaveMatchingLoadedEvent(self, varargin)
            disp('New P-WaveMatching data loaded - updating P-Wave Matching tab');
            
            [pathString, nameString] = fileparts(self.PWaveDurationView.EcgData.Filename);
            filename = [pathString, filesep, nameString '_PWaveMatching.mat'];
            self.PWaveMatchingView.SetPWaveMatchingData(filename);
        end
        
        function Close(self, varargin)
            self.RemoveTabs();
            
            if isobject(self.EcgData)
                delete(self.EcgData)
            end
            
            delete(timerfindall);
            
            delete(self.AnalysisStatusBar);
            
            delete(self.ControlHandle);
        end
    end
end