classdef QRSTDetectionPanel < ECGChannelPanel
    properties
        ReferenceChannel
        RWaveIndices
        QOnsetIndex
        BeatChannel
        RIndex
    end
    
    properties (Access = protected)
        QrstDetector
        
        ReferenceChannelPopup
        
        RPeakMarkers
        RPeakLines
        RPeakListener
        SelectedRPeakIndex
        RPeakDistanceEdit
        
        BeatChannelPopup
        
        QRSTSettingsPanel
                        
        MouseListeners
        LineMode
        TouchedAxesIndex
        TouchedRPeakIndex
    end
    
    events
        QRSTDetectionCompleted
    end
    
    methods
        function self = QRSTDetectionPanel(position)
            self = self@ECGChannelPanel(position);
            
            self.QrstDetector = QRSTDetector();
            
            self.RPeakListener = addlistener(UserInterfacePkg.KeyEventClass.Instance(),...
                'KeyPressed', @self.HandleKeyPressedEvent);
            
            self.LineMode = 'Initial';
            self.MouseListeners = event.listener.empty(0, 0);
            
            self.SelectedRPeakIndex = NaN;
            self.TouchedRPeakIndex = NaN;
            self.TouchedAxesIndex = NaN;
        end
        
        function Create(self, parentHandle)
            Create@ECGChannelPanel(self, parentHandle);
            
            self.CreateMouseListeners();
        end
        
        function SetECGData(self, ecgData)
            self.ReferenceChannel = 1;
            self.BeatChannel = 1;
            SetECGData@ECGChannelPanel(self, ecgData);
            
            self.SelectedRPeakIndex = NaN;
            self.TouchedRPeakIndex = NaN;
            self.TouchedAxesIndex = NaN;
        end
        
        function ecgData = LoadData(self, filename)
            qrstData = load(filename);
            ecgData = qrstData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 5));
            if ~isempty(qrstData.ecgData)
                qrstData.ecgData.Filename = fullfile(pathString, nameString);
            end
                        
            self.RWaveIndices = qrstData.rWaveIndices;
            
            self.SetECGData(qrstData.ecgData);
            
            self.ReferenceChannel = qrstData.referenceChannel;
            set(self.ReferenceChannelPopup, 'value', self.ReferenceChannel);
        end
        
        function DetectRWavesOnLead(self, referenceLead, qrstDetector, visibleLeads)
            if isempty(self.EcgData)
                return;
            end
            
            if nargin < 4
                visibleLeads = 1:(min(3, self.EcgData.GetNumberOfChannels));
            end
            
            if nargin > 2
                self.QrstDetector = qrstDetector;
                self.CreateQRSTSettings();
                self.SetChannelList();
            end
            
            self.SelectedChannels = visibleLeads;
            self.ReferenceChannel = referenceLead;
            set(self.ReferenceChannelPopup, 'value', self.ReferenceChannel);
            
            self.ShowECGData();
            self.ComputeRDetection();


        end
        
        function delete(self)
            delete@ECGChannelPanel(self);
            
            if ~isempty(self.MouseListeners)
                delete(self.MouseListeners);
            end
            
            if ~isempty(self.RPeakListener)
                delete(self.RPeakListener);
            end
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@ECGChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .5 1 .49]);
            self.CreateQRSTSettings();
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .05 .8 .05],...
                'style', 'pushbutton',...
                'string', 'QRST detection completed',...
                'callback', @(src, event) notify(self, 'QRSTDetectionCompleted'));
        end
        
        function CreateQRSTSettings(self, varargin)
            if ishandle(self.QRSTSettingsPanel)
                delete(self.QRSTSettingsPanel)
            end
            
            self.QRSTSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .1 1 .39],...
                'borderType', 'none');
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .9 .4 .1],...
                'style', 'pushbutton',...
                'string', 'R-peak detection',...
                'callback', @self.ComputeRDetection);
            self.ReferenceChannelPopup = uicontrol(...
                'parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .925 .4 .05],...
                'style', 'popupmenu',...
                'string', 'Select reference channel',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetReferenceChannel,...
                'busyAction', 'cancel');
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .8 .4 .075],...
                'style', 'text',...                
                'string', 'R-peak distance (ms)',...
                'tooltipString', 'Change minimal R-peak distance to increase / decrease R-detection sensitivity');
            self.RPeakDistanceEdit = uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .8 .4 .1],...
                'style', 'edit',...
                'string', num2str(1000 * self.QrstDetector.VentricularRefractoryPeriod),...
                'callback', @self.SetVentricularRefractoryPeriod);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .7 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Show average beat',...
                'callback', @self.ShowAverageBeat);
            self.BeatChannelPopup = uicontrol(...
                'parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .725 .4 .05],...
                'style', 'popupmenu',...
                'string', 'Select channel',...
                'backgroundColor', [1 1 1],...
                'callback', @self.SetBeatChannel,...
                'busyAction', 'cancel');
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .4 .05],...
                'style', 'checkbox',...
                'string', 'Show diagnostics',...
                'value', self.QrstDetector.ShowDiagnostics,...
                'callback', @self.SetShowDiagnostics);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SaveQRSTDetection);
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadQRSTDetection);
        end
        
        function SetChannelList(self)
            SetChannelList@ECGChannelPanel(self);
            
            if isempty(self.EcgData.ReferenceLabels)
                set(self.ReferenceChannelPopup, 'string', self.EcgData.ElectrodeLabels);
            else
                set(self.ReferenceChannelPopup, 'string', self.EcgData.ReferenceLabels);
            end
            
            beatChannelList = self.EcgData.ElectrodeLabels;
            if ~isempty(self.EcgData.ReferenceLabels)
                beatChannelList = [beatChannelList; self.EcgData.ReferenceLabels];
            end
            set(self.BeatChannelPopup, 'string', beatChannelList);
            
            set(self.ChannelListbox, 'value', self.ReferenceChannel);
        end
        
        function ShowECGData(self)
            ShowECGData@ECGChannelPanel(self);
            
            self.ShowRWaves();
        end
        
        function ShowRWaves(self)
            if isempty(self.RWaveIndices), return; end
            
            validHandles = ishandle(self.RPeakLines);
            if any(validHandles)
                delete(self.RPeakLines(validHandles));
            end
            
            validHandles = ishandle(self.RPeakMarkers);
            if any(validHandles)
                delete(self.RPeakMarkers(validHandles));
            end
            
            time = self.EcgData.GetTimeRange();
            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
            self.RPeakLines = NaN(numel(self.AxesHandles), numel(self.RWaveIndices));
            self.RPeakMarkers = NaN(numel(self.AxesHandles), 1);
            for axesIndex = 1:numel(self.AxesHandles);
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for rIndex = 1:numel(self.RWaveIndices)
                    self.RPeakLines(axesIndex, rIndex) =...
                        line('xData', [time(self.RWaveIndices(rIndex)) time(self.RWaveIndices(rIndex))],...
                        'yData', yLimits,...
                        'color', [1 0 0],...
                        'parent', self.AxesHandles(axesIndex),...
                        'buttonDownFcn', @self.RPeakSelectedCallback,...
                        'userData', self.RWaveIndices(rIndex));
                end

                self.RPeakMarkers(axesIndex) =...
                        line('xData', time(self.RWaveIndices),...
                        'yData', lineData(self.RWaveIndices, axesIndex),...
                        'lineStyle', 'none',...
                        'marker', 'o', 'markerSize', 7,...
                        'markerEdgeColor', [0 0 0], 'markerFaceColor', [0 0 1],...
                        'parent', self.AxesHandles(axesIndex));
            end
        end
        
        function ShowAverageBeat(self, varargin)
             if isempty(self.RWaveIndices), return; end
             
             [averageBeat, beatStd] = self.QrstDetector.ComputeAverageBeat(self.EcgData, self.RWaveIndices, self.BeatChannel);
             
             figure('name', 'Average beat', 'numberTitle', 'off');
             time = (0:(numel(averageBeat) - 1)) / self.EcgData.SamplingFrequency;
             line('xData', time, 'yData', averageBeat, 'lineStyle', '-', 'lineWidth', 2, 'color', [0,0,1]);
             line('xData', time, 'yData', averageBeat + beatStd, 'lineStyle', ':', 'color', [0,0,1]);
             line('xData', time, 'yData', averageBeat - beatStd, 'lineStyle', ':', 'color', [0,0,1]);
             xlabel('Time (s)');
             ylabel('Voltage (mV)');
        end
        
        function SetReferenceChannel(self, varargin)
            self.ReferenceChannel = get(self.ReferenceChannelPopup, 'value');
        end
        
        function SetBeatChannel(self, varargin)
            self.BeatChannel = get(self.BeatChannelPopup, 'value');
        end
        
        function ComputeRDetection(self, varargin)
            referenceChannelIndex = self.ReferenceChannel;
            noReferenceData = false;
            if isempty(self.EcgData.ReferenceData)
                noReferenceData = true;
                self.EcgData.ReferenceData = self.EcgData.Data(:, self.ReferenceChannel);
                referenceChannelIndex = 1;
            end
            
            self.RWaveIndices = self.QrstDetector.DetectRWaves(self.EcgData, referenceChannelIndex);

            validChannels = 1:self.EcgData.GetNumberOfChannels();
            [self.QOnsetIndex, self.RIndex] = self.QrstDetector.DetectQOnset(self.EcgData, self.RWaveIndices, validChannels);

            if noReferenceData
                self.EcgData.ReferenceData = [];
            end

            self.ShowRWaves();
        end
        
        function LineSelectedCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
                uistack(source, 'bottom');
            end
        end
        
        function RPeakSelectedCallback(self, source, varargin)
            selectedRPeakPosition = get(source, 'userData');
            selectedRPeakIndex = find(self.RWaveIndices == selectedRPeakPosition);
            if ~isnan(self.SelectedRPeakIndex)
                for axesIndex = 1:numel(self.AxesHandles);
                    set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 1);
                end
            end
            
            if selectedRPeakIndex == self.SelectedRPeakIndex
                if strcmp(self.LineMode, 'Moving')
                    for axesIndex = 1:numel(self.AxesHandles);
                        set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 2);
                    end
                end
                self.SelectedRPeakIndex = NaN;
            else
                self.SelectedRPeakIndex = selectedRPeakIndex;
                for axesIndex = 1:numel(self.AxesHandles);
                    set(self.RPeakLines(axesIndex, self.SelectedRPeakIndex), 'lineWidth', 2);
                end
            end
        end
        
        function HandleKeyPressedEvent(self, ~, eventData)
            switch eventData.Key
                case {'delete', 'd'}
                    self.DeleteRPeak(self.SelectedRPeakIndex);
                    self.SelectedRPeakIndex = NaN;
                case 'i'
                    newPeakIndex = self.InsertRPeak();
                    self.SelectedRPeakIndex = newPeakIndex;
                    self.ShowRWaves();
                case {'rightarrow', 'leftarrow'}
                    if isnan(self.SelectedRPeakIndex), return; end
                    switch eventData.Key
                        case 'rightarrow'
                            self.MoveRPeak(self.SelectedRPeakIndex, 1);
                        case 'leftarrow'
                            self.MoveRPeak(self.SelectedRPeakIndex, -1);
                    end
            end
        end
        
        function HandleKeyReleasedEvent(self, ~, eventData)
            if isnan(self.SelectedRPeakIndex), return; end
            
            switch eventData.Key
                case 'rightarrow'
                    self.MoveRPeak(self.SelectedRPeakIndex, 1);
                case 'leftarrow'
                    self.MoveRPeak(self.SelectedRPeakIndex, -1);
            end
        end
        
        function SaveQRSTDetection(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_QRST'];
            ecgData = self.EcgData; %#ok<NASGU>
            rWaveIndices = self.RWaveIndices; %#ok<NASGU>
            qrstDetector = self.QrstDetector; %#ok<NASGU>
            referenceChannel = self.ReferenceChannel; %#ok<NASGU>
            save(filename,...
                'ecgData',...
                'rWaveIndices',...
                'qrstDetector',...
                'referenceChannel', '-v7.3');
        end
        
        function LoadQRSTDetection(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_QRST.mat', 'Select QRST file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_QRST.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function DeleteRPeak(self, rPeakIndex)
            if isnan(self.SelectedRPeakIndex), return; end
            self.RWaveIndices(rPeakIndex) = [];
            delete(self.RPeakLines(:, rPeakIndex));
            self.RPeakLines(:, rPeakIndex) = [];
        end
        
        function newPeakIndex = InsertRPeak(self)
            point = get(self.AxesHandles(1), 'currentPoint');
            mouseXLocation = point(1);
            time = self.EcgData.GetTimeRange();
            [value, newIndex] = min(abs(time - mouseXLocation)); %#ok<ASGLU>
            if ismember(newIndex, self.RWaveIndices), return; end
            
            previousIndexPosition = find(self.RWaveIndices < newIndex, 1, 'last');
            if isempty(previousIndexPosition)
                self.RWaveIndices = [newIndex; self.RWaveIndices];
                newPeakIndex = 1;
            else
                if previousIndexPosition == numel(self.RWaveIndices)
                    self.RWaveIndices = [self.RWaveIndices; newIndex];
                    newPeakIndex = numel(self.RWaveIndices);
                else
                    self.RWaveIndices = [self.RWaveIndices(1:previousIndexPosition);...
                        newIndex; self.RWaveIndices((previousIndexPosition + 1):end)];
                    newPeakIndex = previousIndexPosition + 1;
                end
            end
        end
        
        function MoveRPeak(self, rPeakIndex, amount)
            currentValue = self.RWaveIndices(rPeakIndex);
            newIndex = currentValue + amount;
            if newIndex > 0 &&...
                    newIndex < self.EcgData.GetNumberOfSamples();
                self.RWaveIndices(rPeakIndex) = newIndex;
                
                time = self.EcgData.GetTimeRange();
                newTime = time(self.RWaveIndices(rPeakIndex));
                self.SetRPeak(rPeakIndex, newTime, true);
            end
        end
        
        function SetRPeak(self, rPeakIndex, timePosition, updateIndex)
            for axesIndex = 1:numel(self.AxesHandles)
                    set(self.RPeakLines(axesIndex, rPeakIndex), 'xData', [timePosition, timePosition]);
            end
            
            if updateIndex
                time = self.EcgData.GetTimeRange();
                [~, self.RWaveIndices(rPeakIndex)] = min(abs(time - timePosition));
                
                lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                    self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
                for axesIndex = 1:numel(self.AxesHandles)
                    set(self.RPeakLines(axesIndex, rPeakIndex), 'userData', self.RWaveIndices(rPeakIndex));
                    
                    markerXData = get(self.RPeakMarkers(axesIndex), 'xData');
                    markerYData = get(self.RPeakMarkers(axesIndex), 'yData');
                    markerXData(rPeakIndex) = timePosition;
                    markerYData(rPeakIndex) = lineData(self.RWaveIndices(rPeakIndex), axesIndex);
                    set(self.RPeakMarkers(axesIndex), 'xData', markerXData, 'yData', markerYData);
                end
            end
        end
        
        function SetVentricularRefractoryPeriod(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.QrstDetector.VentricularRefractoryPeriod = value;
        end
        
        function SetShowDiagnostics(self, source, varargin)
            self.QrstDetector.ShowDiagnostics = get(source, 'value');
        end
    end
    
    methods (Access = private)
        function CreateMouseListeners(self)
            mouseEventSingleton = UserInterfacePkg.MouseEventClass.Instance();
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseMotion', @self.MouseMotionCallback);
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseDown', @self.MouseDownCallback);
            self.MouseListeners(end + 1) = addlistener(mouseEventSingleton, 'MouseUp', @self.MouseUpCallback);
        end
        
        function MouseMotionCallback(self, varargin)
            persistent previousPointerShape;
            if isempty(self.RPeakLines), return; end
            
            switch self.LineMode
                case {'Moving'}
                    currentPoint = get(self.AxesHandles(self.TouchedAxesIndex), 'currentPoint');
                    self.SetRPeak(self.TouchedRPeakIndex, currentPoint(1, 1), false);
                otherwise
                    selectedObject = hittest(gcf);
                    [axesIndex, rPeakIndex] = find(selectedObject == self.RPeakLines);
                    if isempty(axesIndex)
                        if ~isnan(self.TouchedRPeakIndex)
                            set(gcf, 'Pointer', previousPointerShape);
                            self.TouchedRPeakIndex = NaN;
                            self.TouchedAxesIndex = NaN;
                        end
                    else
                        if isnan(self.TouchedRPeakIndex)
                            previousPointerShape = get(gcf, 'Pointer');
                            set(gcf, 'Pointer', 'hand');
                            self.TouchedRPeakIndex = rPeakIndex;
                            self.TouchedAxesIndex = axesIndex;
                        end
                    end
            end
        end
        
        function MouseDownCallback(self, varargin)
            if isnan(self.TouchedRPeakIndex), return; end
            
            self.LineMode = 'Moving';
        end
        
        function MouseUpCallback(self, varargin)
            switch(self.LineMode)
                case 'Moving'
                    currentPoint = get(self.AxesHandles(self.TouchedAxesIndex), 'currentPoint');
                    self.SetRPeak(self.TouchedRPeakIndex, currentPoint(1, 1), true);
                    self.LineMode = 'None';
                otherwise
                    self.LineMode = 'None';
            end
        end
    end
end