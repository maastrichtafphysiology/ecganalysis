classdef SampleEntropyPanel < WindowedAnalysisPanel
    properties (Access = private)
        SEResult
        
        SamplesEdit
        ToleranceEdit
    end
    
    methods
        function self = SampleEntropyPanel(position)
            self = self@WindowedAnalysisPanel(position);
            
            self.WindowLength = 10;
            self.WindowShift = 10;
        end
        
        function SetECGData(self, ecgData, rWaveIndices)
            SetECGData@ECGChannelPanel(self, ecgData);
            
            if nargin > 2
                self.RWaveIndices = rWaveIndices;
            end
            
            set(self.V1Popup, 'string', ecgData.ElectrodeLabels);
            if get(self.V1Popup, 'value') > numel(ecgData.ElectrodeLabels)
                set(self.V1Popup, 'value', numel(ecgData.ElectrodeLabels));
            end
        end
        
        function LoadAnalysis(self, filename)
            ecgData = load(filename, 'ecgData');
            ecgData = ecgData.ecgData;
            ecgData.Filename = filename(1:(end - 11));
            SEData = load(filename, 'SE');
            SEData = SEData.SE;
            
            self.AFComplexityCalculator.SampleEntropyParameters = SEData.settings.parameters;
            
            self.V1Index = SEData.settings.v1Index;
            self.WindowLength = SEData.settings.windowLength;
            self.WindowShift = SEData.settings.windowShift;
            
            self.SEResult = SEData.result;
            
            self.SetECGData(ecgData);
            
            self.InitializeSettings();
            
            self.ShowAnalysis();
        end
        
        function analysisData = GetAnalysisData(self)
            analysisData = [];
        end
    end
    
    methods (Access = protected)
        function CreateAnalysisSettings(self)
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .9 .4 .075],...
                'style', 'text',...
                'string', 'Number of samples');
            self.SamplesEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .9 .4 .1],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.samples),...
                'callback', @self.SetSamples);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .8 .4 .075],...
                'style', 'text',...
                'string', 'Tolerance');
            self.ToleranceEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .8 .4 .1],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.tolerance),...
                'callback', @self.SetTolerance);
        end
        
        function InitializeSettings(self)
            InitializeSettings@WindowedAnalysisPanel(self);
            
            set(self.SamplesEdit, 'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.samples));
            set(self.ToleranceEdit, 'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.tolerance));
        end
        
        function ComputeAnalysis(self, varargin)
            self.SEResult = self.ComputeWindowedSampleEntropy();
            
            self.ShowAnalysis();
        end
        
        function ShowAnalysis(self)
            delete(get(self.ResultPanel, 'children'));
            
            electrodeLabel = self.EcgData.ElectrodeLabels{self.V1Index};
            
            s1 = subplot(1,2,[1 2], 'parent', self.ResultPanel);
            
            self.PlotWindowedData(s1, self.SEResult);
            ylabel(s1, 'Sample entropy');
            title(s1, ['Windowed sample entropy (' electrodeLabel, ')']);
        end
        
        function SaveAnalysis(self, varargin)
            [pathString nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            seSettings = struct(...
                'parameters', self.AFComplexityCalculator.SampleEntropyParameters,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift,...
                'v1Index', self.V1Index);
            SE = struct(...
                'result', self.SEResult,...
                'settings', seSettings); %#ok<NASGU>
            
            if exist(filename, 'file')
                save(filename, 'SE', '-append');
            else
                ecgData = self.EcgData; %#ok<NASGU>
                rWaveIndices = self.RWaveIndices; %#ok<NASGU>
                save(filename, 'ecgData', 'rWaveIndices', 'SE');
            end
        end
    end
    
    methods (Access = private)
        function result = ComputeWindowedSampleEntropy(self)
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            result = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            originalData = self.EcgData.Data();
            try
                for windowIndex = 1:numberOfWindows
                    windowedData = originalData(windowStart(windowIndex):windowEnd(windowIndex), :);
                    self.EcgData.Data = windowedData;
                    % running window
                    entropy = self.AFComplexityCalculator.ComputeSampleEntropy(self.EcgData, self.V1Index);
                    result.runningWindowMean(windowIndex) = entropy;
                    
                    %expanding window
                    windowedData = originalData(1:windowEnd(windowIndex), :);
                    self.EcgData.Data = windowedData;
                    entropy = self.AFComplexityCalculator.ComputeSampleEntropy(self.EcgData, self.V1Index);
                    result.expandingWindowMean(windowIndex) = entropy;
                end
            catch ME
                disp(ME);
            end
            self.EcgData.Data = originalData;
        end
        
        function SetSamples(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 1
                self.AFComplexityCalculator.SampleEntropyParameters.samples = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.samples));
        end
        
        function SetTolerance(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value <= 1
                self.AFComplexityCalculator.SampleEntropyParameters.tolerance = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.SampleEntropyParameters.tolerance));
        end
    end    
end