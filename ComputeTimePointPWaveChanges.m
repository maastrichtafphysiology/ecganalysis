% This function determines the differences in average P-wave morphology
% between different time points. It assumes there are 3 time points available, denoted
% by T1-T3 in the filename

% input folder name
foldername = '/Users/matthiaszink/Desktop/Maastricht/AFAB _ Maastricht/ECG Analysis export 171013';

fileListing = dir(fullfile(foldername, '*_PWaveMatching.mat'));
filenames = {fileListing.name};

outputFilename = fullfile(foldername, ['TimePointChangeT1_T2_T3', '.csv']);

% determine the unique patient IDs
filePatientStrings = cellfun(@(x) strsplit(x, '#'), filenames, 'uniformOutput', false);
filePatientID = cellfun(@(x) x{1}, filePatientStrings, 'uniformOutput', false);

patientID = unique(filePatientID);
numberOfPatients = numel(patientID);

T1Files = find(cellfun(@(x) contains(x, 'T1'), filenames));
T2Files = find(cellfun(@(x) contains(x, 'T2'), filenames));
T3Files = find(cellfun(@(x) contains(x, 'T3'), filenames));

% compute the differences in lead morphology between timepoint pairs
disp('Computing time point differences...');
patientParameterLabels = cell(numberOfPatients, 1);
patientParameterValues = cell(numberOfPatients, 1);
for patientIndex = 1:numberOfPatients
    t1Index = ismember(filePatientID(T1Files), patientID(patientIndex));
    t2Index = ismember(filePatientID(T2Files), patientID(patientIndex));
    t3Index = ismember(filePatientID(T3Files), patientID(patientIndex));
    
    if any(t1Index)
        pWaveDataT1 = LoadPWaveMatchingData(fullfile(foldername, filenames{T1Files(t1Index)}));
    else
        pWaveDataT1 = [];
    end
    if any(t2Index)
        pWaveDataT2 = LoadPWaveMatchingData(fullfile(foldername, filenames{T2Files(t2Index)}));
    else
        pWaveDataT2 = [];
    end
    if any(t3Index)
        pWaveDataT3 = LoadPWaveMatchingData(fullfile(foldername, filenames{T3Files(t3Index)}));
    else
        pWaveDataT3 = [];
    end
    
    currentPatientParameterLabels = cell(3,1);
    currentPatientParameterValues = cell(3,1);
    if ~(isempty(pWaveDataT1) || isempty(pWaveDataT2))
        [leadDifferencesT1_T2, leadLabelsT1_T2] =...
            ComputePairedLeadDifferences(pWaveDataT1, pWaveDataT2);
        [currentPatientParameterValues{1}, parameterLabels] = ComputeDifferenceParameters(leadDifferencesT1_T2, leadLabelsT1_T2);
        currentPatientParameterLabels{1} = strcat('T1_T2_', parameterLabels);
    end
    
    if ~(isempty(pWaveDataT1) || isempty(pWaveDataT3))
        [leadDifferencesT1_T3, leadLabelsT1_T3] =...
            ComputePairedLeadDifferences(pWaveDataT1, pWaveDataT3);
        [currentPatientParameterValues{2}, parameterLabels] = ComputeDifferenceParameters(leadDifferencesT1_T3, leadLabelsT1_T3);
        currentPatientParameterLabels{2} = strcat('T1_T3_', parameterLabels);
    end
    
    if ~(isempty(pWaveDataT2) || isempty(pWaveDataT3))
        [leadDifferencesT2_T3, leadLabelsT2_T3] =...
            ComputePairedLeadDifferences(pWaveDataT2, pWaveDataT3);
        [currentPatientParameterValues{3}, parameterLabels] = ComputeDifferenceParameters(leadDifferencesT2_T3, leadLabelsT2_T3);
        currentPatientParameterLabels{3} = strcat('T2_T3_', parameterLabels);
    end
    
    patientParameterLabels{patientIndex} = vertcat(currentPatientParameterLabels{:});
    patientParameterValues{patientIndex} = vertcat(currentPatientParameterValues{:});
end
allParameterLabels = unique(vertcat(patientParameterLabels{:}), 'stable');

collectedResults = NaN(numberOfPatients, numel(allParameterLabels));
for patientIndex = 1:numberOfPatients
    if isempty(patientParameterLabels{patientIndex}), continue; end
    [validParameters, parameterIndices] = ismember(patientParameterLabels{patientIndex}, allParameterLabels);
    currentParameterValues = patientParameterValues{patientIndex};            
    collectedResults(patientIndex, parameterIndices) = currentParameterValues(validParameters);
end

resultTable = array2table(collectedResults, 'VariableNames', allParameterLabels,...
                'RowNames', patientID);
writetable(resultTable, outputFilename, 'WriteRowNames', true);
disp('Done');

%%
function pWaveData = LoadPWaveMatchingData(filename)
data = load(filename);
pWaveMatcher = data.pWaveMatching;
pWaveData = pWaveMatcher.GetPWaveData(data.ecgData, data.baselineCorrectedData,...
    data.pWaveIntervals, data.onsetOffset, data.electrodeOnsetOffset, data.validChannels,...
    data.electrodeTerminalForce);
end

function [leadDifferences, leadLabels] =...
    ComputePairedLeadDifferences(pWaveData1, pWaveData2)
uniqueLeadLabels = unique([pWaveData1.ElectrodeLabels; pWaveData2.ElectrodeLabels]);
numberOfLeads = numel(uniqueLeadLabels);

electrodeOnsetOffset1 = pWaveData1.ElectrodeOnsetOffset;
electrodeOnsetOffset1(isnan(electrodeOnsetOffset1(:, 1)), 1) = pWaveData1.OnsetOffset(1);
electrodeOnsetOffset1(isnan(electrodeOnsetOffset1(:, 2)), 2) = pWaveData1.OnsetOffset(2);
intervalTime1 = pWaveData1.AveragingInterval;

electrodeOnsetOffset2 = pWaveData2.ElectrodeOnsetOffset;
electrodeOnsetOffset2(isnan(electrodeOnsetOffset2(:, 1)), 1) = pWaveData2.OnsetOffset(1);
electrodeOnsetOffset2(isnan(electrodeOnsetOffset2(:, 2)), 2) = pWaveData2.OnsetOffset(2);
intervalTime2 = pWaveData2.AveragingInterval;

leadDifferences = cell(numberOfLeads, 1);
leadLabels = uniqueLeadLabels;
for labelIndex = 1:numberOfLeads
    index1 = ismember(pWaveData1.ElectrodeLabels, uniqueLeadLabels{labelIndex});
    index2 = ismember(pWaveData2.ElectrodeLabels, uniqueLeadLabels{labelIndex});
    if ~(any(index1) && any(index2)), continue; end
    
    % align on onset
%     duration1 = electrodeOnsetOffset1(index1, 2) - electrodeOnsetOffset1(index1, 1);
%     duration2 = electrodeOnsetOffset2(index2, 2) - electrodeOnsetOffset2(index2, 1);
%     timeExtension1 = 0;
%     timeExtension2 = 0;
%     if duration1 > duration2
%         timeExtension2 = duration1 - duration2;
%     else
%         timeExtension1 = duration2 - duration1;
%     end
%     
%     validTime1 = intervalTime1 >= electrodeOnsetOffset1(index1, 1) &...
%         intervalTime1 <= electrodeOnsetOffset1(index1, 2) + timeExtension1;
%     pWave1 = pWaveData1.AveragePWaves(validTime1, index1);
%     validTime2 = intervalTime2 >= electrodeOnsetOffset2(index2, 1) &...
%         intervalTime2 <= electrodeOnsetOffset2(index2, 2) + timeExtension2;
%     pWave2 = pWaveData2.AveragePWaves(validTime2, index2);
%     sampleDuration1 = numel(pWave1);
%     sampleDuration2 = numel(pWave2);
%     if sampleDuration1 > sampleDuration2
%         validTime2(find(validTime2, 1, 'last') + 1) = true;
%         pWave2 = pWaveData2.AveragePWaves(validTime2, index2);
%     elseif sampleDuration2 > sampleDuration1
%         validTime1(find(validTime1, 1, 'last') + 1) = true;
%         pWave1 = pWaveData1.AveragePWaves(validTime1, index1);
%     end
    
    % align on best cross-correlation
    startIndex1 = find(intervalTime1 >= electrodeOnsetOffset1(index1, 1), 1, 'first');
    endIndex1 = find(intervalTime1 <= electrodeOnsetOffset1(index1, 2), 1, 'last');
    pWave1 = pWaveData1.AveragePWaves(startIndex1:endIndex1, index1);
    
    startIndex2 = find(intervalTime2 >= electrodeOnsetOffset2(index2, 1), 1, 'first');
    endIndex2 = find(intervalTime2 <= electrodeOnsetOffset2(index2, 2), 1, 'last');
    pWave2 = pWaveData2.AveragePWaves(startIndex2:endIndex2, index2);
    
    maximumLag = min([10, startIndex1 - 1, startIndex2 - 1]);
    [crossCorrelation, lags] = xcorr(pWave1, pWave2, maximumLag);
    [~, maxLagIndex] = max(crossCorrelation);
    optimumlag = lags(maxLagIndex);
    if optimumlag > 0
        startIndex2 = startIndex2 - optimumlag;
        pWave2 = pWaveData2.AveragePWaves(startIndex2:endIndex2, index2);
    else
        startIndex1 = startIndex1 + optimumlag;
        pWave1 = pWaveData1.AveragePWaves(startIndex1:endIndex1, index1);
    end
    sampleDuration1 = numel(pWave1);
    sampleDuration2 = numel(pWave2);
    durationDifference = sampleDuration2 - sampleDuration1;
    if durationDifference > 0
        numberOfSamples1 = numel(pWaveData1.AveragePWaves(:, index1));
        if endIndex1 + durationDifference <= numberOfSamples1
            pWave1 = pWaveData1.AveragePWaves(startIndex1:(endIndex1 + durationDifference), index1);
        else
            pWave1 = pWaveData1.AveragePWaves(startIndex1:end, index1);
            durationDifference = endIndex1 + durationDifference - numberOfSamples1;
            pWave2 = pWaveData2.AveragePWaves(startIndex2:(endIndex2 - durationDifference), index2);
        end
    else
        numberOfSamples2 = numel(pWaveData2.AveragePWaves(:, index2));
        if endIndex2 - durationDifference <= numberOfSamples2
            pWave2 = pWaveData2.AveragePWaves(startIndex2:(endIndex2 - durationDifference), index2);
        else
            pWave2 = pWaveData2.AveragePWaves(startIndex2:end, index2);
            durationDifference = endIndex2 - durationDifference - numberOfSamples2;
            pWave1 = pWaveData1.AveragePWaves(startIndex1:(endIndex1 - durationDifference), index1);
        end
    end
      
    leadDifferences{labelIndex} = [pWave2 - pWave1, pWave1, pWave2];
end
end

function [parameterValues, parameterLabels] = ComputeDifferenceParameters(leadDifferences, leadLabels)
numberOfLeads = numel(leadDifferences);

parameterLabels = [...
    strcat('Difference_Energy', '_', leadLabels);...
    strcat('Difference_Sum', '_', leadLabels);...
    strcat('Difference_Extremum', '_', leadLabels);...
    strcat('Difference_Extremum_Position', '_', leadLabels);...
    strcat('Correlation', '_', leadLabels);...
    strcat('SpearmanCorrelation', '_', leadLabels)];
differenceEnergy = NaN(numberOfLeads, 1);
differenceSum = NaN(numberOfLeads, 1);
differenceExtremum = NaN(numberOfLeads, 1);
relativeExtremumPosition = NaN(numberOfLeads, 1);
leadCorrelation = NaN(numberOfLeads, 1);
leadSpearmanCorrelation = NaN(numberOfLeads, 1);
for leadIndex = 1:numberOfLeads
    leadDifference = leadDifferences{leadIndex};
    numberOfSamples = size(leadDifference, 1);
    differenceEnergy(leadIndex) = sqrt(sum(leadDifference(:, 1).^2));
    differenceSum(leadIndex) = sum(leadDifference(:, 1));
    [differenceExtremum(leadIndex), extremumIndex] = max(abs(leadDifference(:, 1)));
    relativeExtremumPosition(leadIndex) = extremumIndex / numberOfSamples;
    leadCorrelation(leadIndex) = corr(leadDifference(:, 2), leadDifference(:, 3), 'type', 'Pearson');
    leadSpearmanCorrelation(leadIndex) = corr(leadDifference(:, 2), leadDifference(:, 3), 'type', 'Spearman');
end
parameterValues = [...
    differenceEnergy; differenceSum; differenceExtremum; relativeExtremumPosition; leadCorrelation; leadSpearmanCorrelation];
end