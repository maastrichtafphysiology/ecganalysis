classdef BSPMDualVisualizationPanel < BSPMVisualizationPanel
    properties (Access = private)
        OriginalDataSet
        ProcessedBSPMControl
        ProcessedDataSet
   end
    
    methods
        function self = BSPMDualVisualizationPanel(position)
            self = self@BSPMVisualizationPanel(position);
            
            self.OriginalDataSet = false;
            self.ProcessedDataSet = false;
        end
        
        function SetECGData(self, ecgData)
            SetECGData@BSPMVisualizationPanel(self, ecgData);
            self.OriginalDataSet = true;
        end
        
        function SetProcessedECGData(self, ecgData)
            self.ProcessedBSPMControl.SetECGData(ecgData);
            self.ProcessedDataSet = true;
            set(self.FrameSlider, 'max', size(ecgData.Data, 1));
            self.EcgData = ecgData;
        end
        
        function SetRWaveIndices(self, rWaveIndices)
            self.ProcessedBSPMControl.SetRWaveIndices(rWaveIndices);
            SetRWaveIndices@BSPMVisualizationPanel(self, rWaveIndices);
        end
    end
    
    methods (Access = protected)
        function SetFrame(self, varargin)
            if self.OriginalDataSet
                self.BSPMControl.SetFramePosition(self.FramePosition);
            end
            
            if self.ProcessedDataSet
                self.ProcessedBSPMControl.SetFramePosition(self.FramePosition);
            end
        end
        
        function CreateBSPMControls(self)
            CreateBSPMControls@BSPMVisualizationPanel(self);
            self.BSPMControl.SetPosition([0 .2 .5 .8]);
            
            self.ProcessedBSPMControl = BSPMPanel([.5 .2 .5 .8]);
            self.ProcessedBSPMControl.Create(self.ControlHandle);
            self.ProcessedBSPMControl.Show();
        end
        
        function SetInterpolationFactor(self, source, varargin)
            SetInterpolationFactor@BSPMVisualizationPanel(self, source); 
            self.ProcessedBSPMControl.InterpolationFactor = self.BSPMControl.InterpolationFactor;
        end
        
        function SetShowOnlyTQIntervals(self, source, varargin)
            SetShowOnlyTQIntervals@BSPMVisualizationPanel(self, source);
            value = get(source, 'value');
            self.ProcessedBSPMControl.SetShowOnlyTQIntervals(value);
        end
        
        function SetQTIntervals(self)
            SetQTIntervals@BSPMVisualizationPanel(self);
            self.ProcessedBSPMControl.SetQTIntervals(self.QTIntervals, self.QTIndices);
        end
    end
end