classdef PWave12LeadVCGPanel < PWaveVCGPanel
    properties (Constant)
        VCG_SYNTHESIS_METHODS = {'Inverse Dower matrix', 'Kors matrix'};
        
        VCG_SYNTHESIS_MATRIX = {...
            [0.16, -0.01, -0.17, -0.07, 0.12, 0.23, 0.24, 0.19;...
            -0.23, 0.89, 0.06, -0.02, -0.11, -0.02, 0.04, 0.05;...
            0.02, 0.10, -0.23, -0.31, -0.25, -0.06, 0.06, 0.11];
            [0.38, -0.07, -0.13, 0.05, -0.01, 0.14, 0.06, 0.54;...
            -0.07, 0.93, 0.06, -0.02, -0.05, 0.06, -0.17, 0.13;...
            0.11, -0.23, -0.43, -0.06, -0.14, -0.20, -0.11, 0.31]};
        
        VCG_TRANSFORM_LABELS = {'I', 'II', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
    end
    

    properties (Access = protected)
        XYZLeads
        SelectedMethod
        TypeSignThreshold
        
        MethodPopup
    end
    
    methods
        function self = PWave12LeadVCGPanel(position)
            self = self@PWaveVCGPanel(position);
            
            self.SelectedMethod = 1;
            self.TypeSignThreshold = 0.75;
        end
        
        function Create(self, parentHandle)
            Create@PWaveVCGPanel(self, parentHandle);
        end
        
        function SetPWaveData(self, pWaveData, map)
            SetPWaveData@PWaveVCGPanel(self, pWaveData, map)
            
            self.UpdateVCGLeads();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@PWaveVCGPanel(self);
            
            methodPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .8 1 .15],...
                'title', 'VCG synthesis matrix',...
                'titlePosition', 'centertop');
            
            self.MethodPopup = uicontrol('parent', methodPanel,...
                'units', 'normalized',...
                'position', [.1, 0, .8, 1],...
                'style', 'popupmenu',...
                'string', PWave12LeadVCGPanel.VCG_SYNTHESIS_METHODS,...
                'value', self.SelectedMethod,...
                'callback', @self.SetVCGMethod);
        end
        
        function ComputeXYZLeads(self)
            if isempty(self.PWaveData), return; end
            
            electrodeLabels = self.PWaveData.ElectrodeLabels;
            
            [electrodeFound, indexInElectrodeLabels] = ...
                ismember(PWave12LeadVCGPanel.VCG_TRANSFORM_LABELS, electrodeLabels);
            
            if all(electrodeFound)
                averagePWaves = self.PWaveData.AveragePWaves;
                ecgLeads = averagePWaves(:, indexInElectrodeLabels);
                
                transformMatrix = PWave12LeadVCGPanel.VCG_SYNTHESIS_MATRIX{self.SelectedMethod};
                
                self.XYZLeads = (transformMatrix * ecgLeads')';
            else
                return;
            end
        end
        
        function SetVCGMethod(self, varargin)
            self.SelectedMethod = get(self.MethodPopup, 'value');
            self.UpdateVCGLeads();
        end
        
        function UpdateVCGLeads(self)
            self.ComputeXYZLeads();
            self.ShowXYZPWaves();
            self.ShowXYZLoop();
            self.ShowXYZData();
        end
        
        function ShowXYZPWaves(self)
            delete(get(self.VCGView, 'Children'));
            
            axesHandles = ECGChannelPanel.TightSubplot(3, 1, self.VCGView,...
                0.01, [0.05, 0.025], [0.1, 0.05]);
            
            % X
            xPwave = self.XYZLeads(:, 1);
            self.ShowPWave(axesHandles(1), xPwave);
            text(0.05, 0.95, 'X',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(1));
            set(axesHandles(1), 'xTickLabel', {});
            
            % Y
            yPwave = self.XYZLeads(:, 2);
            self.ShowPWave(axesHandles(2), yPwave);
            text(0.05, 0.95, 'Y',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(2));
            set(axesHandles(2), 'xTickLabel', {});
            
            % Z
            zPwave = self.XYZLeads(:, 3);
            self.ShowPWave(axesHandles(3), zPwave);
            text(0.05, 0.95, 'Z',...
                'color', [0,0,1], 'units', 'normalized',...
                'fontSize', 24, 'fontWeight', 'bold',...
                'parent', axesHandles(3));
            
            xlabel(axesHandles(3), 'Time (ms)');            
        end
        
        function ShowXYZLoop(self)
            delete(get(self.VCGLoopView, 'Children'));
            
            axesHandle = subplot(1,1,1, 'parent', self.VCGLoopView);
            
            intervalTime = self.PWaveData.AveragingInterval;
            
            xPwave = self.XYZLeads(:, 1);
            yPwave = self.XYZLeads(:, 2);
            zPwave = self.XYZLeads(:, 3);
            
            [~, maximumPosition] = max(sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2));
            
            scatter3(xPwave, yPwave, min(zPwave) * ones(size(zPwave)), 5, [.7, .7, .7], 'parent', axesHandle);
            hold(axesHandle, 'on');
            scatter3(max(xPwave) * ones(size(xPwave)), yPwave, zPwave, 5, [.7, .7, .7], 'parent', axesHandle);
            scatter3(xPwave, max(yPwave) * ones(size(yPwave)), zPwave, 5, [.7, .7, .7], 'parent', axesHandle);
            scatter3(xPwave, yPwave, zPwave, 20, intervalTime, 'parent', axesHandle);
            quiver3(0,0,0, xPwave(maximumPosition), yPwave(maximumPosition), zPwave(maximumPosition),...
                'lineWidth', 2, 'color', [0,0,0], 'maxHeadSize', 0.5);
            
            hold(axesHandle, 'off');
            axis(axesHandle, 'tight');
            xlabel(axesHandle, 'X');
            ylabel(axesHandle, 'Y');
            zlabel(axesHandle, 'Z');
            title(axesHandle, 'XYZ loop');
            colorbar;
        end
        
        function ShowXYZData(self)
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            % scale time to seconds
            intervalTime = intervalTime / 1000;
            
            % scale signals to micro volt
            xPwave = self.XYZLeads(:, 1) * 1000;
            yPwave = self.XYZLeads(:, 2) * 1000;
            zPwave = self.XYZLeads(:, 3) * 1000;
            
            xyzWave = sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2);
            
            xArea = trapz(intervalTime(validTime), xPwave(validTime));
            yArea = trapz(intervalTime(validTime), yPwave(validTime));
            zArea = trapz(intervalTime(validTime), zPwave(validTime));
            
            xAreaAbs = trapz(intervalTime(validTime), abs(xPwave(validTime)));
            yAreaAbs = trapz(intervalTime(validTime), abs(yPwave(validTime)));
            zAreaAbs = trapz(intervalTime(validTime), abs(zPwave(validTime)));
            xyzArea = trapz(intervalTime(validTime), abs(xyzWave(validTime)));
            
            % azimuth (transversal XZ plane)& elevation (frontal Y-XZ plane)
            [maximumDistance, maximumPosition] = max(sqrt(xPwave.^2 + yPwave.^2 + zPwave.^2));
            [azimuth, elevation] = cart2sph(xPwave(maximumPosition), zPwave(maximumPosition), yPwave(maximumPosition));
            
            vcgType = self.ComputeVCGType();
            
            resultString = {...
                'Uncorrected absolute | signed XYZ area';...
                '';...
                ['X area: ', num2str(xAreaAbs, '%.2f'), 'muVs', ' | ', num2str(xArea, '%.2f'), 'muVs'];...
                ['Y area: ', num2str(yAreaAbs, '%.2f'), 'muVs', ' | ', num2str(yArea, '%.2f'), 'muVs'];...
                ['Z area: ', num2str(zAreaAbs, '%.2f'), 'muVs', ' | ', num2str(zArea, '%.2f'), 'muVs'];...
                ['XYZ area: ', num2str(xyzArea, '%.2f'), 'muVs'];...
                '';...
                ['Maxmimum distance from origin: ', num2str(maximumDistance, '%.2f'), 'muV'];...
                ['Azimuth (XZ): ', num2str(180 * azimuth /pi, '%.2f'), ' degrees'];...
                ['Elevation (Y-XZ): ', num2str(180 * elevation /pi, '%.2f'), ' degrees'];...
                '';...
                ['Suggested morphology classification: ', vcgType]};
            
            set(self.ResultEdit, 'string', resultString, 'horizontalAlignment', 'left', 'fontSize', 12);
        end
        
        function vcgType = ComputeVCGType(self)
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            % scale time to seconds
            intervalTime = intervalTime / 1000;
            
            xPwave = self.XYZLeads(:, 1);
            yPwave = self.XYZLeads(:, 2);
            zPwave = self.XYZLeads(:, 3);
            
            validTime = validTime(:);
            
            xPositive = xPwave >= 0;
            xArea = trapz(intervalTime(validTime), abs(xPwave(validTime)));
            xPositiveArea = trapz(intervalTime(validTime & xPositive), xPwave(validTime & xPositive));
            xSign = xPositiveArea / xArea;
            
            yPositive = yPwave >= 0;
            yArea = trapz(intervalTime(validTime), abs(yPwave(validTime)));
            yPositiveArea = trapz(intervalTime(validTime & yPositive), yPwave(validTime & yPositive));
            ySign = yPositiveArea / yArea;
            
            zPositive = zPwave >= 0;
            zArea = trapz(intervalTime(validTime), abs(zPwave(validTime)));
            zPositiveArea = trapz(intervalTime(validTime & zPositive), zPwave(validTime & zPositive));
            zSign = zPositiveArea / zArea;
            
            halfPoint = find(cumsum(validTime) > numel(find(validTime)) / 2, 1, 'first');
            
            % X, Y positive, Z negative
            if xSign >= self.TypeSignThreshold && ySign >= self.TypeSignThreshold && zSign < (1 - self.TypeSignThreshold)
                vcgType = 'Type 1';
                return;
            end
            
            % X, Y, Z positive
            if xSign >= self.TypeSignThreshold && ySign >= self.TypeSignThreshold && zSign >= self.TypeSignThreshold
                vcgType = 'Type 4';
                return;
            end
            
            % X, Y positive, Z biphasic (-/+)
            if xSign >= self.TypeSignThreshold && ySign >= self.TypeSignThreshold &&...
                    (zSign < self.TypeSignThreshold && zSign > (1 - self.TypeSignThreshold))
                zFirstHalf = trapz(intervalTime(validTime(1:halfPoint)), zPwave(validTime(1:halfPoint)));
                zSecondHalf = trapz(intervalTime(validTime(halfPoint:end)), zPwave(validTime(halfPoint:end)));
                if zFirstHalf <=0 && zSecondHalf >= 0
                    
                    vcgType = 'Type 2';
                else
                    vcgType = 'Atypical';
                end
                return;
            end
            
            % X positive, Y, Z biphasic (-/+)
            if xSign >= self.TypeSignThreshold &&...
                    (ySign < self.TypeSignThreshold && ySign > (1 - self.TypeSignThreshold)) &&...
                    (zSign < self.TypeSignThreshold && zSign > (1 - self.TypeSignThreshold))
                yFirstHalf = trapz(intervalTime(validTime(1:halfPoint)), yPwave(validTime(1:halfPoint)));
                ySecondHalf = trapz(intervalTime(validTime(halfPoint:end)), yPwave(validTime(halfPoint:end)));
                
                zFirstHalf = trapz(intervalTime(validTime(1:halfPoint)), zPwave(validTime(1:halfPoint)));
                zSecondHalf = trapz(intervalTime(validTime(halfPoint:end)), zPwave(validTime(halfPoint:end)));
                
                if (yFirstHalf <=0 && ySecondHalf >= 0) && (zFirstHalf <=0 && zSecondHalf >= 0)
                    vcgType = 'Type 3';
                else
                    vcgType = 'Atypical';
                end
                return;
            end
            
            vcgType = 'Atypical';
        end
    end
end