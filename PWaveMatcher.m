classdef PWaveMatcher < ECGPreProcessor
    properties
        BaselineCorrection
        BaselineBandwidth
        TemplateShift
        PWaveRange
        CorrelationThreshold
        PeakAmplitudeThreshold
        
        ShowDiagnostics
        DiagnosticsView
        ExampleChannel
        
        minSlope
        fitThreshold
        minAngle
        checkOutliers
    end
    
    properties (Constant)
        AvailableMaps = PWaveMatcher.LoadAvailableBSPMMaps();
    end
    
    methods
        function self = PWaveMatcher()
            self = self@ECGPreProcessor();
            self.ApplyBandpassFilter = false;
            self.BandpassFrequencies = [40, 250];
            self.BaselineCorrection = true;
            self.BaselineBandwidth = 0.010;
            self.TemplateShift = 0.030;
            self.PWaveRange = [-0.250, -0.030];
            self.CorrelationThreshold = 0.9;
            self.PeakAmplitudeThreshold = 0.1;
            
            % P-wave duration settings
            self.minSlope = 0.15;
            self.minAngle = 10;
            
            self.ShowDiagnostics = false;
            self.ExampleChannel = 1;
        end
        
        function [pWaveIntervals, baselineCorrectedData, baselineCorrectedReferenceData] = MatchPWaves(self, ecgData, rWaveIndices, validChannels)
            if nargin < 4
                validChannels = 1:ecgData.GetNumberOfChannels();
            end
            
            if self.ShowDiagnostics
                if ~ishandle(self.DiagnosticsView)
                    self.DiagnosticsView = figure('name', 'P-wave interval estimation');
                end
            end
            
            if self.ApplyCombFilter || self.ApplyNotchFilter
                filteredEcgData = self.Filter(ecgData);
            else
                filteredEcgData = ecgData;
            end
            
            if self.BaselineCorrection
                [baselineCorrectedData, baselineCorrectedReferenceData, includedRWaveIndices] = self.CorrectBaseline(filteredEcgData, rWaveIndices);
            else
                baselineCorrectedData = filteredEcgData.Data;
                baselineCorrectedReferenceData = filteredEcgData.ReferenceData;
                includedRWaveIndices = rWaveIndices;
            end
            
            pWaveIntervals = self.ComputePWaveMatching(filteredEcgData, baselineCorrectedData, includedRWaveIndices, validChannels);
        end
        
        function [intervalAverage, intervalTime] = ComputeUnfilteredPWaveAverageEnergy(self, ecgData, correctedSignals, pWaveIntervals, validChannels)
            if nargin < 5
                validChannels = 1:ecgData.GetNumberOfChannels();
            end
            numberOfIntervals = size(pWaveIntervals, 1);
            numberOfChannels = numel(validChannels);
            intervalLength = pWaveIntervals(1, 2) - pWaveIntervals(1, 1) + 1;
            intervalEnergy = NaN(intervalLength, numberOfIntervals);
            
            for intervalIndex = 1:numberOfIntervals
                intervalIndices = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                intervalSignals = correctedSignals(intervalIndices, validChannels);
                intervalEnergy(:, intervalIndex) = sqrt(sum(intervalSignals.^2, 2) / numberOfChannels);
            end
            
            intervalAverage = mean(intervalEnergy, 2);
            intervalTime = 1000 * self.PWaveRange(1) + 1000 * (0:(intervalLength - 1)) / ecgData.SamplingFrequency;
        end
        
        function [intervalAverage, intervalSD, intervalTime, intervalData] = ComputeUnfilteredPWaveAverage(self, ecgData, channelData, pWaveIntervals)
            numberOfIntervals = size(pWaveIntervals, 1);
            intervalLength = pWaveIntervals(1, 2) - pWaveIntervals(1, 1) + 1;
            intervalData = NaN(intervalLength, numberOfIntervals);
            for intervalIndex = 1:numberOfIntervals
                currentInterval = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                intervalData(:, intervalIndex) = channelData(currentInterval);
            end
            intervalAverage = mean(intervalData, 2);
            intervalSD = std(intervalData, 0, 2);
            
            intervalTime = 1000 * self.PWaveRange(1) + 1000 * (0:(intervalLength-1)) / ecgData.SamplingFrequency;
        end
        
        function [intervalAverage, intervalSD, intervalTime] = ComputeFilteredPWaveAverage(self, ecgData, channelData, pWaveIntervals)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            channelData = self.BandpassFilter(channelData);
            numberOfIntervals = size(pWaveIntervals, 1);
            intervalLength = pWaveIntervals(1, 2) - pWaveIntervals(1, 1) + 1;
            intervalData = NaN(intervalLength, numberOfIntervals);
            for intervalIndex = 1:numberOfIntervals
                currentInterval = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                intervalData(:, intervalIndex) = abs(channelData(currentInterval));
            end
            intervalAverage = mean(intervalData, 2);
            intervalSD = std(intervalData, 0, 2);
            
            intervalTime = 1000 * self.PWaveRange(1) + 1000 * (0:(intervalLength-1)) / ecgData.SamplingFrequency;
        end
        
        function [intervalAverage, intervalTime] = ComputeFilteredPWaveAverageEnergy(self, ecgData, pWaveIntervals)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            filteredSignals = self.BandpassFilter(ecgData.Data);
            
            numberOfIntervals = size(pWaveIntervals, 1);
            numberOfChannels = ecgData.GetNumberOfChannels();
            intervalLength = pWaveIntervals(1, 2) - pWaveIntervals(1, 1) + 1;
            intervalEnergy = NaN(intervalLength, numberOfIntervals);
            for intervalIndex = 1:numberOfIntervals
                intervalIndices = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                intervalEnergy(:, intervalIndex) = sqrt(sum(filteredSignals(intervalIndices, :).^2, 2) / numberOfChannels);
            end
            
            intervalAverage = mean(intervalEnergy, 2);
            intervalTime = 1000 * self.PWaveRange(1) + 1000 * (0:(intervalLength - 1)) / ecgData.SamplingFrequency;
        end
        
        function pWaveData = GetPWaveData(self, ecgData, correctedSignals, pWaveIntervals, onsetOffset, electrodeOnsetOffset, validChannels, electrodeTerminalForce)
            if nargin < 8
                electrodeTerminalForce = NaN(ecgData.GetNumberOfChannels(), 1);
                if nargin < 7
                    validChannels = 1:ecgData.GetNumberOfChannels();
                end
            end
            intervalLength = pWaveIntervals(1, 2) - pWaveIntervals(1, 1) + 1;
            numberOfIntervals = size(pWaveIntervals, 1);
            intervalTime = 1000 * self.PWaveRange(1) + 1000 * (0:(intervalLength-1)) / ecgData.SamplingFrequency;
            numberOfChannels = ecgData.GetNumberOfChannels();
            
%             if isnan(onsetOffset(1))
%                 onsetOffset(1) = 1e3 * self.PWaveRange(1);
%             end
%             
%             if isnan(onsetOffset(2))
%                 if isnan(onsetOffset(3))
%                     onsetOffset(2) = 1e3 * self.PWaveRange(2);
%                 else
%                     onsetOffset(2) = onsetOffset(3);
%                 end
%             end
            
            electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 1)), 1) = onsetOffset(1);
            electrodeOnsetOffset(isnan(electrodeOnsetOffset(:, 2)), 2) = onsetOffset(2);
            
            averagePWaves = NaN(intervalLength, numberOfChannels);
            pWaveArea = NaN(numberOfChannels, 1);
            pWaveRange = NaN(numberOfChannels, 2);
            pWaveComplexity = NaN(numberOfChannels, 1);
            pWaveTerminalForce = NaN(numberOfChannels, 1);
            pWaveEntropy = NaN(numberOfChannels, 1);
            pWaveSampleEntropy = NaN(numberOfChannels, 1);
            activationTimeValue(numberOfChannels) = struct(...
                'Max', NaN, 'Min', NaN, 'MaxAbs', NaN);
            activationTimeSlope(numberOfChannels) = struct(...
                'Max', NaN, 'Min', NaN, 'MaxAbs', NaN);
            
            for channelIndex = 1:numberOfChannels
                channelData = correctedSignals(:, channelIndex);
                intervalData = NaN(intervalLength, numberOfIntervals);
                
                for intervalIndex = 1:numberOfIntervals
                    currentInterval = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                    intervalData(:, intervalIndex) = channelData(currentInterval);
                end
                pWaveAverage = mean(intervalData, 2);
                averagePWaves(:, channelIndex) = pWaveAverage;
                
                validIntervalTime = intervalTime >= electrodeOnsetOffset(channelIndex, 1) & intervalTime <= electrodeOnsetOffset(channelIndex, 2);
                if ~any(validIntervalTime), continue; end
                
                detrendAverage = true;
                pWaveArea(channelIndex) = PWaveMatcher.ComputePWaveArea(pWaveAverage,...
                    intervalTime, electrodeOnsetOffset(channelIndex, :), detrendAverage);
                pWaveRange(channelIndex, :) = [min(pWaveAverage(validIntervalTime)),...
                    max(pWaveAverage(validIntervalTime))];
                pWavePeaks = self.FindPWavePeaks(pWaveAverage, intervalTime, electrodeOnsetOffset(channelIndex, :));
                pWaveComplexity(channelIndex) = numel(pWavePeaks);
                
                if ~isnan(electrodeTerminalForce(channelIndex))
                    pWaveTerminalForce(channelIndex) = PWaveMatcher.ComputePWaveTerminalForce(...
                        pWaveAverage, intervalTime, electrodeOnsetOffset(channelIndex, :), electrodeTerminalForce(channelIndex), detrendAverage);
                end
                
                [pWaveEntropy(channelIndex), pWaveSampleEntropy(channelIndex)] =...
                    PWaveMatcher.ComputePWaveEntropy(pWaveAverage, intervalTime, electrodeOnsetOffset(channelIndex, :), detrendAverage);
                
                [activationValue, activationSlope] = PWaveMatcher.ComputeActivationTime(pWaveAverage, intervalTime, electrodeOnsetOffset(channelIndex, :));
                activationTimeValue(channelIndex) = activationValue;
                activationTimeSlope(channelIndex) = activationSlope;
            end
            
            [averageEnergy, intervalTime] = self.ComputeUnfilteredPWaveAverageEnergy(ecgData, correctedSignals, pWaveIntervals, validChannels);
            
            pWaveData = struct(...
                'ValidChannels', validChannels,...
                'PWaveIntervals', pWaveIntervals,...
                'AveragingInterval', intervalTime,...
                'AveragePWaves', averagePWaves,...
                'AveragePWaveEnergy', averageEnergy,...
                'ElectrodeLabels', {ecgData.ElectrodeLabels},...
                'OnsetOffset', onsetOffset,...
                'ElectrodeOnsetOffset', electrodeOnsetOffset,...
                'PWaveArea', pWaveArea,...
                'PWaveRange', pWaveRange,...
                'Complexity', pWaveComplexity,...
                'PWaveTerminalForce', pWaveTerminalForce,...
                'PWaveEntropy', pWaveEntropy,...
                'PWaveSampleEntropy', pWaveSampleEntropy,...
                'ActivationTimeValue', activationTimeValue,...
                'ActivationTimeSlope', activationTimeSlope);
        end
        
        function filteredSignal = BandpassFilter(self, signal)
            
            nyquistFrequency = self.SamplingFrequency / 2;
            
            bandpassFrequencies(1) = max([0 self.BandpassFrequencies(1)]);
            bandpassFrequencies(2) = min([nyquistFrequency, self.BandpassFrequencies(2)]);
            if bandpassFrequencies(1) > bandpassFrequencies(2)
                ME = MException('AFAnalysis:filterSignal', 'Invalid bandpass bounds');
                throw(ME);
            end
            
            if bandpassFrequencies(2) >= nyquistFrequency
                [b, a] = butter(self.BandpassOrder, bandpassFrequencies(1) / nyquistFrequency, 'high');
            else 
                [b, a] = butter(self.BandpassOrder, bandpassFrequencies / nyquistFrequency, 'bandpass');
            end
            
            numberOfChannels = size(signal, 2);
            filteredSignal = NaN(size(signal));
            parfor channelIndex = 1:numberOfChannels
                filteredSignal(:, channelIndex) = filtfilt(b, a, signal(:, channelIndex));
            end
        end
        
        function filteredSignal = ECGFilter(self, signal)
            if nargin < 4
                filterOrder = 3;
            end
            nyquistFrequency = self.SamplingFrequency / 2;
            ecgFrequencies = [0.5, 40];
            [b, a] = butter(filterOrder, ecgFrequencies / nyquistFrequency, 'bandpass');
            
            numberOfChannels = size(signal, 2);
            filteredSignal = NaN(size(signal));
            parfor channelIndex = 1:numberOfChannels
                filteredSignal(:, channelIndex) = filtfilt(b, a, signal(:, channelIndex));
            end
        end
        
        function [peakIndices, voltageThreshold] = FindPWavePeaks(self, pWaveAverage, intervalTime, onsetOffsetTime)
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            voltageThreshold = (max(pWaveAverage(startIndex:endIndex)) - min(pWaveAverage(startIndex:endIndex))) * self.PeakAmplitudeThreshold;
            peakIndices = PWaveMatcher.ComputePWaveComplexity(pWaveAverage, intervalTime, onsetOffsetTime, voltageThreshold);
        end
        
        function pWaveRange = EstimatePWaveRange(self, ecgData, rWaveIndices, qOnset, validChannels)
            qrstDetector = QRSTDetector();
            for channelIndex = validChannels
                [averageBeat(:,channelIndex), ~, rIndex] = qrstDetector.ComputeAverageBeatNoBaseline(ecgData, rWaveIndices, channelIndex);
            end
            
            averageBeatRms = rms(averageBeat');
            
            % Estimate P peak
%             qOnsetIndex = rIndex + (qOnset/ecgData.SamplingFrequency)*1e3;
            [~,locs,~,p] = findpeaks(averageBeatRms(1:qOnset-0.02*ecgData.SamplingFrequency));
            if isempty(locs)
                pWaveRange=[];
                return
            end
            [~, maxProminence] = max(p);
            pPeakIdx = locs(maxProminence);
                        
            % Estimate P onsets
            allPOnsets=nan(max(validChannels),1);
            for channelIndex = validChannels
                pOnsetCandidates=findchangepts(averageBeat(1:pPeakIdx,channelIndex),...
                            'MinThreshold',mad(averageBeat(1:pPeakIdx,channelIndex))/10,...
                            'Statistic','linear');
                if ~isempty(pOnsetCandidates)
                    allPOnsets(channelIndex,1) = pOnsetCandidates(1,1);
                end
            end
            
            if isempty(allPOnsets)
                pWaveRange=[];
                return
            end
            
            % remove outliers
            allPOnsets(isoutlier(allPOnsets))=[];
            pEstimateOnset = min(allPOnsets);
            
            pWaveRangeOnsetIndex = pEstimateOnset - rIndex - round(0.03*ecgData.SamplingFrequency);

            pWaveRange = [pWaveRangeOnsetIndex, qOnset - round(0.01*ecgData.SamplingFrequency) - rIndex]/ecgData.SamplingFrequency;
            
            if pWaveRange(1,1) < -0.35
                pWaveRange(1,1) = -0.35;
            end
        end
        
        function onsetOffset = DetectPStartEnd(self, ecgData, rWaveIndices, validChannels, pWaveIntervals)
            % Check if data is in muV, if not correct
            if nanmean(max(detrend(ecgData.Data)))<50 
            	rawData=ecgData.Data*1e3;
            else
                rawData=ecgData.Data;
            end
            
            band=[.05 75]; n=8;
            [b,a]=butter(round(n/4),band/ecgData.SamplingFrequency);
            filtECG=filtfilt(b,a,rawData);

            filteredECGdata=ECGData('filtData',filtECG,ecgData.SamplingFrequency);
            [filteredECG, ~, ~] = self.CorrectBaseline(filteredECGdata, rWaveIndices);

            % create pwavedata object with empty ElectrodeOnsetOffset, OnsetOffset and
            % ElectrodeTerminalforce (all NaNs)
            pWaveData = self.GetPWaveData(filteredECGdata, filteredECG,...
                pWaveIntervals, nan(1,3), nan(size(validChannels,2),2), validChannels,...
                nan(size(validChannels,2),1));
                        
            % adjust fitThreshold for Fs, 2000 -> 40, 500 -> 10
            self.fitThreshold=ecgData.SamplingFrequency/50;
            pStartCandidates=cell(size(pWaveData.AveragePWaves,2),1);
            pEndCandidates=cell(size(pWaveData.AveragePWaves,2),1);
            for lead = 1:size(pWaveData.AveragePWaves,2)
                [~,pPeakCandidates,~,pkProminence] = findpeaks(abs(pWaveData.AveragePWaves(:,lead)));
                [~,maxProm]=max(pkProminence);
                pPeak=pPeakCandidates(maxProm);
                
                [pStartCandidates{lead}, pEndCandidates{lead}]=...
                    self.findStartEndCandidates(pWaveData.AveragePWaves(:,lead),...
                    self.fitThreshold,ecgData.SamplingFrequency,pPeak);
            end
            
            self.minSlope=0.15;
            self.minAngle=10;
            [~, ~, glPStart, glPEnd]=...
                self.findBestCandidates(pStartCandidates,pEndCandidates,pWaveData,ecgData.SamplingFrequency);
            
            % global on/offset
            if ~isnan(glPStart)
                onsetOffset(1,1)=pWaveData.AveragingInterval(glPStart);
            else
                onsetOffset(1,1)=NaN;
            end
            if ~isnan(glPEnd)
                onsetOffset(1,2)=pWaveData.AveragingInterval(glPEnd);
            else
                onsetOffset(1,2)=NaN;
            end

        end
        
        function terminalForce = DetectTerminalForce(self, pWaveData, channel, samplingFrequency)
            signal = pWaveData.AveragePWaves(:,channel);
            onsetInd = find(pWaveData.AveragingInterval >= pWaveData.OnsetOffset(1),1,'first');
            if isempty(onsetInd)
                onsetInd = 1;
            end
            offsetInd = find(pWaveData.AveragingInterval >= pWaveData.OnsetOffset(2),1,'first');
            if isempty(offsetInd)
                offsetInd = size(pWaveData.AveragingInterval,2);
            end
            
            % P-wave should be positive before terminal force
            positiveP = find(signal(onsetInd:offsetInd)>0,1,'first');
            if isempty(positiveP)
                terminalForce = NaN;
                return
            else
                onsetInd = onsetInd + positiveP;
            end
             
            belowZero = signal<0;
            windowSize = round(0.025*samplingFrequency);
            convSignal = conv(belowZero,ones(windowSize,1),'same')/windowSize;
            terminalForceIndInPwave = find(convSignal(onsetInd:offsetInd)==1,1,'first');
            
            if ~isempty(terminalForceIndInPwave)
                terminalForceInd = terminalForceIndInPwave + onsetInd - floor(windowSize/2);
                terminalForce = pWaveData.AveragingInterval(terminalForceInd);
            else
                terminalForce = NaN;
            end
                
%             figure;
%             ax(1)=subplot(311);
%             plot(signal)
%             hold(ax(1),'on')
%             xline(onsetInd)
%             xline(offsetInd)
%             ax(2)=subplot(312);
%             plot(belowZero)
%             ax(3)=subplot(313);
%             plot(convSignal)
%             hold(ax(2),'on')
%             xline(terminalForceInd)
%             ylim([-0.2 1.2])
%             linkaxes(ax,'x')
        end
    end
    
    methods (Access = private)
        function [baselineCorrectedData, baselineCorrectedReferenceData, includedRWaveIndices] = CorrectBaseline(self, ecgData, rWaveIndices)
            numberOfChannels = ecgData.GetNumberOfChannels();
            numberOfReferenceChannels = size(ecgData.ReferenceData, 2);
%             if numberOfReferenceChannels == 0
%                 ecgData.ReferenceData = ecgData.Data(:, 1);
%                 ecgData.ReferenceLabels = ecgData.ElectrodeLabels(1);
%                 numberOfReferenceChannels = 1;
%             end
            numberOfSamples = ecgData.GetNumberOfSamples();
            
            bandWidth = round(self.BaselineBandwidth * ecgData.SamplingFrequency);
            rWaveOffset = round(self.PWaveRange(1) * ecgData.SamplingFrequency);
            
            baselineCenters = rWaveIndices + rWaveOffset;
            validCenters = baselineCenters > 0 & baselineCenters <= numberOfSamples;
            baselineCenters = baselineCenters(validCenters);
            includedRWaveIndices = rWaveIndices(validCenters);
            includedRWaveIndices = includedRWaveIndices(1:(end-1));
            numberOfIntervals = numel(baselineCenters) - 1;
            
            v0 = NaN(numberOfIntervals, numberOfChannels);
            v1 = NaN(numberOfIntervals, numberOfChannels);
            if numberOfReferenceChannels > 0
                v0Reference = NaN(numberOfIntervals, numberOfReferenceChannels);
                v1Reference = NaN(numberOfIntervals, numberOfReferenceChannels);
            end
            
            for intervalIndex = 1:numberOfIntervals
                t0 = baselineCenters(intervalIndex);
                t1 = baselineCenters(intervalIndex + 1);
                % average voltage over a small interval
                range1 = (t0 - bandWidth):(t0 + bandWidth);
                range1 = range1(range1 > 0 & range1 <= numberOfSamples);
                range2 = (t1 - bandWidth):(t1 + bandWidth);
                range2 = range2(range2 > 0 & range2 <= numberOfSamples);
                v0(intervalIndex, :) = mean(ecgData.Data(range1, :));
                v1(intervalIndex, :) = mean(ecgData.Data(range2, :));
                if numberOfReferenceChannels > 0
                    v0Reference(intervalIndex, :) = mean(ecgData.ReferenceData(range1, :));
                    v1Reference(intervalIndex, :) = mean(ecgData.ReferenceData(range2, :));
                end
            end
            
            baselineCorrectedData = ecgData.Data;
            baselineCorrectedReferenceData = ecgData.ReferenceData;
            for intervalIndex = 1:numberOfIntervals
                t0 = baselineCenters(intervalIndex);
                t1 = baselineCenters(intervalIndex + 1);
                tt = (t0:(t1-1));
                baseline = v0(intervalIndex, :)' * (t1-tt) / (t1-t0) + v1(intervalIndex, :)' * (tt-t0) / (t1-t0);
                baselineCorrectedData(tt, :) = baselineCorrectedData(tt, :) - baseline';
                if numberOfReferenceChannels > 0
                    referenceBaseline = v0Reference(intervalIndex, :)' * (t1-tt) / (t1-t0) + v1Reference(intervalIndex, :)' * (tt-t0) / (t1-t0);
                    baselineCorrectedReferenceData(tt, :) = baselineCorrectedReferenceData(tt, :) - referenceBaseline';
                end
            end
            
            if self.ShowDiagnostics
                % compute average beat
                rrMin = min(diff(includedRWaveIndices));
                windowStart= -round(rrMin * 0.3);
                windowStop = round(rrMin * 0.7);
                windowIndices = windowStart:windowStop;
                rWaveWindowIndices = bsxfun(@plus,...
                    repmat(windowIndices(:), 1, numel(includedRWaveIndices)),...
                    includedRWaveIndices(:)');
                validWindows = rWaveWindowIndices(1, :) > 0 &...
                    rWaveWindowIndices(end, :) <= size(baselineCorrectedData, 1);
                
                channelIndex = self.ExampleChannel;
                channelLabel = ecgData.ElectrodeLabels{channelIndex};
                exampleChannelData = baselineCorrectedData(:, channelIndex);
                qrstWindows = exampleChannelData(rWaveWindowIndices(:, validWindows));
                averageBeat = mean(qrstWindows, 2);
                beatStd = std(qrstWindows, 0, 2);
                
                plotHandle = subplot(2,2,1, 'parent', self.DiagnosticsView);
                cla(plotHandle);
                title(['Average beat after baseline correction (', channelLabel, ')']);
                time = (0:(numel(averageBeat) - 1)) / ecgData.SamplingFrequency;
                line('xData', time, 'yData', zeros(size(time)),...
                    'color', [.7, .7, .7], 'lineSmoothing', 'on', 'parent', plotHandle);
                line('xData', time, 'yData', averageBeat, 'lineStyle', '-', 'lineWidth', 1, 'color', [0,0,0]);
                line('xData', time, 'yData', averageBeat + beatStd, 'lineStyle', ':', 'color', [0,0,1]);
                line('xData', time, 'yData', averageBeat - beatStd, 'lineStyle', ':', 'color', [0,0,1]);
                xlabel('Time (s)');
                if nanmean(max(detrend(ecgData.Data)))<50
                    ylabel('Voltage (mV)');
                elseif nanmean(max(detrend(ecgData.Data)))>50
                    ylabel('Voltage (muV)');
                end
                axis(plotHandle, 'tight');
            end
        end
        
        function pWaveIntervals = ComputePWaveMatching(self, ecgData, correctedSignals, rWaveIndices, validChannels)
            % create p-wave intervals
            numberOfSamples = ecgData.GetNumberOfSamples();
            pWaveRange = round(self.PWaveRange * ecgData.SamplingFrequency);
            intervalLength = abs(pWaveRange(end) - pWaveRange(1)) + 1;
            
            pWaveIntervals = bsxfun(@plus, rWaveIndices(:), pWaveRange);
            validIntervals = pWaveIntervals(:, 1) > 0 & pWaveIntervals(:, 2) <= numberOfSamples;
            pWaveIntervals = pWaveIntervals(validIntervals, :);
            
            templateShift = round(self.TemplateShift * ecgData.SamplingFrequency);
            
            if self.ShowDiagnostics
                plotHandle = subplot(2,2,2, 'parent', self.DiagnosticsView);
                cla(plotHandle);
                title('P-Wave Energy estimate (RMS)');
                xlabel('Time (ms)');
                ylabel('Voltage Energy (mV^2)');
                lineHandle = NaN;
            end
            
            maxNumberOfInterations = 10;
            improving = true;
            iteration = 1;
            while improving && iteration <= maxNumberOfInterations
                %disp(['Iteration ', num2str(iteration)]);
                numberOfIntervals = numel(find(validIntervals));
                %disp(['Number of p-waves ', num2str(numberOfIntervals)]);
                % determine summed interval energy
                intervalEnergy = NaN(intervalLength, numberOfIntervals);
                for intervalIndex = 1:numberOfIntervals
                    intervalIndices = pWaveIntervals(intervalIndex, 1):pWaveIntervals(intervalIndex, 2);
                    intervalEnergy(:, intervalIndex) = sum(correctedSignals(intervalIndices, validChannels).^2, 2);
                end
                
                % compute average interval energy
                intervalEnergySum = sum(intervalEnergy);
                
                % remove intervals with missing values
                completeIntervals = ~isnan(intervalEnergySum);
                pWaveIntervals = pWaveIntervals(completeIntervals, :);
                intervalEnergy = intervalEnergy(:, completeIntervals);
                intervalEnergySum = intervalEnergySum(completeIntervals);
                numberOfIntervals = numel(intervalEnergySum);
                
                includedIntervals = intervalEnergySum <= median(intervalEnergySum) + 5 * mad(intervalEnergySum, 1);
                intervalAverage = mean(intervalEnergy(:, includedIntervals), 2);
                
                if self.ShowDiagnostics
                    intervalTime = 1000 * (0:(numel(intervalAverage)-1)) / ecgData.SamplingFrequency;
                    if ishandle(lineHandle)
                        set(lineHandle, 'color', [.7 .7 .7]);
                    end
                    lineHandle = line('xData', intervalTime, 'yData', intervalAverage,...
                        'color', [0, 0, 0], 'lineSmoothing', 'on', 'parent', plotHandle);
                end
                
                % determine maximum (shifted) template correlation
                maxCorrelation = NaN(numberOfIntervals, 1);
                correlationLag = NaN(numberOfIntervals, 1);
                for intervalIndex = 1:numberOfIntervals
                    [correlation, lags] = xcorr(intervalAverage, intervalEnergy(:, intervalIndex),...
                        templateShift, 'coeff');
                    [maxCorrelation(intervalIndex), maxPosition] = max(correlation);
                    correlationLag(intervalIndex) = lags(maxPosition);
                end
                
                pWaveIntervals = bsxfun(@minus, pWaveIntervals, correlationLag);
                validIntervals = maxCorrelation >= self.CorrelationThreshold;
                pWaveIntervals = pWaveIntervals(validIntervals, :);
                
                if all(validIntervals) && all(correlationLag == 0)
                    improving = false;
                else
                    %disp(['Number of p-waves matched: ', num2str(numel(find(validIntervals)))]);
                    iteration = iteration + 1;
                end
            end
        end
        
        % P-wave duration algorithm functions
        function [pStart, pEnd]=findStartEndCandidates(self,sig,fitThreshold,Fs,pPeak)
            changePts=findchangepts(sig,...
                        'MinThreshold',fitThreshold,...
                        'Statistic','linear');
            changePts=[1; changePts; length(sig)];

            % check slope of datasegments between pointsChanges
            slope=nan(numel(changePts)-1,1);
            for i=1:numel(changePts)-1
                slope(i,1)=self.computeSlope(sig(changePts(i):changePts(i+1)),Fs);
            end

            % check max slope in signal
            % if max slope < 2.5, P too flat --> no detection
            if ~isempty(slope) && ~isempty(pPeak)
                if max(abs(slope)) < 0.75 
                    pStart = NaN;
                    pEnd = NaN;
                    return
                end
            else 
                pStart = NaN;
                pEnd = NaN;
                return
            end

            % P start: changePts with slopes AFTER changePts
            pStart = [changePts(1:end-1) slope];
            pStart(pStart(:,1)>pPeak,:)=[]; % delete all changePts after pPeak

            % Check if window starts with a stable baseline, if not, P start can
            % not be determined
            if size(pStart,1)>1
                firstSlope=self.computeSlope(sig(pStart(1,1):pStart(2,1)),Fs);
                if abs(firstSlope)>1.5
                    pStart = NaN;
                    pEnd = NaN;
                    warning('No stable baseline')
                    return
                end 
            else
                pStart= NaN;
            end

            % P end: changePts with slopes BEFORE changePts
            pEnd = [changePts(2:end) slope];
            pEnd(pEnd(:,1)<pPeak,:)=[]; % delete all changePts before pPeak  
%             for i=1:size(pEnd,1)-1
%                 if pEnd(i+1,2)>1
%                     pEnd(i,:)=[NaN NaN];
%                 end
%             end
            % All Pend with a slope > 1 are deleted, might be QRS
%             pEnd(abs(pEnd(:,2))>1,:)=[];
%             if abs(lastSlope)>1
%                 pEnd(end,:)=[];
% %                 warning('P window might include QRS onset, last segment deleted')
%             end 

            computeAngleBetweenSlopes=@(x) atand((x(:,2)-x(:,1))./(1+x(:,1).*x(:,2)));
            pStart(:,3)=zeros(size(pStart,1),1);
            for i=1:size(pStart,1)-1
                pStart(i+1,3)=computeAngleBetweenSlopes(pStart(i:i+1,2)');
            end

            pEnd(:,3)=zeros(size(pEnd,1),1);
            for i=1:size(pEnd,1)-1
                pEnd(i,3)=computeAngleBetweenSlopes(pEnd(i:i+1,2)');
            end
        end
        
        function sl=computeSlope(~,sig,Fs)
            W=[ones(length(sig),1) [1:length(sig)]'];
            c=W\sig;
            sl=c(2); % slope: muV per sample
            sl=(sl*Fs)/1e3; % slope: muV per ms
        end    
        
        function [pStart, pEnd, glPStart, glPEnd]=findBestCandidates...
                (self,pStartCandidates,pEndCandidates,pWaveData,samplingFrequency)
            
            sig=pWaveData.AveragePWaves;
            
            maxAllowedDifference = .025*samplingFrequency;
%             computeAngleBetweenSlopes=@(x) atand((x(:,2)-x(:,1))./(1+x(:,1).*x(:,2)));

            cumPStartCandidates=zeros(size(sig));
            cumPEndCandidates=zeros(size(sig));
            for lead = 1:length(pStartCandidates)
                if ~all(isnan(pStartCandidates{lead}(:,1))) && ~isempty(pStartCandidates{lead})           
                    firstPStartCandidates=find(abs(pStartCandidates{lead}(:,2))>self.minSlope & ...
                                           abs(pStartCandidates{lead}(:,3))>self.minAngle,1,'First');
                    cumPStartCandidates(pStartCandidates{lead}(firstPStartCandidates,1),lead)=1;
                end


                if ~all(isnan(pEndCandidates{lead}(:,1))) && ~isempty(pEndCandidates{lead})
                    lastPEndCandidates=find(abs(pEndCandidates{lead}(:,2))>self.minSlope & ...
                                        abs(pEndCandidates{lead}(:,3))>self.minAngle,1,'Last');
                    cumPEndCandidates(pEndCandidates{lead}(lastPEndCandidates,1),lead)=1;
                end
            end
            
            cumSumPStartCandidates=nansum(cumPStartCandidates,2);
            neighbors=11;
            windowedCumSumPStartCandidates=filtfilt(ones(neighbors,1),1,cumSumPStartCandidates);

            cumSumPEndCandidates=sum(cumPEndCandidates,2);
            windowedCumSumPEndCandidates=filtfilt(ones(neighbors,1),1,cumSumPEndCandidates);

            % global Pstart used for finding the best pStart candidates 
            % the real global pStart is lateron calculated from the best
            % pStart candidates
            [~,glPStart]=findpeaks(windowedCumSumPStartCandidates,...
                'MinPeakHeight',.7*max(windowedCumSumPStartCandidates),...
                'SortStr','descend');
            
            if ~isempty(glPStart)
                % if more peaks found, take largest peak
                if numel(glPStart)>1
                    glPStart=glPStart(1);
                end
            else
                glPStart=NaN;
            end
            
            % if global P start is within 50ms of the start of the window, not
            % enough baseline is taken into account
            if glPStart<0.015*samplingFrequency
                glPStart=NaN;
            end
            
            % global pEnd used for finding the best pEnd candidates 
            % the real global pEnd is lateron calculated from the best
            % pEnd candidates            
            [~,glPEnd]=findpeaks(windowedCumSumPEndCandidates,...
                'MinPeakHeight',.7*max(windowedCumSumPEndCandidates),...
                'SortStr','descend');
            
            if ~isempty(glPEnd)
                % if more peaks found, take largest peak
                if numel(glPEnd)>1
                    glPEnd=glPEnd(1);
                end
            else
                glPEnd=NaN;
            end

%             figure; 
%             ax(1)=subplot(311);
%             plot(sig)
%             xline(glPStart)
%             xline(glPEnd)
%             ax(2)=subplot(312);
%             hold on
%             for i=1:15
%                 firstPStartCandidataes=find(abs(pStartCandidates{i}(1:3,2))>self.minSlope & ...
%                                            abs(pStartCandidates{i}(1:3,3))>self.minAngle,1,'First');
%                 plot(pStartCandidates{i}(firstPStartCandidataes,1),...
%                     abs(pStartCandidates{i}(firstPStartCandidataes,2)),'o')
%                 
%                 lastPEndCandidates=find(abs(pEndCandidates{i}(:,2))>self.minSlope & ...
%                                        abs(pEndCandidates{i}(:,3))>self.minAngle,1,'Last');
%                                     
%                 plot(pEndCandidates{i}(lastPEndCandidates,1),...
%                     abs(pEndCandidates{i}(lastPEndCandidates,2)),'x')
%             end
%             ax(3)=subplot(313);
%             plot(windowedCumSumPStartCandidates)
%             hold on
%             plot(windowedCumSumPEndCandidates)
%             plot(glPStart,windowedCumSumPStartCandidates(glPStart),'+')
%             plot(glPEnd,windowedCumSumPEndCandidates(glPEnd),'+')
%             linkaxes(ax,'x')   
        %     
            % find local pStart and pEnd within window around global pStart and pEnd
            pStart=nan(length(pStartCandidates),1);
            pEnd=nan(length(pStartCandidates),1);
            for lead=1:length(pStartCandidates)
                if ~all(isnan(pEndCandidates{lead}(:,1))) && ~isempty(pEndCandidates{lead}) && ~isnan(glPStart)
                    pStartCandidatesMeetsThreshold=pStartCandidates{lead}(abs(pStartCandidates{lead}(:,2))>self.minSlope & ...
                                                                          abs(pStartCandidates{lead}(:,3))>self.minAngle,1);
                    distanceLocalGlobalStart=pStartCandidatesMeetsThreshold-glPStart;
                    
                    pStartCandidatesAlmostMeetsThreshold=pStartCandidates{lead}(abs(pStartCandidates{lead}(:,2))>2/3*self.minSlope & ...
                                                                          abs(pStartCandidates{lead}(:,3))>2/3*self.minAngle,1);
                    if any(abs(distanceLocalGlobalStart)<maxAllowedDifference)
                        pStartWithinWindow=find(abs(distanceLocalGlobalStart)<maxAllowedDifference);
                        pStart(lead)=pStartCandidatesMeetsThreshold(pStartWithinWindow(1),1);
                    elseif ~any(abs(pStartCandidatesAlmostMeetsThreshold-glPStart)<maxAllowedDifference)
                        startCanditatesOutsideWindow=find(abs(pStartCandidates{lead}(:,2))>1.5*self.minSlope & ...
                                           abs(pStartCandidates{lead}(:,3))>1.5*self.minAngle);
                        if ~isempty(startCanditatesOutsideWindow)
                            [~,candidateClosestToGlobalStart]=min(abs(pStartCandidates{lead}(startCanditatesOutsideWindow,1)-glPStart));
                            pStart(lead)=pStartCandidates{lead}(startCanditatesOutsideWindow(candidateClosestToGlobalStart),1);
                        else
                            pStart(lead)=NaN;
                        end
                    else
                        pStart(lead)=NaN;
                    end
                else
                    pStart(lead)=NaN;
                end
                
                if ~all(isnan(pEndCandidates{lead}(:,1))) && ~isempty(pEndCandidates{lead})            
                    pEndCandidatesAngleAboveThreshold=pEndCandidates{lead}(abs(pEndCandidates{lead}(:,2))>self.minSlope & ...
                                                                           abs(pEndCandidates{lead}(:,3))>self.minAngle,1);
                    distanceLocalGlobalEnd=pEndCandidatesAngleAboveThreshold-glPEnd;
                    
                    pEndCandidatesAngleAlmostAboveThreshold=pEndCandidates{lead}(abs(pEndCandidates{lead}(:,2))>2/3*self.minSlope & ...
                                                                                 abs(pEndCandidates{lead}(:,3))>2/3*self.minAngle,1);
                    if any(abs(distanceLocalGlobalEnd)<maxAllowedDifference)
                        pEndWithinWindow=find(abs(distanceLocalGlobalEnd)<maxAllowedDifference);
                        if numel(pEndWithinWindow)>1
                            [~,candidateClosestToGlobalEnd]=min(abs(pEndCandidatesAngleAboveThreshold(pEndWithinWindow,1)-glPEnd));
                            pEnd(lead)=pEndCandidatesAngleAboveThreshold(pEndWithinWindow(candidateClosestToGlobalEnd),1);
                        else
                            pEnd(lead)=pEndCandidatesAngleAboveThreshold(pEndWithinWindow,1);
                        end
                    elseif ~any(abs(pEndCandidatesAngleAlmostAboveThreshold-glPEnd)<maxAllowedDifference)
                        endCandidatesOutsideWindow=find(abs(pEndCandidates{lead}(:,2))>1.5*self.minSlope & ...
                                              abs(pEndCandidates{lead}(:,3))>1.5*self.minAngle);
                        if ~isempty(endCandidatesOutsideWindow)
                            [~,candidateClosestToGlobalEnd]=min(abs(pEndCandidates{lead}(endCandidatesOutsideWindow,1)-glPEnd));
                            pEnd(lead)=pEndCandidates{lead}(endCandidatesOutsideWindow(candidateClosestToGlobalEnd),1);
                        else
                            pEnd(lead)=NaN;
                        end                
                    else
                        pEnd(lead)=NaN;
                    end
                else
                    pEnd(lead)=NaN;
                end
            end

            reliable = self.checkReliability(sig,pStart,pEnd,samplingFrequency);
            pStart(reliable(:,1)==0)=NaN;
            
            glPStart = round(prctile(pStart,10,1));
            glPEnd = round(prctile(pEnd,90,1));
        end
          
        function reliable = checkReliability(~,sig,pStart,~,Fs)
            reliable=ones(size(sig,2),1);
            meanBaselineDeviationPreStart=NaN(size(sig,2),1);
            for lead = 1:size(sig,2)
                if ~isnan(pStart(lead))
                    if pStart(lead)<0.015*Fs
                        reliable(lead,1)=0;
                    end

                    startWindow=1:pStart(lead);
                    meanBaselineDeviationPreStart(lead,1)=mean(std(diff(sig(startWindow,lead))));
                    if abs(meanBaselineDeviationPreStart)>1
                        reliable(lead,1)=0;
                    end
                else
                    reliable(lead,1)=NaN;
                end
            end
        end
    end
    
    methods (Static)
        function [thresholds, duration, optimalIndex, optimalRange] = ComputeNoiseThresholdDuration(ecgData, intervalAverage)
            thresholds = unique(intervalAverage);
            duration = NaN(size(thresholds));
            for thresholdIndex = 1:numel(thresholds)
                validPoints = intervalAverage >= thresholds(thresholdIndex);
                maxLength = max(diff([0; (find(~validPoints)); numel(intervalAverage) + 1]) - 1);
                duration(thresholdIndex) = maxLength / ecgData.SamplingFrequency;
            end
           
           transformedTresholds = log10(thresholds);
           scaledThresholds = (transformedTresholds - min(transformedTresholds)) / (max(transformedTresholds) - min(transformedTresholds));
           scaledDurations = (duration - min(duration)) / (max(duration) - min(duration));
           [~, optimalIndex] = min(scaledThresholds + 0.5 * scaledDurations);
           
           validPoints = intervalAverage >= thresholds(optimalIndex);
           pointIndices = [0; (find(~validPoints)); numel(intervalAverage) + 1];
           [~, maxIndex] = max(diff(pointIndices) - 1);
           optimalRange = [pointIndices(maxIndex), pointIndices(maxIndex + 1)];
           if optimalRange(1) < 1
               optimalRange(1) = 1;
           end
           if optimalRange(end) > numel(intervalAverage)
               optimalRange(2) = numel(intervalAverage);
           end
        end
        
        function [pWaveStartIndex, pWaveEndIndex, qOnset] = ComputeOnsetOffsetDuration(ecgData, intervalAverage, intervalTime, showDiagnostics)
            if nargin < 4
                showDiagnostics = false;
            end
            
            frameLength = round(0.025 * ecgData.SamplingFrequency);
            if mod(frameLength, 2) == 0
                frameLength = frameLength + 1;
            end
            filteredSignal = sgolayfilt(intervalAverage, 5, frameLength);
            firstDerivativeFiltered = gradient(filteredSignal, mean(diff(intervalTime)));
            secondDerivativeFiltered = gradient(firstDerivativeFiltered, mean(diff(intervalTime)));
            
            % find the Q-onset: maximum 2nd derivative peak difference in second half of
            % the interval that is lower than the maximum of the first part
            % (-75ms) of the interval signal
            numberOfSamples = numel(intervalAverage);
            mimimumQOnsetTime = 0.075;
            intervalCutOff = round(ecgData.SamplingFrequency * mimimumQOnsetTime);
            mimimumQOnsetIndex = numberOfSamples - intervalCutOff;
            estimatedPWaveEnergyHeight = max(abs(filteredSignal(1:(numberOfSamples - intervalCutOff))));
            qOnsetFound = false;
            qOnsetRelativeHeight = 1;
            maxSignalHeight = max(abs(filteredSignal));
            [secondDerivativePeakValues, secondDerivativePeakIndices] = findpeaks(secondDerivativeFiltered);
            if isempty(secondDerivativePeakIndices)
                qOnset = NaN;
                pWaveStartIndex = NaN;
                pWaveEndIndex = NaN;
                return;
            end
            validPeakIndices = secondDerivativePeakIndices >= mimimumQOnsetIndex &...
                secondDerivativePeakValues > 0;
            if ~any(validPeakIndices)
                % if no peaks found within the minimum onset time, take
                % the latest one
                validPeakIndices(end) = true;
            end
            % locate zero-crossing before local maximum
            secondDerivativePeakIndices = secondDerivativePeakIndices(validPeakIndices);
            secondDerivativeZeroCrossingIndices = NaN(size(secondDerivativePeakIndices));
            for peakIndex = 1:numel(secondDerivativePeakIndices)
                previousPeakIndex = 1;
                if peakIndex > 1
                    previousPeakIndex = secondDerivativePeakIndices(peakIndex - 1);
                end
                zeroCrossingIndex = find(secondDerivativeFiltered(previousPeakIndex:secondDerivativePeakIndices(peakIndex)) <= 0, 1, 'last');
                if isempty(zeroCrossingIndex)
                    secondDerivativeZeroCrossingIndices(peakIndex) = NaN;
                else
                    secondDerivativeZeroCrossingIndices(peakIndex) = previousPeakIndex + zeroCrossingIndex;
                end
            end
            validIndices = ~isnan(secondDerivativeZeroCrossingIndices);
            secondDerivativePeakIndices = secondDerivativePeakIndices(validIndices);
            secondDerivativeZeroCrossingIndices = secondDerivativeZeroCrossingIndices(validIndices);
            
            maxQOnsetHeight = qOnsetRelativeHeight * estimatedPWaveEnergyHeight;
            while ~qOnsetFound && maxQOnsetHeight < maxSignalHeight
                validPeakIndices = abs(filteredSignal(secondDerivativeZeroCrossingIndices)) <= maxQOnsetHeight;
                candidateDerivativeZeroCrossingIndices = secondDerivativeZeroCrossingIndices(validPeakIndices);
                candidateDerivativePeakIndices = secondDerivativePeakIndices(validPeakIndices);
                if ~isempty(candidateDerivativeZeroCrossingIndices)
                    [~, maxDerivativeIndex] = max(secondDerivativeFiltered(candidateDerivativePeakIndices));
                    qOnset = candidateDerivativeZeroCrossingIndices(maxDerivativeIndex);
                    qOnsetFound = true;
                else
                    maxQOnsetHeight = min(abs(filteredSignal(secondDerivativeZeroCrossingIndices)));
                end
            end
            
            if ~qOnsetFound
                % set to 50ms
                qOnset = numberOfSamples - round(ecgData.SamplingFrequency * 0.05);
            end
            
            % detrend based on qOnset
            tt = (1:qOnset);
            baselineAveragingLength = 0.025;
            baselineAveragingSamples = round(ecgData.SamplingFrequency * baselineAveragingLength);
            signalStartMean = median(filteredSignal(1:baselineAveragingSamples));
            signalQOnsetMean = median(filteredSignal((qOnset-baselineAveragingSamples):qOnset));
            baseline = signalStartMean * (qOnset - tt) / (qOnset - 1) +...
                signalQOnsetMean * (tt - 1) / (qOnset - 1);
            filteredSignal(1:qOnset) = filteredSignal(1:qOnset) - baseline(:);
            firstDerivativeFiltered = firstDerivativeFiltered - (signalQOnsetMean - signalStartMean) / (qOnset - 1);
            estimatedPWaveEnergyHeight = max(abs(filteredSignal(1:(numberOfSamples - intervalCutOff))));
            
            % determine intervals with low slope & limited change in slope
            validSamples = (1:numel(intervalAverage)) < qOnset;
            madFirstDerivative = mad(firstDerivativeFiltered(validSamples));
            madSecondDerivative = mad(secondDerivativeFiltered(validSamples));
            candidateWindows = [0; find(...
                abs(firstDerivativeFiltered(validSamples)) > madFirstDerivative |...
                abs(secondDerivativeFiltered(validSamples)) > 2 * madSecondDerivative |...
                abs(filteredSignal(validSamples)) > estimatedPWaveEnergyHeight / 2); qOnset];
            windowSizes = diff(candidateWindows);
            validWindowSizes = windowSizes > 1;
            validWindowSizes(end) = true;   % always include Q onset
            windowSizes = windowSizes(validWindowSizes);
            windowStartIndices = candidateWindows([validWindowSizes; false]);
            windowEndIndices = candidateWindows([false; validWindowSizes;]);
            [sortedWindowSizes, sortedWindowIndices] = sort(windowSizes, 'descend');
            
            % minimum p-wave duration of 50ms
            minimumIntervalLength = round(ecgData.SamplingFrequency) * 0.05;
            % maximum p-wave duration of 200ms
            maximumIntervalLength = round(ecgData.SamplingFrequency) * 0.2;
            % minimum energy coverage of 75%
            minimumEnergyCoverage = 0.75;
            
            intervalFound = false;
            numberOfWindows = numel(windowSizes);
            if numberOfWindows == 1 % include Q onset if only 1 interval is found
                numberOfWindows = 2;
                windowStartIndices = [windowStartIndices, qOnset];
                windowEndIndices = [windowEndIndices, qOnset];
            end
            pWaveRangeEnergy = sum(abs(filteredSignal(1:(numberOfSamples - intervalCutOff))));
            
            % compute the properties of every possible interval
            windowIntervalLength = NaN(numberOfWindows);
            windowEnergyCoverage = NaN(numberOfWindows);
            for firstWindowIndex = 1:numberOfWindows - 1
                for secondWindowIndex = 2:numberOfWindows
                    pWaveEnergy = sum(abs(filteredSignal(windowEndIndices(firstWindowIndex):windowStartIndices(secondWindowIndex))));
                    windowIntervalLength(firstWindowIndex, secondWindowIndex) = windowStartIndices(secondWindowIndex) - windowEndIndices(firstWindowIndex);
                    windowEnergyCoverage(firstWindowIndex, secondWindowIndex) = pWaveEnergy / pWaveRangeEnergy;
                end
            end
            windowWithInvalidLength = windowIntervalLength < minimumIntervalLength |...
                    windowIntervalLength > maximumIntervalLength;
            windowIntervalLength(windowWithInvalidLength) = NaN;
            windowEnergyCoverage(windowWithInvalidLength) = NaN;
            
            while ~intervalFound && (~isempty(minimumEnergyCoverage) && minimumEnergyCoverage >= 0.25)
                invalidCombinations = windowEnergyCoverage < minimumEnergyCoverage;
                
                windowDecisionMatrix = windowEnergyCoverage ./ windowIntervalLength;
                windowDecisionMatrix(invalidCombinations) = NaN;
                if ~(all(isnan(windowDecisionMatrix(:))))
                    intervalFound = true;
                else
                    minimumEnergyCoverage = max(windowEnergyCoverage(:));
                end
            end
            [maxStartWindow, maxEndWindow] = find(windowDecisionMatrix == max(windowDecisionMatrix(:)));
                     
            if intervalFound
                pWaveStartIndex = windowEndIndices(maxStartWindow);
                pWaveEndIndex = windowStartIndices(maxEndWindow);
            else
                disp('no onset and offset found');
                pWaveStartIndex = NaN;
                pWaveEndIndex = NaN;
            end
            
            if showDiagnostics
                figure;
                plot(intervalTime, intervalAverage);
                line('xData', intervalTime, 'yData', filteredSignal, 'color', 'r');
                line('xData', intervalTime(tt), 'yData', baseline, 'color', 'k', 'lineStyle', ':');
                line('xData', [intervalTime(1) intervalTime(end)], 'yData', [madFirstDerivative, madFirstDerivative],...
                    'color', 'r', 'lineStyle', ':');
                line('xData', [intervalTime(1) intervalTime(end)], 'yData', [-madFirstDerivative, -madFirstDerivative],...
                    'color', 'r', 'lineStyle', ':');
                line('xData', intervalTime, 'yData', firstDerivativeFiltered, 'color', 'r');
                line('xData', [intervalTime(1) intervalTime(end)], 'yData', [madSecondDerivative, madSecondDerivative],...
                    'color', 'b', 'lineStyle', ':');
                line('xData', [intervalTime(1) intervalTime(end)], 'yData', [-madSecondDerivative, -madSecondDerivative],...
                    'color', 'b', 'lineStyle', ':');
                line('xData', [intervalTime(1) intervalTime(end)], 'yData', [estimatedPWaveEnergyHeight / 2, estimatedPWaveEnergyHeight / 2],...
                    'color', 'k', 'lineStyle', ':');
                line('xData', intervalTime, 'yData', secondDerivativeFiltered, 'color', 'b');
                
                if ~isnan(qOnset)
                    line('xData', intervalTime(qOnset),...
                        'yData', intervalAverage(qOnset),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                    line('xData', intervalTime(qOnset),...
                        'yData', filteredSignal(qOnset),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                end
                
                if ~isnan(pWaveStartIndex)
                    line('xData', intervalTime(pWaveStartIndex),...
                        'yData', intervalAverage(pWaveStartIndex),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                    line('xData', intervalTime(pWaveStartIndex),...
                        'yData', filteredSignal(pWaveStartIndex),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                end
                
                if ~isnan(pWaveEndIndex)
                    line('xData', intervalTime(pWaveEndIndex),...
                        'yData', intervalAverage(pWaveEndIndex),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                    line('xData', intervalTime(pWaveEndIndex),...
                        'yData', filteredSignal(pWaveEndIndex),...
                        'lineStyle', 'none', 'marker', 'o', 'markerFaceColor', 'b', 'markerEdgeColor', 'r');
                end
            end
        end
        
        function pWaveArea = ComputePWaveArea(pWaveAverage, intervalTime, onsetOffsetTime, detrendAverage)
            if nargin < 4
                detrendAverage = false;
            end
            
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            if detrendAverage
                tt = (startIndex:endIndex);
                baseline = pWaveAverage(startIndex) * (endIndex - tt) / (endIndex - startIndex) +...
                    pWaveAverage(endIndex) * (tt - startIndex) / (endIndex - startIndex);
                pWaveAverage(startIndex:endIndex) = pWaveAverage(startIndex:endIndex) - baseline(:);
            end
            
            timeDifferences = diff(intervalTime(startIndex:endIndex));
            timeDifferences = [timeDifferences(:); mean(timeDifferences)];
            pWaveArea = sum(abs(pWaveAverage(startIndex:endIndex)) .* timeDifferences);
        end
        
        function [pWaveEntropy, pWaveSampleEntropy] = ComputePWaveEntropy(pWaveAverage, intervalTime, onsetOffsetTime, detrendAverage)
            if detrendAverage
                pWaveAverage = PWaveMatcher.DetrendAveragePWave(pWaveAverage, intervalTime, onsetOffsetTime);
            end
            
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            entropySignal = pWaveAverage(startIndex:endIndex);
            
            % Shannon entropy
            numberOfBins = 10;
            binCounts = histcounts(entropySignal, numberOfBins);
            probabilities = binCounts / sum(binCounts);
            validProbabilities = probabilities > 0;
            if any(validProbabilities)
                pWaveEntropy = -sum(probabilities(validProbabilities) .* log2(probabilities(validProbabilities)));
            else
                pWaveEntropy = 0;
            end
            
            % sample entropy
            sampleEntropyNumberOfSamples = 2;
            sampleEntropyThreshold = 0.35;
            samplingFrequency = 1e3 / mean(diff(intervalTime));
            resampleFrequency = 500;    % default sampling frequency 500Hz
            pWaveSampleEntropy = AlgorithmPkg.AFComplexityCalculator.SampleEntropyResampled(...
                entropySignal, sampleEntropyNumberOfSamples, sampleEntropyThreshold,...
                samplingFrequency, resampleFrequency);
            
%             pWaveSampleEntropy = AlgorithmPkg.AFComplexityCalculator.SampleEntropy(...
%                 entropySignal, sampleEntropyNumberOfSamples, sampleEntropyThreshold);
        end
        
        function [activationValue, activationSlope] = ComputeActivationTime(pWaveAverage, intervalTime, onsetOffsetTime)
            validIntervalTime = intervalTime >= onsetOffsetTime(1) & intervalTime <= onsetOffsetTime(2);
            pWaveIntervalTime = intervalTime(validIntervalTime);
            
            [positivePeakValues, positivePeakIndices] = findpeaks(pWaveAverage);
            validPositivePeaks = validIntervalTime(positivePeakIndices);
            positivePeakIndices = positivePeakIndices(validPositivePeaks);
            positivePeakValues = positivePeakValues(validPositivePeaks);
            [maxValue, maxIndex] = max(positivePeakValues);
            maxValueTime = intervalTime(positivePeakIndices(maxIndex));
            
            [negativePeakValues, negativePeakIndices] = findpeaks(-pWaveAverage);
            validNegativePeaks = validIntervalTime(negativePeakIndices);
            negativePeakIndices = negativePeakIndices(validNegativePeaks);
            negativePeakValues = negativePeakValues(validNegativePeaks);
            [minValue, maxIndex] = max(negativePeakValues);
            minValueTime = intervalTime(negativePeakIndices(maxIndex));
            
            if  abs(maxValue) >= abs(minValue)
                maxAbsTime = maxValueTime;
            else
                maxAbsTime = minValueTime;
            end
            activationValue = struct(...
                'Max', maxValueTime,...
                'Min', minValueTime,...
                'MaxAbs', maxAbsTime);
            
            averageSlope = gradient(sgolayfilt(pWaveAverage, 3, 21));
            [~, maxSlopeIndex] = max(averageSlope(validIntervalTime));
            [~, minSlopeIndex] = min(averageSlope(validIntervalTime));
            [~, maxAbsSlopeIndex] = max(abs(averageSlope(validIntervalTime)));
            activationSlope = struct(...
                'Max', pWaveIntervalTime(maxSlopeIndex),...
                'Min', pWaveIntervalTime(minSlopeIndex),...
                'MaxAbs', pWaveIntervalTime(maxAbsSlopeIndex));
        end
        
        function [voltageThresholds, numberOfPeaks] = ComputeVoltageThresholdComplexity(pWaveAverage, intervalTime, onsetOffsetTime)
            voltageThresholds = unique(abs(pWaveAverage));
            peakIndices = PWaveMatcher.ComputePWaveComplexity(pWaveAverage, intervalTime, onsetOffsetTime, voltageThresholds);
            numberOfPeaks = cellfun(@numel, peakIndices);
        end
        
        function peakComplexityIndices = ComputePWaveComplexity(pWaveAverage, intervalTime, onsetOffsetTime, voltageThreshold)
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            [pWavePositivePeakValues, pWavePositivePeakIndices] = findpeaks(pWaveAverage);
            [pWaveNegativePeakValues, pWaveNegativePeakIndices] = findpeaks(-pWaveAverage);
            
            peakValues = [pWavePositivePeakValues; -pWaveNegativePeakValues];
            peakIndices = [pWavePositivePeakIndices; pWaveNegativePeakIndices];
            [sortedPeakValues, sortedPeakIndices] = sort(peakIndices, 'ascend');
            peakIndices = sortedPeakValues;
            peakValues = peakValues(sortedPeakIndices);
            
            validPeakIndices = peakIndices > startIndex & peakIndices < endIndex;
            peakIndices = peakIndices(validPeakIndices);
            peakValues = peakValues(validPeakIndices);
            
            peakIndices = [startIndex; peakIndices(:); endIndex];
            peakValues = [pWaveAverage(startIndex); peakValues; pWaveAverage(endIndex);];
            
            numberOfThresholds = numel(voltageThreshold);
            peakComplexityIndices = cell(numberOfThresholds, 1);
            for thresholdIndex = 1:numberOfThresholds
                currentPeakIndices = peakIndices;
                currentPeakValues = peakValues;
                changed = true;
                while changed
                    voltageDifferences = diff(currentPeakValues);
                    [minDifference, minIndex] = min(abs(voltageDifferences));
                    if minDifference < voltageThreshold(thresholdIndex)
                        currentPeakIndices([minIndex, minIndex + 1]) = [];
                        currentPeakValues([minIndex, minIndex + 1]) = [];
                        
                        if numel(currentPeakIndices) > 0 && currentPeakIndices(1) > startIndex
                            currentPeakIndices = [startIndex; currentPeakIndices];
                            currentPeakValues = [pWaveAverage(startIndex); currentPeakValues];
                        end
                        if numel(currentPeakIndices) > 0 && currentPeakIndices(end) < endIndex
                            currentPeakIndices = [currentPeakIndices; endIndex];
                            currentPeakValues = [currentPeakValues; pWaveAverage(endIndex)];
                        end
                    else
                        changed = false;
                    end
                end
                
                if numel(currentPeakIndices) > 0 && currentPeakIndices(1) == startIndex
                    currentPeakIndices = currentPeakIndices(2:end);
                end
                if numel(currentPeakIndices) > 0 && currentPeakIndices(end) == endIndex
                    currentPeakIndices = currentPeakIndices(1:(end-1));
                end
                
                peakComplexityIndices{thresholdIndex} = currentPeakIndices;
            end
            
            if numberOfThresholds == 1
                peakComplexityIndices = peakComplexityIndices{1};
            end
        end
        
        function pWaveTerminalForce = ComputePWaveTerminalForce(pWaveAverage, intervalTime, onsetOffsetTime, terminalForceTime, detrendAverage)
            if nargin < 5
                detrendAverage = false;
            end
            
            if isnan(terminalForceTime)
                pWaveTerminalForce = NaN;
                return
            end
            
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            terminalForceIndex = find(intervalTime >= terminalForceTime, 1, 'first');
            
            if detrendAverage
                tt = (startIndex:endIndex);
                baseline = pWaveAverage(startIndex) * (endIndex - tt) / (endIndex - startIndex) +...
                    pWaveAverage(endIndex) * (tt - startIndex) / (endIndex - startIndex);
                pWaveAverage(startIndex:endIndex) = pWaveAverage(startIndex:endIndex) - baseline(:);
            end
            
            timeDifference = mean(diff(intervalTime(startIndex:endIndex)));
            terminalForceData = pWaveAverage(terminalForceIndex:endIndex);
            terminalForceData = terminalForceData(terminalForceData <= 0);
            pWaveTerminalForce = sum(abs(terminalForceData)) * timeDifference;
        end
        
        function detrendedPWave = DetrendAveragePWave(pWaveAverage, intervalTime, onsetOffsetTime, baselineAveragingSamples)
            if nargin < 4
                % default averaging interval of 1 sample
                baselineAveragingSamples = 1;
            end
            if isnan(onsetOffsetTime(1))
                startIndex = 1;
            else
                startIndex = find(intervalTime >= onsetOffsetTime(1), 1, 'first');
            end
            if isnan(onsetOffsetTime(2))
                endIndex = numel(intervalTime);
            else
                endIndex = find(intervalTime <= onsetOffsetTime(2), 1, 'last');
            end
            
            signalStartMedian = median(pWaveAverage(startIndex:(startIndex + baselineAveragingSamples)));
            signalEndMedian = median(pWaveAverage((endIndex-baselineAveragingSamples):endIndex));
            baselineSlope = (signalEndMedian - signalStartMedian) / (endIndex - startIndex);
            
            numberOfSamples = numel(pWaveAverage);
            baselineSamples = 1:numberOfSamples;
            baseline = baselineSlope * (baselineSamples - startIndex) + signalStartMedian;
            
            detrendedPWave = pWaveAverage - baseline(:);
        end
        
        function [detectedMap, detectedMapIndex] = DetectECGMap(electrodeLabels)
            numberOfAvailableMaps = numel(PWaveAnalysisPanel.AvailableMaps);
            leadsMatched = NaN(numberOfAvailableMaps, 1);
            for mapIndex = 1:numberOfAvailableMaps
                currentMap = PWaveAnalysisPanel.AvailableMaps(mapIndex);
                if isempty(currentMap.Back)
                    mapLabels = currentMap.Front;
                else
                    mapLabels = [currentMap.Front, currentMap.Back];
                end
                mapLabels = mapLabels(cellfun(@(x) ~isempty(x), mapLabels));
                mapLabels = unique(mapLabels);
                ecgFoundInMap = ismember(mapLabels, electrodeLabels);
                mapFoundInECG = ismember(electrodeLabels, mapLabels);
                leadsMatched(mapIndex) = numel(find(ecgFoundInMap)) / numel(mapLabels) +...
                    numel(find(mapFoundInECG)) / numel(electrodeLabels);
            end
            
            [~, detectedMapIndex] = max(leadsMatched);
            detectedMap = PWaveMatcher.AvailableMaps(detectedMapIndex);
        end
        
        function maps = LoadAvailableBSPMMaps()
            thisFilePath = mfilename('fullpath');
            [foldername, ~] = fileparts(thisFilePath);
            mapLocation = fullfile(foldername, 'Data', 'BSPMMaps');
            listing = dir([mapLocation filesep '*.mat']);
            
            maps(numel(listing), 1) = struct('name', [], 'Front', [], 'Back', []);
            validMaps = true(numel(listing), 1);
            for listIndex = 1:numel(listing)
                localFilename = listing(listIndex).name;
                if strcmp(localFilename(1:2), '._')
                    validMaps(listIndex) = false;
                    continue;
                end
                filename = fullfile(mapLocation, listing(listIndex).name);
                
                mapData = load(filename);
                fieldNames = fieldnames(mapData);
                
                maps(listIndex).name = fieldNames{1};
                
                if isfield(mapData.(fieldNames{1}), 'Front')
                    maps(listIndex).Front = mapData.(fieldNames{1}).Front;
                else
                    validMaps(listIndex) = false;
                end
                
                if isfield(mapData.(fieldNames{1}), 'Back')
                    maps(listIndex).Back = mapData.(fieldNames{1}).Back;
                end
            end
            maps = maps(validMaps);
        end
        
    end
end