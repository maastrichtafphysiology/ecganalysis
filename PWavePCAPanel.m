classdef PWavePCAPanel < UserInterfacePkg.CustomPanel
    properties
        PWaveData
        Map
        HideDisabledChannels
    end
        
    properties (Access = protected)
        ControlPanel
        CNVPanel
        ComponentPanel
        BSPMapView
        
        ComponentListbox
        
        PCAComponents
        MixingMatrix
        NormalizedVariance
        CumulativeNormalizedVariance
        SelectedComponent
    end
    
    methods
        function self = PWavePCAPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.SelectedComponent = 1;
            self.HideDisabledChannels = false;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateBSPMapView();
        end
        
        function SetPWaveData(self, pWaveData, map)
            self.PWaveData = pWaveData;
            self.Map = map;
            
            self.ComputePWavePCA();
            self.ShowPWavePCA();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.5, .5, .1, .5],...
                'bordertype', 'none');
            componentPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.1, .1, .8, .8],...
                'title', 'PCA component');
            self.ComponentListbox = uicontrol('parent', componentPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 1],...
                'style', 'listbox',...
                'max', 10, 'min', 0,...
                'callback', @self.SetComponent);
            
            self.CNVPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.6, .5, .4, .5],...
                'title', 'Variance described by PCA components');
            
            self.ComponentPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.5, 0, .5, .5],...
                'title', 'PCA component');
        end
        
        function CreateBSPMapView(self)
            self.BSPMapView = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, 0, .5, 1],...
                'backgroundColor', [1,1,1],...
                'title', 'Component distribution map (normalized)');
        end
        
        function SetBSPMapView(self, pcaData)
            delete(get(self.BSPMapView, 'Children'));
            mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            dataLabels = self.PWaveData.ElectrodeLabels;
            
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            data = NaN(size(dataLabels));
            data(validChannelIndices) = pcaData;
            
            mapAxes = axes(...
                'parent', self.BSPMapView,...
                'position', [0 0 0.99 1],...
                'xLim', [0, size(mapLabels, 2) + 1],...
                'yLim', [0 size(mapLabels, 1) + 1]);
            axis(mapAxes, 'image');
            
            % draw circles and electrode label text
            resolution = 256;
            customColorMap = flipud(hot(resolution + 50));
            customColorMap = customColorMap(1:(end-50), :);
            colormap(mapAxes, customColorMap);
            
            mapChannelIndex = NaN(size(mapLabels));
            mapData = NaN(size(mapLabels));
            circlePatches = NaN(size(mapLabels));
            N = 100;
            t = 2*pi/N*(1:N);
            radius = 0.75;
            hold(mapAxes, 'on');
            for i = 1:size(mapLabels, 1)
                for j = 1:size(mapLabels, 2)
                    channelPosition = find(strcmpi(mapLabels{i, j}, dataLabels), 1, 'first');
                    if ~isempty(channelPosition)
                        mapChannelIndex(i, j) = channelPosition;
                        if validChannels(channelPosition)
                            mapData(i, j) = data(channelPosition);
                            color = data(channelPosition);
                        elseif self.HideDisabledChannels
                            continue;
                        else
                            color = [.7,.7,.7];
                        end
                        circlePatches(i, j) = fill(j + radius * cos(t), i + radius * sin(t), color);
                        text(j, i, mapLabels{i, j}, 'horizontalAlignment', 'center', 'hitTest', 'off');
                    end
                end
            end
            hold(mapAxes, 'off');
            set(mapAxes, 'yDir', 'reverse');
            
            colorbar('Location', 'SouthOutside');
            
            axis(mapAxes, 'off');
            
            contextMenu = uicontextmenu;
            uimenu(contextMenu, 'Label', 'Copy map to clipboard', 'callback', {@PWaveAnalysisPanel.CopyMapToClipboard, mapData});
            validCircles = ~isnan(circlePatches);
            set(circlePatches(validCircles), 'UIContextMenu', contextMenu);
        end
    end
    
    methods(Access = private)
        function ComputePWavePCA(self)
            if isempty(self.PWaveData), return; end
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            averagePWaves = self.PWaveData.AveragePWaves(validTime, :);
            
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(self.PWaveData.ElectrodeLabels));
            validChannels(validChannelIndices) = true;
            
            data = averagePWaves(:, validChannels);
            data = data';
            data = bsxfun(@minus, data, mean(data));
            [U, S, V] = svd(data, 'econ');
            
            self.PCAComponents = V;
            self.MixingMatrix = U * S;
            singularValues = diag(S, 0);
            self.CumulativeNormalizedVariance = cumsum(singularValues(:).^2) ./ sum(singularValues(:).^2);
            self.NormalizedVariance = (singularValues(:).^2) ./ sum(singularValues(:).^2);
            
            numberOfComponents = numel(singularValues);
            set(self.ComponentListbox, 'string', cellstr(num2str((1:numberOfComponents)')));
        end
        
        function ShowPWavePCA(self)
            delete(get(self.CNVPanel, 'Children'));
            axesHandle = subplot(1,1,1, 'parent', self.CNVPanel);
            numberOfComponents = numel(self.CumulativeNormalizedVariance);
            
            line('xData', 1:numberOfComponents, 'yData', self.NormalizedVariance,...
                'lineWidth',1.5, 'lineStyle', '--', 'color', [0,0,1],...
                'lineSmoothing', 'on', 'parent', axesHandle); 
            line('xData', 1:numberOfComponents, 'yData', self.CumulativeNormalizedVariance,...
                'lineWidth',1.5, 'color', [1, 0, 0], 'lineSmoothing', 'on', 'parent', axesHandle); 
            
            xlabel(axesHandle, 'number of components');
            ylabel(axesHandle, 'Normalized variance');
            
            legend(axesHandle, {'Component variance'; 'Cumulative variance'}, 'Location', 'Best');
            
            axis(axesHandle, 'tight');
            ylim(axesHandle, [0, 1]);
            
            contextMenu = uicontextmenu;
            uimenu(contextMenu, 'Label', 'Copy data to clipboard', 'callback', @self.CopyCNVDataToClipboard);
            set(axesHandle, 'UIContextMenu', contextMenu);
            
            self.ShowPWaveComponent(self.SelectedComponent);
            
            self.ShowComponentMapDistribution();
        end
        
        function ShowPWaveComponent(self, componentIndex)
            delete(get(self.ComponentPanel, 'Children'));
            axesHandle = subplot(1,1,1, 'parent', self.ComponentPanel);
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            pWaveTime = intervalTime(validTime);
            
            line('xData', pWaveTime, 'yData', zeros(size(pWaveTime)),...
                'color', [.7, .7, .7], 'lineSmoothing', 'on', 'parent', axesHandle);
            
            hold(axesHandle, 'on');
            
            plot(pWaveTime, self.PCAComponents(:, componentIndex),...
                'parent', axesHandle);
            
            hold(axesHandle, 'off');
            
            componentLabels = strcat('Component ', cellstr(num2str(componentIndex(:))));
            legend(axesHandle, componentLabels);
            
            axis(axesHandle, 'tight');
            
            xlabel(axesHandle, 'time (s)');
            
            contextMenu = uicontextmenu;
            uimenu(contextMenu, 'Label', 'Copy data to clipboard', 'callback', @self.CopyComponentSignalsToClipboard);
            set(axesHandle, 'UIContextMenu', contextMenu);
        end
        
        function ShowComponentMapDistribution(self)
            % normalized power of every row of M
            normalizedMixingMatrix = bsxfun(@rdivide, self.MixingMatrix.^2, sum(self.MixingMatrix.^2, 2));
            
            data = sum(normalizedMixingMatrix(:, self.SelectedComponent), 2);
            
            self.SetBSPMapView(data);
        end
        
        function SetComponent(self, source, varargin)
            self.SelectedComponent = get(source, 'value');
            self.ShowPWaveComponent(self.SelectedComponent);
            self.ShowComponentMapDistribution();
        end
        
        function CopyCNVDataToClipboard(self, ~, ~)
            numberOfComponents = numel(self.CumulativeNormalizedVariance);
            
            UtilityPkg.num2clip([(1:numberOfComponents)', self.CumulativeNormalizedVariance(:)]);
        end
        
        function CopyComponentSignalsToClipboard(self, ~, ~)
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            pWaveTime = intervalTime(validTime);
            
            
            UtilityPkg.num2clip([pWaveTime(:), self.PCAComponents(:, self.SelectedComponent)]);
        end
    end
end