classdef BSPMPanel < UserInterfacePkg.CustomPanel
    properties
        InterpolationFactor
        ShowOnlyTQIntervals
        NormalizeData 
    end
    
    properties (Access = private)
        EcgData
        RWaveIndices
        QTIntervals
        TQIndices
        FramePosition
        ChannelIndex
        NormalizationRange
        
        MapFrontAxes
        MapFrontImage
        MapFrontOverlay
        MapBackAxes
        MapBackOverlay
        MapBackImage
        MapFrontChannelIndex
        MapBackChannelIndex
        
        MapFrontNormalizationData
        MapBackNormalizationData
        GlobalMinValue
        GlobalMaxValue
        
        SelectedElectrodePatch
        
        CurrentMapIndex
        
        ChannelAxes
        SignalLine
        PositionLine
        
        RWaveLines
        TQPatches
    end
    
    properties (Constant)
        AvailableMaps = BSPMPanel.LoadAvailableBSPMMaps();
    end
    
    methods
        function self = BSPMPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.FramePosition = 1;
            self.ChannelIndex = 1;
            
            self.InterpolationFactor = 2;
            
            self.ShowOnlyTQIntervals = false;
            self.NormalizeData = true;
            
            self.GlobalMinValue = NaN;
            self.GlobalMaxValue = NaN;
            
            self.CurrentMapIndex = 1;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateBSPMAxes();
            self.CreateChannelAxes();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData.Copy();
            self.NormalizationRange = [1, ecgData.GetNumberOfSamples];
            
            % filter between 1Hz and 20Hz
            filterRange = [1 100];
            filterOrder = 3;
            nyquistFrequency = self.EcgData.SamplingFrequency / 2;
            [b, a] = cheby2(filterOrder, 20,...
                    filterRange / nyquistFrequency);
            for channelIndex = 1:self.EcgData.GetNumberOfChannels()
                self.EcgData.Data(:, channelIndex) =...
                        filtfilt(b, a, self.EcgData.Data(:, channelIndex));
            end
            % end filter
            
            self.SetChannelData();
            
            self.InitializeSignalAxesData();
            self.SetDataForFrame();
        end
        
        function SetQTIntervals(self, QTIntervals, QTIndices)
            self.QTIntervals = QTIntervals;
            if ~isempty(self.EcgData)
                self.TQIndices = ~(ismember(1:self.EcgData.GetNumberOfSamples, QTIndices));
                self.InitializeSignalAxesData();
            end
        end
        
        function SetRWaveIndices(self, rWaveIndices)
            self.RWaveIndices = rWaveIndices;
        end
        
        function SetShowOnlyTQIntervals(self, value)
            self.ShowOnlyTQIntervals = value;
            
            if ~isempty(self.EcgData)
                self.SetChannelData();
                self.SetDataForFrame();
            end
        end
        
        function SetFramePosition(self, framePosition)
            self.FramePosition = framePosition;
            
            self.SetPositionLine();
            self.SetDataForFrame();
        end
        
        function SetBSPMMap(self, mapIndex)
            self.CurrentMapIndex = mapIndex;
            self.CreateBSPMAxes();
            
            if ~isempty(self.EcgData)
                self.SetChannelData();
                self.SetDataForFrame();
            end
        end
        
        function SetNormalizationRange(self, range)
            if isempty(self.EcgData), return; end
            
            rangePosition = floor(range * self.EcgData.SamplingFrequency) + 1;
            if rangePosition(1) < 1 || rangePosition(end) > self.EcgData.GetNumberOfSamples()
                return;
            end
            
            self.NormalizationRange = rangePosition;
            self.SetChannelData();
            self.SetDataForFrame();
        end
        
        function SetNormalizeData(self, value)
            self.NormalizeData = value;
            
            if ~isempty(self.EcgData)
                self.SetChannelData();
                self.SetDataForFrame();
            end
        end
        
        function CreateMovie(self, videoStep, videoRange, filename)
            if nargin < 4
                filename = 'bspm.avi';
            end
            waitbarHandle = waitbar(0 ,'Writing BSPM video'); 
            
            writer = VideoWriter(filename);
            writer.Quality = 100;
            writer.FrameRate = 25;
            open(writer);
            
            numberOfSamples = self.EcgData.GetNumberOfSamples();
            videoStart = floor(videoRange(1) * self.EcgData.SamplingFrequency) + 1;
            videoStop = floor(videoRange(end) * self.EcgData.SamplingFrequency) + 1;
            
            for sampleIndex = videoStart:videoStep:videoStop
                currentECGValues = self.EcgData.Data(sampleIndex, :);
                colorData1 = self.SetMapImage(self.MapFrontImage, self.MapFrontOverlay, currentECGValues, self.MapFrontChannelIndex, self.MapFrontNormalizationData);
                colorData2 = self.SetMapImage(self.MapBackImage, self.MapBackOverlay, currentECGValues, self.MapBackChannelIndex, self.MapBackNormalizationData);
                colorData = [colorData1, ones(size(colorData1, 1), 2, 3), colorData2];
%                 colorData = self.SetMapImage(self.MapFrontImage, self.MapFrontOverlay, currentECGValues, self.MapFrontChannelIndex, self.MapFrontNormalizationData);
                colorData = imresize(colorData, 10, 'nearest');
                colorData(colorData > 1) = 1;
                colorData(colorData < 0) = 0;
                colorData = flipdim(colorData, 1);
                frameData = im2frame(colorData);
                writeVideo(writer, frameData);
                
                if mod(sampleIndex - videoStart, 100) == 0
                    waitbar(sampleIndex / numberOfSamples,...
                        waitbarHandle,...
                        ['Writing BSPM video, sample ' ...
                        num2str(sampleIndex) ' of ' ...
                        num2str(numberOfSamples)]);
                end
            end
            close(waitbarHandle);
            close(writer);
        end   

        function CreateBSPMAxes(self)
            currentMap = BSPMPanel.AvailableMaps(self.CurrentMapIndex);
            
            if ishandle(self.MapFrontImage), delete(self.MapFrontImage); end
            if ishandle(self.MapFrontAxes), delete(self.MapFrontAxes); end
            self.MapFrontAxes = axes(...
                'parent', self.ControlHandle,...
                'outerPosition', [0 0 .5 .8],...
                'xLim', [0, size(currentMap.Front, 2) + 1],...
                'yLim', [0 size(currentMap.Front, 1) + 1],...
                'buttonDownFcn', @self.SelectFrontChannel);
            axis(self.MapFrontAxes, 'image');
            
            self.MapFrontImage = imagesc('parent', self.MapFrontAxes,...
                'cData', [], 'cDataMapping', 'scaled',...
                'xData', [1 size(currentMap.Front, 2)],...
                'yData', [1 size(currentMap.Front, 1)],...
                'buttonDownFcn', @self.SelectFrontChannel,...
                'hitTest', 'on',...
                'eraseMode', 'normal');
%             self.MapFrontOverlay = imagesc('parent', self.MapFrontAxes,...
%                 'cData', [], 'cDataMapping', 'scaled',...
%                 'xData', [1 size(currentMap.Front, 2)],...
%                 'yData', [1 size(currentMap.Front, 1)],...
%                 'hitTest', 'off',...
%                 'eraseMode', 'none',...
%                 'alphaDataMapping', 'none');
            
            if ishandle(self.MapBackImage), delete(self.MapBackImage); end            
            if ishandle(self.MapBackAxes), delete(self.MapBackAxes); end            
            self.MapBackAxes = axes(...
                'parent', self.ControlHandle,...
                'outerPosition', [.5 0 .5 .8],...
                'xLim', [0, size(currentMap.Back, 2) + 1],...
                'yLim', [0 size(currentMap.Back, 1) + 1],...
                'buttonDownFcn', @self.SelectBackChannel);
            axis(self.MapBackAxes, 'image');
                
            self.MapBackImage = imagesc('parent', self.MapBackAxes,...
                'cData', [], 'cDataMapping', 'scaled',...
                'xData', [1 size(currentMap.Back, 2)],...
                'yData', [1 size(currentMap.Back, 1)],...
                'hitTest', 'on',...
                'buttonDownFcn', @self.SelectBackChannel,...
                'eraseMode', 'normal');
%             self.MapBackOverlay = imagesc('parent', self.MapBackAxes,...
%                 'cData', [], 'cDataMapping', 'scaled',...
%                 'xData', [1 size(currentMap.Back, 2)],...
%                 'yData', [1 size(currentMap.Back, 1)],...
%                 'hitTest', 'off',...
%                 'eraseMode', 'none',...
%                 'alphaDataMapping', 'none');
            
            set(self.MapFrontAxes, 'LooseInset', get(self.MapFrontAxes, 'TightInset'))
            set(self.MapBackAxes, 'LooseInset', get(self.MapBackAxes, 'TightInset'))
            
            self.SelectedElectrodePatch = patch(...
                [1 1 2 2],...
                [1 2 2 1],...
                [1 1 1],...
                'edgeColor', [0 0 0],...
                'faceAlpha', 0.5',...
                'parent', self.MapFrontAxes,...
                'hitTest', 'off');
            uistack(self.SelectedElectrodePatch, 'up');
            axis(self.MapFrontAxes, 'off');
            axis(self.MapBackAxes, 'off');
        end
        
        function CreateChannelAxes(self)
            self.ChannelAxes = axes(...
                'parent', self.ControlHandle,...
                'position', [0 .8 1 .2]);
            self.SignalLine = line([0 0], [0 0], 'parent', self.ChannelAxes,...
                'lineSmoothing', 'on');
            self.PositionLine = line([0 0], [0 0], 'parent', self.ChannelAxes,...
                'color', [1 0 0], 'lineWidth', 2);
        end
        
        function InitializeSignalAxesData(self)
            time = self.EcgData.GetTimeRange();
            
            set(self.SignalLine, 'xData', time);
            
            self.ShowSelectedChannel();
        end
        
        function ShowSelectedChannel(self)
            set(self.SignalLine, 'yData', self.EcgData.Data(:, self.ChannelIndex));
            
            set(self.ChannelAxes, 'yLim',...
                [min(self.EcgData.Data(:, self.ChannelIndex)), max(self.EcgData.Data(:, self.ChannelIndex))]);
            
            if ~isempty(self.TQIndices)
                try
                self.SetSignalTQIntervals();
                catch ME
                    disp(ME);
                end
            end
            
            self.SetPositionLine();
            
            xlabel(self.ChannelAxes, self.EcgData.ElectrodeLabels{self.ChannelIndex});
        end
        
        function ShowElectrodePatch(self, position, axesHandle)
            set(self.SelectedElectrodePatch,...
                'xData', [position(1)-.5 position(1)-.5 position(1)+.5 position(1)+.5],...
                'yData', [position(2)-.5 position(2)+.5 position(2)+.5 position(2)-.5],...
                'parent', axesHandle);
            uistack(self.SelectedElectrodePatch, 'top');
        end
        
        function SetPositionLine(self)
            time = self.EcgData.GetTimeRange();
            yLimits = get(self.ChannelAxes, 'yLim');
            set(self.PositionLine, 'xData', [time(self.FramePosition), time(self.FramePosition)],...
                'yData', yLimits);
        end
        
        function SetSignalTQIntervals(self)
            validRWaveLines = ishandle(self.RWaveLines);
            if any(validRWaveLines)
                delete(self.RWaveLines(validRWaveLines));
            end
            
            validTQPatches = ishandle(self.TQPatches);
            if any(validTQPatches)
                delete(self.TQPatches(validTQPatches));
            end
            
            time = self.EcgData.GetTimeRange();
            self.RWaveLines = NaN(numel(self.RWaveIndices), 1);
            self.TQPatches = NaN(numel(self.RWaveIndices), 1);
            yLimits = get(self.ChannelAxes, 'yLim');
            
            for rIndex = 1:numel(self.RWaveIndices)
                self.RWaveLines(rIndex) =...
                    line('xData', [time(self.RWaveIndices(rIndex)) time(self.RWaveIndices(rIndex))],...
                    'yData', yLimits,...
                    'color', [1 0 0],...
                    'parent', self.ChannelAxes);
                
                try %#ok<TRYNC>
                    self.TQPatches(rIndex) =...
                        patch('xData', [time(self.QTIntervals(1, rIndex)), time(self.QTIntervals(1, rIndex)),...
                        time(self.QTIntervals(2, rIndex)), time(self.QTIntervals(2, rIndex))],...
                        'yData', [yLimits(1), yLimits(2), yLimits(2), yLimits(1)],...
                        'faceColor', [.8 .8 .8],...
                        'faceAlpha',0.5,...
                        'parent', self.ChannelAxes);
                end
            end
        end
        
        function SetDataForFrame(self)
            currentECGValues = self.EcgData.Data(self.FramePosition, :);
            self.SetMapImage(self.MapFrontImage, self.MapFrontOverlay, currentECGValues, self.MapFrontChannelIndex, self.MapFrontNormalizationData);
            self.SetMapImage(self.MapBackImage, self.MapBackOverlay, currentECGValues, self.MapBackChannelIndex, self.MapBackNormalizationData);
        end
        
        function colorData = SetMapImage(self, imageHandle, overlayHandle, ecgValues, mapChannelIndex, normalizationData)
            mapData = NaN(size(mapChannelIndex));
            mapData(~isnan(mapChannelIndex)) = ecgValues(mapChannelIndex(~isnan(mapChannelIndex)));
            
            if self.NormalizeData
                mapData = (mapData - normalizationData.minValue) ./...
                    (normalizationData.maxValue - normalizationData.minValue);
            else
                mapData = (mapData - self.GlobalMinValue) ./...
                    (self.GlobalMaxValue - self.GlobalMinValue);
            end
            
            [X, Y] = find(~isnan(mapData));
            [Xint, Yint] = meshgrid(1:(1 / self.InterpolationFactor):size(mapData, 2), 1:(1 / self.InterpolationFactor):size(mapData, 1));
            interpolant = scatteredInterpolant([Y X], mapData(~isnan(mapData)));
            interpolant.Method = 'natural';
            interpolant.ExtrapolationMethod = 'none';
            Z = flipud(interpolant(Xint, Yint));
%             Z = flipud(natural_neighbour(X, Y, mapData(~isnan(mapData)), Yint, Xint));

            X = linspace(1, size(mapData, 2), size(Z, 2));
            Y = linspace(1, size(mapData, 1), size(Z, 1));
            
            colors = jet(256);
            colorData = BSPMPanel.EliminateNanChannels(Z, colors);
            if nargout == 0
                set(imageHandle, 'xData', X, 'yData', Y, 'cData', colorData);
            end
            
%             [numberOfComponents labelMatrix] = AlgorithmPkg.GetNumberOfImageComponents_mex(Z, 0.75);
%             labelMatrix(labelMatrix == 0) = NaN;
%             labelMatrix = labelMatrix / max(labelMatrix(:));
%             colors = [1 1 1; 0 0 0];
%             colorData = BSPMPanel.EliminateNanChannels(labelMatrix, colors);
%             alphaData = ones(size(labelMatrix)) * .2;
%             alphaData(isnan(labelMatrix)) = 0;
%             set(overlayHandle, 'xData', X, 'yData', Y, 'cData', colorData, 'alphaData', alphaData);
        end
        
        function SetChannelData(self)
            if self.ShowOnlyTQIntervals
                validData = self.TQIndices;
            else
                validData = true(size(self.EcgData.Data, 1), 1);
            end
            
            validData(1:self.NormalizationRange(1)) = false;
            validData(self.NormalizationRange(end): end) = false;
            minValues = min(self.EcgData.Data(validData, :));
            maxValues = max(self.EcgData.Data(validData, :));
            
            currentMap = BSPMPanel.AvailableMaps(self.CurrentMapIndex);
            [self.MapFrontChannelIndex, self.MapFrontNormalizationData] =...
                SetMapData(self, currentMap.Front, minValues, maxValues);
            
            [self.MapBackChannelIndex, self.MapBackNormalizationData] =...
                SetMapData(self, currentMap.Back, minValues, maxValues);
            
            self.GlobalMinValue = min(...
                min(self.MapFrontNormalizationData.minValue(~isnan(self.MapFrontNormalizationData.minValue))),...
                min(self.MapBackNormalizationData.minValue(~isnan(self.MapBackNormalizationData.minValue))));
            self.GlobalMaxValue = max(...
                max(self.MapFrontNormalizationData.maxValue(~isnan(self.MapFrontNormalizationData.maxValue))),...
                max(self.MapBackNormalizationData.maxValue(~isnan(self.MapBackNormalizationData.maxValue))));
        end
        
        function [mapChannelIndex, mapNormalizationData] = SetMapData(self, mapLabels,...
                minValues, maxValues)
            
            mapChannelIndex = NaN(size(mapLabels));
            mapNormalizationData = struct(...
                'minValue', NaN(size(mapLabels)),...
                'maxValue', NaN(size(mapLabels)));
            
            for i = 1:size(mapLabels, 1)
                for j = 1:size(mapLabels, 2)
                    channelPosition = find(strcmpi(mapLabels{i, j}, self.EcgData.ElectrodeLabels), 1, 'first');
                    if ~isempty(channelPosition)
                        mapChannelIndex(i, j) = channelPosition;
                        mapNormalizationData.minValue(i, j) = minValues(channelPosition);
                        mapNormalizationData.maxValue(i, j) = maxValues(channelPosition);
                    end
                end
            end
        end
        
        function SelectFrontChannel(self, ~, varargin)
            point = get(self.MapFrontAxes, 'currentPoint');
            point = round(point(1, [1 2]));
            
            flippedMap = flipud(self.MapFrontChannelIndex);
            nonEmptyChannels = ~isnan(flippedMap);
            [channelRows, channelColumns] = find(nonEmptyChannels);
            [~, nearestPointIndex] = min(abs(point(2) - channelRows) + abs(point(1) - channelColumns));
            channelIndex = flippedMap(channelRows(nearestPointIndex), channelColumns(nearestPointIndex));
            
            self.ChannelIndex = channelIndex;
            self.ShowElectrodePatch(point, self.MapFrontAxes);
            self.ShowSelectedChannel(); 
        end
        
        function SelectBackChannel(self, ~, varargin)
            point = get(self.MapBackAxes, 'currentPoint');
            point = round(point(1, [1 2]));
            
            flippedMap = flipud(self.MapBackChannelIndex);
            nonEmptyChannels = ~isnan(flippedMap);
            [channelRows, channelColumns] = find(nonEmptyChannels);
            [~, nearestPointIndex] = min(abs(point(2) - channelRows) + abs(point(1) - channelColumns));
            channelIndex = flippedMap(channelRows(nearestPointIndex), channelColumns(nearestPointIndex));
            
            self.ChannelIndex = channelIndex;
            self.ShowElectrodePatch(point, self.MapBackAxes);
            self.ShowSelectedChannel(); 
        end
    end
    
    methods (Access = private, Static)
        function colorData = EliminateNanChannels(data, myColormap)
            data(data < 0) = 0;
            data(data > 1) = 1;
            data = ceil(data * length(myColormap));
            dataMask = isnan(data);
            RGB = ind2rgb(data, myColormap);
            redData = RGB(:, :, 1);
            greenData = RGB(:, :, 2);
            blueData = RGB(:, :, 3);
            redData(dataMask) = 1;
            greenData(dataMask) = 1;
            blueData(dataMask) = 1;
            colorData = cat(3, redData, greenData, blueData);
        end
    end
    
    methods (Static)
        function maps = LoadAvailableBSPMMaps()
            mapLocation = ['Data' filesep 'BSPMMaps'];
            listing = dir([mapLocation filesep '*.mat']);
            
            maps(numel(listing), 1) = struct('name', [], 'Front', [], 'Back', []);
            validMaps = true(numel(listing), 1);
            for listIndex = 1:numel(listing)
                localFilename = listing(listIndex).name;
                if strcmp(localFilename(1:2), '._')
                    validMaps(listIndex) = false;
                    continue;
                end
                filename = fullfile(mapLocation, listing(listIndex).name);
                
                mapData = load(filename);
                fieldNames = fieldnames(mapData);
                
                maps(listIndex).name = fieldNames{1};
                
                if isfield(mapData.(fieldNames{1}), 'Front')
                    maps(listIndex).Front = mapData.(fieldNames{1}).Front;
                else
                    validMaps(listIndex) = false;
                end
                
                if isfield(mapData.(fieldNames{1}), 'Back')
                    maps(listIndex).Back = mapData.(fieldNames{1}).Back;
                end
            end
            maps = maps(validMaps);
        end
    end
end