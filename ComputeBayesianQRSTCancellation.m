function cancelledSignal = ComputeBayesianQRSTCancellation(signal, rWaveWindowIndices, samplingFrequency)

cancelledSignal = signal;
numberOfElectrodes = size(signal, 2);
numberOfComponents = 5;
[windowLength, numberOfWindows] = size(rWaveWindowIndices);

validWindows = rWaveWindowIndices(1, :) > 0 &...
    rWaveWindowIndices(end, :) <= size(signal, 1);

% X = [x1 ... xJ]
qrstAverages = NaN(windowLength, numberOfElectrodes);
for electrodeIndex = 1:numberOfElectrodes
    electrodeSignal = signal(:, electrodeIndex);
    qrstWindows = electrodeSignal(rWaveWindowIndices(:, validWindows));
    qrstWindows = bsxfun(@minus, qrstWindows, mean(qrstWindows));
    qrstAverages(:, electrodeIndex) = mean(qrstWindows, 2);
end

% A = [1 n]
knownVectors = [ones(windowLength, 1), (1:windowLength)'];
[U, S, V] = svd((eye(windowLength) - knownVectors * pinv(knownVectors)) * qrstAverages);
% H = [K A]
H = [U(:, 1:numberOfComponents) knownVectors];

% LS estimated noise & covariance estimation
pinvH = pinv(H);
numberOfValidWindows = numel(find(validWindows));
autoCorrelation = zeros(2 * windowLength - 1, numberOfElectrodes);
for electrodeIndex = 1:numberOfElectrodes
    for windowIndex = 1:numberOfWindows
        if validWindows(windowIndex)
            currentWindowIndices = rWaveWindowIndices(:, windowIndex);
            lsEstimate = signal(currentWindowIndices, electrodeIndex) - H * pinvH * signal(currentWindowIndices, electrodeIndex);
            autoCorrelation(:, electrodeIndex) = autoCorrelation(:, electrodeIndex) +...
                xcorr(lsEstimate, 'biased') ./ numberOfValidWindows;
        end
    end
end

% BLUE estimate
for electrodeIndex = 1:numberOfElectrodes
    correlationEstimate = autoCorrelation(:, electrodeIndex);
    Cb = toeplitz(correlationEstimate(windowLength:end) ./ windowLength);
    estimationMatrix = H * ((H' * (Cb \ H)) \ (H' / Cb));
    for windowIndex = 1:numberOfWindows
        currentWindowIndices = rWaveWindowIndices(:, windowIndex);
        validPositions = currentWindowIndices > 0 & currentWindowIndices <= numel(signal(:, electrodeIndex));
        %                     cancelledSignal(currentWindowIndices(validPositions), electrodeIndex) = signal(currentWindowIndices(validPositions), electrodeIndex) -...
        %                         estimationMatrix(validPositions, validPositions) * signal(currentWindowIndices(validPositions), electrodeIndex);
        cancelledSignal(:, electrodeIndex) = SubtractQRST(cancelledSignal(:, electrodeIndex),...
            estimationMatrix(validPositions, validPositions) * signal(currentWindowIndices(validPositions), electrodeIndex),...
            currentWindowIndices(validPositions), samplingFrequency);
    end
end
end

function subtractedSignal = SubtractQRST(signal, qrstTemplate, windowIndices, samplingFrequency)
templateShift = 0.01;
delta = ceil(templateShift * samplingFrequency);
r = xcorr(signal(windowIndices), qrstTemplate, delta, 'coeff');
[~, position] = max(abs(r));

shift = position - delta - 1;

windowIndices = windowIndices + shift;
validIndices = windowIndices > 0 & windowIndices <= numel(signal);

currentWindowIndices = windowIndices(validIndices);
currentQrstTemplate = qrstTemplate(validIndices);

covariance = cov(currentQrstTemplate, signal(currentWindowIndices));
amplification = covariance(2) / covariance(1);

%             baseline = - amplification * mean(currentQrstTemplate);
baseline = 0;

qrstTemplateLength = numel(currentQrstTemplate);
qrstRamp = round(qrstTemplateLength / 10);
qrstWindow = ones(size(currentQrstTemplate));

qrstWindow(1:qrstRamp) = 0:(1 / (qrstRamp - 1)):1;
qrstWindow((end - qrstRamp + 1):end) = 1:(-1 / (qrstRamp - 1)):0;

qrstContribution = qrstWindow .* (baseline + amplification * currentQrstTemplate);

subtractedSignal = signal;
subtractedSignal(currentWindowIndices) = signal(currentWindowIndices) - qrstContribution;
end