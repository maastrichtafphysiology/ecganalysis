classdef ECGPreProcessingPanel < PreProcessingPanel

    properties (Access = public)
        LeadSpecificNotchCheckbox
        MedianFilterCheckbox
        NeedsNotchFilter
    end

    methods (Access = public)

        function self = ECGPreProcessingPanel(position)
            self = self@PreProcessingPanel(position);
        end

        function Create(self, parentHandle)
            Create@PreProcessingPanel(self, parentHandle);

            % Change Notch panel
            set(self.NotchCheckbox, 'position', [.01 .6 .3 .05]);
            set(self.CombCheckbox, 'position', [.31 .6 .3 .05]);

            self.LeadSpecificNotchCheckbox = uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.62 .6 .3 .05],...
                'style', 'checkbox',...
                'string', 'Lead Specific Comb',...
                'value', self.EcgPreProcessor.ApplyCombFilterPerLead,...
                'callback', @self.EnableLeadSpecificCombFilter);

            % Add median filter checkbox
            self.MedianFilterCheckbox = uicontrol('parent', self.FilterControlPanel,...
                'units', 'normalized',...
                'position', [.01 .45 .8 .05],...
                'style', 'checkbox',...
                'string', 'Median baseline filter',...
                'value', self.EcgPreProcessor.ApplyMedianBaselineFilter);

        end
    end

    methods (Access = protected)

        function PreProcess(self, varargin)
            if self.EcgPreProcessor.ApplyCombFilterPerLead
                self.CheckPowerlineNoise();
                self.EcgPreProcessor.NotchLeads = self.NeedsNotchFilter;
                self.EcgPreProcessor.NotchFrequency = [self.BuzzFrequency self.BuzzFrequency*2 self.BuzzFrequency*3];
                set(self.NotchEdit,'string', num2str(self.EcgPreProcessor.NotchFrequency))
            end

            self.EcgPreProcessor.ApplyMedianBaselineFilter = self.MedianFilterCheckbox.Value;

            self.ProcessedData = self.EcgPreProcessor.Filter(self.EcgData);
            [self.ProcessedPeriodogramData, self.ProcessedLeadPeriodogramData] =...
                self.EcgPreProcessor.EstimatePSD(self.ProcessedData);
            self.ShowECGData();
        end

        function [medianBuzzFrequency, PeriodogramData] = CheckPowerlineNoise(self)
            [medianBuzzFrequency, PeriodogramData] = CheckPowerlineNoise@PreProcessingPanel(self);

            numberOfChannels = self.EcgData.GetNumberOfChannels();

            parfor channelIndex = 1:numberOfChannels
                powerlineBand = find(PeriodogramData(channelIndex).Frequencies>45 & PeriodogramData(channelIndex).Frequencies<55);
                buzzIndex = PeriodogramData(channelIndex).Frequencies(powerlineBand) == medianBuzzFrequency;
                needsNotchFilter(channelIndex,1) =  ...
                    ttest(10*log10(PeriodogramData(channelIndex).Data(powerlineBand)),...
                    10*log10(PeriodogramData(channelIndex).Data(powerlineBand(buzzIndex))),...
                    'Tail','left',...
                    'Alpha',0.01);
            end
            self.NeedsNotchFilter = needsNotchFilter;
        end

        function EnableNotchFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.EcgPreProcessor.ApplyNotchFilter = enable;
            if enable && self.EcgPreProcessor.ApplyCombFilter || enable && self.EcgPreProcessor.ApplyCombFilterPerLead
                self.EcgPreProcessor.ApplyCombFilter = false;
                self.EcgPreProcessor.ApplyCombFilterPerLead  = false;
                set(self.LeadSpecificNotch, 'value', false);
                set(self.CombCheckbox, 'value', false);
            end
        end
        
        function EnableCombFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.EcgPreProcessor.ApplyCombFilter = enable;
            if enable && self.EcgPreProcessor.ApplyNotchFilter || enable && self.EcgPreProcessor.ApplyCombFilterPerLead
                self.EcgPreProcessor.ApplyNotchFilter = false;
                self.EcgPreProcessor.ApplyCombFilterPerLead  = false;
                set(self.LeadSpecificNotch, 'value', false);
                set(self.NotchCheckbox, 'value', false);
            end
        end

        function EnableLeadSpecificCombFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.EcgPreProcessor.ApplyCombFilterPerLead = enable;
            if enable && self.EcgPreProcessor.ApplyNotchFilter || enable && self.EcgPreProcessor.ApplyCombFilter
                self.EcgPreProcessor.ApplyNotchFilter = false;
                self.EcgPreProcessor.ApplyCombFilter = false;
                set(self.NotchCheckbox, 'value', false);
                set(self.CombCheckbox, 'value', false);
            end
        end

    end
    
end