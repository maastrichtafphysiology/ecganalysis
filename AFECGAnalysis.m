classdef AFECGAnalysis < UserInterfacePkg.CustomFigure
    properties (Access = private)
        EcgData
        
        TabGroup
        QrstCancellationTab
        
        ChannelSelectionView
        PreProcessingView
        QRSTCancellationView
        DFView
        FWaveView
        %         SEView
        %         PCAView
        ParameterOverviewTab
        OverView
        
        BSPMFigure
        BSPMView
        
        ApplicationSettings
        ParallelProcessing
        
        ZoomToggleButton
        PanToggleButton
        
        AnalysisStatusBar
        
        MappedFileTypeIndices
    end
    
    properties (Constant)
        FILETYPEFILTER = {...
            '*.xml', 'MUMC GE XML data (*.xml)';...
            '*.xml', 'Muenster GE XML data (*.xml)';...
            '*.xml', 'Birmingham GE XML data (*.xml)';...
            '*.xml', 'CardioSoft XML data (*.xml)';...
            '*.csv', 'Physionet CSV data (*.csv)';...
            '*.bdf', 'BioSemi TE-ECG Measurement (*.bdf)';...
            '*.bdf', 'BioSemi BSPM Measurement (*.bdf)';...
            '*.txt', 'TVI Measurement (*.txt)';...
            '*.dat', 'Bordeaux BSPM (*.dat)';...
            '*.txt', 'BARD ECG (*.txt)';...
            '*.xml', 'Oxford GE XML data (*.xml)';...
            '*.txt', 'YourRhythmics data (*.txt)';...
            '*.DATA', 'YourRhythmics DATA (*.DATA)';...
            '*.xml', 'Schiller XML data (*.xml)';...
            '*.edf', 'EDF ECG (*.edf)';...
            '*.txt', 'GE Prucka (*.txt)';...
            '*.mat', 'MATLAB data with simulated ECG (*.mat)';...
            '*.mat', 'MATLAB data with ecgData (*.mat)'};
    end
    
    methods
        function self = AFECGAnalysis(position)
            if nargin < 1
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'Noninvasive AF Analysis';
            self.ParallelProcessing = false;
            self.MappedFileTypeIndices = 1:size(AFECGAnalysis.FILETYPEFILTER, 1);
            
            if nargin < 1
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', flipud(gray(256)));
            
            self.Hide();
            
            self.InitializeApplicationSettings();
            
            self.CreateFileMenu();
            
            self.CreateSettingsMenu();
            
            self.CreateToolbar();
            
            self.CreateStatusBar();
            
            self.InitializeEvents();
            
            self.InitializeApplicationSettings();
            
            self.Show();
        end
        
        function delete(self)
            %             self.RemoveTabs();
            %
            %             delete(ApplicationSettings.Instance()); %#ok<PROP>
            %
            %             if isobject(self.ApplicationSettings)
            %                 delete(self.ApplicationSettings);
            %             end
            %
            %             if isobject(self.BSPMView)
            %                 delete(self.BSPMView);
            %             end
            %
            %             if isobject(self.EcgData)
            %                 delete(self.EcgData)
            %             end
            %
            %             delete(timerfindall);
        end
    end
    
    methods (Access = private)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end
            
            switch class(self.EcgData)
                case 'BDFData'
                    self.InitializeBDFTabs();
                case 'ECGData'
                    self.InitializeECGTabs();
            end
        end
        
        function ShowEmptyECGTabs(self, varargin)
            self.EcgData = ECGData('', [], NaN);
            self.InitializeTabs();
        end
        
        function InitializeECGTabs(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
%             self.ChannelSelectionView = NonInvasiveECGVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView = ECGPlotSelectionPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            preProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(preProcessingTab);
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            self.QrstCancellationTab = uitab(self.TabGroup, 'title','QRST Cancellation');
            self.QRSTCancellationView = ECGQRSTCancellationPanel([0 0 1 1]);
            self.QRSTCancellationView.Create(self.QrstCancellationTab);
            self.QRSTCancellationView.Show();
            
            addlistener(self.QRSTCancellationView, 'QRSTCancellationCompleted',...
                @self.HandleQRSTCancellationEvent);
            
            dfTab = uitab(self.TabGroup, 'title','Dominant Frequency');
            self.DFView = DFPanel([0 0 1 1]);
            self.DFView.Create(dfTab);
            self.DFView.Show();
            addlistener(self.DFView, 'AnalysisSaved',...
                @self.HandleDFSaveEvent);
            
            fWaveTab = uitab(self.TabGroup, 'title','F-waves');
            self.FWaveView = FWavePanel([0 0 1 1]);
            self.FWaveView.Create(fWaveTab);
            self.FWaveView.Show();
            addlistener(self.FWaveView, 'AnalysisSaved',...
                @self.HandleFWASaveEvent);
            
            %             seTab = uitab(self.TabGroup, 'title','Sample entropy');
            %             self.SEView = SampleEntropyPanel([0 0 1 1]);
            %             self.SEView.Create(seTab);
            %             self.SEView.Show();
            %
            %             pcaTab = uitab(self.TabGroup, 'title','PCA Complexity');
            %             self.PCAView = PCAComplexityPanel([0 0 1 1]);
            %             self.PCAView.Create(pcaTab);
            %             self.PCAView.Show();
            
            self.ParameterOverviewTab = uitab(self.TabGroup, 'title', 'Overview');
            self.OverView = ECGComplexityOverviewPanel([0 0 1 1]);
            self.OverView.Create(self.ParameterOverviewTab);
            self.OverView.Show();
        end
        
        function InitializeBDFTabs(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
            self.ChannelSelectionView = BDFVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            preProcessingTab = uitab(self.TabGroup, 'title','Pre-processing');
            self.PreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(preProcessingTab);
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            self.QrstCancellationTab = uitab(self.TabGroup, 'title','QRST Cancellation');
            self.QRSTCancellationView = ECGQRSTCancellationPanel([0 0 1 1]);
            self.QRSTCancellationView.Create(self.QrstCancellationTab);
            self.QRSTCancellationView.Show();
            
            addlistener(self.QRSTCancellationView, 'QRSTCancellationCompleted',...
                @self.HandleQRSTCancellationEvent);
            
            dfTab = uitab(self.TabGroup, 'title','Dominant Frequency');
            self.DFView = DFPanel([0 0 1 1]);
            self.DFView.Create(dfTab);
            self.DFView.Show();
            addlistener(self.DFView, 'AnalysisSaved',...
                @self.HandleDFSaveEvent);
            
            fWaveTab = uitab(self.TabGroup, 'title','F-waves');
            self.FWaveView = FWavePanel([0 0 1 1]);
            self.FWaveView.Create(fWaveTab);
            self.FWaveView.Show();
            addlistener(self.FWaveView, 'AnalysisSaved',...
                @self.HandleFWASaveEvent);
            
            %             seTab = uitab(self.TabGroup, 'title','Sample entropy');
            %             self.SEView = SampleEntropyPanel([0 0 1 1]);
            %             self.SEView.Create(seTab);
            %             self.SEView.Show();
            %
            %             pcaTab = uitab(self.TabGroup, 'title','PCA Complexity');
            %             self.PCAView = PCAComplexityPanel([0 0 1 1]);
            %             self.PCAView.Create(pcaTab);
            %             self.PCAView.Show();
            
            if isobject(self.OverView)
                delete(self.OverView);
            end
            
            if strcmp('BSPM', self.EcgData.Type)
                self.CreateBSPMView();
            end
        end
        
        function CreateBSPMView(self)
            if (isobject(self.BSPMView) && isvalid(self.BSPMView))
                delete(self.BSPMView);
            end
            self.BSPMView = BSPMVisualizationPanel([0 0 1 1]);
            bspmPosition = [...
                self.Position(1) + self.Position(3) / 5,...
                self.Position(2),...
                3 * self.Position(3) / 5,...
                5 * self.Position(4) / 5];
            self.BSPMFigure = UserInterfacePkg.CustomFigure(bspmPosition);
            self.BSPMFigure.Create();
            self.BSPMFigure.Name = 'BSPM Visualizer';
            self.BSPMFigure.AddCustomControl(self.BSPMView);
            set(self.BSPMFigure.ControlHandle, 'CloseRequestFcn', @self.CloseBSPMView);
            self.BSPMView.Show();
        end
        
        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end
            
            if isobject(self.PreProcessingView)
                delete(self.PreProcessingView);
            end
            
            if isobject(self.QRSTCancellationView)
                delete(self.QRSTCancellationView);
            end
            
            if isobject(self.DFView)
                delete(self.DFView);
            end
            
            if isobject(self.FWaveView)
                delete(self.FWaveView);
            end
            
            %             if isobject(self.PCAView)
            %                 delete(self.PCAView);
            %             end
            %
            %             if isobject(self.SEView)
            %                 delete(self.SEView);
            %             end
            
            if isobject(self.OverView)
                delete(self.OverView);
            end
            
            if isobject(self.BSPMView) && isvalid(self.BSPMView)
                delete(self.BSPMView);
                delete(self.BSPMFigure);
            end
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Load signal',...
                'callback', @self.LoadSignal);
            uimenu('parent', fileMenu,...
                'label', 'Load result file',...
                'callback', @self.LoadResult);
        end
        
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Algorithm settings',...
                'callback', @self.ShowApplicationSettings);
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load signal',...
                'clickedCallback', @self.LoadSignal);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.newDataCData,...
                'tooltipString', 'Show empty ECG tabs',...
                'clickedCallback', @self.ShowEmptyECGTabs);
        end
        
        function CreateStatusBar(self)
            self.AnalysisStatusBar = UserInterfacePkg.StatusBar([0 0 1 .025]);
            self.AddCustomControl(self.AnalysisStatusBar);
            self.AnalysisStatusBar.AddListener(UserInterfacePkg.StatusEventClass.Instance(), 'StatusChange');
            self.AnalysisStatusBar.Show();
        end
        
        function SetFilename(self)
            if isempty(self.EcgData), return; end
            
            self.Name = ['Noninvasive AF Analysis - ', self.EcgData.Filename];
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);
            
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function LoadSignal(self, varargin)
            
            fileTypeFilters = AFECGAnalysis.FILETYPEFILTER;
            mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);
            
            [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                mappedFiletypeFilters,...
                'Select a signal file');
            if isequal(fileName, 0), return; end
            
            mappedFilterIndex = self.MappedFileTypeIndices(filterIndex);
            if filterIndex > 1
                fileTypeIndices = 1:size(AFECGAnalysis.FILETYPEFILTER, 1);
                fileTypeIndices(mappedFilterIndex) = [];
                self.MappedFileTypeIndices = [mappedFilterIndex, fileTypeIndices];
            end
            
            switch mappedFilterIndex
                case 1
                    ecgData = InputOutputPkg.ECGReader.ReadMUMCBase64XML(fullfile(pathName, fileName));
                case 2
                    ecgData = InputOutputPkg.ECGReader.ReadMuensterXML(fullfile(pathName, fileName));
                case 3
                    ecgData = InputOutputPkg.ECGReader.ReadBirminghamXML(fullfile(pathName, fileName));
                case 4
                    ecgData = InputOutputPkg.ECGReader.ReadCardioSoftFullDisclosure(fullfile(pathName, fileName));
                case 5
                    ecgData = InputOutputPkg.ECGReader.ReadPhysionetCSV(fullfile(pathName, fileName));
                case {6, 7}
                    ecgData = InputOutputPkg.ECGReader.ReadBDF(fullfile(pathName, fileName));
                    ecgData.Organize();
                    switch mappedFilterIndex
                        case 7
                            ecgData.Type = 'BSPM';
                    end
                case 8
                    ecgData = InputOutputPkg.ECGReader.ReadTVIEeg(fullfile(pathName, fileName));
                case 9
                    ecgData = InputOutputPkg.ECGReader.ReadBordeauxBSPM(fullfile(pathName, fileName));
                case 10
                    ecgData = InputOutputPkg.ECGReader.ReadBARDECG(fullfile(pathName, fileName));
                case 11
                    ecgData = InputOutputPkg.ECGReader.ReadOxfordXML(fullfile(pathName, fileName));
                case 12
                    ecgData = InputOutputPkg.ECGReader.ReadYRTXT(fullfile(pathName, fileName));
                case 13
                    ecgData = InputOutputPkg.ECGReader.ReadYourRhythmicsData(fullfile(pathName, fileName));
                case 14
                    ecgData = InputOutputPkg.ECGReader.ReadSchillerXML(fullfile(pathName, fileName));
                case 15
                    ecgData = InputOutputPkg.ECGReader.ReadEDFECG(fullfile(pathName, fileName));
                case 16
                    ecgData = InputOutputPkg.ECGReader.ReadPrucka(fullfile(pathName, fileName));
                case 17
                    ecgData = InputOutputPkg.ECGReader.ReadSimulationECGData(fullfile(pathName, fileName));
                case 18
                    fileData = load(fullfile(pathName, fileName), 'ecgData');
                    ecgData = fileData.ecgData;
                otherwise
                    errordlg('File Error', 'Unkown file type');
                    return;
            end
            
            self.EcgData = ecgData;
            self.SetFilename();
            self.InitializeTabs();
            
            self.ChannelSelectionView.SetECGData(self.EcgData);
            
            switch mappedFilterIndex
                case {1, 2, 3}
                    self.AutomaticUntilQRSTCancellation();
                otherwise
                    return;
            end
        end
        
        function AutomaticUntilQRSTCancellation(self)
            bandpassFrequencies = [0.5, 100];
            self.PreProcessingView.SetECGData(self.EcgData);
            preProcessedData = self.PreProcessingView.PreProcessData(bandpassFrequencies);
            self.QRSTCancellationView.SetECGData(preProcessedData);
            
            qrstDetectionElectrodeIndex = 7;
            qrstCancellator = ECGQRSTCancellator();
            qrstCancellator.OriginalPanTompkins = false;
            qrstCancellator.VentricularRefractoryPeriod = 0.3;
            qrstCancellator.IntegrationWindowLength = 0.2;
            qrstCancellator.SelectProximalPeak = true;
            qrstCancellator.SelectedPeakSign = 0;
            qrstCancellator.ShowDiagnostics = false;
            self.QRSTCancellationView.DetectRWavesOnLead(qrstDetectionElectrodeIndex,...
                qrstCancellator, 1:preProcessedData.GetNumberOfChannels());
            
            set(self.TabGroup, 'selectedTab', self.QrstCancellationTab);
        end
        
        function LoadResult(self, varargin)
            [fileName, pathName] = HelperFunctions.customUigetfile(...
                '*_Result.mat', 'Analysis result file (*_Result.mat)',...
                'Select a signal file');
            if isequal(fileName, 0), return; end
            
            resultData = load(fullfile(pathName, fileName));
            resultData.ecgData.Filename = fullfile(pathName, fileName(1:(end - 11)));
            
            self.ShowEmptyECGTabs();
            
            fullFilename = fullfile(pathName, fileName);
            
            if isobject(self.OverView)
                self.OverView.SetECGData(resultData.ecgData);
                if isfield(resultData, 'ChannelsToAnalyze')
                    self.OverView.SetChannelsToAnalyze(resultData.ChannelsToAnalyze);
                end
            end
            
            if isfield(resultData, 'DF')
                self.DFView.LoadAnalysis(fullFilename);
                self.HandleDFSaveEvent();
            else
                self.DFView.SetECGData(resultData.ecgData, resultData.rWaveIndices);
            end
            
            if isfield(resultData, 'FWave')
                self.FWaveView.LoadAnalysis(fullFilename);
                self.HandleFWASaveEvent();
            else
                self.FWaveView.SetECGData(resultData.ecgData, resultData.rWaveIndices);
            end
            
            self.TabGroup.SelectedTab = self.ParameterOverviewTab;
        end
        
        function InitializeApplicationSettings(self)
            self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
            self.ApplicationSettings.Create();
            self.ApplicationSettings.Hide();
        end
        
        function ShowApplicationSettings(self, varargin)
            if ~isobject(self.ApplicationSettings)
                self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
                self.ApplicationSettings.Create();
            end
            self.ApplicationSettings.Show();
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('nocreate'));
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    if isempty(gcp('nocreate'))
                        parpool;
                    end
                end
            end
        end
        
        function PanOn(self, varargin)
            set(self.ZoomToggleButton, 'state', 'off');
            pan on;
        end
        
        function ZoomOn(self, varargin)
            set(self.PanToggleButton, 'state', 'off');
            zoom on;
        end
        
        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');
            
            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();
            
            if isobject(self.BSPMView)
                self.BSPMView.SetECGData(ecgData);
            end
            
            self.PreProcessingView.SetECGData(ecgData);
        end
        
        function HandlePreProcessingEvent(self, varargin)
            disp('Pre-processing completed');
            
            ecgData = self.PreProcessingView.GetPreProcessedECGData();
            
            if isobject(self.BSPMView)
                self.BSPMView.SetECGData(ecgData);
            end
            
            self.QRSTCancellationView.SetECGData(ecgData);
        end
        
        function HandleQRSTCancellationEvent(self, varargin)
            disp('QRST cancellation completed');
            
            rWaveIndices = self.QRSTCancellationView.RWaveIndices;
            ecgData = self.QRSTCancellationView.GetCancelledECGData();
            
            if isobject(self.DFView)
                self.DFView.SetECGData(ecgData, rWaveIndices);
            end
            
            if isobject(self.FWaveView)
                self.FWaveView.SetECGData(ecgData, rWaveIndices);
            end
            
            %             if isobject(self.SEView)
            %                 self.SEView.SetECGData(ecgData, rWaveIndices);
            %             end
            %
            %             if isobject(self.PCAView)
            %                 self.PCAView.SetECGData(ecgData, rWaveIndices);
            %             end
            
            if isobject(self.OverView)
                self.OverView.SetECGData(ecgData);
            end
            
            if strcmp('BSPM', ecgData.Type)
                if ~(isobject(self.BSPMView) && isvalid(self.BSPMView))
                    self.CreateBSPMView();
                end
                self.BSPMView.SetECGData(ecgData);
                self.BSPMView.SetRWaveIndices(rWaveIndices);
            end
        end
        
        function HandleDFSaveEvent(self, varargin)
            disp('DF analysis saved');
            
            dfData = self.DFView.GetAnalysisData();
            if isobject(self.OverView)
                self.OverView.SetDFData(dfData);
            end
        end
        
        function HandleFWASaveEvent(self, varargin)
            disp('FWA analysis saved');
            
            fwaData = self.FWaveView.GetAnalysisData();
            if isobject(self.OverView)
                self.OverView.SetFWAData(fwaData);
            end
        end
        
        function CloseBSPMView(self, varargin)
            if isobject(self.BSPMView) && isvalid(self.BSPMView)
                delete(self.BSPMView);
                self.BSPMView = [];
                delete(self.BSPMFigure);
            end
        end
        
        function Close(self, varargin)
            self.RemoveTabs();
            
            if isobject(self.EcgData)
                delete(self.EcgData)
            end
            
            delete(timerfindall);
            
            delete(self.AnalysisStatusBar);
            
            delete(self.ControlHandle);
        end
    end
end