classdef ECGQRSTCancellationPanel < QRSTCancellationPanel
    methods
        function self = ECGQRSTCancellationPanel(position)
            self = self@QRSTCancellationPanel(position);
            self.QrstCancellator = ECGQRSTCancellator();
        end
        
        function SetECGData(self, ecgData)
%             if ~any(strcmp('RMS signal', ecgData.ReferenceLabels))
%                 ecgData.ReferenceLabels = [ecgData.ReferenceLabels, 'RMS signal'];
%                 ecgData.ReferenceData = sqrt(median(ecgData.Data.^2, 2));
%             end
            
            SetECGData@QRSTCancellationPanel(self, ecgData);
        end
        
        function DetectRWavesOnLead(self, referenceLead, qrstCancellator, visibleLeads)
            if isempty(self.EcgData)
                return;
            end
            
            if nargin < 3
                visibleLeads = 1:(min(3, self.EcgData.GetNumberOfChannels));
            end
            
            if nargin > 1
                self.QrstCancellator = qrstCancellator;
                self.CreateQRSTSettings();
                
                set(self.CancellationMethodPopup, 'string', ECGQRSTCancellator.CANCELLATION_METHODS);
                set(self.CancellationMethodPopup, 'value', 2);
                set(self.QRSTCancellationButton, 'enable', 'off');
            end
            
            self.SelectedChannels = visibleLeads;
            self.ReferenceChannel = referenceLead;
            set(self.ChannelListbox, 'value', self.ReferenceChannel);
            
            self.ShowECGData();
            self.ComputeRDetection();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@QRSTCancellationPanel(self);
            
            set(self.CancellationMethodPopup, 'string', ECGQRSTCancellator.CANCELLATION_METHODS);
            set(self.CancellationMethodPopup, 'value', 2);
            set(self.QRSTCancellationButton, 'enable', 'off');
            self.CreateQRSReductionSettings();
        end
        
        function CreateQRSReductionSettings(self, varargin)
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .3 .4 .05],...
                'style', 'checkbox',...
                'string', 'Reduce QRS',...
                'value', self.QrstCancellator.QRSResidueReduction,...
                'callback', @self.SetQRSResidueReduction);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .25 .2 .05],...
                'style', 'text',...                
                'string', '< R (ms)');
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .2 .2 .05],...
                'style', 'edit',...                
                'string', num2str(1000 * self.QrstCancellator.QRSRange(1)),...
                'callback', @self.SetQRSRangeBefore);
            
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .25 .2 .05],...
                'style', 'text',...                
                'string', '> R (ms)');
            uicontrol('parent', self.QRSTSettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .2 .2 .05],...
                'style', 'edit',...
                'string', num2str(1000 * self.QrstCancellator.QRSRange(2)),...
                'callback', @self.SetQRSRangeAfter);
        end
        
        function SetCancellationMethod(self, varargin)
            methodIndex = get(self.CancellationMethodPopup, 'value');
            self.QrstCancellator.CancellationMethod = ECGQRSTCancellator.CANCELLATION_METHODS{methodIndex};
            
            set(self.QRSTCancellationButton, 'enable', 'on');
            switch self.QrstCancellator.CancellationMethod
                case ECGQRSTCancellator.CANCELLATION_METHODS{2}
                    set(self.ClusterSensitivityPanel, 'visible', 'on');
                    set(self.QRSTCancellationButton, 'enable', 'off');
                otherwise
                    set(self.ClusterSensitivityPanel, 'visible', 'off');
            end
        end
    end
end