classdef WindowedAnalysisPlotPanel < ECGPlotChannelPanel
    properties (Access = protected)
        ResultPanel
        ResultTemporalPanel
        ResultOverallPanel
        SettingsPanel
        WindowLengthEdit
        WindowShiftEdit
        
        AFComplexityCalculator
        RWaveIndices
        WindowLength
        WindowShift
        SelectedWindowIndex
        SelectedChannelIndex
    end
    
    events
        AnalysisSaved
    end
    
    methods
        function self = WindowedAnalysisPlotPanel(position)
            self = self@ECGPlotChannelPanel(position);
            self.AFComplexityCalculator = AlgorithmPkg.AFComplexityCalculator();
            
            self.WindowLength = 5;
            self.WindowShift = 1;
        end
        
        function SetECGData(self, ecgData, rWaveIndices)
            if nargin > 2
                self.RWaveIndices = rWaveIndices;
            end
            
            SetECGData@ECGPlotChannelPanel(self, ecgData);
        end
    end
    
    methods (Abstract)
        LoadAnalysis(self, filename)
        GetAnalysisData(self)
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            CreateControlPanel@ECGPlotChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .7 1 .29]);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.05 .625 .4 .05],...
                'style', 'pushbutton',...
                'string', 'Compute',...
                'callback', @self.ComputeAnalysis);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.55 .625 .4 .05],...
                'style', 'pushbutton',...
                'string', 'Save',...
                'callback', @self.SaveAnalysis);            
            
            % Windowed results
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.05 .605 .4 .025],...
                'style', 'text',...
                'string', 'Window length (s)');
            self.WindowLengthEdit = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.05 .555 .4 .05],...
                'style', 'edit',...
                'string', num2str(self.WindowLength),...
                'callback', @self.SetWindowLength);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.55 .605 .4 .025],...
                'style', 'text',...
                'string', 'Window shift (s)');
            self.WindowShiftEdit = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.55 .555 .4 .05],...
                'style', 'edit',...
                'string', num2str(self.WindowShift),...
                'callback', @self.SetWindowShift);
            
            self.SettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .55],...
                'title', 'Settings');
            
            self.CreateAnalysisSettings();
        end
        
        function CreateECGAxes(self)
            CreateECGAxes@ECGPlotChannelPanel(self);
            
            set(self.AxesPanel, 'position', [.2 .5 .4 .5],...
                'backgroundColor', [1,1,1],...
                'bordertype', 'none');
            
            set(self.ScrollSlider, 'backgroundColor', [.7,.7,.7]);
            set(self.ZoomSlider, 'backgroundColor', [.7,.7,.7]);
            
            self.CreateResultPanel();
        end
        
        function CreateResultPanel(self)
            self.ResultPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .4 .5],'backgroundColor', [1,1,1],...
                'bordertype', 'none');
            self.ResultTemporalPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.6 .0 .4 .5],'backgroundColor', [1,1,1],...
                'bordertype', 'none');
            self.ResultOverallPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.6 .5 .4 .5],'backgroundColor', [1,1,1],...
                'bordertype', 'none');
        end
        
        function InitializeSettings(self)
            set(self.WindowLengthEdit, 'string', num2str(self.WindowLength));
            set(self.WindowShiftEdit, 'string', num2str(self.WindowShift));
        end
        
        function SetWindowLength(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 0
                self.WindowLength = value;
            end
            set(source, 'string', num2str(self.WindowLength));
        end
        
        function SetWindowShift(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 0
                self.WindowShift = value;
            end
            set(source, 'string', num2str(self.WindowShift));
        end
        
        function PlotWindowedData(self, plotHandle, windowedResult)
            time = self.EcgData.GetTimeRange();
            
            line('xData', time(windowedResult.windowCenter),'yData',windowedResult.expandingWindowMean,...
                'color', [1 0 0], 'lineSmoothing', 'on', 'hitTest', 'off',...
                'lineStyle', '--', 'parent', plotHandle);
            line('xData', time(windowedResult.windowCenter),...
                'yData',windowedResult.expandingWindowMean + windowedResult.expandingWindowStd,...
                'color', [1 0 0], 'lineSmoothing', 'on','hitTest', 'off',...
                'lineStyle', ':', 'parent', plotHandle);
            line('xData', time(windowedResult.windowCenter),...
                'yData',windowedResult.expandingWindowMean - windowedResult.expandingWindowStd,...
                'color', [1 0 0], 'lineSmoothing', 'on','hitTest', 'off',...
                'lineStyle', ':', 'parent', plotHandle);
            
            line('xData', time(windowedResult.windowCenter),'yData',windowedResult.runningWindowMean,...
                'color', [0 0 1], 'marker', '.', 'markerEdgeColor', [0,0,0], 'markerFaceColor', [0,0,1],...
                'lineSmoothing', 'on', 'hitTest', 'off', 'parent', plotHandle);
            line('xData', time(windowedResult.windowCenter),...
                'yData',windowedResult.runningWindowMean + windowedResult.runningWindowStd,...
                'color', [0 0 1], 'lineSmoothing', 'on',...
                'lineStyle', ':', 'hitTest', 'off', 'parent', plotHandle);
            line('xData', time(windowedResult.windowCenter),...
                'yData',windowedResult.runningWindowMean - windowedResult.runningWindowStd,...
                'color', [0 0 1], 'lineSmoothing', 'on',...
                'lineStyle', ':', 'hitTest', 'off', 'parent', plotHandle);
            
%             xlabel(plotHandle, 'window start time (seconds)');
        end
    end
    
    methods (Access = protected, Abstract)
        CreateAnalysisSettings(self)
        ShowAnalysis(self, varargin)
        ComputeAnalysis(self, varargin)
        SaveAnalysis(self, varargin)
    end
    
    methods (Static)
        function arrayString = CopyToClipboard(array)
            arrayString = num2str(array);
            arrayString(:,end+1) = char(10); 
            arrayString = reshape(arrayString',1, numel(arrayString));
            
            arraystringshift = [' ', arrayString];
            arrayString = [arrayString, ' '];
            
            arrayString = arrayString((double(arrayString)~=32 | double(arraystringshift)~=32) & ~(double(arraystringshift==10) & double(arrayString)==32) );
            
            arrayString(double(arrayString)==32) = char(9);
            
            clipboard('copy', arrayString);
        end
    end
end