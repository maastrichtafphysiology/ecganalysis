classdef NonInvasiveECGVisualizationPanel < ECGVisualizationPanel
    properties (Access = protected)
        ChannelSNR
    end
    
    methods
        function self = NonInvasiveECGVisualizationPanel(position)
            self = self@ECGVisualizationPanel(position);
        end
        
        function SetECGData(self, ecgData)
%             self.ChannelSNR = self.ComputeChannelSNR(ecgData);
            SetECGData@ECGVisualizationPanel(self, ecgData);
        end
    end
    
    methods (Access = protected)
        function ShowECGData(self)
            ShowECGData@ECGVisualizationPanel(self);
            
%             SNRValues = self.ChannelSNR(self.SelectedChannels);
%             for channelIndex = 1:numel(SNRValues)
%                 text('parent', self.AxesHandles(channelIndex), 'string', ['SNR: ', num2str(SNRValues(channelIndex), '%.0f'), 'dB'],...
%                     'units', 'normalized', 'position', [.99, .99, 0],...
%                     'horizontalAlignment', 'right',...
%                     'verticalAlignment', 'top',...
%                     'backgroundColor',[.7 .7 .7],...
%                     'edgeColor', [0 0 0]);
%             end
        end
        
        function snr = ComputeChannelSNR(self, ecgData)
            noiseFrequencyCutoff = 20;
            nyquistFrequency = ecgData.SamplingFrequency / 2;
            [bLowpass, aLowpass] = cheby2(3, 20,...
                    noiseFrequencyCutoff / nyquistFrequency, 'low');
            snr = NaN(ecgData.GetNumberOfChannels, 1);
            numberOfSamples = ecgData.GetNumberOfSamples();
                
            for channelIndex = 1:ecgData.GetNumberOfChannels
                signal = ecgData.Data(:, channelIndex);
                ecgSignal = filtfilt(bLowpass, aLowpass, signal);
                noiseSignal = signal - ecgSignal;
                snr(channelIndex) = 20 * log10(sqrt(sum(signal.^2) / numberOfSamples) /...
                    sqrt(sum(noiseSignal.^2) / numberOfSamples));
            end
        end
    end
end