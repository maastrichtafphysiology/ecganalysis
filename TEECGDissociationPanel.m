classdef TEECGDissociationPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
    end
    
    properties (Access = protected)
        TemplateMatchingView
        DeflectionAnalysisView
    end
    
    methods
        function self = TEECGDissociationPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position); 
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.TemplateMatchingView = TEECGTemplateMatchingPanel([0, .7, 1, .3]);
            self.TemplateMatchingView.Create(self.ControlHandle);
            self.TemplateMatchingView.Show();
            
            addlistener(self.TemplateMatchingView, 'TemplateMatchingCompleted',...
                @self.HandleTemplateMatchingEvent);
            
            self.DeflectionAnalysisView = TEECGDeflectionAnalysisPanel([0, 0, 1, .7]);
            self.DeflectionAnalysisView.Create(self.ControlHandle);
            self.DeflectionAnalysisView.Show();
            
            addlistener(self.DeflectionAnalysisView, 'IntrinsicDeflectionsDetermined',...
                @self.HandleIntrinsicDeflectionEvent);
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            self.TemplateMatchingView.SetECGData(ecgData);
        end
        
        function delete(self)
            self.EcgData = [];
        end
    end
    
    methods (Access = protected)
        function HandleTemplateMatchingEvent(self, varargin)
            disp('TE-ECG template matching completed');
            
            self.DeflectionAnalysisView.SetECGData(self.TemplateMatchingView.EcgData);
            templateMatchingResult = self.TemplateMatchingView.GetTemplateMatchingResult();
            self.DeflectionAnalysisView.SetTemplateMatchingResult(templateMatchingResult);
        end
        
        function HandleIntrinsicDeflectionEvent(self, varargin)
            disp('TE-ECG deflections determined');
            
            intrinsicDeflectionData = self.DeflectionAnalysisView.GetIntrinsicDeflections();
        end
    end
end