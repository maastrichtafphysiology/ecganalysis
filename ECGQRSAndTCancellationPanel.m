classdef ECGQRSAndTCancellationPanel < ECGQRSTCancellationPanel
    properties
        TQIndices
        TPeakLines
        TPeakMarkers
        QPeakLines
        QPeakMarkers
        
        TRangePanel
        TWaveClusterSensitivityPanel
    end
    
    methods
        function self = ECGQRSAndTCancellationPanel(position)
            self = self@ECGQRSTCancellationPanel(position);
            self.QrstCancellator = ECGQRSAndTCancellator();
        end
        
        function ecgData = LoadData(self, filename)
            ecgData = LoadData@ECGQRSTCancellationPanel(self, filename);
            qrstData = load(filename);
            self.TQIndices = qrstData.TQIndices;
        end
    end
    
    methods (Access = protected)
        function CreateQRSTSettings(self, varargin)
            CreateQRSTSettings@ECGQRSTCancellationPanel(self, varargin{:});
            
            set(self.QRSTRangePanel, 'position', [0 .5 1 .1]);
            
            self.TRangePanel = uipanel('parent', self.QRSTSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .4 1 .1],...
                'borderType', 'none');
            
            uicontrol('parent', self.TRangePanel,...
                'units', 'normalized',...
                'position', [.1, 2/3, .2, 1/3],...
                'style', 'text',...                
                'string', '< T(end) (ms)');
            uicontrol('parent', self.TRangePanel,...
                'units', 'normalized',...
                'position', [.1, 0, .2, 2/3],...
                'style', 'edit',...                
                'string', num2str(1000 * self.QrstCancellator.TRange(1)),...
                'callback', @self.SetTRangeBefore);
            
            uicontrol('parent', self.TRangePanel,...
                'units', 'normalized',...
                'position', [.3, 2/3, .2, 1/3],...
                'style', 'text',...                
                'string', '> T(end) (ms)');
            uicontrol('parent', self.TRangePanel,...
                'units', 'normalized',...
                'position', [.3, 0, .2, 2/3],...
                'style', 'edit',...
                'string', num2str(1000 * self.QrstCancellator.TRange(2)),...
                'callback', @self.SetTRangeAfter);
            
            self.TWaveClusterSensitivityPanel = uipanel('parent', self.TRangePanel,...
                'units', 'normalized', ...
                'position', [.5, 0, .3, 1],...
                'borderType', 'none');
            uicontrol('parent', self.TWaveClusterSensitivityPanel,...
                'units', 'normalized',...
                'position', [0, 2/3, 1, 1/3],...
                'style', 'text',...                
                'string', 'Sensitivity (0-1)',...
                'tooltipString', 'Cluster similarity');
            uicontrol('parent', self.TWaveClusterSensitivityPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, 2/3],...
                'style', 'edit',...
                'string', num2str(self.QrstCancellator.TWaveClusterSensitivity),...
                'callback', @self.SetTWaveClusterSensitivity);
        end
        
        function ComputeQRSTCancellation(self, varargin)
            if isempty(self.TQIndices)
                ComputeQRSTCancellation@ECGQRSTCancellationPanel(self, varargin{:});
                return;
            end
            
            referenceChannelIndex = self.ReferenceChannel;
            noReferenceData = false;
            if isempty(self.EcgData.ReferenceData)
                noReferenceData = true;
                self.EcgData.ReferenceData = self.EcgData.Data(:, self.ReferenceChannel);
                referenceChannelIndex = 1;
            end
            self.QrstCancellator.ReferenceChannelIndex = referenceChannelIndex;
            
            self.CancelledEcgData = self.QrstCancellator.CancelQRST(self.EcgData, self.RWaveIndices, self.TQIndices);
            
            if noReferenceData
                self.EcgData.ReferenceData = [];
            end
            
            self.ShowQRSTCancellation();
        end
        
        function ShowECGData(self)
            ShowECGData@ECGQRSTCancellationPanel(self);
            
            self.ShowQTIntervals();
        end
        
        function ShowQTIntervals(self)
            if isempty(self.TQIndices), return; end
            
            validHandles = ishandle(self.TPeakLines);
            if any(validHandles)
                delete(self.TPeakLines(validHandles));
            end
            
            validHandles = ishandle(self.TPeakMarkers);
            if any(validHandles)
                delete(self.TPeakMarkers(validHandles));
            end
            
            validHandles = ishandle(self.QPeakLines);
            if any(validHandles)
                delete(self.QPeakLines(validHandles));
            end
            
            validHandles = ishandle(self.QPeakMarkers);
            if any(validHandles)
                delete(self.QPeakMarkers(validHandles));
            end
            
            time = self.EcgData.GetTimeRange();
            lineData = [self.EcgData.Data(:, self.SelectedChannels),...
                self.EcgData.ReferenceData(:, self.SelectedLeadChannels)];
            
            tIndices = self.TQIndices.tIndices;
            self.TPeakLines = NaN(numel(self.AxesHandles), numel(tIndices));
            self.TPeakMarkers = NaN(numel(self.AxesHandles), 1);
            for axesIndex = 1:numel(self.AxesHandles);
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for tIndex = 1:numel(tIndices)
                    self.TPeakLines(axesIndex, tIndex) =...
                        line('xData', [time(tIndices(tIndex)) time(tIndices(tIndex))],...
                        'yData', yLimits,...
                        'color', [0 0 0],...
                        'lineStyle', ':',...
                        'parent', self.AxesHandles(axesIndex));
                end

                self.TPeakMarkers(axesIndex) =...
                        line('xData', time(tIndices),...
                        'yData', lineData(tIndices, axesIndex),...
                        'lineStyle', 'none',...
                        'marker', 'o', 'markerSize', 7,...
                        'markerEdgeColor', [0 0 0], 'markerFaceColor', [0 0 0],...
                        'parent', self.AxesHandles(axesIndex));
            end
            
            qIndices = self.TQIndices.qIndices;
            self.QPeakLines = NaN(numel(self.AxesHandles), numel(qIndices));
            self.QPeakMarkers = NaN(numel(self.AxesHandles), 1);
            for axesIndex = 1:numel(self.AxesHandles);
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for qIndex = 1:numel(qIndices)
                    self.QPeakLines(axesIndex, qIndex) =...
                        line('xData', [time(qIndices(qIndex)) time(qIndices(qIndex))],...
                        'yData', yLimits,...
                        'color', [0 0 0],...
                        'lineStyle', ':',...
                        'parent', self.AxesHandles(axesIndex));
                end

                self.QPeakMarkers(axesIndex) =...
                        line('xData', time(qIndices),...
                        'yData', lineData(qIndices, axesIndex),...
                        'lineStyle', 'none',...
                        'marker', 'o', 'markerSize', 7,...
                        'markerEdgeColor', [0 0 0], 'markerFaceColor', [0 0 0],...
                        'parent', self.AxesHandles(axesIndex));
            end
        end
    end
    
    methods (Access = private)
        function SetTRangeBefore(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.TRange(1) = value / 1000;
        end
        
        function SetTRangeAfter(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.TRange(2) = value / 1000;
        end
        
        function SetTWaveClusterSensitivity(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QrstCancellator.TWaveClusterSensitivity = value;
        end
    end
end