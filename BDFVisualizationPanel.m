% classdef BDFVisualizationPanel < NonInvasiveECGVisualizationPanel
classdef BDFVisualizationPanel < ECGPlotSelectionPanel
    
    properties
        LALabelText
        RALabelText
        LLLabelText
        
        WCTCheckbox
        BSPMMapPopUp
    end
    
    properties (Constant)
        AvailableMaps = BDFVisualizationPanel.LoadAvailableBSPMMaps();
    end
    
    methods
        function self = BDFVisualizationPanel(position)
%             self = self@NonInvasiveECGVisualizationPanel(position);
            self = self@ECGPlotSelectionPanel(position);
        end
        
        function SetECGData(self, ecgData)
            wctCorrected = ecgData.ApplyWCT();
%             wctCorrected = false;
            set(self.WCTCheckbox, 'value', wctCorrected);
            
%             SetECGData@NonInvasiveECGVisualizationPanel(self, ecgData);
            SetECGData@ECGPlotSelectionPanel(self, ecgData);
            
            set(self.LALabelText, 'string', ecgData.LAChannelLabel);
            set(self.RALabelText, 'string', ecgData.RAChannelLabel);
            set(self.LLLabelText, 'string', ecgData.LLChannelLabel);
        end
    end
    
    methods (Access = protected)
        function CreateChannelSelection(self)
%             CreateChannelSelection@NonInvasiveECGVisualizationPanel(self);
            CreateChannelSelection@ECGPlotSelectionPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .3 1 .69]);
            
            self.CreateWCTLeadSelection();
            
            self.WCTCheckbox = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .25 .4 .05],...
                'style', 'checkbox',...
                'string', 'WCT',...
                'callback', @self.SetWCT,...
                'value', false);
            
            maps = BDFVisualizationPanel.AvailableMaps;
            self.BSPMMapPopUp = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.5 .25 .4 .05],...
                'style', 'popup',...
                'string', {maps.name},...
                'callback', @self.SetBSPMMap);
        end
        
        function CreateWCTLeadSelection(self)
            set(self.LeadListbox, 'position', [0, 0, 1, .5]);
            
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [0 .8 .2 .1],...
                'style', 'text',...
                'string', 'LA:')
            self.LALabelText = uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.2 .8 .3 .1],...
                'style', 'text',...
                'string', '',...
                'fontWeight', 'bold');
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.6 .8 .3 .1],...
                'style', 'pushbutton',...
                'string', 'Set',...
                'callback', @self.SetLA);
            
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [0 .7 .2 .1],...
                'style', 'text',...
                'string', 'RA:')
            self.RALabelText = uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.2 .7 .3 .1],...
                'style', 'text',...
                'string', '',...
                'fontWeight', 'bold');
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.6 .7 .3 .1],...
                'style', 'pushbutton',...
                'string', 'Set',...
                'callback', @self.SetRA);
            
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [0 .6 .2 .1],...
                'style', 'text',...
                'string', 'LL:')
            self.LLLabelText = uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.2 .6 .3 .1],...
                'style', 'text',...
                'string', '',...
                'fontWeight', 'bold');
            uicontrol('parent', self.LeadPanel,...
                'units', 'normalized',...
                'position', [.6 .6 .3 .1],...
                'style', 'pushbutton',...
                'string', 'Set',...
                'callback', @self.SetLL);
        end
        
        function SetLA(self, varargin)
            if isempty(self.SelectedChannels), return; end
            
            if self.EcgData.WCTCorrected
                msgbox('Disable WCT correction before changing LA', 'Error changing WCT');
                return;
            end
            
            channelIndex = self.SelectedChannels(1);
            newLabel = self.EcgData.SetLA(channelIndex);
            set(self.LALabelText, 'string', newLabel);
            
            self.SetChannelList();
            self.ShowECGData();
        end
        
        function SetRA(self, varargin)
            if isempty(self.SelectedChannels), return; end
            
            if self.EcgData.WCTCorrected
                msgbox('Disable WCT correction before changing RA', 'Error changing WCT');
                return;
            end
            
            channelIndex = self.SelectedChannels(1);
            newLabel = self.EcgData.SetRA(channelIndex);
            set(self.RALabelText, 'string', newLabel);
            
            self.SetChannelList();
            self.ShowECGData();
        end
        
        function SetLL(self, varargin)
            if isempty(self.SelectedChannels), return; end
            
            if self.EcgData.WCTCorrected
                msgbox('Disable WCT correction before changing LL', 'Error changing WCT');
                return;
            end
            
            channelIndex = self.SelectedChannels(1);
            newLabel = self.EcgData.SetLL(channelIndex);
            set(self.LLLabelText, 'string', newLabel);
            
            self.SetChannelList();
            self.ShowECGData();
        end
        
        function SetWCT(self, source, varargin)
            wctCorrected = self.EcgData.ApplyWCT();
            set(source, 'value', wctCorrected);
            self.ShowECGData();
        end
        
        function SetBSPMMap(self, source, varargin)
            mapIndex = get(source, 'value');
            currentMap = BDFVisualizationPanel.AvailableMaps(mapIndex);
            mapLabels = [currentMap.Front(:); currentMap.Back(:)];
            mapLabels = mapLabels(~cellfun(@isempty, mapLabels));
            
            mapMembers = ismember(self.EcgData.ElectrodeLabels, mapLabels);
            self.ValidChannels = find(mapMembers);
            set(self.ValidChannelsListbox, 'value', self.ValidChannels);
        end
    end
    
    methods (Static)
        function maps = LoadAvailableBSPMMaps()
            filepath = fileparts(mfilename('fullpath'));
            mapLocation = fullfile(filepath, 'Data', 'BSPMMAps');
            listing = dir([mapLocation filesep '*.mat']);
            
            maps(numel(listing), 1) = struct('name', [], 'Front', [], 'Back', []);
            for listIndex = 1:numel(listing)
                filename = fullfile(mapLocation, listing(listIndex).name);
                
                mapData = load(filename);
                fieldNames = fieldnames(mapData);
                
                maps(listIndex).name = fieldNames{1};
                
                if isfield(mapData.(fieldNames{1}), 'Front')
                    maps(listIndex).Front = mapData.(fieldNames{1}).Front;
                end
                
                if isfield(mapData.(fieldNames{1}), 'Back')
                    maps(listIndex).Back = mapData.(fieldNames{1}).Back;
                end
            end
        end
    end
end