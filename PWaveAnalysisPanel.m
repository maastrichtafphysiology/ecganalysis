classdef PWaveAnalysisPanel < UserInterfacePkg.CustomPanel
    properties (Constant)
        VCG_12_LEAD_LABELS = {'I', 'II', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};    
        EXTENDED_ECG_LABELS = {'I', 'II', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'A1', 'A2', 'A3'};
    end
    
    properties
        PWaveData
        XYZIndices
    end
    
    properties (Access = protected)
        VisualizationPanel
        ControlPanel
        AnalysisTypeDropDown
        MapDropDown
        Map
        ShowMapLabels
        HideDisabledChannels
        
        ActivationSettingsPanel
        ActivationTypeSelection
        ActivationSubtypeSelection
        
        PWaveVCGView
        PWavePCAView
    end
    
    properties (Constant)
        AvailableMaps = PWaveMatcher.LoadAvailableBSPMMaps();
        AnalysisType = {'Summary'; 'Overall P-Wave energy'; 'P-wave signals'; 'P-wave duration'; 'P-wave area';...
            'P-wave amplitude'; 'P-wave complexity'; 'P-wave activation';...
            'VCG P-wave analysis'; 'P-wave PCA'};
    end
    
    methods
        function self = PWaveAnalysisPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            
            self.Map = self.AvailableMaps(1);
            self.ShowMapLabels = true;
            self.HideDisabledChannels = true;
            self.ActivationTypeSelection = 'Slope';
            self.ActivationSubtypeSelection = 'Min';
            self.XYZIndices = NaN(3, 2);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            
            self.VisualizationPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, .2, 1, .8],...
                'borderType', 'none',...
                'backgroundColor', [1,1,1]);
        end
        
        function SetPWaveData(self, pWaveData)
            self.PWaveData = pWaveData;
            
            if ~isfield(pWaveData, 'ValidChannels')
                numberOfChannels = numel(pWaveData.ElectrodeLabels);
                self.PWaveData.ValidChannels = 1:numberOfChannels;
            end
            
            self.SetECGMap();
            
            self.ShowPWaveData();
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, 0, 1, .2]);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .85 .1 .15],...
                'style', 'text',...
                'string', 'BSPM map');
            maps = PWaveAnalysisPanel.AvailableMaps;
            self.MapDropDown = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .7 .1 .15],...
                'style', 'popup',...
                'string', {maps.name}',...
                'callback', @self.SetBSPMMap);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .6 .1 .1],...
                'style', 'checkbox',...
                'string', 'Show map labels',...
                'value', self.ShowMapLabels,...
                'callback', @self.SetShowMapLabels);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .4 .1 .1],...
                'style', 'checkbox',...
                'string', 'Hide disabled channels',...
                'value', self.HideDisabledChannels,...
                'callback', @self.SetHideDisabledChannels);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.3 .85 .2 .15],...
                'style', 'text',...
                'string', 'Analysis type');
            self.AnalysisTypeDropDown = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.3 .7 .2 .15],...
                'style', 'popup',...
                'string', PWaveAnalysisPanel.AnalysisType,...
                'callback', @self.ShowPWaveData);
            
            self.ActivationSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [.3, .1, .2, .5]);
            
            activationTypeButtonGroup = uibuttongroup('visible', 'off',...
                'units', 'normalized', 'position', [0 .5 1 .5], 'parent', self.ActivationSettingsPanel);
            amplitudeRadioHandle = uicontrol('style','radiobutton', 'string', 'Amplitude',...
                'units', 'normalized', 'position', [0 0 .5 1], 'parent', activationTypeButtonGroup, 'handleVisibility','off');
            slopeRadioHandle = uicontrol('style','radiobutton', 'string', 'Slope',...
                'units', 'normalized', 'position', [.5 0 .5 1], 'parent', activationTypeButtonGroup, 'handleVisibility','off');
            set(activationTypeButtonGroup, 'SelectionChangeFcn', @self.ActivationTypeSelectionCallback);
            set(activationTypeButtonGroup, 'SelectedObject', slopeRadioHandle);
            set(activationTypeButtonGroup, 'visible','on');
            
            activationSubtypeButtonGroup = uibuttongroup('visible', 'off',...
                'units', 'normalized', 'position', [0 0 1 .5], 'parent', self.ActivationSettingsPanel);
            maxRadioHandle = uicontrol('style','radiobutton', 'string', 'Max',...
                'units', 'normalized', 'position', [0 0 1/3 1], 'parent', activationSubtypeButtonGroup, 'handleVisibility','off');
            minRadioHandle = uicontrol('style','radiobutton', 'string', 'Min',...
                'units', 'normalized', 'position', [1/3 0 1/3 1], 'parent', activationSubtypeButtonGroup, 'handleVisibility','off');
            uicontrol('style','radiobutton', 'string', 'Max abs',...
                'units', 'normalized', 'position', [2/3 0 1/3 1], 'parent', activationSubtypeButtonGroup, 'handleVisibility','off');
            set(activationSubtypeButtonGroup, 'SelectionChangeFcn', @self.ActivationSubtypeSelectionCallback);
            set(activationSubtypeButtonGroup, 'SelectedObject', minRadioHandle);
            set(activationSubtypeButtonGroup, 'visible','on');
            set(self.ActivationSettingsPanel, 'visible', 'off');
            
            % save to .mat
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.6 .8 .2 .1],...
                'style', 'pushbutton',...
                'string', 'Save Analysis to .mat',...
                'callback', @self.SaveAnalysisToMat);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.6 .6 .2 .1],...
                'style', 'pushbutton',...
                'string', 'Load Analysis',...
                'callback', @self.LoadAnalysis);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.6 .4 .2 .1],...
                'style', 'pushbutton',...
                'string', 'Export to .csv',...
                'callback', @self.ExportAnalysisSummary);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.6 .2 .2 .1],...
                'style', 'pushbutton',...
                'string', 'Recompute analysis',...
                'callback', @self.Recompute);
        end
        
        function ActivationTypeSelectionCallback(self, ~, eventData)
            self.ActivationTypeSelection = get(eventData.NewValue, 'string');
            self.ShowPWaveData();
        end
        
        function ActivationSubtypeSelectionCallback(self, ~, eventData)
            self.ActivationSubtypeSelection = get(eventData.NewValue, 'string');
            self.ShowPWaveData();
        end
        
        function ShowPWaveData(self, varargin)
            delete(get(self.VisualizationPanel, 'Children'));
            set(self.ActivationSettingsPanel, 'visible', 'off');
            
            value = get(self.AnalysisTypeDropDown, 'value');
            switch value
                case 1
                    self.ShowPWaveResultPanel();
                case 2
                    self.ShowOverallPWaveEnergy();
                case 3
                    self.ShowPWaveSignalMap();
                case 4
                    self.ShowPWaveDurationMap();
                case 5
                    self.ShowPWaveAreaMap();
                case 6
                    self.ShowPWaveAmplitudeMap();
                case 7
                    self.ShowPWaveComplexityMap();
                case 8
                    set(self.ActivationSettingsPanel, 'visible', 'on');
                    self.ShowPWaveActivationMap();
                case 9
                    self.ShowPWaveVCGPanel();
                case 10
                    self.ShowPWavePCAPanel();
                otherwise
                    return;
            end
        end
        
        function SetBSPMMap(self, source, varargin)
            mapIndex = get(source, 'value');
            self.Map = PWaveAnalysisPanel.AvailableMaps(mapIndex);
            
            if ~isempty(self.PWaveData)
                self.ShowPWaveData();
            end
        end
        
        function SetECGMap(self)
            if isempty(self.PWaveData), return; end
            
            electrodeLabels = self.PWaveData.ElectrodeLabels;
            [detectedMap, detectedMapIndex] = PWaveMatcher.DetectECGMap(electrodeLabels);
            
            self.Map = detectedMap;
            set(self.MapDropDown, 'value', detectedMapIndex);
        end
        
        function ShowOverallPWaveEnergy(self)
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            intervalTime = self.PWaveData.AveragingInterval;
            pWaveEnergy = self.PWaveData.AveragePWaveEnergy;
            
            % overall p-wave energy
            line('xData', intervalTime, 'yData', pWaveEnergy,...
                'color', [0, 0, 0], 'lineSmoothing', 'on',...
                'lineWidth', 1.5, 'parent', plotHandle);   
            yLimits = get(plotHandle, 'yLim');
            
            % onset/offset/Q-onset
            onsetOffset = self.PWaveData.OnsetOffset;
            line(...
                'xData', ones(1, 2) * onsetOffset(1), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2, 'parent', plotHandle);
            line(...
                'xData', ones(1, 2) * onsetOffset(2), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2,  'parent', plotHandle);
            line(...
                'xData', ones(1, 2) * onsetOffset(3), 'yData', yLimits,...
                'color', [.7,.7,.7], 'lineStyle', '--', 'lineWidth', 2,  'parent', plotHandle);
            
            set(plotHandle, 'lineWidth', 1.5);
            set(plotHandle, 'xLim', [min(intervalTime), max(intervalTime)]);
            
            
            xlabel(plotHandle, 'Time (ms)');
            ylabel(plotHandle, 'Voltage RMS (mV)');
            title(plotHandle, 'P-Wave Energy estimate (RMS)');
            
            box(plotHandle, 'on');
        end
        
        function ShowPWaveSignalMap(self, parentHandle)
            if nargin < 2
                parentHandle = self.VisualizationPanel;
                snapshot = true;
            else
                snapshot = false;
            end
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            intervalTime = self.PWaveData.AveragingInterval;
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            pWaveTime = intervalTime(validTime);
            averagePWaves = self.PWaveData.AveragePWaves(validTime, :);
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            yLimits = [min(averagePWaves(:)), max(averagePWaves(:))];
            xLimits = [pWaveTime(1), pWaveTime(end)];
            
            numberOfMapRows = size(mapLabels, 1);
            numberOfMapColumns = size(mapLabels, 2);
            axesHandles = NaN(numberOfMapRows, numberOfMapColumns);
            
            axesMagnification = 1.5;
            axesSize = axesMagnification * [1 / numberOfMapColumns, 1 / numberOfMapRows];
            axesSeparation = [1 / numberOfMapColumns, 1 / numberOfMapRows] - (axesMagnification - 1) .* [1 / numberOfMapColumns, 1 / numberOfMapRows].^2;
            
            showDetails = false;
            if numel(dataLabels) <= 20 
                showDetails = true;
            end
            
            for mapRow = 1:numberOfMapRows
                for mapColumn = 1:numberOfMapColumns
                    channelPosition = find(strcmpi(mapLabels{mapRow, mapColumn}, dataLabels), 1, 'first');
                    if ~isempty(channelPosition)
                        if showDetails
                            axesPosition = [0.025 + (mapColumn - 1) * axesSeparation(1) * 0.95,...
                                0.05 + (numberOfMapRows - mapRow) * axesSeparation(2) * 0.9,...
                                axesSize * 0.9];
                        else
                            axesPosition = [(mapColumn - 1) * axesSeparation(1),...
                                (numberOfMapRows - mapRow) * axesSeparation(2),...
                                axesSize];
                        end
                        axesHandles(mapRow, mapColumn) = axes('units', 'normalized', 'position', axesPosition,...
                            'parent', parentHandle, 'gridColor', [1,0,0]);
                        
                        if self.ShowMapLabels
                            if showDetails
                                title(axesHandles(mapRow, mapColumn),  mapLabels{mapRow, mapColumn});
                            else
                                text(mean(pWaveTime), 0, mapLabels{mapRow, mapColumn},...
                                    'horizontalAlignment', 'center', 'color', [.7,.7,.7],...
                                    'fontUnits', 'normalized', 'fontSize', 0.5);
                            end
                        end
                        
                        line('xData', pWaveTime, 'yData', zeros(size(pWaveTime)),...
                            'color', [.7,.7,.7], 'parent', axesHandles(mapRow, mapColumn));
                        pWaveLine = line('xData', pWaveTime, 'yData', averagePWaves(:, channelPosition),...
                            'color', [0,0,0], 'parent', axesHandles(mapRow, mapColumn),...
                            'lineWidth', 2, 'lineSmoothing', 'on');
                        
                        xlim(axesHandles(mapRow, mapColumn), xLimits);
                        ylim(axesHandles(mapRow, mapColumn), yLimits);
                        if showDetails
                            grid(axesHandles(mapRow, mapColumn), 'on');
                            if mapRow < numberOfMapRows
                                set(axesHandles(mapRow, mapColumn), 'xTickLabels', {});
                            else
                                xlabel(axesHandles(mapRow, mapColumn), 'time (ms)');
                            end
                            if mapColumn > 1
                                set(axesHandles(mapRow, mapColumn), 'yTickLabels', {});
                            else
                                if range(yLimits)<10
                                    ylabel(axesHandles(mapRow, mapColumn), 'voltage (mV)');
                                elseif range(yLimits)>10
                                    ylabel(axesHandles(mapRow, mapColumn), 'voltage (muV)');
                                end
                            end
                        else
                            axis(axesHandles(mapRow, mapColumn), 'off');
                        end
                        
                        if ~validChannels(channelPosition)
                            if self.HideDisabledChannels
                                set(pWaveLine, 'visible', 'off');
                            else
                                set(pWaveLine, 'color', [.7 .7 .7]);
                            end
                        end
                    end
                end
            end
            
            function SignalSnapshot(varargin)
                screenSize = get(0, 'screensize');
%                 figurePosition = screenSize;
                figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];
                figureHandle = figure('name', 'Snapshot', 'numberTitle', 'off',...
                    'color', [1 1 1], 'outerPosition', figurePosition);
                self.ShowPWaveSignalMap(figureHandle);
                
                if ispc
                    homePath = getenv('USERPROFILE'); 
                else
                    homePath = getenv('HOME');
                end
                filename = fullfile(homePath, 'Desktop', 'PWaveSignals');
            
                print(figureHandle, filename, '-depsc', '-tiff');
                delete(figureHandle);
            end
            
            if snapshot
                uicontrol(...
                    'parent', parentHandle,...
                    'units', 'normalized',...
                    'position', [.95 0 .05 .05],...
                    'style', 'pushbutton',...
                    'string', 'Snapshot',...
                    'callback', @SignalSnapshot);
            end
        end
        
        function SetShowMapLabels(self, source, varargin)
            self.ShowMapLabels = get(source, 'value') > 0;
            self.ShowPWaveData();
        end
        
        function SetHideDisabledChannels(self, source, varargin)
            self.HideDisabledChannels = get(source, 'value') > 0;
            self.ShowPWaveData();
        end
        
        function ShowPWaveDurationMap(self)
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            pWaveDuration = NaN(numel(dataLabels), 1);
            for electrodeIndex = 1:numel(dataLabels)
                electrodeOnsetOffset = self.PWaveData.ElectrodeOnsetOffset(electrodeIndex, :);
                if isnan(electrodeOnsetOffset(1))
                    electrodeOnsetOffset(1) = self.PWaveData.OnsetOffset(1);
                end
                if isnan(electrodeOnsetOffset(2))
                    electrodeOnsetOffset(2) = self.PWaveData.OnsetOffset(2);
                end
                pWaveDuration(electrodeIndex) = electrodeOnsetOffset(2) - electrodeOnsetOffset(1);
            end
            
            showElectrodes = true;
            interpolationFactor = 0;
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            
            PWaveAnalysisPanel.CreateInterpolatedBSPMMap(plotHandle, pWaveDuration,...
                dataLabels, mapLabels, showElectrodes, self.ShowMapLabels, interpolationFactor,...
                validChannels, self.HideDisabledChannels);
            
            colorbarHandle = findobj(self.VisualizationPanel,'tag','Colorbar');
            if ishandle(colorbarHandle)
                xlabel(colorbarHandle, 'P-wave duration (ms)');
            end
        end
        
        function ShowPWaveAreaMap(self)
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            pWaveArea = self.PWaveData.PWaveArea;
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            showElectrodes = true;
            interpolationFactor = 1;
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            PWaveAnalysisPanel.CreateInterpolatedBSPMMap(plotHandle, pWaveArea,...
                dataLabels, mapLabels, showElectrodes, self.ShowMapLabels, interpolationFactor,...
                validChannels, self.HideDisabledChannels);
            
            colorbarHandle = findobj(self.VisualizationPanel,'tag','Colorbar');
            if ishandle(colorbarHandle)
                xlabel(colorbarHandle, 'P-wave area (mV * ms)');
            end
        end
        
        function ShowPWaveComplexityMap(self)
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            pWaveComplexity = self.PWaveData.Complexity;
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            showElectrodes = true;
            interpolationFactor = 1;
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            PWaveAnalysisPanel.CreateInterpolatedBSPMMap(plotHandle, pWaveComplexity,...
                dataLabels, mapLabels, showElectrodes, self.ShowMapLabels, interpolationFactor,...
                validChannels, self.HideDisabledChannels);
            
            colorbarHandle = findobj(self.VisualizationPanel,'tag','Colorbar');
            if ishandle(colorbarHandle)
                xlabel(colorbarHandle, 'P-wave complexity (number of peaks)');
            end
        end
        
        function ShowPWaveAmplitudeMap(self)
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            pWaveAmplitude = self.PWaveData.PWaveRange(:, end) - self.PWaveData.PWaveRange(:, 1);
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            showElectrodes = true;
            interpolationFactor = 1;
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            PWaveAnalysisPanel.CreateInterpolatedBSPMMap(plotHandle, pWaveAmplitude,...
                dataLabels, mapLabels, showElectrodes, self.ShowMapLabels, interpolationFactor,...
                validChannels, self.HideDisabledChannels);
            
            colorbarHandle = findobj(self.VisualizationPanel,'tag','Colorbar');
            if ishandle(colorbarHandle)
                xlabel(colorbarHandle, 'P-wave amplitude (mV)');
            end
        end
        
        function ShowPWaveActivationMap(self)
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            dataLabels = self.PWaveData.ElectrodeLabels;
            
            switch self.ActivationTypeSelection
                case 'Amplitude'
                    typeData = self.PWaveData.ActivationTimeValue;
                case 'Slope'
                    typeData = self.PWaveData.ActivationTimeSlope;
                otherwise
                    return;
            end
            
            switch self.ActivationSubtypeSelection
                case 'Max'
                    activationTime = vertcat(typeData.Max);
                case 'Min'
                    activationTime = vertcat(typeData.Min);
                case 'Max abs'
                    activationTime = vertcat(typeData.MaxAbs);
                otherwise
                    return;
            end
            
            plotHandle = subplot(1,1,1, 'parent', self.VisualizationPanel);
            
            showElectrodes = true;
            interpolationFactor = 1;
            validChannelIndices = self.PWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            PWaveAnalysisPanel.CreateInterpolatedBSPMMap(plotHandle, activationTime,...
                dataLabels, mapLabels, showElectrodes, self.ShowMapLabels, interpolationFactor,...
                validChannels, self.HideDisabledChannels);
            
            colorbarHandle = findobj(self.VisualizationPanel,'tag','Colorbar');
            if ishandle(colorbarHandle)
                xlabel(colorbarHandle, 'P-wave activation time (ms)');
            end
        end
        
        function ShowPWaveVCGPanel(self)
            if isempty(self.PWaveData), return; end
            
            % check for 12-lead ECG or other type
            electrodeLabels = self.PWaveData.ElectrodeLabels;
            electrodeFound = ismember(PWaveAnalysisPanel.VCG_12_LEAD_LABELS, electrodeLabels);
            
            if all(electrodeFound)
                self.PWaveVCGView = PWave12LeadVCGPanel([0,0,1,1]);
                self.PWaveVCGView.Create(self.VisualizationPanel);
                self.PWaveVCGView.Show();
                
                self.PWaveVCGView.SetPWaveData(self.PWaveData, self.Map);
            else
                self.PWaveVCGView = PWaveBSPMVCGPanel([0,0,1,1]);
                self.PWaveVCGView.Create(self.VisualizationPanel);
                self.PWaveVCGView.Show();
                
                addlistener(self.PWaveVCGView, 'XYZIndices', 'PostSet',...
                    @self.HandleXYZIndicesSetEvent);
                
                self.PWaveVCGView.XYZIndices = self.XYZIndices;
                self.PWaveVCGView.SetPWaveData(self.PWaveData, self.Map);
            end
        end
        
        function ShowPWavePCAPanel(self)
           self.PWavePCAView = PWavePCAPanel([0,0,1,1]);
           self.PWavePCAView.Create(self.VisualizationPanel);
           self.PWavePCAView.HideDisabledChannels = self.HideDisabledChannels;
           self.PWavePCAView.Show();
           
           if isempty(self.PWaveData)
               return;
           else
               self.PWavePCAView.SetPWaveData(self.PWaveData, self.Map);
           end
        end
        
        function ShowPWaveResultPanel(self)
            if isempty(self.PWaveData), return; end
            
            % P-wave duration
            numberOfElectrodes = numel(self.PWaveData.ElectrodeLabels);
            pWaveDuration = NaN(numberOfElectrodes, 1);
            for electrodeIndex = 1:numberOfElectrodes
                electrodeOnsetOffset = self.PWaveData.ElectrodeOnsetOffset(electrodeIndex, :);
                if isnan(electrodeOnsetOffset(1))
                    electrodeOnsetOffset(1) = self.PWaveData.OnsetOffset(1);
                end
                if isnan(electrodeOnsetOffset(2))
                    electrodeOnsetOffset(2) = self.PWaveData.OnsetOffset(2);
                end
                pWaveDuration(electrodeIndex) = electrodeOnsetOffset(2) - electrodeOnsetOffset(1);
            end
            
            % P-wave area
            pWaveArea = self.PWaveData.PWaveArea;
            
            % P-wave amplitude
            pWaveAmplitude = self.PWaveData.PWaveRange(:, end) - self.PWaveData.PWaveRange(:, 1);
            
            % P-wave complexity
            pWaveComplexity = self.PWaveData.Complexity;
            
            resultTable = table(...
                pWaveDuration,...
                pWaveArea,...
                pWaveAmplitude,...
                pWaveComplexity,...
                'variableNames', {'Duration', 'Area', 'Amplitude', 'Complexity'},...
                'rowNames', self.PWaveData.ElectrodeLabels);
            
            uitable(...
                'parent', self.VisualizationPanel,...
                'units', 'normalized',...
                'position', [0 0 1 .9],...
                'data',  resultTable{:, :},...
                'columnName', resultTable.Properties.VariableNames,...
                'rowName', resultTable.Properties.RowNames);
        end
        
        function SaveAnalysisToMat(self, varargin)
            if isempty(self.PWaveData), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*_PWave.mat', 'Save analysis as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            PWaveData = self.PWaveData; %#ok<PROP,NASGU>
            Map = self.Map; %#ok<PROP,NASGU>
            XYZIndices = self.XYZIndices; %#ok<PROP,NASGU>
            save(filename, 'PWaveData', 'Map', 'XYZIndices');
        end
        
        function LoadAnalysis(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*.mat', 'Select analysis file');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            resultData = load(filename);
            if isfield(resultData, 'Map')
                self.Map = resultData.Map;
                mapStrings = get(self.MapDropDown, 'string');
                mapIndex = find(strcmpi(self.Map.name, mapStrings));
                if ~isempty(mapIndex)
                    set(self.MapDropDown, 'value', mapIndex(1));
                end
            end
            
            if isfield(resultData, 'XYZIndices')
                self.XYZIndices = resultData.XYZIndices;
            else
                self.XYZIndices = NaN(3, 2);
            end
            
            if isfield(resultData, 'PWaveData')
                self.SetPWaveData(resultData.PWaveData);
            else
                disp('Invalid analysis file: does not contain PWaveData field.');
            end
        end
        
        function ExportAnalysisSummary(self, varargin)
            if isempty(self.PWaveData), return; end
            
            [filename, pathname] = HelperFunctions.customUiputfile('*.csv', 'Export as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            outputFilename = fullfile(pathname, filename);
            
            % P-wave duration
            numberOfElectrodes = numel(self.PWaveData.ElectrodeLabels);
            pWaveDuration = NaN(numberOfElectrodes, 1);
            for electrodeIndex = 1:numberOfElectrodes
                electrodeOnsetOffset = self.PWaveData.ElectrodeOnsetOffset(electrodeIndex, :);
                if isnan(electrodeOnsetOffset(1))
                    electrodeOnsetOffset(1) = self.PWaveData.OnsetOffset(1);
                end
                if isnan(electrodeOnsetOffset(2))
                    electrodeOnsetOffset(2) = self.PWaveData.OnsetOffset(2);
                end
                pWaveDuration(electrodeIndex) = electrodeOnsetOffset(2) - electrodeOnsetOffset(1);
            end
            pWaveDispersion = max(pWaveDuration) - min(pWaveDuration);
            
            % P-wave area
            pWaveArea = self.PWaveData.PWaveArea;
            
            % P-wave amplitude
            pWaveAmplitude = self.PWaveData.PWaveRange(:, end) - self.PWaveData.PWaveRange(:, 1);
            
            % P-wave complexity
            pWaveComplexity = self.PWaveData.Complexity;
            
            outputData = dataset(...
                {numel(self.PWaveData.ElectrodeLabels), 'NumberOfElectrodes'},...
                {size(self.PWaveData.PWaveIntervals, 1), 'NumberOfIntervals'},...
                {self.PWaveData.OnsetOffset(1), 'Onset'},...
                {self.PWaveData.OnsetOffset(2), 'Offset'},...
                {self.PWaveData.OnsetOffset(3), 'Q_Onset'},...
                {pWaveDispersion, 'Dispersion'},...
                {mean(pWaveArea), 'Area_mean'},...
                {median(pWaveArea), 'Area_median'},...
                {min(pWaveArea), 'Area_min'},...
                {max(pWaveArea), 'Area_max'},...
                {mean(pWaveAmplitude), 'Amplitude_mean'},...
                {median(pWaveAmplitude), 'Amplitude_median'},...
                {min(pWaveAmplitude), 'Amplitude_min'},...
                {max(pWaveAmplitude), 'Amplitude_max'},...
                {mean(pWaveComplexity), 'Complexity_mean'},...
                {median(pWaveComplexity), 'Complexity_median'},...
                {min(pWaveComplexity), 'Complexity_min'},...
                {max(pWaveComplexity), 'Complexity_max'});
            
            export(outputData, 'file', outputFilename, 'delimiter', ',');
        end
        
        function Recompute(self, varargin)
            if isempty(self.PWaveData), return; end
            
            numberOfChannels = numel(self.PWaveData.ElectrodeLabels);
            intervalTime = self.PWaveData.AveragingInterval;
            OnsetOffset = self.PWaveData.OnsetOffset;
            validIntervalTime = intervalTime >= OnsetOffset(1) & intervalTime <= OnsetOffset(2);
            for channelIndex = 1:numberOfChannels
                pWaveAverage = self.PWaveData.AveragePWaves(:, channelIndex);
                detrendAverage = true;
                
                self.PWaveData.PWaveArea(channelIndex) = PWaveMatcher.ComputePWaveArea(pWaveAverage,...
                    intervalTime, OnsetOffset, detrendAverage);
                self.PWaveData.PWaveRange(channelIndex, :) = [min(pWaveAverage(validIntervalTime)),...
                    max(pWaveAverage(validIntervalTime))];
                
                [activationValue, activationSlope] = PWaveMatcher.ComputeActivationTime(pWaveAverage, intervalTime, OnsetOffset);
                self.PWaveData.ActivationTimeValue(channelIndex) = activationValue;
                self.PWaveData.ActivationTimeSlope(channelIndex) = activationSlope;
            end
            
            self.ShowPWaveData();
        end
        
        function HandleXYZIndicesSetEvent(self, ~, eventData)
            self.XYZIndices = eventData.AffectedObject.XYZIndices;
        end
    end
    
    methods (Static)
        function CreateInterpolatedBSPMMap(axesHandle, data, dataLabels, mapLabels, showElectrodes, showText, interpolationFactor, validChannels, hideInvalidChannels)
            if nargin < 9
                hideInvalidChannels = false;
                if nargin < 8
                    validChannels = true(size(dataLabels));
                end
            end
            axis(axesHandle, 'image');
            mapImage = imagesc('parent', axesHandle,...
                'cData', [], 'cDataMapping', 'scaled',...
                'xData', [1 size(mapLabels, 2)],...
                'yData', [1 size(mapLabels, 1)]);
            
            mapChannelIndex = NaN(size(mapLabels));
            if hideInvalidChannels
                data = data(validChannels);
                validDataLabels = dataLabels(validChannels);
                validChannels = validChannels(validChannels);
            else
                validDataLabels = dataLabels;
            end
            for i = 1:size(mapLabels, 1)
                for j = 1:size(mapLabels, 2)
                    channelPosition = find(strcmpi(mapLabels{i, j}, validDataLabels), 1, 'first');
                    if ~isempty(channelPosition)
                        mapChannelIndex(i, j) = channelPosition;
                    end
                end
            end
            mapData = NaN(size(mapChannelIndex));
            mapData(~isnan(mapChannelIndex)) = data(mapChannelIndex(~isnan(mapChannelIndex)));
            
            [Y, X] = find(~isnan(mapData));
            if interpolationFactor == 0
                Z = mapData;
            else
                [Xint, Yint] = meshgrid(1:(1 / interpolationFactor):size(mapData, 2), 1:(1 / interpolationFactor):size(mapData, 1));
                interpolant = scatteredInterpolant([X, Y], mapData(~isnan(mapData)));
                interpolant.Method = 'linear';
                interpolant.ExtrapolationMethod = 'linear';
                Z = interpolant(Xint, Yint);
                X = linspace(1, size(mapData, 2), size(Z, 2));
                Y = linspace(1, size(mapData, 1), size(Z, 1));
            end
            set(mapImage, 'xData', unique(X), 'yData', unique(Y), 'cData', Z);
            transparencyMatrix = ones(size(Z));
            transparencyMatrix(isnan(Z)) = 0;
            set(mapImage, 'alphaData', transparencyMatrix);
            
            N = 100;
            t = 2*pi/N*(1:N);
            radius = 0.5;
            hold(axesHandle, 'on');
            for i = 1:size(mapLabels, 1)
                for j = 1:size(mapLabels, 2)
                    channelPosition = mapChannelIndex(i, j);
                    if ~isnan(channelPosition)
                        color = mapData(i, j);
                        if showElectrodes
                            fill(j + radius * cos(t), i + radius * sin(t), color);
                        end
                        if showText
                            if validChannels(channelPosition)
                                textColor = [0,0,0];
                            else
                                textColor = [1,0,0];
                            end
                            text(j, i, mapLabels{i, j},...
                                'HorizontalAlignment', 'center',...
                                'FontSize', 8, 'color', textColor);
                        end
                    end
                end
            end
            hold(axesHandle, 'off');
            
            axis(axesHandle, 'tight');
            set(axesHandle, 'yDir', 'reverse');
            axis(axesHandle, 'off');
            cbarAxes = colorbar(axesHandle, 'southOutside');
            % set(cbarAxes, 'xTicklabel', {});
            % colorbar;
            
            contextMenu = uicontextmenu;
            uimenu(contextMenu, 'Label', 'Copy map to clipboard', 'callback', {@PWaveAnalysisPanel.CopyMapToClipboard, Z});
            set(mapImage, 'UIContextMenu', contextMenu);
        end
        
        function CopyMapToClipboard(self, source, mapData)
           UtilityPkg.num2clip(mapData);
        end
    end
end