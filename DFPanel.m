classdef DFPanel < WindowedAnalysisPanel    
    properties (Access = private)
        FFTPointsEdit
        DFRangeStartEdit
        DFRangeEndEdit
        
        SpectrumRangeStartEdit
        SpectrumRangeEndEdit
        
        SpectrumRange
        SpectrumHandle
        SpectrumPanel
        
        Frequencies
        Power
        WindowedOI
        WindowFrequencyPower
        WindowedDF
        WindowedSC
        WindowedRR
        DFIndex
        
        DFAxesHandles
    end
    
    methods
        function self = DFPanel(position)
            self = self@WindowedAnalysisPanel(position);
            
            self.WindowLength = 10;
            self.WindowShift = 5;
            
            self.SpectrumRange = [0, 20];
        end
        
        function SetECGData(self, ecgData, rWaveIndices)
            SetECGData@WindowedAnalysisPanel(self, ecgData, rWaveIndices);
            set(self.SettingsPanel, 'title', ['Settings (Fs=', num2str(self.EcgData.SamplingFrequency), 'Hz)']);
        end
        
        function LoadAnalysis(self, filename)
            ecgData = load(filename, 'ecgData');
            ecgData = ecgData.ecgData;
            ecgData.Filename = filename(1:(end - 11));
            dfData = load(filename, 'DF');
            dfData = dfData.DF;
            rWaveIndices = load(filename, 'rWaveIndices');
            rWaveIndices = rWaveIndices.rWaveIndices;
            
            self.Frequencies = dfData.frequencies;
            self.Power = dfData.powers;
            self.DFIndex = dfData.dfIndex;
            self.WindowedDF = dfData.windowedDF;
            self.WindowedOI = dfData.windowedOI;
            if isfield(dfData, 'windowFrequencyPower')
                self.WindowFrequencyPower = dfData.windowFrequencyPower;
            end
            if isfield(dfData, 'windowedRR')
                self.WindowedRR = dfData.windowedRR;
            end
            
            self.AFComplexityCalculator.DFParameters = dfData.settings.parameters;
            
            self.V1Index = dfData.settings.v1Index;
            self.WindowLength = dfData.settings.windowLength;
            self.WindowShift = dfData.settings.windowShift;
            
            self.SetECGData(ecgData, rWaveIndices);
            
            self.InitializeSettings();
            
            self.ShowAnalysis();
        end
        
        function analysisData = GetAnalysisData(self)
            dfSettings = struct(...
                'parameters', self.AFComplexityCalculator.DFParameters,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift,...
                'v1Index', self.V1Index);
            
            analysisData = struct(...
                'frequencies', self.Frequencies,...
                'powers', self.Power,...
                'dfIndex', self.DFIndex,...
                'windowedDF', self.WindowedDF,...
                'windowedOI', self.WindowedOI,...
                'windowedSC', self.WindowedSC,...
                'windowFrequencyPower', {self.WindowFrequencyPower},...
                'windowedRR', self.WindowedRR,...
                'settings', dfSettings);
        end
    end
    
    methods (Access = protected)
        function CreateAnalysisSettings(self)
            set(self.SettingsPanel, 'position', [0 .3 1 .15]);
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .7 .4 .2],...
                'style', 'text',...
                'string', 'FFT points');
            self.FFTPointsEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .7 .4 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints),...
                'callback', @self.SetNumberOfFFTPoints);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .4 .4 .2],...
                'style', 'text',...
                'string', 'DF range (Hz)');
            self.DFRangeStartEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .4 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)),...
                'callback', @self.SetDFRangeStart);
            self.DFRangeEndEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .4 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)),...
                'callback', @self.SetDFRangeEnd);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .4 .2],...
                'style', 'text',...
                'string', 'Spectrum range (Hz)');
            self.SpectrumRangeStartEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.SpectrumRange(1)),...
                'callback', @self.SetSpectrumRangeStart);
            self.SpectrumRangeEndEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .1 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.SpectrumRange(2)),...
                'callback', @self.SetSpectrumRangeEnd);
            
            self.SpectrumPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .3],...
                'title', 'Spectrum');
        end
        
        function InitializeSettings(self)
            InitializeSettings@WindowedAnalysisPanel(self);
            
            set(self.FFTPointsEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints));
            set(self.DFRangeStartEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)));
            set(self.DFRangeEndEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)));
        end
        
        function ShowECGData(self)
            ShowECGData@WindowedAnalysisPanel(self);
            self.ShowQuality();
            self.ShowAnalysis();
        end
        
        function ComputeAnalysis(self, varargin)
            [self.DFIndex, self.Power, self.Frequencies] =...
                self.AFComplexityCalculator.ComputeDominantFrequency(self.EcgData);
            
            [self.WindowedDF, self.WindowedOI, self.WindowedSC] = self.ComputeWindowedDominantFrequency();
            
            self.WindowedRR = self.ComputeWindowedRRInterval();
            
            self.ShowAnalysis();
        end
        
        function ShowAnalysis(self, varargin)
            if isempty(self.Frequencies), return; end
            
            delete(get(self.ResultPanel, 'children'));
            
            time = self.EcgData.GetTimeRange();
            windowedDFInfo = self.WindowedDF(self.SelectedChannels);
            electrodeLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            
            self.DFAxesHandles = ECGChannelPanel.TightSubplot(numel(electrodeLabels) + 1, 1, self.ResultPanel,...
                0.02, [0.05, 0.05], [0.025, 0.05]);
            
            % Windowed DF
            for lineIndex = 1:numel(electrodeLabels)
                self.PlotWindowedData(self.DFAxesHandles(lineIndex), windowedDFInfo(lineIndex));
                ylabel(self.DFAxesHandles(lineIndex), 'Frequency (Hz)');
            
                text('parent', self.DFAxesHandles(lineIndex), 'string', electrodeLabels{lineIndex},...
                    'fontWeight', 'demi',...
                    'units', 'normalized', 'position', [0, .5, 0],...
                    'horizontalAlignment', 'right');
                set(self.DFAxesHandles(lineIndex), 'xTick', [],...
                    'YAxisLocation', 'right', 'xLim', [time(1), time(end)]);
                
                set(self.DFAxesHandles(lineIndex), 'ylim', self.AFComplexityCalculator.DFParameters.DFRange + [-0.1, 0.1]);
                set(self.DFAxesHandles(lineIndex), 'buttonDownFcn', {@self.ShowPowerSpectrum, self.SelectedChannels(lineIndex), windowedDFInfo(lineIndex).windowCenter});
                
                contextMenu = uicontextmenu('Parent', ancestor(self.DFAxesHandles(lineIndex), 'figure'));
                uimenu(contextMenu, 'Label', 'Copy DF-OI data to clipboard', 'callback', {@self.CopyWindowedDataToClipboard, self.SelectedChannels(lineIndex)});
                set(self.DFAxesHandles(lineIndex), 'UIContextMenu', contextMenu);
            end
            
            % RR interval
            self.PlotWindowedData(self.DFAxesHandles(end), self.WindowedRR);
            ylabel(self.DFAxesHandles(end), 'Interval (ms)');
            
            text('parent', self.DFAxesHandles(end), 'string', 'RR',...
                'fontWeight', 'bold',...
                'units', 'normalized', 'position', [0, .5, 0],...
                'horizontalAlignment', 'right');
            set(self.DFAxesHandles(end), 'xTick', [],...
                'YAxisLocation', 'right', 'xLim', [time(1), time(end)],...
                'box', 'on');
            contextMenu = uicontextmenu(ancestor(self.DFAxesHandles(end), 'figure'));
            uimenu(contextMenu, 'Label', 'Copy RR data to clipboard', 'callback', @self.CopyWindowedRRDataToClipboard);
            set(self.DFAxesHandles(end), 'UIContextMenu', contextMenu);
            
            set(self.DFAxesHandles(end),'xTickMode', 'auto');
            xlabel(self.DFAxesHandles(end), 'time (seconds)');
            if numel(self.DFAxesHandles) > 1
                if ishandle(self.DFAxesHandles)
                    linkaxes([self.AxesHandles; self.DFAxesHandles], 'x');
                end
            end
        end
        
        function ShowQuality(self)
            if isempty(self.WindowedSC), return; end
            
            qualityResolution = 100;
            bias = 10;
            qualityColors = hot(qualityResolution + bias);
            qualityColors = qualityColors((bias + 1):end, :);
            time = self.EcgData.GetTimeRange();
            
            scWindowInfo = self.WindowedSC(self.SelectedChannels);
            for channelIndex = 1:numel(scWindowInfo)
                scValues = scWindowInfo(channelIndex);
                numberOfWindows = numel(scValues.windowStart);
                windowColorIndex = round((qualityResolution - 1) * (scValues.runningWindowMean)) + 1;
                axisYLimit = get(self.AxesHandles(channelIndex), 'ylim');
                patchHandles = NaN(numberOfWindows, 1);
                for windowIndex = 1:numberOfWindows
                    windowStartTime = time(scValues.windowStart(windowIndex));
                    windowEndTime = time(scValues.windowEnd(windowIndex));
                    patchHandles(windowIndex) = patch('xData', [windowStartTime, windowStartTime, windowEndTime, windowEndTime],...
                        'yData', [axisYLimit(1), axisYLimit(end), axisYLimit(end), axisYLimit(1)],...
                        'facecolor', qualityColors(windowColorIndex(windowIndex), :),...
                        'edgeColor', 'none',...
                        'parent', self.AxesHandles(channelIndex));
                end
                uistack(patchHandles, 'bottom');
            end
        end
        
        function SaveAnalysis(self, varargin)
            data = [self.Frequencies(:), self.Power(:)];
            WindowedAnalysisPanel.CopyToClipboard(data);
            
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            dfSettings = struct(...
                'parameters', self.AFComplexityCalculator.DFParameters,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift,...
                'v1Index', self.V1Index);
            DF = struct(...
                'frequencies', self.Frequencies,...
                'powers', self.Power,...
                'dfIndex', self.DFIndex,...
                'windowedDF', self.WindowedDF,...
                'windowedOI', self.WindowedOI,...
                'windowedSC', self.WindowedSC,...
                'windowFrequencyPower', {self.WindowFrequencyPower},...
                'windowedRR', self.WindowedRR,...
                'settings', dfSettings); %#ok<NASGU>
            
            if exist(filename, 'file')
                save(filename, 'DF', '-append');
            else
                ecgData = self.EcgData; %#ok<NASGU>
                rWaveIndices = self.RWaveIndices; %#ok<NASGU>
                save(filename, 'ecgData', 'rWaveIndices', 'DF');
            end
            
            notify(self, 'AnalysisSaved');
        end
        
        function SetWindowLength(self, source, varargin)
            SetWindowLength@WindowedAnalysisPanel(self, source, varargin{:});
%             self.AFComplexityCalculator.DFParameters.WindowLength = self.WindowLength * self.EcgData.SamplingFrequency;
        end
    end
    
    methods (Access = private)
        function [dfResult, oiResult, scResult, windowedFrequencyPower] = ComputeWindowedDominantFrequency(self)
            samplingFrequency = self.EcgData.SamplingFrequency;
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            bandwidth = 1;
            
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            
            dfResult(numberOfChannels) = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            oiResult = dfResult;
            scResult = dfResult;
            windowedFrequencyPower = cell(numberOfWindows, 1);
            
            for channelIndex = 1:(numberOfChannels - 1)
                dfResult(channelIndex) = dfResult(numberOfChannels);
                oiResult(channelIndex) = oiResult(numberOfChannels);
                scResult(channelIndex) = scResult(numberOfChannels);
            end
            
            originalData = self.EcgData.Data();
            complexityCalculator = self.AFComplexityCalculator;
            complexityCalculator.DFParameters.FundamentalFrequency = true;
            complexityCalculator.DFParameters.Threshold = 0.5;
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing windowed dominant frequency...', 0, self));
            
            windowDF = NaN(numberOfWindows, numberOfChannels);
            expandingWindowDF = NaN(numberOfWindows, numberOfChannels);
            windowOI = NaN(numberOfWindows, numberOfChannels);
            expandingWindowOI = NaN(numberOfWindows, numberOfChannels);
            windowSC = NaN(numberOfWindows, numberOfChannels);
            expandingWindowSC = NaN(numberOfWindows, numberOfChannels);
            parfor windowIndex = 1:numberOfWindows
                windowedData = originalData(windowStart(windowIndex):windowEnd(windowIndex), :);
                
                % running window
                [~, frequencies, ~, dominantFrequencyValues, ~, spectra] = ...
                    complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
                windowDF(windowIndex, :) = dominantFrequencyValues;
                windowedFrequencyPower{windowIndex} = frequencies;
                
                regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
                windowOI(windowIndex, :) = regularityIndices;
                
                spectralConcentrations = complexityCalculator.ComputeSignalSpectralConcentration(spectra);
                windowSC(windowIndex, :) = spectralConcentrations;
                
                %expanding window
                windowedData = originalData(1:windowEnd(windowIndex), :);
                [~, ~, ~, dominantFrequencyValues, ~, spectra] = ...
                    complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
                expandingWindowDF(windowIndex, :) = dominantFrequencyValues;
                
                regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
                expandingWindowOI(windowIndex, :) = regularityIndices;
                
                spectralConcentrations = complexityCalculator.ComputeSignalSpectralConcentration(spectra);
                expandingWindowSC(windowIndex, :) = spectralConcentrations;
            end
            
            self.WindowFrequencyPower = windowedFrequencyPower;
            
            for channelIndex = 1:(numberOfChannels)
                dfResult(channelIndex).runningWindowMean = windowDF(:, channelIndex);
                dfResult(channelIndex).expandingWindowMean = expandingWindowDF(:, channelIndex);
                
                oiResult(channelIndex).runningWindowMean = windowOI(:, channelIndex);
                oiResult(channelIndex).expandingWindowMean = expandingWindowOI(:, channelIndex);
                
                scResult(channelIndex).runningWindowMean = windowSC(:, channelIndex);
                scResult(channelIndex).expandingWindowMean = expandingWindowSC(:, channelIndex);
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing windowed dominant frequency...Done', 1, self));
        end
        
        function rrResult = ComputeWindowedRRInterval(self)
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            rrResult = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            time = self.EcgData.GetTimeRange();
            rWaveTimes = time(self.RWaveIndices);
            for windowIndex = 1:numberOfWindows
                % running window
                validRRtimes = rWaveTimes(self.RWaveIndices >= windowStart(windowIndex) &...
                    self.RWaveIndices < windowEnd(windowIndex));
                if numel(validRRtimes) > 1
                    rrResult.runningWindowMean(windowIndex) = mean(diff(validRRtimes));
                    rrResult.runningWindowStd(windowIndex) = std(diff(validRRtimes));
                end
                
                % expanding window
                validRRtimes = rWaveTimes(self.RWaveIndices < windowEnd(windowIndex));
                if numel(validRRtimes) > 1
                    rrResult.expandingWindowMean(windowIndex) = mean(diff(validRRtimes));
                    rrResult.expandingWindowStd(windowIndex) = std(diff(validRRtimes));
                end
            end
        end
        
        function SetNumberOfFFTPoints(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 0
                self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints = value;
                self.AFComplexityCalculator.DFParameters.WindowLength = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints));
        end
        
        function SetDFRangeStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.AFComplexityCalculator.DFParameters.DFRange(1) = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)));
        end
        
        function SetDFRangeEnd(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.AFComplexityCalculator.DFParameters.DFRange(2) = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)));
        end
        
        function SetSpectrumRangeStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.SpectrumRange(1) = value;
            end
            set(source, 'string', num2str(self.SpectrumRange(1)));
            
            if ishandle(self.SpectrumHandle)
                set(self.SpectrumHandle, 'xlim', self.SpectrumRange);
            end
        end
        
        function SetSpectrumRangeEnd(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.SpectrumRange(2) = value;
            end
            set(source, 'string', num2str(self.SpectrumRange(2)));
            
            if ishandle(self.SpectrumHandle)
                set(self.SpectrumHandle, 'xlim', self.SpectrumRange);
            end
        end
        
        function ShowPowerSpectrum(self, source, ~, channelIndex, windowCenter)
            delete(get(self.SpectrumPanel, 'children'));
            
            currentPoint = get(source, 'currentPoint');
            selectedTime = currentPoint(1);
            time = self.EcgData.GetTimeRange();
            centerTimes = time(windowCenter);
            [~, selectedWindowIndex] = min(abs(centerTimes - selectedTime));
            powerData = self.WindowFrequencyPower{selectedWindowIndex};         
            powerData = powerData(:, channelIndex);
            dfData = self.WindowedDF(channelIndex);
            selectedDF = dfData.runningWindowMean(selectedWindowIndex);
            oiData = self.WindowedOI(channelIndex);
            selectedOI = oiData.runningWindowMean(selectedWindowIndex);
            if ~isempty(self.WindowedSC)
                scData = self.WindowedSC(channelIndex);
                selectedSC = scData.runningWindowMean(selectedWindowIndex);
            else
                selectedSC = NaN;
            end
            
            plotHandle = subplot(1,1,1, 'parent', self.SpectrumPanel);
            line('xData', [selectedDF, selectedDF], 'yData', [0, powerData(self.Frequencies(:, channelIndex)==selectedDF)],...
                'color', [1,0,0], 'lineStyle', '-', 'lineWidth', 1.5);
            line('xData', self.Frequencies(:, channelIndex), 'yData', powerData,...
                'color', [0,0,1], 'lineSmoothing', 'on', 'parent', plotHandle);
            set(plotHandle, 'xlim', self.SpectrumRange);
            xlabel(plotHandle, 'Frequency (Hz)');
            ylabel(plotHandle, 'Power');
            title(plotHandle, [self.EcgData.ElectrodeLabels{channelIndex}, ' at t=', num2str(centerTimes(selectedWindowIndex)), 's']);
            
            infoText = {...
                ['DF: ', num2str(selectedDF, '%.2f'), 'Hz'];...
                ['OI: ', num2str(100 * selectedOI, '%.0f'), '%'];...
                ['SC: ', num2str(100 * selectedSC, '%.0f'), '%']};
            
            text(1, 1, 1, infoText, 'units', 'normalized',...
                'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'right',...
                'verticalAlignment', 'top', 'parent', plotHandle);
        end
        
        function CopyWindowedDataToClipboard(self, source, ~, channelIndex)
            dfData = self.WindowedDF(channelIndex);
            oiData = self.WindowedOI(channelIndex);
            
            time = self.EcgData.GetTimeRange();
            centerTimes = time(dfData.windowCenter);
            
            UtilityPkg.num2clip([centerTimes(:), dfData.runningWindowMean(:), oiData.runningWindowMean(:)]);
        end
        
        function CopyWindowedRRDataToClipboard(self, ~, ~)
            rrData = self.WindowedRR;
            time = self.EcgData.GetTimeRange();
            centerTimes = time(rrData.windowCenter);
            
            UtilityPkg.num2clip([centerTimes(:), rrData.runningWindowMean(:), rrData.runningWindowStd(:)]);
        end
    end
end