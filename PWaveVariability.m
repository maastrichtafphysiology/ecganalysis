classdef PWaveVariability < handle
    % Wrapper class for P-Wave variability computation functions
    properties

    end

    methods
        function self = PWaveVariability()

        end
    end

    methods (Static)
        function [b2bSimilarity, b2bDistance, ECGvariability, SpatialSimilarity]...
                = ComputeSpatioTemporalVariability(pWaveMatchingData)
            drawFlag = 0;

            % suppress warnings
            warning('off', 'stats:LinearModel:RankDefDesignMat');
            warning('off', 'stats:pca:ColRankDefX');

            customData = struct(...
                'rawEcgData', pWaveMatchingData.ecgData.Data,...
                'samplingFrequency', pWaveMatchingData.ecgData.SamplingFrequency,...
                'rWaveIndices', pWaveMatchingData.rWaveIndices,...
                'ecgLabels', {pWaveMatchingData.ecgData.ElectrodeLabels},...
                'pWaveWindow', pWaveMatchingData.pWaveMatching.PWaveRange,...
                'pWaveOnsetOffset', pWaveMatchingData.onsetOffset(1:2));
            [b2bSimilarity, b2bDistance, SpatialSimilarity,...
                nB2Bgood_available, SpatSim_quality, ECGvariability] =...
                PWaveVariability.SpatioTemporalVar(customData, drawFlag);
        end
    end

    methods (Static, Access = private)
        function [Sim,ED, SpatSim,nB2Bgood_available,SpatSim_quality,ECGvar] =...
                SpatioTemporalVar(testData,drawFlag)
            % Spatio temporal variability parameters

            Rx=testData.rWaveIndices;
            ecg=testData.rawEcgData;
            fs=testData.samplingFrequency;
            listaLeads=testData.ecgLabels(:)';  % convert to 1xN cell array
            PwindowR=-testData.pWaveWindow;
            onsetOffset=testData.pWaveOnsetOffset;

            % Window around the R peak to consider for abnormal beat detection
            Blen=[80, 200]*fs/1000;

            [ecg_filt]= PWaveVariability.filtering2(ecg,0, fs); %drawflag always 0 here

            % first 3 PCs addition
            ecg_filt_prePCA=ecg_filt-mean(ecg_filt);
            [coeff, scores, latent, tsquared, explained, mu]=pca(ecg_filt_prePCA);

            ecg_filt=[ecg_filt,scores(:,1), scores(:,2) scores(:,3)];
            listaLeads=[listaLeads,'PC1', 'PC2', 'PC3'];

            % Selection of lead 2 for P analysis
            lead=find(listaLeads=="II");


            % Remove Rx too close to the beginning or the end of the recording
            while (Rx(1)-Blen(1))<=0; Rx(1)=[]; end
            while (Rx(end)+Blen(2))>size(ecg_filt,1); Rx(end)=[];end

            % Remove the abnormal beats
            [Rx3]=PWaveVariability.compute_durationQRS_Rremoval_DBscan(ecg_filt,Rx,lead,fs,Blen,.9);
            %[Rx3]=compute_durationQRS_Rremoval(ecg_filt,Rx,lead,fs,Blen,.9);

            if drawFlag
                figure,plot([1:length(ecg_filt)]/fs,ecg_filt(:,2))
                hold on,plot(Rx/fs,ecg_filt(Rx,2),'*r')
                hold on,plot(Rx3(~isnan(Rx3))/fs,ecg_filt(Rx3(~isnan(Rx3)),2),'+g')
                xlabel('Time [s]','FontSize',20)
                ylabel('Amplitude [\muV]','FontSize',20)
            end

            % PwindowR(1) = segment starting P window to R peak
            % PwindowR(2) = segment ending P window to R peak
            Pstart=round(PwindowR(1)*fs);
            Plength=round(PwindowR(1)*fs-PwindowR(2)*fs);

            % Remove the abnomal Pwave
            [ok, P_allineate, Template, buchi] =...
                PWaveVariability.analysis_Pwave5temp_PsizeR(ecg_filt,Rx3,Pstart,Plength,listaLeads,lead,fs,drawFlag);


            %Selection of a 10 ms windows around the detected P wave (10 + 10)
            Plength=length(Template);
            PmOn=floor((Pstart/fs*1000-abs(onsetOffset(1))-10)/1000*fs); % P_start from the begninning of the Pwindow (+10 ms as margin)
            PmOff=ceil((Pstart/fs*1000-abs(onsetOffset(1))+abs(onsetOffset(1)-onsetOffset(2))+10)/1000*fs); % P_end from the begninning of the Pwindow (+10 ms as margin)

            %%%%% EDIT SZ: check if PmOn and PmOff are within P wave range
            PmOn = max([1, PmOn]);
            PmOff = min([PmOff, Plength]);
            %%%%%

            if size(buchi,1)~=size(P_allineate,1)
                display('Error - contact Rita');
                return
            end

            %%  P extraction
            ok_p=zeros(size(P_allineate,1));

            m2=nan(size(P_allineate,1),PmOff-PmOn+1);  explained=nan(size(P_allineate,1),1); explainedIII=nan(size(P_allineate,1),1);

            for i=1:size(P_allineate,1) % for each beat
                for j=1:length(listaLeads) % for each lead
                    mm(j,:)=P_allineate(i,PmOn+Plength*(j-1):Plength*j-(Plength-PmOff))-nanmean(P_allineate(i,PmOn+Plength*(j-1):Plength*j-(Plength-PmOff))); %m contiene le onde P di tutti i lead per il battito i: (17 x 300)
                end

                % In some subject leads absent --> NaN
                if find(isnan(mm(:,1)))
                    mm=mm(~isnan(mm(:,1)),:);
                end


                for l= 1:size(mm,1)
                    beats.(listaLeads{l})(i,:)=mm(l,:);
                end

                % PCA beat per beat for the spatial similarity computation
                [~,~,~,~,exp]=pca(mm(1:end-3,:)');

                explained(i,1)=sum(exp(1:2));
            end


            %% For all leads: cleaning

            minpts=3;epsilon=0.07;

            if drawFlag
                figure,
            end
            for l= 1:length(listaLeads)

                [coeff]=pca(beats.(listaLeads{l})');
                labels.(listaLeads{l}) = dbscan(coeff(:,1:2),epsilon,minpts);
                %     labelsOrig.(listaLeads{l})=labels.(listaLeads{l});
                %     labelsType=unique(labels.(listaLeads{l}));


                ook(:,l)=labels.(listaLeads{l})==1;
                ok_p=find(labels.(listaLeads{l})==1); beatsOK.(listaLeads{l})=beats.(listaLeads{l})(ok_p,:);
                nok=find(labels.(listaLeads{l})~=1);buchi2.(listaLeads{l})=buchi;

                for ii=1:length(nok)
                    if nok(ii)>1; buchi2.(listaLeads{l})(nok(ii)-1,2)=0; end
                    if nok(ii)<size(buchi,1); buchi2.(listaLeads{l})(nok(ii)+1,1)=0; end
                end
                buchi2.(listaLeads{l})=buchi2.(listaLeads{l})(ok_p,:);
                if size(buchi2.(listaLeads{l}),1)~=size(beatsOK.(listaLeads{l}),1);
                    display('Error - contact Rita 2'); return;end
                clear temp cc temp2 ok_p nok

                if drawFlag
                    subplot(ceil(length(listaLeads)/6),6,l);plot(beatsOK.(listaLeads{l})')
                    xlim([-inf inf]),title(listaLeads{l},'Interpreter', 'none')
                end
                clear labelsType coeff

                % Euclidean distance b2b
                [EucDisb2B,~,~,~,~, ~, ED.(strcat(listaLeads{l},'_b2b_ED_median'))] = PWaveVariability.DisEuc_fx2(beatsOK.(listaLeads{l}),[],1,buchi2.(listaLeads{l})(:,2));

                nB2Bgood_available.(strcat(listaLeads{l}))=length(find(~isnan(EucDisb2B)));

                % Similarity
                [~,~,Sim.(strcat(listaLeads{l},'_b2b_sim_median'))]= PWaveVariability.Similarity_fx2(beatsOK.(listaLeads{l}),[],1,buchi2.(listaLeads{l})(:,2));
            end

            if drawFlag
                set(gcf,'Position',get(0, 'Screensize'),'color','white');
            end

            % spatial similarity considdering only the good P among all leads
            ook2=ook(:,1:end-3);
            explained2=explained(sum(ook2,2)==size(ook2,2));
            SpatSim=nanmedian(explained2);
            SpatSim_quality=size(explained2,1)/size(explained,1)*100;




            %% Other sources of ECG variability
            Rx4=Rx3; Rx4(find(~ok))=NaN; % considering the beat on which I computed similarity (more or less, because during the variability analysis some other P were removed,lead per lead)
            RR_Pselected=[NaN; diff(Rx4)];

            axis=PWaveVariability.compute_QRSaxis(ecg_filt,Rx4,listaLeads,0); %drawFlag always 0 here  - not so important
            axis=axis';
            Daxis=[NaN, diff(axis)];
            [~, ~,~, ~, ECGvar.axisRMSSD]=PWaveVariability.TimeDom(Daxis,isnan(Daxis));

            [~, ~,~, ~, ECGvar.rrRMSSD]=PWaveVariability.TimeDom(RR_Pselected/fs*1000,isnan(RR_Pselected));  %rr in ms!!

            %% NOISE

            %IsoSegments=Rx4(sort(RR_k(~isnan(RRs))));

            noise=nan(size(Rx,1),length(listaLeads));

            if drawFlag
                figure
            end
            for l=1:length(listaLeads)
                IsoSegments=Rx(buchi2.(listaLeads{l})(:,3));

                for i=2:length(IsoSegments)

                    x_temp=IsoSegments(i)+(onsetOffset(1)-70)/1000*fs:IsoSegments(i)+(onsetOffset(1)-20)/1000*fs;
                    Noise_temp=ecg_filt(round(x_temp),l);

                    %         LinReg=fitlm(x_temp,Noise_temp,'purequadratic');
                    %         Noise_reg.(listaLeads{l})(i,:)=(LinReg.Coefficients.Estimate(1)+LinReg.Coefficients.Estimate(2)*x_temp+LinReg.Coefficients.Estimate(3)*x_temp.^2);

                    %%%% EDIT SZ: simple linear regression instead of fitlm
                    x_samples = 1:numel(x_temp);
                    A = [ones(size(x_samples(:))), x_samples(:), x_samples(:).^2];
                    b = Noise_temp;
                    coefficients = A\b;
                    Noise_reg.(listaLeads{l})(i,:)=  A * coefficients;
                    %%%%

                    NoiseSegm.(listaLeads{l})(i,:)=Noise_temp'-Noise_reg.(listaLeads{l})(i,:);

                    if drawFlag
                        ax(l)=subplot(ceil(length(listaLeads)/6),6,l); hold on
                        plot(NoiseSegm.(listaLeads{l})(i,:)); title(listaLeads{l},'Interpreter', 'none')
                    end
                    noise(buchi2.(listaLeads{l})(i,3),l)=nanstd(NoiseSegm.(listaLeads{l})(i,:));
                end
                clear IsoSegments
                ECGvar.(strcat('NoiseLev_',listaLeads{l}))=nanmedian(noise(:,l));
            end

            if drawFlag
                linkaxes(ax,'y')
            end


        end

        function [ecg_filt] = filtering2(ecg,drawFlag, fs)
            ecg_filt=zeros(size(ecg));
            [b,a] = butter(4,.5/(fs/2),'high');  % high pass  0.5 Hz
            [b2,a2] = fir1(64,80/(fs/2)); %LowPass

            for i=1:size(ecg,2)
                if drawFlag; [pxx, f]=pwelch(ecg(:,i),[],[],[],fs);figure, plot(f,pow2db(pxx));hold on; xlabel('Frequency (Hz)');ylabel('PSD [db/Hz]');xlim([0 300]); end
                % High-Pass Filter
                ecg_temp= filtfilt(b,a,ecg(:,i));
                if drawFlag; [pxx1, f1]=pwelch(ecg_temp,[],[],[],fs); plot(f1,pow2db(pxx1),'g'); end
                % Low-Pass Filter
                ecg_temp= filtfilt(b2,a2,ecg_temp);
                [pxx1, f1]=pwelch(ecg_temp,[],[],[],fs);
                if drawFlag;
                    plot(f1,pow2db(pxx1),'o');
                end

                %% PowerLineRemoval
                PSDpartial=pxx1(f1>48 & f1<52);
                Fpartial=f1(f1>48 & f1<52);
                [~,PLF]=max(PSDpartial);
                PLF=Fpartial(PLF);
                %     IQR=quantile(ecg_temp,[.25 .75]);
                %     IQR=diff(IQR);

                %             [ecg_filt(:,i),e,f]= PowerLineRemoval(ecg_temp,fs,PLF,IQR/3000);
                [ecg_filt(:,i),e,f]= PWaveVariability.PowerLineRemoval(ecg_temp,fs,PLF,median(abs(diff(ecg_temp)))/250);
                %             [ecg_filt(:,i),e,f]= PowerLineRemoval(ecg_temp,fs,PLF,0.0125);

                if drawFlag %&& i==1;
                    [pxx2, f2]=pwelch(ecg_filt(:,i),[],[],[],fs); plot(f2,pow2db(pxx2),'ok');
                    figure('units','normalized','outerposition',[0 0 1 1]),
                    subplot(2,2,1),plot(f2(f2>5),pxx2(f2>5),'ok');hold on,plot(f1(f1>5),pxx1(f1>5));
                    xlabel('Frequency [Hz]'),ylabel('Power a.u.V^2/Hz')
                    subplot(2,2,2),plot(ecg_temp),hold on, plot(ecg_filt(:,i)),plot(e)
                    xlabel('Samples first 2 secs'),ylabel('a.u.V'),xlim([-inf inf])
                    legend({'pre50Hz','filtered50Hz','e'},'location','eastoutside')
                    subplot(2,2,3),plot(f2(f2>40 & f2<60),pxx2(f2>40 & f2<60),'ok');hold on,plot(f1(f2>40 & f2<60),pxx1(f2>40 & f2<60));
                    xlabel('Frequency [Hz]'),ylabel('Power a.u.V^2/Hz')
                    subplot(2,2,4),plot(ecg_temp(1:2*fs)),hold on, plot(ecg_filt(1:2*fs,i)),plot(e(1:2*fs))
                    xlabel('Samples first 2 secs'),ylabel('a.u.V'),xlim([-inf inf])
                    %         saveas(gcf,strcat('.',OS,'PwavesFilteringPowerLine',OS,'Filtering_',patient,'.jpg'))
                    % suptitle(patient(1:5));

                    close all
                end
            end
        end

        function [ok, P_allineate, Template, buchi] =...
                analysis_Pwave5temp_PsizeR(A,qrs_i_raw,Pstart,Plength,listaLeads,lead,fs,drawFlag)
            % Questa funzione analizza le onde P di un ECG.
            % Confronto tra shift rispetto Lead 2 (QUI) e rispetto ogni template (in analysis_Pwave6).
            % Modifico il modo di calcoalre template come beat migliore che shifta gli
            % altri 19 migliori e poi faccio media.

            % 0) Uso Pwindow di Mathias
            % 1) METTO TUTTE ONDE P IN Ptot
            % 2) METTO BATTITI DI TUTTI I LEADS SULLA STESSA RIGA
            % 3) TROVO I 20 BATTITI + CORRELATI, LI MEDIO CREANDO IL MIO TEMPLATE: solo
            % su LEADS 2 (se li consideravamo tutti: o li allineavamo, ma in quel caso
            % si assumeva un comportamento discendente che portava bias, o no e in quel
            % caso c'erano di nuovo salti importanti che portavano bias nella
            % correlazione.
            % 4) ALLINEO TUTTI I BATTITI AL TEMPLATE scartando solo quelli in cui shift
            % troppo grande (proabilmente QRS o T)


            %% Trovo onde P
            % A matrice con segnale ecg con tante colonne quanti lead fs = 1000Hz
            % qrs_i_raw posizione qrs
            for j=1:size(A,2)
                P=nan(length(qrs_i_raw),Plength);
                xx=[];
                for k=1:length(qrs_i_raw)
                    if qrs_i_raw(k)-Pstart>0
                        P(k,:)=A(qrs_i_raw(k)-Pstart:qrs_i_raw(k)-(Pstart-Plength+1),j);  % onda P x letteratura 300 ms
                        xx=[xx;k]; % indici delle righe piene
                    end
                end
                Ptot(:,:,j)=P; % #beats x 300 durata x # Leads
            end
            clear('P');


            %% Creo un array di #beats righe: in ogni riga metto in successione il singolo battito su tutte le Leads

            P_allLeads=zeros(size(Ptot,1),Plength);   % #beats x 300*17
            for i=1:size(Ptot,1) % Per ogni battito
                for j=1:size(Ptot,3) % Per ogni Leads
                    P_allLeads(i,1+Plength*(j-1):j*Plength)=Ptot(i,:,j); %no allineamento
                end
            end

            NOT_CONSIDER=isnan(P_allLeads(:,1));
            MatCorr=eye(size(P_allLeads,1));  % Teoricamente la faro Su i primi 20 battiti, per il momento la faccio su tutti
            %Usando x corr ci sta di+; cmq <.95  -> 0.7928   %Non fa il detrendo prima
            %di calcolare coeff
            for i=1:size(P_allLeads,1)
                for j=i+1:size(P_allLeads,1)
                    temp=xcorr(P_allLeads(i,1+Plength*(lead-1):Plength*lead),P_allLeads(j,1+Plength*(lead-1):Plength*lead),'coeff');   % Prendo soltanto Lead2
                    % NON POSSO USARE coorcoef (fa detrend prima di calcolare- SAREBBE
                    % OK, il prob è che corcoeff=xcorr a lag 0, quindi non ammette sfasamento
                    MatCorr(i,j)=max(temp);  %no ABS --> perche ci si aspetta che le onde siano concordi nel tempo sullo stesso lead
                    MatCorr(j,i)=max(temp);
                end
            end
            performance=nanmean(MatCorr,2);
            performance(NOT_CONSIDER)=NaN;
            [~, best]=max(performance); % ans =    0.7627  %TROPPO BASSA!!!!
            %% METTO UNA SOGLIA FITTIZIA
            % Corr_Thr=max([.9*mean(performance),.9]);
            Corr_Thr=max([.9*mean(performance),.6]);

            %Trovo i primi 20 battiti con correlazione rispetto Best maggiore della soglia
            % i=1;x=[];
            % while length(x)<20 & i<=size(MatCorr,1)
            %     if MatCorr(i,best)>Corr_Thr & i~=best
            %         x=[x;i];
            %     end
            %     i=i+1;
            % end
            %
            % % trovi i 20 battiti migliori cioe piu correlati con tutti basandoci su
            % perfomance
            [ap bp]=sort(performance,'descend');
            xp=find(ap>=Corr_Thr);
            bp2=bp(xp);%pos > soglia
            x=bp2;

            Template=nan(min(20,size(x,1)),Plength);
            Template(1,:)=Ptot(x(1),:,lead);
            for i=2:min(20,size(x,1)) % x ogni battito
                [fc, lags]=xcorr(Template(1,:),Ptot(x(i),:,lead),'coeff');
                [m(i), p]=max(fc);
                if m(i)>=Corr_Thr && abs(lags(p))<Plength/5 % se traslo + di Plength, sto di sicuro facendo un errore, sovrappondo Leads diversi!!
                    % if lags(p) diverso da zero due casi a seconda che sia
                    % maggiore o minore di zero per traslare a destra o sinistra
                    % % traslando l'onda bisogna quindi tagliarne un pezzo
                    if lags(p)>0
                        Template(i,:)=[ones(1,lags(p))*Ptot(x(i),1,lead),Ptot(x(i),1:end-lags(p),lead)];
                    end
                    if lags(p)<0
                        Template(i,:)=[Ptot(x(i),abs(lags(p))+1:end,lead),ones(1,abs(lags(p)))*Ptot(x(i),end,lead)];
                    end
                    if lags(p)==0
                        Template(i,:)=Ptot(x(i),:,lead);
                    end
                end
            end
            countok=length(find(~isnan(Template(:,1)))); % quanti beats sto considerando
            Template=nanmean(Template);

            % Old no allineamento
            % if length(x)>=20
            %     Template=mean(P_allLeads(x(1:20),Plength+1:Plength*2));    % 1x 300*17
            %     figure,title(strcat('Template su 20 beats_',nome(find(nome==OS,1,'last')+1:find(nome==OS,1,'last')+5)),'FontSize',23,'Interpreter','none')
            %     hold on
            % else % HO usato i 10 sec, quindi non posso trovare 20 battiti
            %     Template=P_allLeads(best,Plength+1:Plength*2);
            %     figure,title(strcat('Template best beat_',nome(find(nome==OS,1,'last')+1:find(nome==OS,1,'last')+5)),'FontSize',23,'Interpreter','none')
            %     hold on
            % end
            % if drawFlag
            % figure,plot(Template); title({['Template on ',num2str(countok),' beats']; ['sbj ']},'FontSize',23,'Interpreter','none');
            % end
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(Template)+50]);
            % set(gcf,'Position',get(0, 'Screensize'),'color','white','name',nome(find(nome==OS,1,'last')+1:end-9));

            ok=zeros(size(P_allLeads,1),1);%a vector of 0 of length = number of pwaves you have
            P_allineate=nan(size(P_allLeads));

            if length(ok)~=length(qrs_i_raw); display('dimensioni diverse'); return; end
            %% Allineamento
            for i=1:size(P_allLeads,1) %per ogni battito
                o=P_allLeads(i,:);
                [fc, lags]=xcorr(Template,o(1+Plength*(lead-1):Plength*lead),'coeff');
                [m(i), p]=max(fc);
                pppp(i)=p;
                if abs(lags(p))<30/1000*fs %abs(lags(p))<Plength/5 % se traslo + di Plength, sto di sicuro facendo un errore, sovrappondo Leads diversi!!
                    ok(i)=1;
                    %         plot(o)

                    % % %         if lags(p) diverso da zero due casi a seconda che sia
                    % maggiore o minore di zero per traslare a destra o sinistra

                    % % % traslando l'onda bisogna quindi tagliarne un pezzo
                    %         if lags(p)>0
                    %             o_tr=[ones(1,lags(p))*o(1),o(1:end-lags(p))];
                    %         end
                    %         if lags(p)<0
                    %             o_tr=[o(abs(lags(p))+1:end),ones(1,abs(lags(p)))*o(end)];
                    %         end
                    %         if lags(p)==0
                    %             o_tr=o;
                    %         end
                    %         P_allineate(i,:)=o_tr;

                    %        %Metodo Corino! alla fine di ogni onda devi sostiutuire la parte che viene traslata nell?altra onda con degli zeri
                    if lags(p)>0    %,o_tr2=[ones(1,lags(p))*o(1),o(1:end-lags(p))]; vecchio metodo in cui si fix solo inizio e fine e non i singoli lead
                        o_tr=[];
                        for j=1:size(Ptot,3)
                            o_tr=[o_tr, ones(1,lags(p))*Ptot(i,1,j),Ptot(i,1:end-lags(p),j)];
                        end
                    end
                    if lags(p)<0  %,o_tr=[o(abs(lags(p))+1:end),ones(1,abs(lags(p)))*o(end)];
                        o_tr=[];
                        for j=1:size(Ptot,3)
                            o_tr=[o_tr,Ptot(i,abs(lags(p))+1:end,j),ones(1,abs(lags(p)))*Ptot(i,end,j)];
                        end
                    end
                    if lags(p)==0
                        o_tr=o;
                    end
                    P_allineate(i,:)=o_tr;

                end
            end

            % x contiente la posizione delle onde che sono ben correlate con il nostro
            % template finale t quindi saranno quelle che analizziamo
            ok_idx=find(ok==1);
            P_allineate=P_allineate(find(ok==1),:);   % #beats_ok x 300*17
            Plead=P_allineate(:,1+Plength*(lead-1):Plength*lead);

            Plead_prePCA=Plead-mean(Plead,2);
            [coeff, scores, latent, tsquared, explained, mu]=pca(Plead_prePCA');

            minpts=10;epsilon=0.07;
            labels = dbscan(coeff(:,1:2),epsilon,minpts);
            labelsType=unique(labels);


            % For sbj 10_046
            % idx=find(coeff(:,1)>0.08 & coeff(:,2)>0.04);
            % idxOK=find(coeff(:,1)<0.08);
            % figure,plot(Plead_prePCA(idx,:)')
            % figure,plot(Plead_prePCA(idxOK,:)')

            % For sbj 10_228
            % idx=find(coeff(:,1)<0);
            % figure,plot(P_allineate(idx,:)')
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(P_allLeads)+50]);

            if drawFlag
                figure,
                subplot(ceil(length(labelsType)/2)+1,2,1),plot(Plead_prePCA'), title('All Pok'), xlabel('Time [samples])'), ylabel('Amplitude [a.u.]')
                subplot(ceil(length(labelsType)/2)+1,2,2),gscatter(coeff(:,1),coeff(:,2),labels); xlabel('coeff PC1'), ylabel('coeff PC2'), title('dbscan')
                for i=1:length(labelsType)
                    subplot(ceil(length(labelsType)/2)+1,2,2+i),plot(Plead_prePCA(labels==labelsType(i),:)'),
                    xlabel('Time [samples])'), ylabel('Amplitude [a.u.]'), title(labelsType(i))
                end
            end
            if sum(size(labelsType))>2 % at least 2 clusters
                LabelsTabulate=tabulate(labels);
                [~,te]=min(LabelsTabulate(:,2));
                Label2Del=LabelsTabulate(te,1);
                ok(ok_idx(find(labels==Label2Del)))=0;
                P_allineate(labels==Label2Del,:)=[];
                ok_idx(find(labels==Label2Del))=[];
            end


            %% 4 beat2beat P wave variability

            buchi=zeros(size(P_allineate,1),2);% prima colonna=1 se battito precedente valido; seconda colonna=1 se battico consecutivo valido
            differenze=diff(ok_idx);
            for i=1:size(buchi,1)-1
                if differenze(i)==1
                    buchi(i,2)=1;
                end
            end
            buchi(2:end,1)=buchi(1:end-1,2);
            buchi(:,3)=ok_idx; % original position in qrs_i_raw

            % figure,plot(P_allineate');hold on, title('P allineate','FontSize',24);
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(P_allineate)+50]);
            % set(gcf,'Position',get(0, 'Screensize'),'color','white','name',nome(find(nome==OS,1,'last')+1:end-9));
            % %saveas(gcf,strcat(nome,OS,'M1_P_allineateL2_',nome(find(nome==OS,1,'last')+1:end),'.fig'))
            %
            Pmedia_allLeads=mean(P_allineate);
            % figure;plot(Pmedia_allLeads);hold on; title(sprintf('P media su %d battiti, allLeads',size(P_allineate,1)),'FontSize',24);
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(Pmedia_allLeads)+50]);
            % set(gcf,'Position',get(0, 'Screensize'),'color','white','name',nome(find(nome==OS,1,'last')+1:end-9));
            % %saveas(gcf,strcat(nome,OS,'M1_02PmediaL2_',nome(find(nome==OS,1,'last')+1:end),'.fig'))

            % if drawFlag
            % figure,plot(P_allineate');hold on, title(sprintf('Average P on %d beats',size(P_allineate,1)),'FontSize',24);
            % plot(Pmedia_allLeads,'k','Linewidth',2)
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(P_allineate)+50]);
            %
            %
            % figure,plot(P_allLeads');hold on, title('P waves before alignment','FontSize',24);
            % for i=1:size(Ptot,3); line([1+Plength*(i-1) 1+Plength*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(P_allLeads)+50]);
            % end

            for i=1:length(listaLeads)
                Pavg(:,i)=Pmedia_allLeads(1+Plength*(i-1):Plength*(i));
            end

            if max(abs(Pavg(:)))<3.4*10^38; Pavg=single(Pavg);end
            Pavg=array2table(Pavg,'VariableNames',listaLeads);
            cancella=[]; % Elimino lead senza informazione
            for i=1:length(listaLeads)
                if length(find(isnan(Pmedia_allLeads(1+Plength*(i-1):Plength*(i)))))==length(Pmedia_allLeads(1+Plength*(i-1):Plength*(i)))
                    cancella=[cancella;i];
                end
            end
            Pavg(:,cancella)=[]; listaLeadsNoNaN=Pavg.Properties.VariableNames;

            if drawFlag
                figure,plot(table2array(Pavg)),legend(listaLeadsNoNaN,'Location','bestoutside','Interpreter','none');
                title([num2str(length(find(ok))),' Averaged beats: '],'Interpreter','none');
            end

        end

        function [Params]=ComputePwaveParam(P,Pon,Poff)

            Pwave = P(Pon:Poff);

            pPos = [];
            pNeg = [];
            if length(Pwave(Pwave > P(Pon))) > 5
                pPos=findpeaks(Pwave(Pwave > P(Pon)));
            end
            if length(-Pwave(Pwave < P(Pon))) > 5
                pNeg=findpeaks(-Pwave(Pwave < P(Pon)));
            end

            if(isempty(pPos))
                Params(1,1) = NaN;
            else
                Params(1,1) = max(pPos)-P(Pon);
            end

            if(isempty(pNeg))
                Params(1,2) = NaN;
            else
                Params(1,2) = -max(pNeg)-P(Pon);
            end

            Params(1,3) = Poff-Pon;
            Params=Params(:);
        end

        function [Rx]=compute_durationQRS_Rremoval_DBscan(ecg_filt,Rx,V5,fs,Blen,soglia)


            for j=1:size(ecg_filt,2) %per ogni lead
                B=zeros(length(Rx),sum(Blen));
                xx=[];
                for k=1:length(Rx)
                    if Rx(k)-Blen(1)>0 & Rx(k)+Blen(2)<=size(ecg_filt,1)  % esclude i Nan, perché gli estremi sono già stati eliminati
                        B(k,:)=ecg_filt(Rx(k)-Blen(1):Rx(k)+Blen(2)-1,j);
                        xx=[xx;k]; % indici delle righe piene
                    end
                end
                Btot(:,:,j)=B(xx,:); % #beats x durata x # Leads
                % templM2(:,j)=mean(B(xx,:),1); % 550*17, senza allineamento. Poi trasponevo e fine (funzione voleva tempM2 17 x durata)
            end
            if size(Btot,1)~=(length(Rx)-length(find(isnan(Rx))))
                fprintf('\n\n Error!!!!!!!!!\n\n'); return;
            end

            B_allLeads=zeros(size(Btot,1),sum(Blen));   % #beats x 150*17
            for i=1:size(Btot,1) % Per ogni battito
                for j=1:size(Btot,3) % Per ogni Leads
                    B_allLeads(i,1+sum(Blen)*(j-1):j*sum(Blen))=Btot(i,:,j); %no allineamento
                end
            end

            MatCorr=eye(size(B_allLeads,1));
            %Usando x corr ci sta di+; cmq <.95  -> 0.7928   %Non fa il detrend prima di calcolare coeff
            parfor i=1:size(B_allLeads,1)
                v=zeros(1,size(B_allLeads,1));
                for j=i+1:size(B_allLeads,1)
                    %temp=xcorr(B_allLeads(i,sum(Blen)+1:sum(Blen)*2),B_allLeads(j,sum(Blen)+1:sum(Blen)*2),'coeff');   % Prendo soltanto Lead2
                    temp=xcorr(Btot(i,:,V5),Btot(j,:,V5),'coeff');
                    % NON POSSO USARE coorcoef (fa detrend prima di calcolare- SAREBBE
                    % OK, il prob è che corcoeff=xcorr a lag 0, quindi non ammette sfasamento
                    v(j)=max(temp);
                end
                MatCorr(i,:)=v;
            end
            MatCorr=triu(MatCorr)+triu(MatCorr,1)'+eye(size(B_allLeads,1));

            performance=mean(MatCorr,2);[~, best]=max(performance);
            Corr_Thr=max([.9*mean(performance),.6]); % SOglia minima, in caso corr troppo bassa

            % trovi i 20 battiti migliori cioe piu correlati con tutti basandoci su perfomance
            [ap bp]=sort(performance,'descend');
            xp=find(ap>=Corr_Thr); bp2=bp(xp);%pos > soglia
            if length(bp2)>=20
                Template=mean(B_allLeads(bp2(1:20),sum(Blen)*(V5-1)+1:sum(Blen)*V5));    % 1x 550*17
                % figure,title('Template su 20 beats','FontSize',23), hold on
            else % troppi pochi sopra soglia
                Template=B_allLeads(best,sum(Blen)*(V5-1)+1:sum(Blen)*V5);
                %  figure,title('Template best beat','FontSize',23), hold on
            end
            %plot(Template);

            ok=zeros(size(B_allLeads,1),1);%a vector of 0 of length = number of pwaves you have
            Beat_allin=zeros(size(B_allLeads));

            parfor i=1:size(B_allLeads,1) % per ogni battito
                o=B_allLeads(i,:); %al posto di Btot sotto potevo mettere o(sum(Blen)+1:sum(Blen)*2)
                [fc, lags]=xcorr(Template,Btot(i,:,V5),'coeff'); %confronto solo Lead V5
                [m(i), p]=max(fc);
                pppp(i)=p; % prima riga seguente aveva soglia a 150 ms, ora 30 ms
                if m(i)>soglia && abs(lags(p))<30/1000*fs % se traslo + di sum(Blen), sto di sicuro facendo un errore, sovrappondo Leads diversi!!
                    ok(i)=1;
                    %         plot(o)

                    % % %         if lags(p) diverso da zero due casi a seconda che sia
                    % maggiore o minore di zero per traslare a destra o sinistra

                    % % % traslando l'onda bisogna quindi tagliarne un pezzo

                    if lags(p)>0    %,o_tr2=[ones(1,lags(p))*o(1),o(1:end-lags(p))]; vecchio metodo in cui si fix solo inizio e fine e non i singoli lead
                        o_tr=[];
                        for j=1:size(ecg_filt,2)
                            o_tr=[o_tr, ones(1,lags(p))*Btot(i,1,j),Btot(i,1:end-lags(p),j)];
                        end
                    end
                    if lags(p)<0  %,o_tr=[o(abs(lags(p))+1:end),ones(1,abs(lags(p)))*o(end)];
                        o_tr=[];
                        for j=1:size(ecg_filt,2)
                            o_tr=[o_tr,Btot(i,abs(lags(p))+1:end,j),ones(1,abs(lags(p)))*Btot(i,end,j)];
                        end
                    end
                    if lags(p)==0
                        o_tr=o;
                    end
                    Beat_allin(i,:)=o_tr;
                end
            end

            % x contiente la posizione delle onde che sono ben correlate con il nostro
            % template finale t quindi saranno quelle che analizziamo

            ok_idx=find(ok==1);
            Beat_allin=Beat_allin(find(ok==1),:);   % #beats_ok x 550*17

            Plead=Beat_allin(:,sum(Blen)*(V5-1):sum(Blen)*V5);

            Plead_prePCA=Plead-mean(Plead,2);
            [coeff, scores, latent, tsquared, explained, mu]=pca(Plead_prePCA');

            minpts=10;epsilon=0.07;
            labels = dbscan(coeff(:,1:2),epsilon,minpts);
            labelsType=unique(labels);

            % figure,
            % subplot(ceil(length(labelsType)/2)+1,2,1),plot(Plead_prePCA'), title('All Pok'), xlabel('Time [samples])'), ylabel('Amplitude [a.u.]')
            % subplot(ceil(length(labelsType)/2)+1,2,2),gscatter(coeff(:,1),coeff(:,2),labels); xlabel('coeff PC1'), ylabel('coeff PC2'), title('dbscan')
            % for i=1:length(labelsType)
            %     subplot(ceil(length(labelsType)/2)+1,2,2+i),plot(Plead_prePCA(labels==labelsType(i),:)'),
            %     xlabel('Time [samples])'), ylabel('Amplitude [a.u.]'), title(labelsType(i))
            % end


            if sum(size(labelsType))>2 % at least 2 clusters
                LabelsTabulate=tabulate(labels);
                [~,te]=min(LabelsTabulate(:,2));
                Label2Del=LabelsTabulate(te,1);
                ok(ok_idx(find(labels==Label2Del)))=0;
            end

            Rx(xx(ok==0))=NaN;


            %
            % figure,plot(Beat_allin'),hold on, title('Beat allineati','FontSize',24)
            % for i=1:size(Btot,3); line([1+sum(Blen)*(i-1) 1+sum(Blen)*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(Beat_allin)+50]);
            % set(gcf,'Position',get(0, 'Screensize'),'color','white','name',nome(find(nome==OS,1,'last')+1:end-9));
            %
            % BeatM_allLeads=mean(Beat_allin,1);
            % figure,plot(BeatM_allLeads);hold on; title(sprintf('Beat medi su %d battiti, allLeads',size(Beat_allin,1)),'FontSize',24);
            % for i=1:size(Btot,3); line([1+sum(Blen)*(i-1) 1+sum(Blen)*(i-1) ],ylim,'color','k','Linewidth',2);end; xlim([0 length(BeatM_allLeads)+50]);
            % set(gcf,'Position',get(0, 'Screensize'),'color','white','name',nome(find(nome==OS,1,'last')+1:end-9));
        end

        function [axis]=compute_QRSaxis(ecg_filt,Rx,listaLead,drowflag)%(templM2)
            % Function to compute R axis: QUADRANT METHOD


            I=ecg_filt(:,listaLead=="I");
            aVF=ecg_filt(:,listaLead=="aVF");
            axis=nan(length(Rx),1);
            parfor i=1:length(Rx) %per ogni lead
                if ~isnan(Rx(i))
                    [theta,rho] = cart2pol(I(Rx(i)),-aVF(Rx(i)));
                    axis(i)=-rad2deg(theta);
                end
            end

            if drowflag
                figure,plot(axis,'ok')

                figure,
                for i=1:6
                    %  if ismember({},listaLead{i}
                    h(i)=subplot(6,1,i),plot(ecg_filt(1:10000,i)), hold on, plot(Rx(Rx<10000),ecg_filt(Rx(Rx<10000),i),'+r')
                    ylabel(listaLead{i})
                end

                linkaxes(h,'xy')
            end

        end

        function [ED, diAVG, diMedian, diNAVG, diNMedian, diNbisAVG,diNbisMedian,diMad,diNMad,diNbisMad] = DisEuc_fx2(Pwaves,Template,Passi,buchi)
            %DisEuc_fx: Function calculating the euclidean distance, or towards a
            %Template, o between waves in different times

            %% Input
            %   Pwaves: Matrix of aligned (through crosscorrelation, excluding P waves vith max coeff<=0.8)
            %           P waves # N beats x Length.
            %           After alignment, the Pwaves with max crosscoeff with the average
            %            < threshold (empirically chosen 0.9) are excluded.
            %           Each P wave starts 30 ms before Ponset (or the maximum available <------------------------------------------------------
            %          if less than 30 ms are present) and ends 30 ms after P offset
            %   Template: If Template=[], the similarity is computed each waves vs all
            %   the others OR the first N=Passi waves
            %   Passi: If Template=[], you can chose how many waves to consider:
            %       Passi=1: each wave vs consecutive
            %       Passi=1:2; each waves vs the 2s after
            %       Passi= # beats: each beat vs all the others
            %   buchi= vector of the same dimension od the number of beats. 1 if the
            %          following beat is valid, 0 if not. Thus last value always 0
            %% Outputs
            % Average, Median and Mad values across the beats for:
            %   -dis normalized with the length of the Window  (diAVG,diMedian,diMad)
            %   -dis normalized with the RMS of the first beat (diNAVG,diNMedian,diNMad)
            %   -dis normalized with the RMS of the second beat
            %       (diNbisAVG,diNbisMedian,diNbisMedian

            % example for euclidean distance with only the immediately following beat -->
            %          =DisEuc_fx2(Pwaves,[],1,buchi);

            %% Function

            if ~isempty(Template)
                %  euclidean distance beat2template
                di=nan(size(Pwaves,1),1);
                diN=nan(size(Pwaves,1),1);
                for ii=1:size(Pwaves,1)
                    di(ii,1)=pdist2(Pwaves(ii,:),Template)/size(Pwaves,2);
                    diN(ii,1)=pdist2(Pwaves(ii,:),Template)/sqrt(sum(Pwaves(ii,:).^2));
                end
                diAVG=nanmean(di);diMedian=nanmedian(di); diMad=mad(di,1);
                diNAVG=nanmean(diN);diNMedian=nanmedian(diN);diNMad=mad(diN,1);

            else
                %2. euclidean distance beat2beat
                di2=ones(size(Pwaves,1))*nan;
                di2N=ones(size(Pwaves,1))*nan;
                di3N=ones(size(Pwaves,1))*nan;
                di4N=ones(size(Pwaves,1))*nan;
                for ii=1:size(Pwaves,1)-1
                    for jj=ii+Passi(1):min([size(Pwaves,1) ii+Passi(end)])
                        di2(ii,jj)=pdist2(Pwaves(ii,:),Pwaves(jj,:))/size(Pwaves(ii,:),2);
                        di2N(ii,jj)=pdist2(Pwaves(ii,:),Pwaves(jj,:))/sqrt(sum(Pwaves(ii,:).^2));
                        if sum(size(Passi))~=2 % I fill the other side of the matrix
                            di3N(jj,ii)=pdist2(Pwaves(ii,:),Pwaves(jj,:))/sqrt(sum(Pwaves(jj,:).^2));
                            di4N(ii,jj)= di2N(ii,jj); di4N(jj,ii)= di3N(jj,ii);
                        elseif exist('buchi') && buchi(ii)==0
                            % if the following beat in the vector is not the real consecutive one (abormal beat in between)
                            di2(ii,jj)=NaN; di2N(ii,jj)=NaN; di3N(ii,jj)=NaN;
                        elseif exist('buchi') && buchi(ii)==1  % ADDED 19/11/2019
                            di3N(ii,jj)=pdist2(Pwaves(ii,:),Pwaves(jj,:))/sqrt(sum(Pwaves(jj,:).^2));
                        end
                    end
                end
            end


            diAVG=nanmean(di2(:));diMedian=nanmedian(di2(:));diMad=mad(di2(:),1);
            diNAVG=nanmean(di2N(:));diNMedian=nanmedian(di2N(:));diNMad=mad(di2N(:),1);
            diNbisAVG=nanmean(di3N(:));diNbisMedian=nanmedian(di3N(:));diNbisMad=mad(di3N(:),1);

            if sum(size(Passi))~=2
                diNAVG=nanmean(di4N(:));diNMedian=nanmedian(di4N(:));diNMad=mad(di4N(:),1);
                diNbisAVG=nanmean(di4N(:));diNbisMedian=nanmedian(di4N(:));diNbisMad=mad(di4N(:),1);
                ED=nanmedian(di4N,2);
            else
                ED=diag(di3N,1);
            end
        end

        function [y,e,f]=PowerLineRemoval(x, fs, powLineFreq, d)


            %   x = input signal
            %   fs = sampling frequency
            %   powLineFreq = freqquncy of powerline (50 vs 60 Hz)
            %   d = adaptive step = defualt 1)

            if nargin < 4   d =1;   end;

            N = cos(2*pi*powLineFreq/fs);
            e = [0 0];
            for n=2:length(x)-1
                e(n+1)= 2*N*e(n)-e(n-1);
                y(n+1)=x(n+1)-e(n+1);
                f(n+1) = (x(n+1)-e(n+1))-(x(n)-e(n));
                if f(n) > 0
                    e(n+1) = e(n+1)+d;
                else
                    e(n+1) = e(n+1)-d;
                end
            end

        end

        function [Sim, smean, smedian, smad] = Similarity_fx2(Pwaves,Template,Passi,buchi)
            %Similarity_fx: Function calculating the similarity, or towards a
            %Template, o between waves in different times

            %% Input
            %   Pwaves: Matrix of aligned (through crosscorrelation, excluding P waves vith max coeff<=0.8)
            %           P waves # N beats x Length.
            %           After alignment, the Pwaves with max crosscoeff with the average
            %           < threshold (empirically chosen 0.9) are excluded
            %           Each P wave starts 30 ms before Ponset (or the maximum available <------------------------------------------------------
            %           if less than 30 ms are present) and ends 30 ms after P offset
            %   Template: If Template=[], the similarity is computed each waves vs all
            %   the others OR the first N=Passi waves
            %   Passi: If Template=[], you can chose how many waves to consider:
            %       Passi=1: each wave vs consecutive
            %       Passi=1:2; each waves vs the 2s after
            %       Passi= # beats: each beat vs all the others
            %   buchi= vector of the same dimension od the number of beats. 1 if the
            %          following beat is valid, 0 if not. Thus last value always 0
            %% Outputs
            % Average, Median and Mad Similarity across the beats.
            % example for similarity with only following beats -->
            %          =Similarity_fx2(Pwaves,[],1,buchi);

            %% Function

            if ~isempty(Template)
                %  similarity beat2template
                s3=ones(size(Pwaves,1),1)*nan;
                for ii=1:size(Pwaves,1)
                    s3(ii)=Pwaves(ii,:)*Template'/(norm(Pwaves(ii,:))*norm(Template));
                end
                smean=nanmean(s3(:));smedian=nanmedian(s3(:));smad=mad(s3(:),1);

            else
                %2. similarity euclidea beat2beat
                s=ones(size(Pwaves,1))*nan;
                for ii=1:size(Pwaves,1)-1
                    for jj=ii+Passi(1):min([size(Pwaves,1) ii+Passi(end)])  % all waves
                        s(ii,jj)=Pwaves(ii,:)*Pwaves(jj,:)'/(norm(Pwaves(ii,:))*norm(Pwaves(jj,:)));
                        if sum(size(Passi))==2 && buchi(ii)==0 % modificato jj-->ii il 19/11/28
                            % if the following beat in the vector is not the real consecutive one (abormal beat in between)
                            s(ii,jj)=NaN;
                        end
                    end
                end

                smean=nanmean(s(:));smedian=nanmedian(s(:));smad=mad(s(:),1);
                Sim=nanmedian(s,2);
            end
        end

        function [M SD SDd NNx MSSD]=TimeDom(rr,rr_l);

            %[M SD SDd NNx MSSD]=TimeDom(rr,zeros(length(rr),1));  %rr deve essere in ms!!

            %% Input
            % RR originale
            % rr_l label dell'rr  da old: (0= buoni battiti)

            %% Output
            % Media, Standard Deviation, SD di SD, pnm10/20/30..., Rmssd

            rr2=rr(find(rr_l==0));% qui ci sono meno rr, solo quelli buoni!
            M=mean(rr2);
            SD=std(rr2);

            drr=diff(rr);
            drr_l=zeros(length(drr),1);
            x=find(rr_l==1);
            % drr_l(x)=1;
            % drr_l(x+1)=1;
            if x
                if x(1)~=1
                    drr_l(x-1)=1;
                else
                    drr_l(x(2:end)-1)=1;
                end
            end
            drr_l(x)=1;
            drr_l=drr_l(1:length(drr));
            drr=drr(find(drr_l==0));
            SDd=std(drr);
            for px=10:10:100
                NNx(px/10) = sum(abs(drr)>px)*100/length(drr); %%%aggiungere pNNx con ciclo
            end
            MSSD = sqrt(mean(drr.^2));

        end
    end
end