classdef TEECGPlotVisualizationPanel < ECGPlotSelectionPanel
    properties
        ValidTEChannels
    end
    
    properties (Access = protected)
        TEAxes
        TEECGLines
        FilteredTEData
      
        TEPanel
        TEListbox
        SelectedTEChannels
        
        ValidTEChannelPanel
        ValidTEChannelsListbox
    end
    
    methods
        function self = TEECGPlotVisualizationPanel(position)
            self = self@ECGPlotSelectionPanel(position);
        end
        
        function SetECGData(self, ecgData)
            self.SelectedTEChannels = 1;
            self.ValidTEChannels = 1:ecgData.GetNumberOfTEChannels;
            
            SetECGData@ECGPlotSelectionPanel(self, ecgData);
        end
        
        function ecgData = GetECGDataToAnalyze(self)
            if ~isempty(self.ValidChannels)
                validChannels = {self.ValidChannels, self.ValidTEChannels};
                ecgData = self.EcgData.Copy(validChannels, self.ValidTime);
            else
                ecgData = TEECGData.empty(0);
            end
        end
    end
    
    methods (Access = protected)
        function  SetChannelList(self)
            SetChannelList@ECGPlotSelectionPanel(self);
           
            set(self.TEListbox, 'string', self.EcgData.TELabels);
            set(self.TEListbox, 'value', find(self.SelectedTEChannels));
            
            set(self.ValidTEChannelsListbox, 'string', self.EcgData.TELabels);
            set(self.ValidTEChannelsListbox, 'value', find(self.ValidTEChannels));
        end
        
        function ShowECGData(self)
            self.ShowTEData();
            
            ShowECGData@ECGPlotSelectionPanel(self);
        end
        
        function ShowTEData(self)
            lineData = self.EcgData.TEData(:, self.SelectedTEChannels);
            lineLabels = self.EcgData.TELabels(self.SelectedTEChannels);
            
            self.TEECGLines = plot(self.TEAxes,...
                1, 1:numel(self.SelectedTEChannels),...
                '-');
            
            self.TEAxes.XAxis.Exponent = 0;
            self.TEAxes.XAxis.ExponentMode = 'manual';
            self.TEAxes.YAxis.Exponent = 0;
            self.TEAxes.YAxis.ExponentMode = 'manual';
            
            self.TEAxes.XLabel.String = 'Time (seconds)';
            self.TEAxes.TickLength = [0.001,0.001];
            self.TEAxes.YLabel.String = 'Channel';
            
            [signalPosition, signalAddition] = ECGPlotChannelPanel.AutoStackNoOverlap(lineData);
            
            self.TEAxes.YTick = flip(signalPosition);
            self.TEAxes.YTickLabel = flip(lineLabels(:));
            self.TEAxes.TickLabelInterpreter = 'none';
            
            visualizedSignals = bsxfun(@plus, lineData, signalAddition);
            signalRange = [min(visualizedSignals(:)), max(visualizedSignals(:))];
            dlt = diff(signalRange) / 50;
            signalRange(1) = signalRange(1) - dlt;
            signalRange(2) = signalRange(2) + dlt;
            self.TEAxes.YLimMode = 'manual';
            self.TEAxes.YLim = signalRange;
        end
        
        function CreateECGAxes(self)
            CreateECGAxes@ECGPlotSelectionPanel(self);
            
            self.TEAxes = axes(...
                'Parent', self.AxesPanel,...
                'TickLabelInterpreter','none',...
                'ActivePositionProperty','Position');
        end
        
        function CreateChannelSelection(self)
            CreateChannelSelection@ECGPlotSelectionPanel(self);
            
            set(self.VisibleChannelPanel, 'position', [0 .5 .5 .5]);
            set(self.ValidChannelPanel, 'position', [.5 .5 .5 .5]);
            
            self.TEPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [0 .2 .5 .3],...
                'title', 'TE-ECG',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.TEListbox = uicontrol(...
                'parent', self.TEPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetTEChannels,...
                'busyAction', 'cancel');
            
            self.ValidTEChannelPanel = uipanel('parent', self.ChannelPanel,...
                'units', 'normalized', ...
                'position', [.5 .2 .5 .3],...
                'title', 'Channels to analyze',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.ValidTEChannelsListbox = uicontrol(...
                'parent', self.ValidTEChannelPanel,...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'style', 'listbox',...
                'backgroundColor', [1 1 1],...
                'min', 0, 'max', 10,...
                'callback', @self.SetValidTEChannels,...
                'busyAction', 'cancel');
        end
        
        function InitializeControls(self, varargin)
            InitializeControls@ECGPlotSelectionPanel(self, varargin{:});
            
            mainAxisPosition = get(self.MainAxes, 'position');
            teAxisPosition = [mainAxisPosition(1:3), mainAxisPosition(4) /2];
            mainAxisPosition = [mainAxisPosition(1),...
                mainAxisPosition(2) + mainAxisPosition(4) /2,...
                mainAxisPosition(3), mainAxisPosition(4) /2];
            
            set(self.MainAxes, 'position', mainAxisPosition);
            set(self.TEAxes, 'units', 'normalized', 'position', teAxisPosition);
        end
        
        function ReDraw(self, varargin)
            ReDraw@ECGPlotSelectionPanel(self, varargin{:});
            
            lineData = self.FilteredTEData(:, self.SelectedTEChannels);
            lineLabels = self.EcgData.TELabels(self.SelectedTEChannels);
            
            self.PlotLines(lineData, lineLabels, self.TEAxes, self.TEECGLines);
        end
        
        function UpdateFilteredData(self, redrawPlot)
            self.FilteredTEData = self.EcgData.TEData;
            if self.ApplyBandpass
                bandpassFilter = designfilt('bandpassiir', 'FilterOrder', 4,...
                    'HalfPowerFrequency1', self.BandpassFrequencies(1),...
                    'HalfPowerFrequency2', self.BandpassFrequencies(2),...
                    'SampleRate', self.EcgData.SamplingFrequency);
                self.FilteredTEData = filtfilt(bandpassFilter, self.FilteredTEData);
            end
            
            if self.ApplyNotch
                for notchIndex = 1:numel(self.NotchFrequency)
                    notchFrequency = self.NotchFrequency(notchIndex);
                    bandstopFilter = designfilt('bandstopiir', 'FilterOrder',2,...
                        'HalfPowerFrequency1', notchFrequency  - self.NotchWidth / 2,...
                        'HalfPowerFrequency2', notchFrequency + self.NotchWidth / 2,...
                        'SampleRate', self.EcgData.SamplingFrequency);
                    self.FilteredTEData = filtfilt(bandstopFilter, self.FilteredTEData);
                end
            end
            
            UpdateFilteredData@ECGPlotSelectionPanel(self, redrawPlot);
        end
        
        function SetTEChannels(self, varargin)
            self.SelectedTEChannels = get(self.TEListbox, 'value');
            self.ShowECGData();
        end
        
        function SetValidTEChannels(self, varargin)
            self.ValidTEChannels = get(self.ValidTEChannelsListbox, 'value');
            self.ShowECGData();
        end
    end
end