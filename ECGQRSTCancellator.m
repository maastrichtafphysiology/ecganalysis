classdef ECGQRSTCancellator < QRSTCancellator
    properties
        QRSResidueReduction
        QRSRange
        AROrder
        VRThreshold
    end
    
    properties (Constant)
        CANCELLATION_METHODS = {...
            'ABS',...
            'ABS clustered',...
            'Adaptive SVD',...
            'Bayesian',...
            'PCA'}
    end
    
    methods
        function self = ECGQRSTCancellator()
            self = self@QRSTCancellator();
            
            self.VentricularRefractoryPeriod = 0.2;
            self.CancellationMethod = ECGQRSTCancellator.CANCELLATION_METHODS{2};
            self.QRSTRange = [.5 .9];
            self.ClusterSensitivity = 0.75;
            self.ShowDiagnostics = true;
            
            self.TemplateShift = 0.005;
            self.RPeakSearchWindow = [0.03 0.03];
            self.IntegrationWindowLength = 0.05;
            
            self.QRSResidueReduction = true;
            self.QRSRange = [0.05 0.05];
            self.AROrder = 10;
            self.VRThreshold = 0.5;
            
            self.InterpolationFactor = 4;
            self.HighpassPostFilter = 1;
        end
        
        function rWaveIndices = DetectWindowRWaves(self, ecgData, referenceChannel)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            data = ecgData.ReferenceData(:, referenceChannel);
            signal = data - mean(data);
            
            order = 3;
            nyquistFrequency = ecgData.SamplingFrequency / 2;
            
            [b, a] = cheby2(order, 20,...
                [7 / nyquistFrequency 17 / nyquistFrequency]);
            filteredSignal = filtfilt(b, a, signal);
            signal = abs(diff(filteredSignal));
            
            featureWindowWidth = 200;
            thresholdSignalWindowWidth = 3000;
            
            % s1(n)
            featureWindow = ones(featureWindowWidth + 1, 1) / (featureWindowWidth + 1);
            featureSignal = conv(signal, featureWindow);
            featureSignal = featureSignal((featureWindowWidth / 2):(end - (featureWindowWidth / 2)));
            
            % thresholds
            thresholdWindow1 = ones(thresholdSignalWindowWidth + 1, 1) / (thresholdSignalWindowWidth + 1);
            thresholdSignal1 = conv(signal, thresholdWindow1);
            thresholdSignal1 = thresholdSignal1((thresholdSignalWindowWidth / 2):(end - (thresholdSignalWindowWidth / 2)));
            
            geometricMean = sqrt(featureWindowWidth * thresholdSignalWindowWidth);
            thresholdWindow2 = ones(floor(geometricMean / 2) + ceil(geometricMean / 2) + 1, 1) / ...
                (floor(geometricMean / 2) + ceil(geometricMean / 2) + 1);
            thresholdSignal2 = conv(signal, thresholdWindow2);
            thresholdSignal2 = thresholdSignal2(ceil(geometricMean / 2):(end - floor(geometricMean / 2)));
            
            thresholdSignal = (featureSignal + thresholdSignal1 + thresholdSignal2) / 3;
            
            % initial QRS detection
            aboveThreshold = featureSignal > thresholdSignal;
            abovethresholdStart = find(diff(aboveThreshold) > 0);
            abovethresholdEnd = find(diff(aboveThreshold) < 0);
            if abovethresholdStart(1) > abovethresholdEnd(1)
                abovethresholdStart = [1; abovethresholdStart];
            end
            if abovethresholdStart(end) > abovethresholdEnd(end)
                abovethresholdEnd = [abovethresholdEnd; numel(featureSignal)];
            end
            
            % T-wave avoidance
            tWaveAvoidanceSignal = thresholdSignal;
            for intervalIndex = 1:numel(abovethresholdStart)
                tWaveAvoidanceSignal(abovethresholdStart(intervalIndex):abovethresholdEnd(intervalIndex)) =...
                    max(tWaveAvoidanceSignal(abovethresholdStart(intervalIndex):abovethresholdEnd(intervalIndex)));
            end
            
            decayFactor = 20 / self.SamplingFrequency;
            for sampleIndex = 2:numel(thresholdSignal)
                if ~aboveThreshold(sampleIndex)
                    tWaveAvoidanceSignal(sampleIndex) = decayFactor * thresholdSignal(sampleIndex) +...
                        (1 - decayFactor) * tWaveAvoidanceSignal(sampleIndex - 1);
                end
            end
            
            % QRS detection with T-wave avoidance
            aboveThreshold = featureSignal > tWaveAvoidanceSignal;
            abovethresholdStart = find(diff(aboveThreshold) > 0);
            abovethresholdEnd = find(diff(aboveThreshold) < 0);
            if abovethresholdStart(1) > abovethresholdEnd(1)
                abovethresholdStart = [1; abovethresholdStart];
            end
            if abovethresholdStart(end) > abovethresholdEnd(end)
                abovethresholdEnd(end) = [abovethresholdEnd; numel(featureSignal)];
            end
            
            % merging
            mergeThreshold = 0.05 * self.SamplingFrequency;
            validStart = true(size(abovethresholdStart));
            validEnd = true(size(abovethresholdEnd));
            for intervalIndex = 1:(numel(abovethresholdEnd) - 1)
                if abovethresholdStart(intervalIndex + 1) - abovethresholdEnd(intervalIndex) < mergeThreshold
                    validStart(intervalIndex + 1) = false;
                    validEnd(intervalIndex) = false;
                end
            end
            abovethresholdStart = abovethresholdStart(validStart);
            abovethresholdEnd = abovethresholdEnd(validEnd);
            
            % delete short intervals
            validIntervals = abovethresholdEnd - abovethresholdStart > mergeThreshold;
            abovethresholdStart = abovethresholdStart(validIntervals);
            abovethresholdEnd = abovethresholdEnd(validIntervals);
            
            % determine R-peaks
            rWaveIndices = NaN(size(abovethresholdStart));
            for intervalIndex = 1:numel(abovethresholdStart)
                [~, maxIndex] = max(signal(abovethresholdStart(intervalIndex):abovethresholdEnd(intervalIndex)));
                rWaveIndices(intervalIndex) = abovethresholdStart(intervalIndex) + maxIndex - 1;
            end
            
            if self.ShowDiagnostics
                time = ecgData.GetTimeRange();
                figure;
                plotHandles = NaN(5,1);
                plotHandles(1) = subplot(5, 1, 1);
                plot(time, data);
                title('Original');
                line('xData', time(rWaveIndices), 'yData', data(rWaveIndices),...
                    'parent', plotHandles(1), 'lineStyle', 'none',...
                    'marker', 'o');
                
                plotHandles(2) = subplot(5, 1, 2);
                plot(time, filteredSignal);
                title('Bandpass');
                
                plotHandles(3) = subplot(5, 1, 3);
                plot(time(1:(end - 1)), signal);
                title('Rectified');
                
                plotHandles(4) = subplot(5, 1, 4);
                plot(time, featureSignal);
                title('Feature signal + threshold signal');
                line('xData', time, 'yData', thresholdSignal,...
                    'parent', plotHandles(4), 'lineStyle', '--');
                
                plotHandles(5) = subplot(5, 1, 5);
                plot(time, featureSignal);
                title('Feature signal + T-wave avoidance signal');
                line('xData', time, 'yData', tWaveAvoidanceSignal,...
                    'parent', plotHandles(5), 'lineStyle', '--');
                
                validHandles = ishandle(plotHandles);
                
                linkaxes(plotHandles(validHandles), 'x');
            end
        end
        
        function cancelledEcgData = CancelQRST(self, ecgData, rWaveIndices, memberIndices)
            if nargin < 4
                memberIndices = ones(size(rWaveIndices, 2), 1);
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Performing QRST cancellation...', 0, self));
            
            % sort rWaveIndices
            rWaveIndices = sort(rWaveIndices, 'ascend');
            
%             profile on;
            
            cancelledEcgData = CancelQRST@QRSTCancellator(self, ecgData, rWaveIndices, memberIndices);
            
            if self.QRSResidueReduction
                cancelledEcgData = self.ReduceQRSResidue(cancelledEcgData, rWaveIndices);
            end
            
%             profile viewer;
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Performing QRST cancellation...Done', 1, self));
        end
        
        function reducedEcgData = ReduceQRSResidue(self, ecgData, rWaveIndices)
            filterOrder = self.AROrder;
            qrInterval = round(ecgData.SamplingFrequency * self.QRSRange(1));
            rsInterval = round(ecgData.SamplingFrequency * self.QRSRange(2));
            
            intervalStart = [1; rWaveIndices + rsInterval];
            intervalEnd = [rWaveIndices - qrInterval; ecgData.GetNumberOfSamples];
            validIntervals = intervalStart >= 1 & intervalEnd <= ecgData.GetNumberOfSamples &...
                [true; diff(rWaveIndices) > qrInterval + rsInterval + 1; true] &...
                intervalStart < intervalEnd;
            intervalStart = intervalStart(validIntervals);
            intervalEnd = intervalEnd(validIntervals);
            
            VRIndices = ECGQRSTCancellator.ComputeVRIndex(ecgData, [intervalStart, intervalEnd]);
            
            interpolatedData = ecgData.Data;
            for channelIndex = 1:ecgData.GetNumberOfChannels
                for intervalIndex = 1:(numel(intervalStart) - 1)
                    if VRIndices(intervalIndex, channelIndex) <= self.VRThreshold
                        continue;
                    end
                    
                    signal = ecgData.Data(intervalStart(intervalIndex):intervalEnd(intervalIndex + 1), channelIndex);
                    interpolatedSignal = ECGQRSTCancellator.LSAR(signal, intervalEnd(intervalIndex) - intervalStart(intervalIndex),...
                        intervalStart(intervalIndex + 1) - intervalStart(intervalIndex), filterOrder);
                    interpolatedData(intervalStart(intervalIndex):intervalEnd(intervalIndex + 1), channelIndex) = interpolatedSignal;
                end
            end
            
            reducedEcgData = ecgData.Copy();
            reducedEcgData.Data = interpolatedData;
        end
    end
    
    methods (Access = protected)
        
        function data = CancelQRSTMethod(self, data, ecgData, rWaveWindowIndices, memberIndices)
            switch self.CancellationMethod
                case ECGQRSTCancellator.CANCELLATION_METHODS(1:3)
                    cancellationMethod = self.CancellationMethod;
                    
                    newData = data;
%                     for channelIndex = 1:ecgData.GetNumberOfChannels
                    parfor channelIndex = 1:ecgData.GetNumberOfChannels
                        channelData = data(:, channelIndex);
                        switch cancellationMethod
                            case ECGQRSTCancellator.CANCELLATION_METHODS{1}
                                channelData = self.ComputeABSCancellation(channelData, rWaveWindowIndices);
                            case ECGQRSTCancellator.CANCELLATION_METHODS{2}
                                channelData = self.ComputeClusteredABSCancellation(channelData, rWaveWindowIndices, memberIndices);
                            case ECGQRSTCancellator.CANCELLATION_METHODS{3}
                                channelData = self.ComputeSVDCancellation(channelData, rWaveWindowIndices);
                        end
                        
                        newData(:, channelIndex) = channelData;
                    end
                    data = newData;
                case ECGQRSTCancellator.CANCELLATION_METHODS{4}
                    data = self.ComputeBayesianCancellation(data, rWaveWindowIndices);
                case ECGQRSTCancellator.CANCELLATION_METHODS{5}
                    data = self.ComputePCACancellation(data);
            end
        end
        
        function cancelledSignal = ComputePCACancellation(self, signals)
            numberOfChannels = size(signals, 2);
            signals = signals';
            signals = bsxfun(@minus, signals, mean(signals));
            [u, s, v] = svd(signals, 'econ');
            
            componentRange = 2:(numberOfChannels - 1);
            cancelledSignal = (u * s(:, componentRange) * v(:, componentRange)')';
        end
    end
    
    methods (Static)
        function result = LSAR(signal, qIndex, sIndex, filterOrder)
            result = signal;
            numberOfSamples = numel(signal);
            
            % Check intervals
            samplesBeforeQ = qIndex - filterOrder - 1;
            if samplesBeforeQ < 0
                qIndex = qIndex - samplesBeforeQ;
                samplesBeforeQ = 0;
            end
            
            samplesAfterS = numberOfSamples - (sIndex + filterOrder);
            if samplesAfterS < 0
                sIndex = sIndex + samplesAfterS;
                samplesAfterS = 0;
            end
            
            if sIndex <= qIndex
                return;
            end
            
            % Estimate AR coefficients
            XKnown = NaN(samplesBeforeQ + samplesAfterS, filterOrder);
            xKnown = NaN(samplesBeforeQ + samplesAfterS, 1);
            correctedIndex = 1;
            
            % From start until Q
            for sampleIndex = (filterOrder + 1):(qIndex - 1)
                XKnown(correctedIndex, :) = signal((sampleIndex - 1):-1:(sampleIndex - filterOrder));
                xKnown(correctedIndex) = signal(sampleIndex);
                correctedIndex = correctedIndex + 1;
            end
            
            % From S until end
            for sampleIndex = (sIndex + filterOrder + 1):numberOfSamples
                XKnown(correctedIndex, :) = signal((sampleIndex - 1):-1:(sampleIndex - filterOrder));
                xKnown(correctedIndex) = signal(sampleIndex);
                correctedIndex = correctedIndex + 1;
            end
            
            if rank(XKnown' * XKnown) < filterOrder
                return;
            end
            
            coefficients = (XKnown' * XKnown) \ (XKnown' * xKnown);
            
            % interpolate QS interval
            
            % Construct A1
            xUnknown = signal(qIndex:sIndex);
            A1 = zeros(numel(xUnknown) + filterOrder, numel(xUnknown));
            for columnIndex = 1:numel(xUnknown)
                A1(columnIndex, columnIndex) = 1;
                A1((columnIndex + 1):(columnIndex + filterOrder), columnIndex) = -coefficients;
            end
            
            % Construct A2
            xKnown = [signal((qIndex - filterOrder):(qIndex - 1));...
                zeros(sIndex - qIndex + 1, 1);...
                signal((sIndex + 1):(sIndex + filterOrder))];
            A2 = zeros(numel(xUnknown) + filterOrder, numel(xKnown));
            for columnIndex = 1:filterOrder
                A2(1:columnIndex, columnIndex) = -coefficients((filterOrder - columnIndex + 1):end);
            end
            
            for columnIndex = (numel(xKnown) - filterOrder + 1):numel(xKnown)
                A2(columnIndex - filterOrder, columnIndex) = 1;
                A2((columnIndex - filterOrder + 1):(numel(xUnknown) + filterOrder), columnIndex) = -coefficients(1:(numel(xKnown) - columnIndex));
            end
            
            xUnknownLSEstimate = -(A1' * A1) \ ((A1' * A2) * xKnown);
            result(qIndex:sIndex) = xUnknownLSEstimate;
            
            showDiagnostics = false;
            if showDiagnostics
                figure;
                plot(signal, 'b');
                hold on;
                plot(result, 'r');
                plot(qIndex, signal(qIndex), 'k*');
                plot(sIndex, signal(sIndex), 'k*');
                hold off;
            end
        end
        
        function VRIndices = ComputeVRIndex(ecgData, SQIntervals)
            nyquistFrequency = ecgData.SamplingFrequency / 2;
            [b, a] = cheby2(3, 20,...
                12 / nyquistFrequency, 'high');
            
            numberOfIntervals = size(SQIntervals, 1);
            VRIndices = NaN(numberOfIntervals - 1, ecgData.GetNumberOfChannels);
            for channelIndex = 1:ecgData.GetNumberOfChannels
                signal = filtfilt(b, a, ecgData.Data(:, channelIndex));
                for intervalIndex = 1:(numberOfIntervals - 1)
                    atrialSignal = signal(SQIntervals(intervalIndex, 1):SQIntervals(intervalIndex + 1, 2));
                    atrialPower = sum(atrialSignal.^2) / numel(atrialSignal);
                    ventricularResidueSignal = signal(SQIntervals(intervalIndex, 2):SQIntervals(intervalIndex + 1, 1));
                    ventricularResiduePower = sum(ventricularResidueSignal.^2);
                    maxAbsValue = max(abs(ventricularResidueSignal));
                    VRIndices(intervalIndex, channelIndex) = maxAbsValue * sqrt(ventricularResiduePower) / atrialPower;
                end
            end
            VRIndices = bsxfun(@rdivide, VRIndices, max(abs(VRIndices)));
        end
    end
end