classdef PWaveMatchingPanel < ECGVisualizationPanel
    properties
        RWaveIndices
        QOnsetIndex
        RIndex
        PWaveChannel
        PWaveChannelLabels
        PWaveRangeOnset
        PWaveRangeOffset
        GetPWaveRangeBtn
    end
    
    properties (Access = protected)
        PWaveMatching
        
        PWaveChannelPopup
        
        PWaveSettingsPanel
        PWaveRangePanel
        
        BandpassPanel
        BandpassCheckbox
        BandpassStopEdit
        NotchCheckbox
        CombCheckbox
        
        SavePWaveMatchingButton
        MatchingCompletedButton
        
        ResultPanel
        ResultAxes
        PWaveIntervals
        BaselineCorrectedData
        BaselineCorrectedReferenceData
        
        CorrectedDataLines
        CorrectedReferenceDataLines
        
        PWavePatches
        PWaveReferencePatches
                
        OnsetOffset
        OnsetOffsetLineHandles
        
        ElectrodeOnsetOffset
        ElectrodeOnsetOffsetLineHandles
        ShadowOnsetOffsetLineHandles
        ElectrodeTerminalForce
        ElectrodeTerminalForceLineHandles
        
        PWaveComplexityLine
        
        AverageEnergyInformationText
        AverageInformationText
    end
    
    events
        PWaveMatchingCompleted
    end
    
    methods
        function self = PWaveMatchingPanel(position)          
            self = self@ECGVisualizationPanel(position);
            
            self.PWaveMatching = PWaveMatcher();
        end
        
        function Create(self, parentHandle)
            Create@ECGVisualizationPanel(self, parentHandle);
            self.OnsetOffset = NaN(1, 3);
        end
        
        function SetECGData(self, ecgData)
            self.SelectedChannels = 1;
            self.PWaveChannel = 1;
            SetECGData@ECGVisualizationPanel(self, ecgData);
            
            numberOfChannels = ecgData.GetNumberOfChannels();
            numberOfReferenceChannels = size(ecgData.ReferenceData, 2);
            self.ElectrodeOnsetOffset = NaN(numberOfChannels + numberOfReferenceChannels, 2);
            self.ElectrodeTerminalForce = NaN(numberOfChannels + numberOfReferenceChannels, 1);
            self.OnsetOffset = NaN(1, 3);

            self.SetChannelList();
            self.ShowECGData();
        end
        
        function pWaveData = GetPWaveData(self)
            if isempty(self.PWaveIntervals)
                pWaveData = [];
                return;
            end
            
            pWaveData = self.PWaveMatching.GetPWaveData(self.EcgData, self.BaselineCorrectedData,...
                self.PWaveIntervals, self.OnsetOffset, self.ElectrodeOnsetOffset, self.ValidChannels);
        end
        
        function pWaveMatchingData = GetPWaveMatchingData(self)
            if isempty(self.PWaveIntervals)
                pWaveMatchingData = [];
                return;
            end
            
            pWaveMatchingData = struct(...
                'ecgData', self.EcgData,...
                'rWaveIndices', self.RWaveIndices,...
                'baselineCorrectedData', self.BaselineCorrectedData,...
                'pWaveMatching', self.PWaveMatching,...
                'pWaveIntervals', self.PWaveIntervals,...
                'onsetOffset', self.OnsetOffset,...
                'electrodeOnsetOffset', self.ElectrodeOnsetOffset,...
                'electrodeTerminalForce', self.ElectrodeTerminalForce,...
                'validChannels', self.ValidChannels);
        end
        
        function ComputeAndShowPWaveMatching(self, pWaveMatcher)
            if nargin > 1
                self.PWaveMatching = pWaveMatcher;
                self.CreatePWaveSettings();
            end
            
            self.ComputePWaveMatching();
        end
        
        function SetPWaveMatchingData(self, filename)
            self.LoadData(filename);
        end
    end
    
    methods (Access = protected)
        function CreateECGAxes(self)
            CreateECGAxes@ECGVisualizationPanel(self);
            
            set(self.AxesPanel, 'position', [.2 .8 .8 .2]);
            
            self.CreateResultPanel();
        end
        
        function CreateControlPanel(self)
            CreateControlPanel@ECGVisualizationPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .6 1 .4]);
            self.CreatePWaveSettings();
            
            set(self.ValidTimePanel, 'Visible', 'off');
            delete(self.UseSelectionButton);
        end
        
        function CreatePWaveSettings(self, varargin)
            if ishandle(self.PWaveSettingsPanel)
                delete(self.PWaveSettingsPanel)
            end
            
            self.PWaveSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .6],...
                'title', 'P wave matching',...
                'titlePosition', 'centertop');
            
            
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.025 .9 .4 .05],...
                'style', 'checkbox',...
                'string', 'Baseline correction',...
                'value', self.PWaveMatching.BaselineCorrection,...
                'callback', @self.EnableBaselineCorrection);
            
            % Baseline correction
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.525 .9 .35 .05],...
                'style', 'text',...                
                'string', 'Baseline bandwidth',...
                'horizontalAlignment', 'left',...
                'tooltipString', 'Bandwidth (in ms) around R-peak');
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.875 .9 .1 .05],...
                'style', 'edit',...
                'string', num2str(1000 * self.PWaveMatching.BaselineBandwidth),...
                'callback', @self.SetBaselineBandwidth);
            
            % Template shift & correlation
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.525 .85 .35 .05],...
                'style', 'text',...                
                'string', 'Template shift',...
                'horizontalAlignment', 'left',...
                'tooltipString', 'Maximum P-wave template shift (in ms)');
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.875 .85 .1 .05],...
                'style', 'edit',...
                'string', num2str(1000 * self.PWaveMatching.TemplateShift),...
                'callback', @self.SetTemplateShift);
            
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.025 .85 .35 .05],...
                'style', 'text',...
                'string', 'Correlation threshold',...
                'horizontalAlignment', 'left',...
                'tooltipString', 'Minimum P-wave correlation with template (0-1)');
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.375 .85 .1 .05],...
                'style', 'edit',...
                'string', num2str(self.PWaveMatching.CorrelationThreshold),...
                'callback', @self.SetCorrelationThreshold);
            
            
            % P-wave range
            self.PWaveRangePanel = uipanel('parent', self.PWaveSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .75 1 .1],...
                'title', 'P-wave range (in ms relative to R)');
            
            uicontrol('parent', self.PWaveRangePanel,...
                'units', 'normalized',...
                'position', [.1, 3/5, .3, 2/5],...
                'style', 'text',...                
                'string', 'Onset');
            self.PWaveRangeOnset = uicontrol('parent', self.PWaveRangePanel,...
                'units', 'normalized',...
                'position', [.1, 0, .3, 3/5],...
                'style', 'edit',...                
                'string', num2str(1000 * self.PWaveMatching.PWaveRange(1)),...
                'callback', @self.SetPWaveRangeOnset);
            
            uicontrol('parent', self.PWaveRangePanel,...
                'units', 'normalized',...
                'position', [.4, 3/5, .3, 2/5],...
                'style', 'text',...                
                'string', 'Offset');
            self.PWaveRangeOffset = uicontrol('parent', self.PWaveRangePanel,...
                'units', 'normalized',...
                'position', [.4, 0, .3, 3/5],...
                'style', 'edit',...
                'string', num2str(1000 * self.PWaveMatching.PWaveRange(2)),...
                'callback', @self.SetPWaveRangeOffset);

            self.GetPWaveRangeBtn = uicontrol('parent', self.PWaveRangePanel,...
                'units', 'normalized',...
                'position', [.7, 0, .3, 3/5],...
                'style', 'pushbutton',...
                'string', 'Get range',...
                'callback', @self.GetPWaveRange);
            
            % Filter settings
            filterControlPanel = uipanel('parent', self.PWaveSettingsPanel,...
                'units', 'normalized', ...
                'position', [0 .455 1 .275],...
                'title', 'Filter settings',...
                'titlePosition', 'centertop');
            
            uicontrol('parent', filterControlPanel,...
                'units', 'normalized',...
                'position', [0, .85, 1, .1],...
                'style', 'text',...                
                'string', 'High frequency bandpass filter');
            self.BandpassPanel = uipanel('parent', filterControlPanel,...
                'units', 'normalized', ...
                'position', [.05 .5 .9 .35]);
            uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .2 .8],...
                'style', 'edit',...
                'string', num2str(self.PWaveMatching.BandpassFrequencies(1)),...
                'tooltipString', 'High pass frequency',...
                'callback', @self.SetBandpassStart);
            self.BandpassStopEdit = uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.4 .1 .2 .8],...
                'style', 'edit',...
                'string', num2str(self.PWaveMatching.BandpassFrequencies(2)),...
                'tooltipString', 'Low pass frequency',...
                'callback', @self.SetBandpassStop);
            uicontrol('parent', self.BandpassPanel,...
                'units', 'normalized',...
                'position', [.7 .1 .2 .8],...
                'style', 'edit',...
                'string', num2str(self.PWaveMatching.BandpassOrder),...
                'tooltipString', 'Filter order',...
                'callback', @self.SetBandpassOrder);
            
            self.NotchCheckbox = uicontrol('parent', filterControlPanel,...
                'units', 'normalized',...
                'position', [.1 .4 .4 .1],...
                'style', 'checkbox',...
                'string', 'Notch filter',...
                'value', self.PWaveMatching.ApplyNotchFilter,...
                'callback', @self.EnableNotchFilter);
            self.CombCheckbox = uicontrol('parent', filterControlPanel,...
                'units', 'normalized',...
                'position', [.5 .4 .4 .1],...
                'style', 'checkbox',...
                'string', 'Comb filter',...
                'value', self.PWaveMatching.ApplyCombFilter,...
                'callback', @self.EnableCombFilter);
            notchPanel = uipanel('parent', filterControlPanel,...
                'units', 'normalized', ...
                'position', [.05 0 .9 .35]);
            uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.1 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch /Comb fundamental frequency',...
                'string', num2str(self.PWaveMatching.NotchFrequency),...
                'callback', @self.SetNotchFrequency);
            uicontrol('parent', notchPanel,...
                'units', 'normalized',...
                'position', [.7 .1 .2 .8],...
                'style', 'edit',...
                'tooltipString', 'Notch precision',...
                'string', num2str(self.PWaveMatching.NotchOrder),...
                'callback', @self.SetNotchOrder);
            
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .35 .9 .1],...
                'style', 'pushbutton',...
                'string', 'Compute P-wave matching',...
                'callback', @self.ComputePWaveMatching);
            
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.1 .3 .4 .05],...
                'style', 'checkbox',...
                'string', 'Show diagnostics',...
                'value', self.PWaveMatching.ShowDiagnostics,...
                'callback', @self.SetShowDiagnostics);
            
            self.SavePWaveMatchingButton = uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Save to .mat',...
                'callback', @self.SavePWaveMatching,...
                'enable', 'off');
            uicontrol('parent', self.PWaveSettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .1 .4 .1],...
                'style', 'pushbutton',...
                'string', 'Load from .mat',...
                'callback', @self.LoadPWaveMatching);
            
            self.MatchingCompletedButton = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.1 .0 .8 .05],...
                'style', 'pushbutton',...
                'string', 'P Wave matching completed',...
                'callback', @(src, event) notify(self, 'PWaveMatchingCompleted'),...
                'enable', 'off');
        end
        
        function CreateResultPanel(self)
            self.ResultPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.2 0 .8 .8]);
            
            self.PWaveChannelPopup = uicontrol(...
                'parent', self.ResultPanel,...
                'units', 'normalized',...
                'position', [0 0 .1 .05],...
                'style', 'popupmenu',...
                'string', 'Select channel',...
                'backgroundColor', [1 1 1],...
                'callback', @self.ShowAveragePWave,...
                'busyAction', 'cancel');
            
            self.PWaveMatching.DiagnosticsView = self.ResultPanel;
        end
        
        function CreateResultAxes(self)
            validResultAxes = ishandle(self.ResultAxes);
            if any(validResultAxes)
                delete(self.ResultAxes(validResultAxes));
            end
            self.ResultAxes = ECGChannelPanel.TightSubplot(2, 3, self.ResultPanel,...
                [0.1, 0.05], [0.05, 0.05], [0.05, 0.025]);
        end
        
        function SetChannelList(self)
            SetChannelList@ECGVisualizationPanel(self);
            
            beatChannelList = self.EcgData.ElectrodeLabels;
            if ~isempty(self.EcgData.ReferenceLabels)
                beatChannelList = [beatChannelList(:); self.EcgData.ReferenceLabels(:)];
            end
            set(self.PWaveChannelPopup, 'string', beatChannelList);
            self.PWaveChannelLabels = beatChannelList;
        end
        
        function ShowECGData(self)
            ShowECGData@ECGVisualizationPanel(self);
            self.ShowBaselineCorrectedData();
            self.ShowPWaveIntervals();
        end
        
        function SavePWaveMatching(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_PWaveMatching'];
            ecgData = self.EcgData;  %#ok<NASGU>
            rWaveIndices = self.RWaveIndices; %#ok<NASGU>
            baselineCorrectedData = self.BaselineCorrectedData;%#ok<NASGU>
            pWaveMatching = self.PWaveMatching; %#ok<NASGU>
            pWaveIntervals = self.PWaveIntervals; %#ok<NASGU>
            onsetOffset = self.OnsetOffset;%#ok<NASGU>
            electrodeOnsetOffset = self.ElectrodeOnsetOffset;%#ok<NASGU>
            electrodeTerminalForce = self.ElectrodeTerminalForce;%#ok<NASGU>
            validChannels = self.ValidChannels;%#ok<NASGU>
            save(filename,...
                'ecgData',...
                'rWaveIndices',...
                'baselineCorrectedData',...
                'pWaveMatching',...
                'pWaveIntervals',...
                'onsetOffset',...
                'electrodeOnsetOffset',...
                'electrodeTerminalForce',...
                'validChannels');
        end
        
        function LoadPWaveMatching(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_PWaveMatching.mat',...
                    'Select P-wave matching file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_PWaveMatching.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function ecgData = LoadData(self, filename)
            pWaveData = load(filename);
            ecgData = pWaveData.ecgData;
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 14));
            if ~isempty(pWaveData.ecgData)
                pWaveData.ecgData.Filename = fullfile(pathString, nameString);
            end
                        
            self.RWaveIndices = pWaveData.rWaveIndices;
            self.PWaveIntervals = pWaveData.pWaveIntervals;
            self.ValidChannels = pWaveData.validChannels;
            self.BaselineCorrectedData = pWaveData.baselineCorrectedData;
            
            self.SetECGData(pWaveData.ecgData);
            
            self.PWaveMatching = pWaveData.pWaveMatching;
            self.CreatePWaveSettings();
            
            self.OnsetOffset = pWaveData.onsetOffset;
            self.ElectrodeOnsetOffset = pWaveData.electrodeOnsetOffset;
            
            if isfield(pWaveData, 'electrodeTerminalForce')
                self.ElectrodeTerminalForce = pWaveData.electrodeTerminalForce;
            else
                self.ElectrodeTerminalForce = NaN(ecgData.GetNumberOfChannels(), 1);
            end
            
            self.ShowPWaveMatching();
            self.SetElectrodeMenuColor();
        end
                
        % Visualization functions
        function ShowBaselineCorrectedData(self)
            if isempty(self.BaselineCorrectedData), return; end
            
            validHandles = ishandle(self.CorrectedDataLines);
            if any(validHandles)
                delete(self.CorrectedDataLines(validHandles));
            end
            
            time = self.EcgData.GetTimeRange();
            lineColor = [0, 0, 1];
            
            lineData = self.BaselineCorrectedData(:, self.SelectedChannels);
            self.CorrectedDataLines = NaN(numel(self.SelectedChannels), 1);
            for lineIndex = 1:size(lineData, 2)
                self.CorrectedDataLines(lineIndex) = ...
                    line('xData', time, 'yData', lineData(:, lineIndex),...
                    'color', lineColor,...
                    'lineWidth', 1,...
                    'parent', self.AxesHandles(lineIndex),...
                    'lineSmoothing', 'on');
            end
            
            validHandles = ishandle(self.CorrectedReferenceDataLines);
            if any(validHandles)
                delete(self.CorrectedReferenceDataLines(validHandles));
            end
            
            referenceLineData = self.BaselineCorrectedReferenceData(:, self.SelectedLeadChannels);
            for lineIndex = 1:size(referenceLineData, 2)
                self.CorrectedReferenceDataLines(lineIndex) = ...
                    line('xData', time, 'yData', referenceLineData(:, lineIndex),...
                    'color', lineColor,...
                    'lineWidth', 1,...
                    'parent', self.AxesHandles(lineIndex + numel(self.SelectedChannels)),...
                    'lineSmoothing', 'on');
            end
        end
        
        function ShowPWaveIntervals(self)
            if isempty(self.PWaveIntervals), return; end
            numberOfIntervals = size(self.PWaveIntervals, 1);
            pWaveTimeIntervals = self.PWaveIntervals / self.EcgData.SamplingFrequency;
            
            patchColor = [1, 0, 0];
            patchAlpha = 0.5;
            
            validHandles = ishandle(self.PWavePatches);
            if any(validHandles)
                delete(self.PWavePatches(validHandles));
            end
            
            lineData = self.BaselineCorrectedData(:, self.SelectedChannels);
            self.PWavePatches = NaN(numel(self.SelectedChannels), numberOfIntervals);
            for lineIndex = 1:size(lineData, 2)
                for intervalIndex = 1:numberOfIntervals
                    currentIntervalTime = pWaveTimeIntervals(intervalIndex, :);
                    currentIntervalIndices = self.PWaveIntervals(intervalIndex, 1):self.PWaveIntervals(intervalIndex, 2);
                    intervalYLimits = [min(lineData(currentIntervalIndices, lineIndex)), max(lineData(currentIntervalIndices, lineIndex))];
                    self.PWavePatches(lineIndex, intervalIndex) = ...
                        patch('xData', [currentIntervalTime(1), currentIntervalTime(1), currentIntervalTime(2), currentIntervalTime(2)],...
                        'yData', [intervalYLimits(1), intervalYLimits(2), intervalYLimits(2), intervalYLimits(1)],...
                        'faceColor', patchColor,...
                        'faceAlpha', patchAlpha,...
                        'lineWidth', 1,...
                        'parent', self.AxesHandles(lineIndex),...
                        'lineSmoothing', 'on');
                end
            end
            
            validHandles = ishandle(self.PWaveReferencePatches);
            if any(validHandles)
                delete(self.PWaveReferencePatches(validHandles));
            end
            
            lineData = self.BaselineCorrectedReferenceData(:, self.SelectedLeadChannels);
            self.PWaveReferencePatches = NaN(numel(self.SelectedLeadChannels), numberOfIntervals);
            for lineIndex = 1:size(lineData, 2)
                for intervalIndex = 1:numberOfIntervals
                    currentIntervalTime = pWaveTimeIntervals(intervalIndex, :);
                    currentIntervalIndices = self.PWaveIntervals(intervalIndex, 1):self.PWaveIntervals(intervalIndex, 2);
                    intervalYLimits = [min(lineData(currentIntervalIndices, lineIndex)), max(lineData(currentIntervalIndices, lineIndex))];
                    self.PWaveReferencePatches(lineIndex, intervalIndex) = ...
                        patch('xData', [currentIntervalTime(1), currentIntervalTime(1), currentIntervalTime(2), currentIntervalTime(2)],...
                        'yData', [intervalYLimits(1), intervalYLimits(2), intervalYLimits(2), intervalYLimits(1)],...
                        'faceColor', patchColor,...
                        'faceAlpha', patchAlpha,...
                        'lineWidth', 1,...
                        'parent', self.AxesHandles(lineIndex + numel(self.SelectedChannels)),...
                        'lineSmoothing', 'on');
                end
            end
        end
        
        function ShowAveragePWave(self, varargin)
            self.ShowUnfilteredAveragePWave();
            self.ShowFilteredAveragePWave();
            self.UpdateElectrodeOnsetOffSet();
        end
        
        function ShowUnfilteredAveragePWave(self, varargin)
            plotHandle = self.ResultAxes(4);
            cla(plotHandle);
            
            self.PWaveChannel = get(self.PWaveChannelPopup, 'value');
            if self.PWaveChannel <= self.EcgData.GetNumberOfChannels
                channelData = self.BaselineCorrectedData(:, self.PWaveChannel);
                channelLabel = self.EcgData.ElectrodeLabels{self.PWaveChannel};
            elseif (self.PWaveChannel - self.EcgData.GetNumberOfChannels) <= size(self.BaselineCorrectedReferenceData, 2)
                channelData = self.BaselineCorrectedReferenceData(:, self.PWaveChannel - self.EcgData.GetNumberOfChannels);
                channelLabel = self.EcgData.ReferenceLabels{self.PWaveChannel - self.EcgData.GetNumberOfChannels};
            else
                return;
            end
            
            onsetOffsetMenu = uicontextmenu;
            uimenu(onsetOffsetMenu,'label', 'Onset','callback', @self.SetElectrodeOnset);
            uimenu(onsetOffsetMenu,'label', 'Offset','callback', @self.SetElectrodeOffset);
            uimenu(onsetOffsetMenu,'label', 'Terminal force','callback', @self.SetElectrodeTerminalForce);
            
            extendedIntervals = self.PWaveIntervals;
            pWaveEndIndex = round(self.PWaveMatching.PWaveRange(2) * self.EcgData.SamplingFrequency);
            if self.PWaveMatching.PWaveRange(2) < 0
                extendedIntervals(:, 2) = self.PWaveIntervals(:, 2) - pWaveEndIndex;
            end
            [intervalAverage, intervalSD, intervalTime, intervalData] = self.PWaveMatching.ComputeUnfilteredPWaveAverage(self.EcgData, channelData, extendedIntervals);
            line('xData', intervalTime, 'yData', zeros(size(intervalTime)),...
                'color', [.7, .7, .7], 'lineSmoothing', 'on', 'parent', plotHandle);
            
            hold(plotHandle, 'on');
            
            plot(intervalTime, intervalData,...
                'color', [.7, .7, .7],...
                'parent', plotHandle);
            
            line('xData', intervalTime, 'yData', intervalAverage,...
                'color', [0, 0, 0], 'lineWidth', 2,...
                'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            
            axis(plotHandle, 'tight');
            
%             [pWaveOnsetIndex, pWaveOffsetIndex, qIndex] =...
%                 self.PWaveMatching.ComputeOnsetOffsetDuration(self.EcgData, intervalAverage, intervalTime, self.PWaveMatching.ShowDiagnostics);
%             if ~isnan(pWaveOnsetIndex)
%                 self.SetEstimatedOnsetOffset(plotHandle, intervalTime([pWaveOnsetIndex, pWaveOffsetIndex]));
%             end
            
            % if on- and offset are marked, adjust the y-limits of the plot
            if ~any(isnan(self.OnsetOffset))
                pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= self.OnsetOffset(1) & intervalTime <= self.OnsetOffset(end));
                pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
                yLimits = [pWaveRange(1) - diff(pWaveRange) * .1, pWaveRange(2) + diff(pWaveRange) * .1];
                ylim(plotHandle, yLimits);
            else
                pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= 1000 * self.PWaveMatching.PWaveRange(1) &...
                    intervalTime <= 1000 * self.PWaveMatching.PWaveRange(2));
                pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
                yLimits = [pWaveRange(1) - diff(pWaveRange) * .1, pWaveRange(2) + diff(pWaveRange) * .1];
                ylim(plotHandle, yLimits);
            end
            
            xlabel(plotHandle, 'Time (ms)');
            ylabel(plotHandle, 'Voltage (mV)');
            title(plotHandle, ['Average P-wave (', channelLabel, ') (unfiltered)']);
        end
        
        function ShowFilteredAveragePWave(self, varargin)
            plotHandle = self.ResultAxes(5);
            axes(plotHandle);
            cla(plotHandle);
            
            self.PWaveChannel = get(self.PWaveChannelPopup, 'value');
            if self.PWaveChannel <= self.EcgData.GetNumberOfChannels
                channelData = self.EcgData.Data(:, self.PWaveChannel);
                channelLabel = self.EcgData.ElectrodeLabels{self.PWaveChannel};
            elseif (self.PWaveChannel - self.EcgData.GetNumberOfChannels) <= size(self.BaselineCorrectedReferenceData, 2)
                channelData = self.EcgData.ReferenceData(:, self.PWaveChannel - self.EcgData.GetNumberOfChannels);
                channelLabel = self.EcgData.ReferenceLabels{self.PWaveChannel - self.EcgData.GetNumberOfChannels};
            else
                return;
            end
            
            onsetOffsetMenu = uicontextmenu;
            uimenu(onsetOffsetMenu,'label', 'Onset','callback', @self.SetElectrodeOnset);
            uimenu(onsetOffsetMenu,'label', 'Offset','callback', @self.SetElectrodeOffset);
            
            extendedIntervals = self.PWaveIntervals;
            pWaveEndIndex = round(self.PWaveMatching.PWaveRange(2) * self.EcgData.SamplingFrequency);
            if self.PWaveMatching.PWaveRange(2) < 0
                extendedIntervals(:, 2) = self.PWaveIntervals(:, 2) - pWaveEndIndex;
            end
            [intervalAverage, intervalSD, intervalTime] = self.PWaveMatching.ComputeFilteredPWaveAverage(self.EcgData, channelData, extendedIntervals);
            line('xData', intervalTime, 'yData', zeros(size(intervalTime)),...
                'color', [.7, .7, .7], 'lineSmoothing', 'on', 'parent', plotHandle);
            line('xData', intervalTime, 'yData', intervalAverage,...
                'color', [0, 0, 0], 'lineSmoothing', 'on',...
                'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);

            axis(plotHandle, 'tight');
            xLimits = get(plotHandle, 'xlim');
            
            pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= 1000 * self.PWaveMatching.PWaveRange(1) &...
            intervalTime <= 1000 * self.PWaveMatching.PWaveRange(2));
            pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
            yLimits = [pWaveRange(1) - diff(pWaveRange) * .1, pWaveRange(2) + diff(pWaveRange) * .1];
            ylim(plotHandle, yLimits);
            
            xlim(plotHandle, xLimits);
            xlabel(plotHandle, 'Time (ms)');
            ylabel(plotHandle, 'Voltage (mV)');
            title(plotHandle, ['Average P-wave (', channelLabel, ') (high frequency)']);
            
            self.ShowAverageElectrodeInformation();
        end
        
        function ShowFilteredAveragePWaveEnergy(self, varargin)
            plotHandle = self.ResultAxes(2);
            cla(plotHandle);
            
            extendedIntervals = self.PWaveIntervals;
            pWaveEndIndex = round(self.PWaveMatching.PWaveRange(2) * self.EcgData.SamplingFrequency);
            if self.PWaveMatching.PWaveRange(2) < 0
                extendedIntervals(:, 2) = self.PWaveIntervals(:, 2) - pWaveEndIndex;
            end
            [intervalAverage, intervalTime] = self.PWaveMatching.ComputeFilteredPWaveAverageEnergy(self.EcgData, extendedIntervals);
            
            axes(plotHandle);
            onsetOffsetMenu = uicontextmenu;
            uimenu(onsetOffsetMenu,'label', 'Onset','callback', @self.SetOnset);
            uimenu(onsetOffsetMenu,'label', 'Offset','callback', @self.SetOffset);
            
            line('xData', intervalTime, 'yData', intervalAverage,...
                'color', [0, 0, 0], 'lineSmoothing', 'on',...
                'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            
            axis(plotHandle, 'tight');
            xLimits = get(plotHandle, 'xlim');
            
            pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= 1000 * self.PWaveMatching.PWaveRange(1) &...
            intervalTime <= 1000 * self.PWaveMatching.PWaveRange(2));
            pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
            yLimits = [pWaveRange(1) - diff(pWaveRange) * .1, pWaveRange(2) + diff(pWaveRange) * .1];
            ylim(plotHandle, yLimits);
            
            xlim(plotHandle, xLimits);
            xlabel(plotHandle, 'Time (ms)');
            ylabel(plotHandle, 'Voltage RMS (mV)');
            title(plotHandle, 'P-Wave Energy estimate (RMS) (high frequency)');
            
            plotHandle = self.ResultAxes(3);
            cla(plotHandle);
            [intervalAverage, intervalTime] = self.PWaveMatching.ComputeFilteredPWaveAverageEnergy(self.EcgData, self.PWaveIntervals);
            [thresholds, duration, optimalIndex, optimalRange] = self.PWaveMatching.ComputeNoiseThresholdDuration(self.EcgData, intervalAverage);
            
            line('xData', thresholds, 'yData', 1000 * duration, 'parent', plotHandle);
            line('xData', [thresholds(optimalIndex), thresholds(optimalIndex)],...
                'yData', 1000 * [min(duration), duration(optimalIndex)],...
                'color', [.7,.7,.7], 'lineStyle', '--',...
                'parent', plotHandle)
            line('xData', [min(thresholds), thresholds(optimalIndex)],...
                'yData', 1000 * [duration(optimalIndex), duration(optimalIndex)],...
                'color', [.7,.7,.7], 'lineStyle', '--',...
                'parent', plotHandle)
            
            axis(plotHandle, 'tight');
            xlabel(plotHandle, 'Noise threshold (mV)');
            ylabel(plotHandle, 'Duration (ms)');
            title(plotHandle, 'P-Wave duration based on noise threshold');
            
            self.SetEstimatedOnsetOffset(self.ResultAxes(2), intervalTime(optimalRange));
        end
        
        function ShowUnfilteredAveragePWaveEnergy(self)
            plotHandle = self.ResultAxes(1);
            cla(plotHandle);
            
            extendedIntervals = self.PWaveIntervals;
            pWaveEndIndex = round(self.PWaveMatching.PWaveRange(2) * self.EcgData.SamplingFrequency);
            if self.PWaveMatching.PWaveRange(2) < 0
                extendedIntervals(:, 2) = self.PWaveIntervals(:, 2) - pWaveEndIndex;
            end
            [intervalAverage, intervalTime] = self.PWaveMatching.ComputeUnfilteredPWaveAverageEnergy(self.EcgData, self.BaselineCorrectedData, extendedIntervals);
            
            onsetOffsetMenu = uicontextmenu;
            uimenu(onsetOffsetMenu,'label', 'Onset','callback', @self.SetOnset);
            uimenu(onsetOffsetMenu,'label', 'Offset','callback', @self.SetOffset);
            uimenu(onsetOffsetMenu,'label', 'Q-onset','callback', @self.SetQOnset);
            
            line('xData', intervalTime, 'yData', intervalAverage,...
                'color', [0, 0, 0], 'lineSmoothing', 'on',...
                'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            
            self.ShowAverageEnergyInformation();
            
            axis(plotHandle, 'tight');
            xLimits = get(plotHandle, 'xlim');
            
            ylimits = get(plotHandle, 'ylim');
            ylimits(1) = 0;
            ylim(plotHandle, ylimits);
            
            % if on- and offset are marked, adjust the y-limits of the plot
            if ~any(isnan(self.OnsetOffset))
                pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= self.OnsetOffset(1) & intervalTime <= self.OnsetOffset(end));
                pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
                yLimits = [0, pWaveRange(2) + diff(pWaveRange) * .1];
                ylim(plotHandle, yLimits);
            else
                pWaveDataWithinOnsetOffset = intervalAverage(intervalTime >= 1000 * self.PWaveMatching.PWaveRange(1) &...
                    intervalTime <= 1000 * self.PWaveMatching.PWaveRange(2));
                pWaveRange = [min(pWaveDataWithinOnsetOffset), max(pWaveDataWithinOnsetOffset)];
                yLimits = [pWaveRange(1) - diff(pWaveRange) * .1, pWaveRange(2) + diff(pWaveRange) * .1];
                ylim(plotHandle, yLimits);
            end
            
            xlim(plotHandle, xLimits);
            xlabel(plotHandle, 'Time (ms)');
            ylabel(plotHandle, 'Voltage RMS (mV)');
            title(plotHandle, 'P-Wave Energy estimate (RMS)');
            
%             [pWaveOnsetIndex, pWaveOffsetIndex, qIndex] =...
%                 self.PWaveMatching.ComputeOnsetOffsetDuration(self.EcgData, intervalAverage, intervalTime, self.PWaveMatching.ShowDiagnostics);
%             if ~isnan(pWaveOnsetIndex)
%                 self.SetEstimatedOnsetOffset(self.ResultAxes(1), intervalTime([pWaveOnsetIndex, pWaveOffsetIndex]));
%             end
        end
        
        function ShowAverageEnergyInformation(self)
            [intervalAverage, intervalTime] = self.PWaveMatching.ComputeUnfilteredPWaveAverageEnergy(self.EcgData, self.BaselineCorrectedData, self.PWaveIntervals);
            numberOfIntervals = size(self.PWaveIntervals, 1);
            numberOfRIntervals = numel(self.RWaveIndices);
            duration = round(self.OnsetOffset(2) - self.OnsetOffset(1));
            
            detrendAverage = true;
            pWaveArea = self.PWaveMatching.ComputePWaveArea(intervalAverage, intervalTime, self.OnsetOffset, detrendAverage);
            
            infoText = {...
                ['Number of intervals: ', num2str(numberOfIntervals), ' of ', num2str(numberOfRIntervals)];...
                ['Duration: ' num2str(duration), 'ms'];...
                ['Area: ', num2str(pWaveArea, '%.2f'), 'mV*ms']};
            
            plotHandle = self.ResultAxes(1);
            if ishandle(self.AverageEnergyInformationText)
                delete(self.AverageEnergyInformationText);   
            end
            self.AverageEnergyInformationText = text(1, 1, 1, infoText, 'units', 'normalized',...
                'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'right',...
                'verticalAlignment', 'top', 'parent', plotHandle);
        end
        
        function ShowAverageElectrodeInformation(self)
            plotHandle = self.ResultAxes(4);
            self.PWaveChannel = get(self.PWaveChannelPopup, 'value');
            if self.PWaveChannel <= self.EcgData.GetNumberOfChannels
                channelData = self.BaselineCorrectedData(:, self.PWaveChannel);
            elseif (self.PWaveChannel - self.EcgData.GetNumberOfChannels) <= size(self.BaselineCorrectedReferenceData, 2)
                channelData = self.BaselineCorrectedReferenceData(:, self.PWaveChannel - self.EcgData.GetNumberOfChannels);
            else
                return;
            end
            
            [intervalAverage, ~, intervalTime] = self.PWaveMatching.ComputeUnfilteredPWaveAverage(self.EcgData, channelData, self.PWaveIntervals);
            electrodeOnsetOffset = self.ElectrodeOnsetOffset(self.PWaveChannel, :);
            if isnan(electrodeOnsetOffset(1))
                electrodeOnsetOffset(1) = self.OnsetOffset(1);
            end
            if isnan(electrodeOnsetOffset(2))
                electrodeOnsetOffset(2) = self.OnsetOffset(2);
            end
            [peakIndices, peakThreshold] = self.PWaveMatching.FindPWavePeaks(intervalAverage, intervalTime, electrodeOnsetOffset);
            if ishandle(self.PWaveComplexityLine)
                delete(self.PWaveComplexityLine);
            end
            if ~isempty(peakIndices)
                self.PWaveComplexityLine = line('xData', intervalTime(peakIndices), 'yData', intervalAverage(peakIndices), 'lineStyle',...
                    'none', 'marker', 'o', 'markerFaceColor', [1, 0, 0], 'markerEdgeColor', [0, 0, 0],...
                    'hitTest', 'off', 'parent', plotHandle);
            end
            
            duration = round(electrodeOnsetOffset(end) - electrodeOnsetOffset(1));
            
            detrendAverage = true;
            pWaveArea = self.PWaveMatching.ComputePWaveArea(intervalAverage, intervalTime, electrodeOnsetOffset, detrendAverage);
            
            infoText = {...
                ['Number of peaks: ', num2str(numel(peakIndices))];...
                ['Peak threshold: ', num2str(1000 * peakThreshold, '%.0f'), 'muV'];...
                ['Duration: ' num2str(duration), 'ms'];...
                ['Area: ', num2str(pWaveArea, '%.2f'), 'mV*ms']};
            
            terminalForceTime = self.ElectrodeTerminalForce(self.PWaveChannel);
            if ~isnan(terminalForceTime)
                pWaveTerminalForce =...
                    self.PWaveMatching.ComputePWaveTerminalForce(intervalAverage, intervalTime, electrodeOnsetOffset, terminalForceTime, detrendAverage);
                infoText = [infoText;...
                    {['Terminal force: ' num2str(pWaveTerminalForce, '%.2f'), 'mV*ms']}];
            end
            
            if ishandle(self.AverageInformationText)
                delete(self.AverageInformationText);   
            end
            self.AverageInformationText = text(1, 1, 1, infoText, 'units', 'normalized',...
                'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'right',...
                'verticalAlignment', 'top', 'parent', plotHandle);
            
            % threshold - complexity relation
            plotHandle = self.ResultAxes(6);
            [voltageThresholds, numberOfPeaks] = PWaveMatcher.ComputeVoltageThresholdComplexity(intervalAverage, intervalTime, electrodeOnsetOffset);
            cla(plotHandle);
            line('xData', voltageThresholds, 'yData', numberOfPeaks, 'parent', plotHandle);
            xlabel(plotHandle, 'Noise threshold (mV)');
            ylabel(plotHandle, 'Complexity (Number of peaks)');
            title(plotHandle, 'P-Wave complexity based on noise threshold');
            axis(plotHandle, 'tight');
        end
        
        function SetOnset(self, varargin)
            point = get(gca, 'currentPoint');
            if isnan(self.OnsetOffset(2)) || point(1, 1) < self.OnsetOffset(2)
                self.OnsetOffset(1) = point(1, 1);
                self.UpdateOnsetOffSet();
            end
        end
        
        function SetOffset(self, varargin)
            point = get(gca, 'currentPoint');
            if isnan(self.OnsetOffset(1)) || point(1, 1) > self.OnsetOffset(1)
                self.OnsetOffset(2) = point(1, 1);
                self.UpdateOnsetOffSet();
            end
        end
        
        function SetQOnset(self, varargin)
            point = get(gca, 'currentPoint');
            self.OnsetOffset(3) = point(1, 1);
            self.UpdateOnsetOffSet();
        end
        
        function SetElectrodeOnset(self, varargin)
            point = get(gca, 'currentPoint');
            if isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 2)) ||...
                    point(1, 1) < self.ElectrodeOnsetOffset(self.PWaveChannel, 2)
                self.ElectrodeOnsetOffset(self.PWaveChannel, 1) = point(1, 1);
                self.UpdateElectrodeOnsetOffSet();
                self.UpdateElectrodeMenuColor();
            end
        end
        
        function SetElectrodeOffset(self, varargin)
            point = get(gca, 'currentPoint');
            if isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 1)) ||...
                    point(1, 1) > self.ElectrodeOnsetOffset(self.PWaveChannel, 1)
                self.ElectrodeOnsetOffset(self.PWaveChannel, 2) = point(1, 1);
                self.UpdateElectrodeOnsetOffSet();
                self.UpdateElectrodeMenuColor();
            end
        end
        
        function SetElectrodeTerminalForce(self, varargin)
            point = get(gca, 'currentPoint');
            if (isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 1)) ||...
                    point(1, 1) > self.ElectrodeOnsetOffset(self.PWaveChannel, 1)) &&...
                    (isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 2)) ||...
                    point(1, 1) < self.ElectrodeOnsetOffset(self.PWaveChannel, 2))
                
                self.ElectrodeTerminalForce(self.PWaveChannel) = point(1, 1);
                self.UpdateElectrodeOnsetOffSet();
            end
        end
        
        function UpdateOnsetOffSet(self)
            validHandles = ishandle(self.OnsetOffsetLineHandles);
            
            if any(validHandles(:))
                delete(self.OnsetOffsetLineHandles(validHandles));
            end
            
            self.OnsetOffsetLineHandles = NaN(2,3);
            
            plotHandle = self.ResultAxes(1);
            yLimits = get(plotHandle, 'yLim');
            self.OnsetOffsetLineHandles(1, 1) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(1), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2, 'parent', plotHandle);
            self.OnsetOffsetLineHandles(1, 2) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(2), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2,  'parent', plotHandle);
            self.OnsetOffsetLineHandles(1, 3) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(3), 'yData', yLimits,...
                'color', [0, 0, 1], 'lineStyle', '--', 'lineWidth', 2,  'parent', plotHandle);
            
            plotHandle = self.ResultAxes(2);
            yLimits = get(plotHandle, 'yLim');
            self.OnsetOffsetLineHandles(2, 1) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(1), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(self.OnsetOffsetLineHandles(2, 1), 'bottom');
            self.OnsetOffsetLineHandles(2, 2) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(2), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(self.OnsetOffsetLineHandles(2, 2), 'bottom');
            self.OnsetOffsetLineHandles(2, 3) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(3), 'yData', yLimits,...
                'color', [.8,.8, 1], 'lineStyle', '--', 'lineWidth', 2,  'parent', plotHandle);
            
            self.ShowAverageEnergyInformation();
            self.UpdateElectrodeOnsetOffSet();
            
            if ~any(isnan(self.OnsetOffset))
                set(self.MatchingCompletedButton, 'enable', 'on');
                set(self.SavePWaveMatchingButton, 'enable', 'on');
            else
                set(self.MatchingCompletedButton, 'enable', 'off');
                set(self.SavePWaveMatchingButton, 'enable', 'off');
            end
        end
        
        function UpdateElectrodeOnsetOffSet(self)
            validHandles = ishandle(self.ElectrodeOnsetOffsetLineHandles);
            if any(validHandles(:))
                delete(self.ElectrodeOnsetOffsetLineHandles(validHandles));
            end
            self.ElectrodeOnsetOffsetLineHandles = NaN(2,2);
            
            validHandles = ishandle(self.ShadowOnsetOffsetLineHandles);
            if any(validHandles(:))
                delete(self.ShadowOnsetOffsetLineHandles(validHandles));
            end
            self.ShadowOnsetOffsetLineHandles = NaN(2,2);
            
            validHandles = ishandle(self.ElectrodeTerminalForceLineHandles);
            if any(validHandles(:))
                delete(self.ElectrodeTerminalForceLineHandles(validHandles));
            end
            self.ElectrodeTerminalForceLineHandles = NaN(2,1);
            
            onsetOffsetMenu = uicontextmenu;
            uimenu(onsetOffsetMenu,'label', 'Delete','callback', @self.DeleteElectrodeOnsetOffset);
            
            plotHandle = self.ResultAxes(4);
            yLimits = get(plotHandle, 'yLim');
            self.ShadowOnsetOffsetLineHandles(1, 1) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(1), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(self.ShadowOnsetOffsetLineHandles(1, 1), 'bottom');
            self.ShadowOnsetOffsetLineHandles(1, 2) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(2), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(self.ShadowOnsetOffsetLineHandles(1, 2), 'bottom');
            if ~isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 1))
                self.ElectrodeOnsetOffsetLineHandles(1, 1) = line(...
                    'xData', ones(1, 2) * self.ElectrodeOnsetOffset(self.PWaveChannel, 1), 'yData', yLimits,...
                    'color', [0,0,1], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'onset',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            if ~isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 2))
                self.ElectrodeOnsetOffsetLineHandles(1, 2) = line(...
                    'xData', ones(1, 2) * self.ElectrodeOnsetOffset(self.PWaveChannel, 2), 'yData', yLimits,...
                    'color', [0,0,1], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'offset',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            if ~isnan(self.ElectrodeTerminalForce(self.PWaveChannel))
                self.ElectrodeTerminalForceLineHandles(1, 1) = line(...
                    'xData', ones(1, 2) * self.ElectrodeTerminalForce(self.PWaveChannel), 'yData', yLimits,...
                    'color', [0,1,0], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'terminalforce',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            
            plotHandle = self.ResultAxes(5);
            yLimits = get(plotHandle, 'yLim');
            self.ShadowOnsetOffsetLineHandles(2, 1) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(1), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            self.ShadowOnsetOffsetLineHandles(2, 2) = line(...
                'xData', ones(1, 2) * self.OnsetOffset(2), 'yData', yLimits,...
                'color', [1,.8,.8], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            if ~isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 1))
                self.ElectrodeOnsetOffsetLineHandles(2, 1) = line(...
                    'xData', ones(1, 2) * self.ElectrodeOnsetOffset(self.PWaveChannel, 1), 'yData', yLimits,...
                    'color', [.8,.8,1], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'onset',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            if ~isnan(self.ElectrodeOnsetOffset(self.PWaveChannel, 2))
                self.ElectrodeOnsetOffsetLineHandles(2, 2) = line(...
                    'xData', ones(1, 2) * self.ElectrodeOnsetOffset(self.PWaveChannel, 2), 'yData', yLimits,...
                    'color', [.8,.8,1], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'onset',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            if ~isnan(self.ElectrodeTerminalForce(self.PWaveChannel))
                self.ElectrodeTerminalForceLineHandles(2) = line(...
                    'xData', ones(1, 2) * self.ElectrodeTerminalForce(self.PWaveChannel), 'yData', yLimits,...
                    'color', [0, 1, 0], 'lineStyle', '--', 'lineWidth', 2, 'tag', 'terminalforce',...
                    'uicontextmenu', onsetOffsetMenu, 'parent', plotHandle);
            end
            
            self.ShowAverageElectrodeInformation();
        end
        
        function SetEstimatedOnsetOffset(self, plotHandle, onsetOffset)
            yLimits = get(plotHandle, 'yLim');
            onsetLineHandle = line(...
                'xData', ones(1, 2) * onsetOffset(1), 'yData', yLimits,...
                'color', [.7,.7,.7], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(onsetLineHandle, 'bottom');
            offsetLineHandle = line(...
                'xData', ones(1, 2) * onsetOffset(2), 'yData', yLimits,...
                'color', [.7,.7,.7], 'lineStyle', '--', 'lineWidth', 2,...
                'hitTest', 'off', 'parent', plotHandle);
            uistack(offsetLineHandle, 'bottom');
        end
        
        function SetElectrodeMenuColor(self)
            numberOfChannels = size(self.ElectrodeOnsetOffset, 1);
            for pWaveChannel = 1:numberOfChannels
                self.UpdateElectrodeMenuColor(pWaveChannel);
            end
        end
        
        function UpdateElectrodeMenuColor(self, pWaveChannel)
            if nargin < 2
                pWaveChannel = self.PWaveChannel;
            end
            
            if(~any(isnan(self.ElectrodeOnsetOffset(pWaveChannel, :))))
                colorString = 'green';
            elseif (any(~isnan(self.ElectrodeOnsetOffset(pWaveChannel, :))))
                colorString = 'red';
            else
                colorString = 'black';
            end
            electrodeLabel = self.PWaveChannelLabels{pWaveChannel};
            htmlString = sprintf('<HTML><BODY text = "%s">%s', colorString, electrodeLabel);
            pullDownString = get(self.PWaveChannelPopup, 'string');
            pullDownString{pWaveChannel} = htmlString;
            set(self.PWaveChannelPopup, 'string', pullDownString);
        end
        
        function DeleteElectrodeOnsetOffset(self, varargin)
            if strcmp(get(gco, 'tag'), 'onset')
                self.ElectrodeOnsetOffset(self.PWaveChannel, 1) = NaN;
            else
                if strcmp(get(gco, 'tag'), 'offset')
                    self.ElectrodeOnsetOffset(self.PWaveChannel, 2) = NaN;
                else
                    self.ElectrodeTerminalForce(self.PWaveChannel) = NaN;
                end
            end
            self.UpdateElectrodeOnsetOffSet();
            self.UpdateElectrodeMenuColor();
        end
        
        function GetPWaveRange(self,varargin)
            self.PWaveMatching.PWaveRange = self.PWaveMatching.EstimatePWaveRange(self.EcgData, self.RWaveIndices, self.QOnsetIndex, self.ValidChannels);
            
            set(self.PWaveRangeOnset, 'string', num2str(1000 * self.PWaveMatching.PWaveRange(1)))
            set(self.PWaveRangeOffset, 'string', num2str(1000 * self.PWaveMatching.PWaveRange(2)))
        end

        function ComputePWaveMatching(self, varargin)
            if isempty(self.EcgData), return; end
               
            [self.PWaveIntervals, self.BaselineCorrectedData, self.BaselineCorrectedReferenceData] = ...
                self.PWaveMatching.MatchPWaves(self.EcgData, self.RWaveIndices, self.ValidChannels);

            % P-wave duration algorithm here
            self.OnsetOffset(1,1:2) = self.PWaveMatching.DetectPStartEnd(self.EcgData, self.RWaveIndices, self.ValidChannels,...
                self.PWaveIntervals);

            if ~isnan(self.QOnsetIndex)
                self.OnsetOffset(1,3) = ((self.QOnsetIndex-self.RIndex)/self.EcgData.SamplingFrequency)*1e3;
            else 
                self.OnsetOffset(1,3) = nan;
            end

            pWaveData = self.PWaveMatching.GetPWaveData(self.EcgData, self.BaselineCorrectedData,...
                self.PWaveIntervals, self.OnsetOffset, nan(size(self.ValidChannels,2),2), self.ValidChannels,...
                nan(size(self.ValidChannels,2),1));
            
            v1Channel = find(ismember(self.EcgData.ElectrodeLabels, 'V1'));
            if isempty(v1Channel)
                v1Channel = find(ismember(self.EcgData.ElectrodeLabels, 'v1'));
            end
            
            self.ElectrodeTerminalForce = nan(size(self.ValidChannels,2),1);
            if ~isempty(v1Channel)
                self.ElectrodeTerminalForce(v1Channel) = self.PWaveMatching.DetectTerminalForce(pWaveData, v1Channel, self.EcgData.SamplingFrequency);
            end
            
            self.ShowECGData();
            
            self.ShowPWaveMatching();
            self.UpdateOnsetOffSet();

        end
        
        function ShowPWaveMatching(self)
            self.CreateResultAxes();           
            self.ShowUnfilteredAveragePWaveEnergy();
            self.ShowFilteredAveragePWaveEnergy();
            self.UpdateOnsetOffSet();
            self.ShowAveragePWave();
            self.UpdateElectrodeOnsetOffSet();
        end
        
        % Set functions
        function EnableBaselineCorrection(self, source, varargin)
            value = get(source, 'value');
            self.PWaveMatching.BaselineCorrection = value;
        end
        
        function SetBaselineBandwidth(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.PWaveMatching.BaselineBandwidth = value;
        end
        
        function SetTemplateShift(self, source, varargin)
            value = str2double(get(source, 'string')) / 1000;
            self.PWaveMatching.TemplateShift = value;
        end
        
        function SetCorrelationThreshold(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.PWaveMatching.CorrelationThreshold = value;
        end
        
        function SetPWaveRangeOnset(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.PWaveMatching.PWaveRange(1) = value / 1000;
        end
        
        function SetPWaveRangeOffset(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.PWaveMatching.PWaveRange(2) = value / 1000;
        end
        
        function EnableNotchFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.PWaveMatching.ApplyNotchFilter = enable;
            if enable && self.PWaveMatching.ApplyCombFilter
                self.PWaveMatching.ApplyCombFilter = false;
                set(self.CombCheckbox, 'value', false);
            end
        end
        
        function EnableCombFilter(self, source, varargin)
            enable = get(source, 'value') > 0;
            self.PWaveMatching.ApplyCombFilter = enable;
            if enable && self.PWaveMatching.ApplyNotchFilter
                self.PWaveMatching.ApplyNotchFilter = false;
                set(self.NotchCheckbox, 'value', false);
            end
        end
        
        function SetBandpassStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value < self.PWaveMatching.BandpassFrequencies(2)
               self.PWaveMatching.BandpassFrequencies(1) = value; 
            end
            set(source, 'string', num2str(self.PWaveMatching.BandpassFrequencies(1)));
        end
        
        function SetBandpassStop(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > self.PWaveMatching.BandpassFrequencies(1)
               self.PWaveMatching.BandpassFrequencies(2) = value; 
            end
            set(source, 'string', num2str(self.PWaveMatching.BandpassFrequencies(2)));
        end
        
        function SetBandpassOrder(self, source, varargin)
            value = round(str2double(get(source, 'string')));
            if value > 0
               self.PWaveMatching.BandpassOrder = value; 
            end
            set(source, 'string', num2str(self.PWaveMatching.BandpassOrder));
        end
        
        function SetNotchFrequency(self, source, varargin)
            value = str2num(get(source, 'string')); %#ok<ST2NM>
            if all(value > 0)
               self.PWaveMatching.NotchFrequency = value; 
            end
            set(source, 'string', num2str(self.PWaveMatching.NotchFrequency));
        end
        
        function SetNotchOrder(self, source, varargin)
            value = round(str2double(get(source, 'string')));
            if value > 0
               self.PWaveMatching.NotchOrder = value; 
            end
            set(source, 'string', num2str(self.PWaveMatching.NotchOrder));
        end
        
        function SetShowDiagnostics(self, source, varargin)
            self.PWaveMatching.ShowDiagnostics = get(source, 'value');
        end
    end
end