classdef PWaveVCGPanel < UserInterfacePkg.CustomPanel
    properties
        PWaveData
        Map
    end
    
    properties (Access = protected)
        ControlPanel
        MapView
        VCGView
        VCGLoopView
        
        ResultEdit
    end
    
    methods
        function self = PWaveVCGPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            self.CreateMapView();
            self.CreateVCGView();
            self.CreateVCGLoopView();
        end
        
        function SetPWaveData(self, pWaveData, map)
            self.PWaveData = pWaveData;
            self.Map = map;
            
            self.InitializeMapView();
        end
    end
    
    methods (Abstract, Access = protected)
        ShowXYZPWaves(self)
        
        ShowXYZLoop(self)
        
        ShowXYZData(self)
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.8, .5, .2, .5],...
                'bordertype', 'none');
            
            self.ResultEdit = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [0, 0, 1, .5],...
                'style', 'text',...
                'string', '');
        end
        
        function CreateMapView(self)
            self.MapView = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, 0, .5, 1],...
                'backgroundColor', [1,1,1]);
        end
        
        function CreateVCGView(self)
            self.VCGView = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.5, 0, .3, 1]);
        end
        
        function CreateVCGLoopView(self)
            self.VCGLoopView = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.8, 0, .2, .5]);
        end
        
        function InitializeMapView(self)
            mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            dataLabels = self.PWaveData.ElectrodeLabels;
            mapAxes = axes(...
                'parent', self.MapView,...
                'position', [0 0 1 1],...
                'xLim', [0, size(mapLabels, 2) + 1],...
                'yLim', [0 size(mapLabels, 1) + 1]);
            axis(mapAxes, 'image');
            
            % draw circles and electrode label text
            mapChannelIndex = NaN(size(mapLabels));
            N = 100;
            t = 2*pi/N*(1:N);
            radius = 0.75;
            hold(mapAxes, 'on');
            for i = 1:size(mapLabels, 1)
                for j = 1:size(mapLabels, 2)
                    channelPosition = find(strcmpi(mapLabels{i, j}, dataLabels), 1, 'first');
                    if ~isempty(channelPosition)
                        mapChannelIndex(i, j) = channelPosition;
                        color = [1, 1, 1];
                        fill(j + radius * cos(t), i + radius * sin(t), color);
                        text(j, i, mapLabels{i, j}, 'horizontalAlignment', 'center');
                    end
                end
            end
            hold(mapAxes, 'off');
            set(mapAxes, 'yDir', 'reverse');
            axis(mapAxes, 'off');
        end
       
        function ShowPWave(self, axesHandle, pWaveSignal)
            intervalTime = self.PWaveData.AveragingInterval;
            
            validTime = intervalTime >= self.PWaveData.OnsetOffset(1) & intervalTime <= self.PWaveData.OnsetOffset(2);
            fillTime = intervalTime(validTime);
            fillTime = [fillTime(1); fillTime(:); fillTime(end)];
            
            fill(fillTime, [0; pWaveSignal(validTime); 0], [.7, .7, .7], 'parent', axesHandle);
            
            line('xData', intervalTime, 'yData', zeros(size(intervalTime)),...
                'color', [.7, .7, .7], 'lineSmoothing', 'on', 'parent', axesHandle);
            
            line('xData', intervalTime, 'yData', pWaveSignal,...
                'color', [0, 0, 0], 'lineSmoothing', 'on',...
                'parent', axesHandle);
            
            yLimits = get(axesHandle, 'yLim');
            line(...
                'xData', ones(1, 2) * self.PWaveData.OnsetOffset(1), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2, 'parent', axesHandle);
            line(...
                'xData', ones(1, 2) * self.PWaveData.OnsetOffset(2), 'yData', yLimits,...
                'color', [1,0,0], 'lineStyle', '--', 'lineWidth', 2,  'parent', axesHandle);
            line(...
                'xData', ones(1, 2) * self.PWaveData.OnsetOffset(3), 'yData', yLimits,...
                'color', [.7,.7,.7], 'lineStyle', '--', 'lineWidth', 2,  'parent', axesHandle);
            axis(axesHandle, 'tight');
            
            ylabel(axesHandle, 'Voltage (mV)');
        end
    end
end