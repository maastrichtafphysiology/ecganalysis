classdef TEECGTemplateMatchingPanel < TemplateMatchingPanel
    methods
        function self = TEECGTemplateMatchingPanel(position)
            self = self@TemplateMatchingPanel(position);
        end
    end
    
    methods (Access = protected)
          function CreateControlPanel(self)
            CreateControlPanel@TemplateMatchingPanel(self);
            
            set(self.CompletedButton, 'position', [.1 0 .8 .15]);
          end
        
        function CreateTemplateMatchingSettings(self)
            
            templateSettingsPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .15 1 .34],...
                'borderType', 'none');
            
            uicontrol('parent', templateSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .8 .9 .2],...
                'style', 'pushbutton',...
                'string', 'Template editor...',...
                'callback', @self.ShowTemplateEditor);
            
            % template duration
            durationsPanel = uipanel('parent', templateSettingsPanel,...
                'units', 'normalized', ...
                'title', 'Template durations (ms)',...
                'position', [.05 .2 .9 .6]);
            self.DurationsControls(1) =...
                uicontrol('style', 'edit',...
                'parent', durationsPanel,...
                'units', 'normalized',...
                'position', [0 0 1/3 1],...
                'BackgroundColor', 'white',...
                'callback', @self.DurationControlCallback);
            self.DurationsControls(2) =...
                uicontrol('style', 'edit',...
                'parent', durationsPanel,...
                'units', 'normalized',...
                'position', [1/3 0 1/3 1],...
                'BackgroundColor', 'white',...
                'callback', @self.DurationControlCallback);
            self.DurationsControls(3) =...
                uicontrol('style', 'edit',...
                'parent', durationsPanel,...
                'units', 'normalized',...
                'position', [2/3 0 1/3 1],...
                'BackgroundColor', 'white',...
                'callback', @self.DurationControlCallback);
            self.SetDurationControls();
            
            uicontrol('parent', templateSettingsPanel,...
                'units', 'normalized',...
                'position', [.05 0 .9 .2],...
                'style', 'pushbutton',...
                'string', 'Compute template matching',...
                'callback', @self.ComputeTemplateMatching);
        end
        
        function ShowDeflections(self)
            if isempty(self.FilteredData)
                ShowDeflections@DeflectionECGChannelPanel(self);
            else
                unfilteredData = self.EcgData.Data;
                self.EcgData.Data = self.FilteredData;
                ShowDeflections@DeflectionECGChannelPanel(self);
                self.EcgData.Data = unfilteredData;
            end
            
            self.ShowFilteredData();
        end
        
        function ShowFilteredData(self)
            validHandles = ishandle(self.FilteredLineHandles);
            if any(validHandles)
                delete(self.FilteredLineHandles(validHandles));
            end
            
            if isempty(self.FilteredData), return; end
            
            time = self.EcgData.GetTimeRange();
            lineColor = [.7 .7 .7];
            self.FilteredLineHandles = NaN(numel(self.SelectedChannels), 1);
            for lineIndex = 1:numel(self.SelectedChannels)                
                    self.FilteredLineHandles(lineIndex) = line('xData', time, 'yData', self.FilteredData(:, self.SelectedChannels(lineIndex)),...
                        'color', lineColor,...
                        'parent', self.AxesHandles(lineIndex),...
                        'lineSmoothing', 'on');
                    uistack(self.FilteredLineHandles(lineIndex), 'down');
            end
        end
    end
    
    methods (Access = private)
        function ShowTemplateEditor(self, varargin)
            self.TemplateEditorFigure.Show();
        end
        
        function ComputeTemplateMatching(self, varargin)
            if isempty(self.EcgData), return; end
            
            [self.DeflectionData, self.FilteredData] = self.DeflectionDetector.Apply(self.EcgData, self.TemplateEditorFigure.SelectedTemplates());
            self.ShowDeflections();
        end
        
        function DurationControlCallback(self, varargin)
            durationsStart = round(str2double(get(self.DurationsControls(1), 'string')));
            durationsStep = round(str2double(get(self.DurationsControls(2), 'string')));
            durationsEnd = round(str2double(get(self.DurationsControls(3), 'string')));
            if any(isnan([durationsStart, durationsStep, durationsEnd]))
                return;
            end
            
            if durationsStart <= durationsEnd || (isnan(durationsStart) || isnan(durationsEnd))
                durations = durationsStart:durationsStep:durationsEnd;
                self.DeflectionDetector.TemplateDurations = durations;
                applicationSettings = ApplicationSettings.Instance();
                applicationSettings.TemplateMatching.Durations = durations;
            end
            self.SetDurationControls();
        end
        
        function SetDurationControls(self)
            durations = self.DeflectionDetector.TemplateDurations;
            step = round(durations(end) - durations(1)) / (numel(durations) - 1);
            set(self.DurationsControls(1), 'string', num2str(durations(1)));
            set(self.DurationsControls(3), 'string', num2str(durations(end)));
            set(self.DurationsControls(2), 'string', num2str(step));
        end
        
        function EnableBandpassFilter(self, source, varargin)
            self.DeflectionDetector.ApplyBandpassFilter = get(source, 'value') > 0;
            
            if self.DeflectionDetector.ApplyRectifiedFilter
                self.DeflectionDetector.ApplyRectifiedFilter = false;
                set(self.RectifiedFilterCheck, 'value', false);
            end
        end
        
        function EnableRectifiedFilter(self, source, varargin)
            self.DeflectionDetector.ApplyRectifiedFilter = get(source, 'value') > 0;
            
            if self.DeflectionDetector.ApplyBandpassFilter
                self.DeflectionDetector.ApplyBandpassFilter = false;
                set(self.BandpassFilterCheck, 'value', false);
            end
        end
        
        function SetBandpassStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0 && value < self.DeflectionDetector.BandpassFrequencies(2)
               self.DeflectionDetector.BandpassFrequencies(1) = value; 
            end
            set(source, 'string', num2str(self.DeflectionDetector.BandpassFrequencies(1)));
        end
        
        function SetBandpassStop(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > self.DeflectionDetector.BandpassFrequencies(1)
               self.DeflectionDetector.BandpassFrequencies(2) = value; 
            end
            set(source, 'string', num2str(self.DeflectionDetector.BandpassFrequencies(2)));
        end
        
        function SetBandpassOrder(self, source, varargin)
            value = round(str2double(get(source, 'string')));
            if value > 0
               self.DeflectionDetector.BandpassOrder = value; 
            end
            set(source, 'string', num2str(self.DeflectionDetector.BandpassOrder));
        end
        
        function SetFilterControls(self)
            set(self.BandpassFilterCheck, 'value', self.DeflectionDetector.ApplyBandpassFilter);
            set(self.BandpassStartEdit, 'string', num2str(self.DeflectionDetector.BandpassFrequencies(1)));
            set(self.BandpassStopEdit, 'string', num2str(self.DeflectionDetector.BandpassFrequencies(2)));
            set(self.BandpassOrderEdit, 'string', num2str(self.DeflectionDetector.BandpassOrder));
        end
        
        function SaveTemplateMatching(self, filename)
            ecgData = self.EcgData; %#ok<NASGU>
            deflectionDetectionResults = self.DeflectionData; %#ok<NASGU>
            deflectionDetector = self.DeflectionDetector; %#ok<NASGU>
            filteredData = self.FilteredData; %#ok<NASGU>
            
            save(filename,...
                'ecgData',...
                'filteredData',...
                'deflectionDetectionResults',...
                'deflectionDetector');
        end
        
        function SaveCurrentTemplateMatching(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_TM'];
            
            self.SaveTemplateMatching(filename);
        end
        
        function SaveNewTemplateMatching(self, varargin)
            [filename, pathname] = HelperFunctions.customUiputfile('_TM.mat', 'Save template matching result as...');
            
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            [pathString, nameString] = fileparts(filename);
            
            if ~strcmp(nameString((end-2):end), '_TM')
                errordlg('Filename must end with "_TM"', 'File name error');
                return;
            end
            self.EcgData.Filename = fullfile(pathString, nameString(1:(end - 3)));
            
            self.SaveTemplateMatching(filename);
        end
        
        function LoadCurrentTemplateMatching(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_TM.mat', 'Select TM file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_TM.mat'];
            end
            
            self.LoadData(filename);
        end
        
        function LoadNewTemplateMatching(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*_TM.mat', 'Select TM file');
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            self.LoadData(filename);
        end
    end
end