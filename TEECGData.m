classdef TEECGData < ECGData
    properties
        TEData
        TELabels
    end
    
    methods
        function self = TEECGData(filename, data, samplingFrequency, varargin)
            self = self@ECGData(filename, data, samplingFrequency, varargin{:}); 
        end
        
        function SetTEData(self, teData, teLabels)
            self.TEData = teData;
            self.TELabels = teLabels;
        end
        
        function value = GetNumberOfTEChannels(self)
            value = size(self.TEData, 2);
        end
        
        function ecgDataCopy = Copy(self, selectedChannels, timeRange)
            time = self.GetTimeRange();
            if nargin < 3
                timeRange = [time(1) time(end)];
                if nargin < 2
                    selectedECGChannels = true(size(self.Data, 2), 1);
                    selectedTEChannels = true(size(self.TEData, 2), 1);
                    selectedChannels = {selectedECGChannels, selectedTEChannels};
                end
            end
            
            selectedECGChannels = selectedChannels{1};
            selectedTEChannels = selectedChannels{2};
            
            validTime = time >= timeRange(1) & time <= timeRange(2);
            data = self.Data(validTime, selectedECGChannels);
            labels = self.ElectrodeLabels(selectedECGChannels);
            ecgDataCopy = TEECGData(self.Filename, data,...
                self.SamplingFrequency,...
                labels);
            
            if ~isempty(self.ReferenceData)
                ecgDataCopy.ReferenceData = self.ReferenceData(validTime, :);
                ecgDataCopy.ReferenceLabels = self.ReferenceLabels;
            end
            
            if ~isempty(self.TEData)
                teData = self.TEData(validTime, selectedTEChannels);
                teLabels = self.TELabels(selectedTEChannels);
                ecgDataCopy.SetTEData(teData, teLabels)
            end
            
            ecgDataCopy.Type = self.Type;
        end
    end
end