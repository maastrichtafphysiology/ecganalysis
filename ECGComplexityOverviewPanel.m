classdef ECGComplexityOverviewPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        ChannelsToAnalyze
        ExcludedIntervals
        
        DFData
        FWAData
    end
    
    properties (Access = protected)
        ControlPanel
        ChannelParameterTable
        
        Channel1Popup
        Channel2Popup
        
        DFComparisonAxis
        FWAComparisonAxis
    end
    
    methods
        function self = ECGComplexityOverviewPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position); 
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
        end
        
        function SetECGData(self, ecgData)
            self.EcgData = ecgData;
            
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            self.ChannelsToAnalyze = true(numberOfChannels, 1);
            
            dfData = NaN(numberOfChannels, 1);
            fwaData = NaN(numberOfChannels, 1);
            
            tableData = [num2cell([dfData, fwaData]), num2cell(self.ChannelsToAnalyze)];
            
            electrodeLabels = self.EcgData.ElectrodeLabels;
            self.ChannelParameterTable.Data = tableData;
            self.ChannelParameterTable.ColumnName = {'DF', 'FWA', 'Analyze'};
            self.ChannelParameterTable.RowName = electrodeLabels;
            self.ChannelParameterTable.ColumnEditable = [false, false, true];
            
            set(self.Channel1Popup, 'string', electrodeLabels);
            set(self.Channel2Popup, 'string', electrodeLabels);
        end
        
        function SetDFData(self, dfData)
            self.DFData = dfData;
            self.ShowChannelTableData();
            self.ShowChannelComparison();
        end
        
        function SetFWAData(self, fwaData)
           self.FWAData = fwaData;
           self.ShowChannelTableData();
           self.ShowChannelComparison();
        end
        
        function SetChannelsToAnalyze(self, channelsToAnalyze)
            self.ChannelsToAnalyze = channelsToAnalyze;
            self.ChannelParameterTable.Data(:, end) = num2cell(self.ChannelsToAnalyze);
        end
        
        function SetQCData(self, excludedIntervals)
            self.ExcludedIntervals = excludedIntervals;
            self.ShowChannelComparison();
        end
        
        function delete(self)
            self.EcgData = [];
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
           self.CreateChannelParameterOverview();
           self.CreateChannelComparison();
           
           uicontrol('parent', self.ControlHandle,...
               'units', 'normalized',...
               'position', [.05 0 .1 .05],...
               'style', 'pushbutton',...
               'string', 'Save channel selection',...
               'callback', @self.SaveChannelSelection);
           
           uicontrol('parent', self.ControlHandle,...
               'units', 'normalized',...
               'position', [.15 0 .1 .05],...
               'style', 'pushbutton',...
               'string', 'Export overview data',...
               'callback', @self.ExportData);
        end
        
        function CreateChannelParameterOverview(self)
            self.ChannelParameterTable = uitable(...
                'parent', self.ControlHandle,...
                'units', 'normalized',...
                'position', [0 .1 .3 .9],...
                'backgroundColor', [1 1 1],...
                'cellEditCallback', @self.ChangeChannelSelection,...
                'cellSelectionCallback', [],...
                'interruptible', 'off',...
                'busyAction', 'queue');
        end
        
        function CreateChannelComparison(self)
            comparisonPanel = uipanel(...
                'parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [.3 0 .7 1]);
            
            self.Channel1Popup = uicontrol(...
                'parent', comparisonPanel,...
                'units', 'normalized',...
                'position', [.1 .9 .4 .1],...
                'style', 'popup',...
                'string', 'No leads available',...
                'callback', @self.ShowChannelComparison);
            
            self.Channel2Popup = uicontrol(...
                'parent', comparisonPanel,...
                'units', 'normalized',...
                'position', [.5 .9 .4 .1],...
                'style', 'popup',...
                'string', 'No leads available',...
                'callback', @self.ShowChannelComparison);
            
            self.DFComparisonAxis = axes(...
                'parent', comparisonPanel,...
                'units', 'normalized',...
                'outerposition', [0 .45 1 .45],...
                'title', 'Dominant frequency (DF)');
            
            self.FWAComparisonAxis = axes(...
                'parent', comparisonPanel,...
                'units', 'normalized',...
                'outerposition', [0 0 1 .45],...
                'title', 'F-wave amplitude (DF)');
        end
        
        function ShowChannelTableData(self)
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            
            if isempty(self.DFData)
                dfData = NaN(numberOfChannels, 1);
            else
                dfData = [self.DFData.windowedDF.expandingWindowMean];
                dfData = dfData(end, :)';
            end
            
            if isempty(self.FWAData)
                fwaData = NaN(numberOfChannels, 1);
            else
                fwaData = [self.FWAData.windowedFWA.expandingWindowMean];
                fwaData = fwaData(end, :)';
            end
            
            tableData = [num2cell([dfData, fwaData]), num2cell(self.ChannelsToAnalyze)];
            self.ChannelParameterTable.Data = tableData;
        end
        
        function ShowChannelComparison(self, varargin)
            selectedLead1Index = get(self.Channel1Popup, 'value');
            selectedLead2Index = get(self.Channel2Popup, 'value');
            
            electrodeLabels = self.EcgData.ElectrodeLabels;
            time = self.EcgData.GetTimeRange();
            colors = lines(2);
            invalidColor = [.7 .7 .7];
            
            validTime = true(size(time));
            numberOfIntervals = size(self.ExcludedIntervals, 1);
            for intervalIndex = 1:numberOfIntervals
                intervalStart = self.ExcludedIntervals{intervalIndex, 1};
                intervalEnd = self.ExcludedIntervals{intervalIndex, 2};
                if ~(isempty(intervalStart) || isempty(intervalEnd)) &&...
                        intervalStart < intervalEnd
                    validTime(time >= intervalStart & time < intervalEnd) = false;
                end
            end
            
            % DF
            if ~isempty(self.DFData)
                dfData1 = self.DFData.windowedDF(selectedLead1Index);
                dfData2 = self.DFData.windowedDF(selectedLead2Index);
                plotHandle = self.DFComparisonAxis;
                
                cla(plotHandle);
                line('xData', time(dfData1.windowCenter),'yData', dfData1.runningWindowMean,...
                    'color', colors(1, :), 'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', colors(1, :),...
                    'hitTest', 'off', 'displayName', electrodeLabels{selectedLead1Index},...
                    'parent', plotHandle);
                line('xData', time(dfData2.windowCenter),'yData', dfData2.runningWindowMean,...
                    'color', colors(2, :), 'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', colors(2, :),...
                    'hitTest', 'off', 'displayName', electrodeLabels{selectedLead2Index},...
                    'parent', plotHandle);
                
                invalidCenters = ~validTime(dfData1.windowCenter);
                if any(invalidCenters)
                    line('xData', time(dfData1.windowCenter(invalidCenters)),...
                        'yData',dfData1.runningWindowMean(invalidCenters),...
                    'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', invalidColor,...
                    'hitTest', 'off', 'lineStyle', 'none',...
                    'displayName', [electrodeLabels{selectedLead1Index}, ' excluded'],...
                    'parent', plotHandle);
                line('xData', time(dfData2.windowCenter(invalidCenters)),...
                    'yData',dfData2.runningWindowMean(invalidCenters),...
                    'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', invalidColor,...
                    'hitTest', 'off', 'lineStyle', 'none',...
                    'displayName', [electrodeLabels{selectedLead2Index}, ' excluded'],...
                    'parent', plotHandle);
                end
                
                legend(plotHandle, 'show');
                
                xlabel(plotHandle, 'time (seconds)');
                ylabel(plotHandle, 'DF (Hz)');
                title(plotHandle, 'Dominant frequency (DF)');
                
                [rankCorrelation, rankPValue] = corr(dfData1.runningWindowMean, dfData2.runningWindowMean,...
                    'type', 'spearman');
                [linearCorrelation, linearPValue] = corr(dfData1.runningWindowMean, dfData2.runningWindowMean,...
                    'type', 'pearson');
                
                infoText = {...
                    ['Spearman: ', num2str(rankCorrelation, '%.2f'), ' (p=', num2str(rankPValue, '%.2e'), ')'];...
                    ['Pearson: ', num2str(linearCorrelation, '%.2f'), ' (p=', num2str(linearPValue, '%.2e'), ')']};
                
                if any(invalidCenters) && ~all(invalidCenters)
                [rankCorrelationCorrected, rankPValueCorrected] =...
                    corr(dfData1.runningWindowMean(~invalidCenters),...
                    dfData2.runningWindowMean(~invalidCenters),...
                    'type', 'spearman');
                [linearCorrelationCorrected, linearPValueCorrected] =...
                    corr(dfData1.runningWindowMean(~invalidCenters),...
                    dfData2.runningWindowMean(~invalidCenters),...
                    'type', 'pearson');
                
                 infoText{1} = [infoText{1}, ' | Adjusted: ', num2str(rankCorrelationCorrected, '%.2f'), ' (', num2str(rankPValueCorrected, '%.2e'), ')'];
                 infoText{2} = [infoText{2}, ' | Adjusted: ', num2str(linearCorrelationCorrected, '%.2f'), ' (', num2str(linearPValueCorrected, '%.2e'), ')'];
                end
                
                text(0, 1, 1, infoText, 'units', 'normalized',...
                    'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'left',...
                    'verticalAlignment', 'bottom', 'parent', plotHandle);
            end
            
            % FWA
            if ~isempty(self.FWAData)
                fwaData1 = self.FWAData.windowedFWA(selectedLead1Index);
                fwaData2 = self.FWAData.windowedFWA(selectedLead2Index);
                
                plotHandle = self.FWAComparisonAxis;
                
                cla(plotHandle);
                line('xData', time(fwaData1.windowCenter),'yData',fwaData1.runningWindowMean,...
                    'color', colors(1, :), 'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', colors(1, :),...
                    'lineSmoothing', 'on', 'hitTest', 'off', 'displayName', electrodeLabels{selectedLead1Index},...
                    'parent', plotHandle);
                line('xData', time(fwaData2.windowCenter),'yData',fwaData2.runningWindowMean,...
                    'color', colors(2, :), 'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', colors(2, :),...
                    'lineSmoothing', 'on', 'hitTest', 'off', 'displayName', electrodeLabels{selectedLead2Index},...
                    'parent', plotHandle);
                
                invalidCenters = ~validTime(fwaData1.windowCenter);
                if any(invalidCenters)
                    line('xData', time(fwaData1.windowCenter(invalidCenters)),...
                        'yData',fwaData1.runningWindowMean(invalidCenters),...
                    'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', invalidColor,...
                    'hitTest', 'off', 'lineStyle', 'none',...
                    'displayName', [electrodeLabels{selectedLead1Index}, ' excluded'],...
                    'parent', plotHandle);
                line('xData', time(fwaData2.windowCenter(invalidCenters)),...
                    'yData',fwaData2.runningWindowMean(invalidCenters),...
                    'marker', 'o', 'markerEdgeColor', [0,0,0], 'markerFaceColor', invalidColor,...
                    'hitTest', 'off', 'lineStyle', 'none',...
                    'displayName', [electrodeLabels{selectedLead2Index}, ' excluded'],...
                    'parent', plotHandle);
                end
                
                legend(plotHandle, 'show');
                
                xlabel(plotHandle, 'time (seconds)');
                ylabel(plotHandle, 'FWA (mV)');
                title(plotHandle, 'F-Wave amplitude');
                
                [rankCorrelation, rankPValue] = corr(fwaData1.runningWindowMean, fwaData2.runningWindowMean,...
                    'type', 'spearman');
                [linearCorrelation, linearPValue] = corr(fwaData1.runningWindowMean, fwaData2.runningWindowMean,...
                    'type', 'pearson');
                
                infoText = {...
                    ['Spearman: ', num2str(rankCorrelation, '%.2f'), ' (p=', num2str(rankPValue, '%.2e'), ')'];...
                    ['Pearson: ', num2str(linearCorrelation, '%.2f'), ' (p=', num2str(linearPValue, '%.2e'), ')']};
                
                if any(invalidCenters) && ~all(invalidCenters)
                [rankCorrelationCorrected, rankPValueCorrected] =...
                    corr(fwaData1.runningWindowMean(~invalidCenters),...
                    fwaData2.runningWindowMean(~invalidCenters),...
                    'type', 'spearman');
                [linearCorrelationCorrected, linearPValueCorrected] =...
                    corr(fwaData1.runningWindowMean(~invalidCenters),...
                    fwaData2.runningWindowMean(~invalidCenters),...
                    'type', 'pearson');
                
                 infoText{1} = [infoText{1}, ' | Adjusted: ', num2str(rankCorrelationCorrected, '%.2f'), ' (', num2str(rankPValueCorrected, '%.2e'), ')'];
                 infoText{2} = [infoText{2}, ' | Adjusted: ', num2str(linearCorrelationCorrected, '%.2f'), ' (', num2str(linearPValueCorrected, '%.2e'), ')'];
                end
                
                text(0, 1, 1, infoText, 'units', 'normalized',...
                    'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'left',...
                    'verticalAlignment', 'bottom', 'parent', plotHandle);
            end
        end
        
        function ChangeChannelSelection(self,  ~, cellEditData)
           selectedIndex = cellEditData.Indices;
            if selectedIndex(2) == 3
                self.ChannelsToAnalyze(selectedIndex) = cellEditData.EditData;
            end
        end
        
        function SaveChannelSelection(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            ChannelsToAnalyze = self.ChannelsToAnalyze; %#ok<PROPLC>
                      
            if exist(filename, 'file')
                save(filename, 'ChannelsToAnalyze', '-append');
            else
                errordlg('Please save DF and/or F-Wave analysis first', 'No _Result.mat file found');
            end
        end
        
        function ExportData(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            electrodeLabels = self.EcgData.ElectrodeLabels;
            time = self.EcgData.GetTimeRange();
            
            validTime = true(size(time));
            numberOfIntervals = size(self.ExcludedIntervals, 1);
            for intervalIndex = 1:numberOfIntervals
                intervalStart = self.ExcludedIntervals{intervalIndex, 1};
                intervalEnd = self.ExcludedIntervals{intervalIndex, 2};
                if ~(isempty(intervalStart) || isempty(intervalEnd)) &&...
                        intervalStart < intervalEnd
                    validTime(time >= intervalStart & time < intervalEnd) = false;
                end
            end
            
            if ~isempty(self.DFData)
                outputFilename = [pathString, filesep, nameString '_overviewDF.csv'];
                
                centerIndex = self.DFData.windowedDF(1).windowCenter;
                validCenters = validTime(centerIndex);
                
                resultTable = array2table([time(centerIndex)',...
                    validCenters',...
                    horzcat(self.DFData.windowedDF.runningWindowMean),...
                    horzcat(self.DFData.windowedSC.runningWindowMean)],...
                    'variableNames', ['time'; 'valid';...
                    electrodeLabels; strcat(electrodeLabels, {'_SC'})]);
                
                writetable(resultTable, outputFilename, 'WriteRowNames', false);
            end
            
            if ~isempty(self.FWAData)
                outputFilename = [pathString, filesep, nameString '_overviewFWA.csv'];
                
                centerIndex = self.FWAData.windowedFWA(1).windowCenter;
                validCenters = validTime(centerIndex);
                
                resultTable = array2table([time(centerIndex)',...
                    validCenters',...
                    horzcat(self.FWAData.windowedFWA.runningWindowMean)],...
                    'variableNames', ['time'; 'valid'; electrodeLabels]);
                
                writetable(resultTable, outputFilename, 'WriteRowNames', false);
            end
            
        end
    end
end