%% computeGlobalPWaveDuration
% Computes global P-wave duration from AFAB extended ECG data

%% Load patient IDs
load('D:\AFAB registry\ECG Data\exECG Data\includedPatientsBaselineStudy.mat')
folder='D:\AFAB registry\ECG Data\exECG Data';

figureHandle = figure('name', 'P-Wave averages & annotations',...
    'numberTitle', 'off', 'toolbar', 'none', 'menu', 'none',...
    'position', [100, 100, 1200, 800]);
set(figureHandle,'PaperOrientation','landscape');
set(figureHandle,'PaperUnits','normalized');
set(figureHandle,'PaperPosition', [0 0 1 1]);

pWaveDurationView = PWaveDurationPanel([0 0 1 1]);
pWaveDurationView.Create(figureHandle);
pWaveDurationView.Show();

% loop patients
for curPatient = 1:size(baselinePatients,1)
    filename = fullfile(folder, num2str(baselinePatients.ID(curPatient)), baselinePatients.File{curPatient});
    pWaveData = load(filename);

    pWaveDurationView.SetPWaveDurationData(pWaveData);
    pWaveDurationView.DetectPStartEnd(pWaveData);
    pWaveData.electrodeOnsetOffset=repmat(pWaveDurationView.OnsetOffset(1,[1 2]),...
        size(pWaveData.electrodeOnsetOffset,1),1);
        
    pWaveData.onsetOffset = pWaveDurationView.OnsetOffset;
    
    pWaveDurationView.SetPWaveDurationData(pWaveData);
    
    exportFilename = insertAfter(filename,'_T1_','glP_');
    save(exportFilename,'pWaveData')
        
    figureFilename = fullfile(folder, num2str(baselinePatients.ID(curPatient)), ...
        [num2str(baselinePatients.ID(curPatient)) '_glP.pdf']);
%     print(figureHandle, '-dpdf', figureFilename);
end
%% computeGlobalPWaveDuration
% Computes global P-wave duration from AFAB extended ECG data
 
%% Load patient IDs
load('D:\AFAB registry\ECG Data\exECG Data\includedPatientsBaselineStudy.mat')
folder='D:\AFAB registry\ECG Data\exECG Data';
 
figureHandle = figure('name', 'P-Wave averages & annotations',...
    'numberTitle', 'off', 'toolbar', 'none', 'menu', 'none',...
    'position', [100, 100, 1200, 800]);
set(figureHandle,'PaperOrientation','landscape');
set(figureHandle,'PaperUnits','normalized');
set(figureHandle,'PaperPosition', [0 0 1 1]);
 
pWaveDurationView = PWaveDurationPanel([0 0 1 1]);
pWaveDurationView.Create(figureHandle);
pWaveDurationView.Show();
 
% loop patients
for curPatient = 1:size(baselinePatients,1)
    filename = fullfile(folder, num2str(baselinePatients.ID(curPatient)), baselinePatients.File{curPatient});
    pWaveData = load(filename);
 
    pWaveDurationView.SetPWaveDurationData(pWaveData);
    pWaveDurationView.DetectPStartEnd(pWaveData);
    pWaveData.electrodeOnsetOffset=repmat(pWaveDurationView.OnsetOffset(1,[1 2]),...
        size(pWaveData.electrodeOnsetOffset,1),1);
    pWaveData.onsetOffset = pWaveDurationView.OnsetOffset;
    
    pWaveDurationView.SetPWaveDurationData(pWaveData);   
    
    numberOfEcgLeads=size(pWaveData.baselineCorrectedData,2);
    if numberOfEcgLeads==22 && nanmean(max(pWaveData.baselineCorrectedData))<50 
        pWaveData.baselineCorrectedData=pWaveData.baselineCorrectedData*2e3;
    end
    
    exportFilename = insertAfter(filename,'_T1_','glP_');
    save(exportFilename,'-struct','pWaveData')
        
%     figureFilename = fullfile(folder, num2str(baselinePatients.ID(curPatient)), ...
%         [num2str(baselinePatients.ID(curPatient)) '_glP.pdf']);
%     print(figureHandle, '-dpdf', figureFilename);
end
