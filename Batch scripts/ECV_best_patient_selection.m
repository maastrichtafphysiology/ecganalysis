load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECV database_2013_11_27.mat');

rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/QRST Cancellation 2013_11_27';
load(fullfile(rootFolder, 'analysisResults_2013_11_27.mat'));

load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECGQuality_2013_11_27.mat');
qualityECGIDs = ECGQuality.FileID(ECGQuality.ECG > 1 & ECGQuality.QRST > 1);
[qualityECG, ~] = ismember(ecgIdList, qualityECGIDs);

dataMatrix = result(qualityECG, :);
labels = parameterLabels';

thresholds = (70:7:140)';
nSR = zeros(size(thresholds));
nAF = zeros(size(thresholds));

parameterPerformance = NaN(numel(thresholds), 3);

for thresholdIndex = 1:numel(thresholds)

recurrenceThreshold = thresholds(thresholdIndex);
minimumFU = max([0, recurrenceThreshold - 28]);

outcome = ComputeECVRecurrence(ElectricalCardioversion, recurrenceThreshold, minimumFU);

nSR(thresholdIndex) = numel(find(outcome == 0));
nAF(thresholdIndex) = numel(find(outcome == 1));

% select valid outcome
outcomeIDs = (1:numel(outcome))';
validOutcome = outcome(~isnan(outcome));
validOutcomeIDs = outcomeIDs(~isnan(outcome));

% matchs ECG IDs to valid outcome IDs
[availableOutcomeECG, ecgIndex] = ismember(validOutcomeIDs, ecgIdList(qualityECG));
validECGOutcome = validOutcome(availableOutcomeECG);
validECGOutcomeIDs = validOutcomeIDs(availableOutcomeECG);

ecgData = mat2dataset([dataMatrix(ecgIndex(ecgIndex > 0), :), validECGOutcome], 'VarNames', [labels; 'Outcome']);
ecgData.Outcome = logical(ecgData.Outcome);
ecgData = ecgData(:, [1:48, 61:end]);

parameterRange = 1:60;
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(ecgData(:, [parameterRange, end]));

parameterPerformance(thresholdIndex, :) = [sensitivity, specificity, mean(AUC)];
disp([num2str(thresholds(thresholdIndex)), ' AUC: ', num2str(mean(AUC)), '�', num2str(std(AUC))]);
end

