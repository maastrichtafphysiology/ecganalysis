%% load clinical database
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/Clinical DB 20131030.mat');
outcome = BordeauxAblation.Termination_AF;
outcomeIDs = BordeauxAblation.Number;
validOutcomes = ~isnan(outcome);
disp(['Patients with outcome: ', num2str(numel(find(validOutcomes)))]);

outcome = outcome(validOutcomes);
outcomeIDs = outcomeIDs(validOutcomes);

%% locate available ECGs
% rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/FU';
dataListing = dir(fullfile(rootFolder, '*_QRST.mat'));
filenameList = cell(numel(dataListing), 1);
idList = cell(numel(dataListing), 1);
for listingIndex = 1:numel(dataListing)
    filename = dataListing(listingIndex).name;
    filenameList{listingIndex} = fullfile(rootFolder, filename);
    stringParts = textscan(filename, '%s', 'delimiter', {' ', '_'});
    idList{listingIndex} = ['FU ', stringParts{1}{4}];
end

additionalFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/Add';
additionalDataListing = dir(fullfile(additionalFolder, '*_QRST.mat'));
additionalFilenameList = cell(numel(additionalDataListing), 1);
additionalIdList = cell(numel(additionalDataListing), 1);
for listingIndex = 1:numel(additionalDataListing)
    filename = additionalDataListing(listingIndex).name;
    additionalFilenameList{listingIndex} = fullfile(additionalFolder, filename);
    stringParts = textscan(filename, '%s', 'delimiter', {' ', '_'});
    additionalIdList{listingIndex} = ['Add ', stringParts{1}{4}];
end

filenameList = [filenameList; additionalFilenameList];
idList = [idList; additionalIdList];

%% Match ECG's to outcome
[availableOutcomeECG, fileIDIndex] = ismember(outcomeIDs, idList);
validOutcomes = outcome(availableOutcomeECG);
validOutcomeIDs = outcomeIDs(availableOutcomeECG);
validFilenameList = filenameList(fileIDIndex(availableOutcomeECG));

%% Optimize individual parameter settings
analyser = NonInvasiveBatchAnalyser();
% analyser.ComputeDFMaxAUC(validFilenameList, validOutcomes > 0, [2^7, 2^8, 2^9, 2^10, 2^11, 2^12]);
% analyser.ComputeOIMaxAUC(validFilenameList, validOutcomes > 0, 1:10);
% analyser.ComputeSEMaxAUC(validFilenameList, validOutcomes > 0, [2^7, 2^8, 2^9, 2^10, 2^11, 2^12]);
% analyser.ComputePCAMaxAUC(validFilenameList, validOutcomes > 0, 0.75:0.05:0.95);
% analyser.ComputeFWaveAmplitudeMaximumAUC(validFilenameList, validOutcomes > 0, 10:5:200);
% analyser.ComputeSampleEntropyMaximumAUC(validFilenameList, validOutcomes > 0,...
%     [2^9 2^10 2^11], 2:10, 0.1:0.05:1);
% analyser.ComputeWEMaxAUC(validFilenameList, validOutcomes > 0, 4:12);

%% Compute noninvasive parameters
try
    matlabpool close;
end
matlabpool;

analyser = NonInvasiveBatchAnalyser();
[result, parameterLabels, patientIDs] = analyser.ComputeNonInvasiveParameters('', validFilenameList);
% [result, parameterLabels, patientIDs] = analyser.ComputeMultivariateParameters('', validFilenameList);
save(fullfile(rootFolder, 'Ablation_SingleLead_1Hz_PWELCH_default_2014_05_23.mat'), 'result', 'parameterLabels',...
    'validOutcomes', 'validOutcomeIDs', 'validFilenameList');

matlabpool close;

%% Load result data
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/FU';
load(fullfile(rootFolder, 'analysisResults_2014_04_22.mat'));

dataMatrix = result;
labels = parameterLabels';

% load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/ExcludedECGs.mat');
% validIDs = ~ismember(lower(validOutcomeIDs), lower(excludedIDs));
% validOutcomes = validOutcomes(validIDs);
% dataMatrix = dataMatrix(validIDs, :);

%% Logistic regression
% validParameterIndices = bsxfun(@plus, (7:9)', 12 * (0:4));
validParameterIndices = 1:60;
% validParameterIndices = 37:48;
validParameterIndices = validParameterIndices(:);
patientRange = 1:size(dataMatrix, 1);
p = randperm(numel(validParameterIndices));
validParameterIndices = validParameterIndices(p);

ds = mat2dataset([dataMatrix(patientRange, validParameterIndices), validOutcomes(patientRange)], 'VarNames', [labels(validParameterIndices); 'Outcome']);
ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));

[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(ds);
% sensitivity = confusion(2, 2) / sum(confusion(2, :));
% specificity = confusion(1, 1) / sum(confusion(1, :));
ROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);


%% Lasso LR (Cross validation)
validParameterIndices = [1:48, 61:72];
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = ...
    NonInvasiveBatchAnalyser.ComputeLassoGLM(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

parameterPerformance = [sensitivity, specificity, AUC];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% LDA (CVShrink)
validParameterIndices = 52:66;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, AUC, TPR, FPR, ldaClassifier, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeOptimalLDA(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

parameterPerformance = [sensitivity, specificity, AUC];
disp(ldaClassifier);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% SVM
validParameterIndices = 13:24;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, svmStruct] = ...
    NonInvasiveBatchAnalyser.ComputeOptimizedSVM(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

disp(svmStruct);
%% Decision Tree
validParameterIndices = [1:48, 61:72];
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

NonInvasiveBatchAnalyser.ComputeDecisionTree(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

%% Parameter per lead
leadResults = NaN(numel(labels), 3);
patientRange = 1:numel(validOutcomes);
for validParameterIndex = 1:numel(labels);
    ds = mat2dataset([dataMatrix(patientRange, validParameterIndex), validOutcomes(patientRange)], 'VarNames', [labels(validParameterIndex); 'Cardioversion']);
        ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));
     [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(ds);
    
    leadResults(validParameterIndex, 1) = sensitivity;
    leadResults(validParameterIndex, 2) = specificity;
    leadResults(validParameterIndex, 3) = AUC;
end

%% Bootstrapped logistic regression
maximumNumberOfParameters = 6;
numberOfIterations = 1e6;
validParameterIndices = [1:48, 61:72];
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);
p = randperm(numel(validParameterIndices));
validParameterIndices = validParameterIndices(p);

ds = mat2dataset([dataMatrix(patientRange, validParameterIndices), validOutcomes(patientRange)], 'VarNames', [labels(validParameterIndices); 'Cardioversion']);
ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));

parameterScore = NonInvasiveBatchAnalyser.BootstrappedLogisticRegression(ds, maximumNumberOfParameters, numberOfIterations);