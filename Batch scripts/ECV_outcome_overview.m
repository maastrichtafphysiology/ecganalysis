%% load clinical database
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECV database_2013_11_27.mat');

rhythm = dataset2cell(ElectricalCardioversion(:, 154:3:190));
rhythm = cell2mat(rhythm(2:end, :));
days = dataset2cell(ElectricalCardioversion(:, 156:3:192));
days = cell2mat(days(2:end, :));

SR = rhythm == 1 | rhythm == 4 | rhythm == 5 | rhythm == 6;
AF = rhythm == 2 | rhythm ==3;
rhythm(SR) = 0;
rhythm(AF) = 1;

thresholds = (1:300)';
nSR = zeros(size(thresholds));
nAF = zeros(size(thresholds));

for thresholdIndex = 1:numel(thresholds)

recurrenceThreshold = thresholds(thresholdIndex);
minimumFU = recurrenceThreshold - 1;

patientRecurrence = ComputeECVRecurrence(ElectricalCardioversion, recurrenceThreshold, minimumFU);

nSR(thresholdIndex) = numel(find(patientRecurrence == 0));
nAF(thresholdIndex) = numel(find(patientRecurrence == 1));
end