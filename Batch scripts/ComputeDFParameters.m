%% Load file
load('/Users/Zeemering/Desktop/C017 BSPM zonder E-strip met TEECG_QRST.mat');

%% Settings
complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
windowLength = 4;
windowShift = 1;
numberOfPoints = 2^14;
DFRange = [3, 12];
OIThreshold = 0.4;
lagResolution = 0.25;
maximumLag = 2;

complexityCalculator.DFParameters.NumberOfFFTPoints = numberOfPoints;
complexityCalculator.DFParameters.WindowLength = numberOfPoints;
complexityCalculator.DFParameters.DFRange = DFRange;

%% Compute windowed DF
samplingFrequency = cancelledEcgData.SamplingFrequency;
windowSampleLength = round(windowLength * cancelledEcgData.SamplingFrequency);
windowSampleShift = round(windowShift * cancelledEcgData.SamplingFrequency);

windowStart = 1:windowSampleShift:cancelledEcgData.GetNumberOfSamples;
windowEnd = (windowSampleLength):windowSampleShift:cancelledEcgData.GetNumberOfSamples;
numberOfWindows = numel(windowEnd);
windowStart = windowStart(1:numberOfWindows);

bandwidth = 1;

numberOfChannels = cancelledEcgData.GetNumberOfChannels();

dfResult(numberOfChannels) = struct(...
    'windowStart', windowStart,...
    'windowEnd', windowEnd,...
    'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
    'runningWindowMean', NaN(numberOfWindows, 1),...
    'runningWindowStd',NaN(numberOfWindows, 1),...
    'expandingWindowMean', NaN(numberOfWindows, 1),...
    'expandingWindowStd', NaN(numberOfWindows, 1));
dfResult = dfResult(1:numberOfChannels);

oiResult = dfResult;
windowedFrequencyPower = cell(numberOfWindows, 1);

for channelIndex = 1:(numberOfChannels - 1)
    dfResult(channelIndex) = dfResult(numberOfChannels);
    oiResult(channelIndex) = oiResult(numberOfChannels);
end

originalData = cancelledEcgData.Data();

windowDF = NaN(numberOfWindows, numberOfChannels);
expandingWindowDF = NaN(numberOfWindows, numberOfChannels);
windowOI = NaN(numberOfWindows, numberOfChannels);
expandingWindowOI = NaN(numberOfWindows, numberOfChannels);
parfor windowIndex = 1:numberOfWindows
    windowedData = originalData(windowStart(windowIndex):windowEnd(windowIndex), :);
    
    % running window
    [~, frequencies, ~, dominantFrequencyValues, ~, spectra] = ...
        complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
    windowDF(windowIndex, :) = dominantFrequencyValues;
    windowedFrequencyPower{windowIndex} = frequencies;
    
    regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
    windowOI(windowIndex, :) = regularityIndices;
    
    %expanding window
    windowedData = originalData(1:windowEnd(windowIndex), :);
    [~, ~, ~, dominantFrequencyValues, ~, spectra] = ...
        complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
    expandingWindowDF(windowIndex, :) = dominantFrequencyValues;
    
    regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
    expandingWindowOI(windowIndex, :) = regularityIndices;
end

for channelIndex = 1:(numberOfChannels)
    dfResult(channelIndex).runningWindowMean = windowDF(:, channelIndex);
    dfResult(channelIndex).expandingWindowMean = expandingWindowDF(:, channelIndex);
    
    oiResult(channelIndex).runningWindowMean = windowOI(:, channelIndex);
    oiResult(channelIndex).expandingWindowMean = expandingWindowOI(:, channelIndex);
end

%% Compute windowed RR
rrResult = struct(...
    'windowStart', windowStart,...
    'windowEnd', windowEnd,...
    'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
    'runningWindowMean', NaN(numberOfWindows, 1),...
    'runningWindowStd',NaN(numberOfWindows, 1),...
    'expandingWindowMean', NaN(numberOfWindows, 1),...
    'expandingWindowStd', NaN(numberOfWindows, 1));

time = cancelledEcgData.GetTimeRange();
rWaveTimes = time(rWaveIndices);
for windowIndex = 1:numberOfWindows
    % running window
    validRRtimes = rWaveTimes(rWaveIndices >= windowStart(windowIndex) &...
        rWaveIndices < windowEnd(windowIndex));
    if numel(validRRtimes) > 1
        rrResult.runningWindowMean(windowIndex) = mean(diff(validRRtimes));
        rrResult.runningWindowStd(windowIndex) = std(diff(validRRtimes));
    end
    
    % expanding window
    validRRtimes = rWaveTimes(rWaveIndices < windowEnd(windowIndex));
    if numel(validRRtimes) > 1
        rrResult.expandingWindowMean(windowIndex) = mean(diff(validRRtimes));
        rrResult.expandingWindowStd(windowIndex) = std(diff(validRRtimes));
    end
end

%% Compute DF Excel table
time = cancelledEcgData.GetTimeRange();
centerTimes = time(rrResult.windowCenter);
DFExcelTable = table(centerTimes(:), rrResult.runningWindowMean,...
    dfResult.runningWindowMean,...
    'VariableNames', horzcat({'Time', 'RR'}, cancelledEcgData.ElectrodeLabels'));
OIExcelTable = table(centerTimes(:),...
    oiResult.runningWindowMean,...
    'VariableNames', horzcat({'Time'}, cancelledEcgData.ElectrodeLabels'));

%% DF differences
DFDifferences = NaN(numberOfChannels);
DFDifferencesWithHighOI = NaN(numberOfChannels);
OIAboveThreshold = NaN(1, numberOfChannels);
for electrodeIndex1 = 1:numberOfChannels
    electrode1DF = dfResult(electrodeIndex1).runningWindowMean;
    validElectrode1 = oiResult(electrodeIndex1).runningWindowMean >= OIThreshold;
    OIAboveThreshold(electrodeIndex1) = numel(find(validElectrode1)) / numel(validElectrode1);
    for electrodeIndex2 = 1:numberOfChannels
        electrode2DF = dfResult(electrodeIndex2).runningWindowMean;
        DFDifferences(electrodeIndex1, electrodeIndex2) = mean(electrode2DF - electrode1DF);
        
        validElectrode2 = oiResult(electrodeIndex2).runningWindowMean >= OIThreshold;
        
        DFDifferencesWithHighOI(electrodeIndex1, electrodeIndex2) =...
           mean(electrode2DF(validElectrode1 & validElectrode2) - electrode1DF(validElectrode1 & validElectrode2));
    end
end

DFDifferencesExcelTable = array2table(DFDifferences, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);

DFDifferencesWithHighOIExcelTable = array2table(DFDifferencesWithHighOI, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);
OIAboveThresholdTable = array2table(OIAboveThreshold, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  {'OI'});

%% Lagged correlation
interpolatedTime = centerTimes(1):lagResolution:centerTimes(end);
maximumLagSamples = round(maximumLag / lagResolution);
DFLagCorrelation = NaN(numberOfChannels, numberOfChannels, 2 * maximumLagSamples + 1);
DFLagCorrelationWithHighOI = NaN(numberOfChannels, numberOfChannels, 2 * maximumLagSamples + 1);
for electrodeIndex1 = 1:numberOfChannels
    electrode1DF = dfResult(electrodeIndex1).runningWindowMean;
    interpolatedDF1 = interp1(centerTimes, electrode1DF, interpolatedTime, 'linear', 'extrap');
    interpolatedDF1 = interpolatedDF1 - mean(interpolatedDF1);
    
    validElectrode1 = oiResult(electrodeIndex1).runningWindowMean >= OIThreshold;
    interpolatedDF1Valid = interp1(centerTimes(validElectrode1), electrode1DF(validElectrode1), interpolatedTime, 'linear', 'extrap');
    interpolatedDF1Valid = interpolatedDF1Valid - mean(interpolatedDF1Valid);
    
    for electrodeIndex2 = 1:numberOfChannels
        electrode2DF = dfResult(electrodeIndex2).runningWindowMean;
        interpolatedDF2 = interp1(centerTimes, electrode2DF, interpolatedTime, 'linear', 'extrap');
        interpolatedDF2 = interpolatedDF2 - mean(interpolatedDF2);
        [correlation, lags] = xcorr(interpolatedDF1, interpolatedDF2, maximumLagSamples, 'coeff');
        DFLagCorrelation(electrodeIndex1, electrodeIndex2, :) = correlation;
        
        validElectrode2 = oiResult(electrodeIndex2).runningWindowMean >= OIThreshold;
        interpolatedDF2Valid = interp1(centerTimes(validElectrode2), electrode2DF(validElectrode2), interpolatedTime, 'linear', 'extrap');
        interpolatedDF2Valid = interpolatedDF2Valid - mean(interpolatedDF2Valid);
        [correlation, lags] = xcorr(interpolatedDF1Valid, interpolatedDF2Valid, maximumLagSamples, 'coeff');
        DFLagCorrelationWithHighOI(electrodeIndex1, electrodeIndex2, :) = correlation;
    end
end

[maximumLagCorrelation, maxPosition] = max(DFLagCorrelation, [], 3);
optimalLag = (maxPosition - (maximumLagSamples + 1)) * lagResolution;

MaximumLagCorrelationExcelTable = array2table(maximumLagCorrelation, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);
OptimalLagExcelTable = array2table(optimalLag, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);

[maximumLagCorrelationWithHighOI, maxPosition] = max(DFLagCorrelationWithHighOI, [], 3);
optimalLagWithHighOI = (maxPosition - (maximumLagSamples + 1)) * lagResolution;

MaximumLagCorrelationWithHighOIExcelTable = array2table(maximumLagCorrelationWithHighOI, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);
OptimalLagWithHighOIExcelTable = array2table(optimalLagWithHighOI, 'VariableNames', cancelledEcgData.ElectrodeLabels,...
    'RowNames',  cancelledEcgData.ElectrodeLabels);
%%
clearvars -except *Table;
disp('Done');