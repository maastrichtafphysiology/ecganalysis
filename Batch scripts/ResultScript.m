folderName = '/Volumes/StefTheoData/Physionet/ResultFiles (Matlab)';

analyser = NonInvasiveBatchAnalyser();

analyser.LoadFileData(folderName);

analyser.ShowAnalysis();

% type = 'N';
% performance = analyser.ComputeOptimalPerformance(type);
