% folderName = '/Volumes/Macintosh HD 2/Data/Human/Flec-SL/Birmingham/Pseudo';
folderName = '/Volumes/Macintosh HD 2/Data/Human/Flec-SL/Birmingham/ParameterTestSet';

analyser = NonInvasiveBatchAnalyser();

%% Parameter computation
try
    matlabpool close;
end
matlabpool;

[result parameterLabels patientIDs] = analyser.ComputeNonInvasiveParameters(folderName);
save(fullfile(folderName, 'analysisResults.mat'), 'result', 'parameterLabels', 'patientIDs');

matlabpool close;

%% Analysis
load(fullfile(folderName, 'analysisResults.mat'));
load(fullfile(folderName, 'outcomeCleaned.mat'));

channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};

patientOutcomeIndex = NaN(size(patientIDs));
for patientIndex = 1:numel(patientIDs)
    patientFound = strcmpi(patientIDs{patientIndex}, outcome(:, 1));
    if any(patientFound)
        patientOutcomeIndex(patientIndex) = find(patientFound);
    end
end
validPatients = ~isnan(patientOutcomeIndex);

analyser.ShowNonInvasiveParameters(result(validPatients), parameterLabels, channelLabels, outcome(patientOutcomeIndex(validPatients), :));

%% Separate analyses
load(fullfile(folderName, 'analysisResults.mat'));
load(fullfile(folderName, 'outcomeCleaned.mat'));

channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};

patientOutcomeIndex = NaN(size(patientIDs));
for patientIndex = 1:numel(patientIDs)
    patientFound = strcmpi(patientIDs{patientIndex}, outcome(:, 1));
    if any(patientFound)
        patientOutcomeIndex(patientIndex) = find(patientFound);
    end
end
validPatients = ~isnan(patientOutcomeIndex);
result = result(validPatients);
outcome = outcome(patientOutcomeIndex(validPatients), :);

dataMatrix = cellfun(@(x) x(:), result, 'uniformOutput', false);
dataMatrix = horzcat(dataMatrix{:})';

labels = cell(numel(channelLabels), numel(parameterLabels));
for channelLabelIndex = 1:numel(channelLabels)
    for parameterLabelIndex = 1:numel(parameterLabels)
        labels{channelLabelIndex, parameterLabelIndex} = [parameterLabels{parameterLabelIndex}, ' (',...
            channelLabels{channelLabelIndex}, ')'];
    end
end
labels = labels(:);

newOutcome = cell(size(outcome(:, 3)));
newOutCome(strcmpi('y', outcome(:, 3))) = {'SR'};
newOutCome(strcmpi('n', outcome(:, 3))) = {'AF'};
NonInvasiveBatchAnalyser.ComputeSVM(dataMatrix, labels, newOutCome);