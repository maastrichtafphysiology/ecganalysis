classdef NonInvasiveBatchAnalyser < handle
    properties
        FolderName
        
        FileData
        FileTypes
    end
    
    properties (Access = private)
        DFData
        FWaveData
        SEData
        
        ParameterDistributionPanel
        SingleParameterROCPanel
        CombinedParameterROCPanel
        LogisticRegressionPanel
        SVMPanel
        PCAPanel
        
        SignificantParameterIndices
    end
    
    methods
        function self = NonInvasiveBatchAnalyser()
            
        end
        
        function LoadFileData(self, folderName)
            self.FolderName = folderName;
            listing = dir(fullfile(folderName, '*_Result.mat'));
            
            outcomeData = load(fullfile(folderName, 'outcomes.mat'));
            outcomes = outcomeData.outcomes;
            
            self.FileData = cell(size(listing));
            self.FileTypes = cell(size(listing));
            for fileIndex = 1:numel(listing)
                filename = fullfile(folderName, listing(fileIndex).name);
                self.FileData{fileIndex} = load(filename);
                
                outcomeFileMatch = strcmp(listing(fileIndex).name(1:3), outcomes(:, 1));
                if any(outcomeFileMatch)
                    self.FileTypes(fileIndex) = outcomes(outcomeFileMatch, 2);
                end
            end
            
%             validTypes = strcmp('N', self.FileTypes) | strcmp('T', self.FileTypes);
%             self.FileTypes = self.FileTypes(validTypes);
%             self.FileData = self.FileData(validTypes);
            sOrT = strcmp('S', self.FileTypes) | strcmp('T', self.FileTypes);
            self.FileTypes(sOrT) = {'S or T'};
        end
        
        function ShowAnalysis(self)
            screenSize = get(0,'Screensize');
            figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];

            figureHandle = figure('Name', 'Noninvasive Analysis Results',...
                'NumberTitle', 'off', 'position', figurePosition);
            tabGroup = uitabgroup(...
                'parent', figureHandle,...
                'position', [0 .025 1 .975]);
            
            parameterDistributionTab = uitab(tabGroup, 'title','Parameter distribution');
            self.ParameterDistributionPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', parameterDistributionTab);
            
            singleParameterROCTab = uitab(tabGroup, 'title','Single parameter ROC');
            self.SingleParameterROCPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', singleParameterROCTab);
            
            combinedParameterROCTab = uitab(tabGroup, 'title','Combined parameter ROC');
            self.CombinedParameterROCPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', combinedParameterROCTab);
            
            self.ComputeParameterDistributions();
            
            self.ShowParameterDistributions();
            
            self.ShowROCCurves();
            
            self.ShowCombinedROCCurves();
        end
        
        function performance = ComputeOptimalPerformance(self, type)
            types = false(size(self.FileTypes));
            
            types(strcmp(type, self.FileTypes)) = true;
            
            performance = struct(...
                'DF', [],...
                'FWave', [],...
                'SE', []);
            
            % Sample entropy
            windowValues = [];
            for fileIndex = 1:numel(self.FileData)
                windowValues = [windowValues, self.FileData{fileIndex}.SE.result.runningWindowMean]; %#ok<AGROW>
            end
            
            numberOfWindows = size(windowValues, 1);
            performance.SE = NaN(numberOfWindows, 1);
            for windowIndex = 1:numberOfWindows
                means = windowValues(windowIndex, :);
%                 b = glmfit(means, types, 'binomial');
%                 p = glmval(b, means, 'logit');
                
                discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
                [label, score] = predict(discriminantClassifier, means(:));
            
                [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(self.FileTypes, score(:, 2) - score(:, 1), 'N');
                performance.SE(windowIndex) = AUC;
            end
            
            % Dominant frequency
            windowValues = [];
            for fileIndex = 1:numel(self.FileData)
                windowValues = [windowValues, self.FileData{fileIndex}.DF.windowedDF.runningWindowMean]; %#ok<AGROW>
            end
            
            numberOfWindows = size(windowValues, 1);
            performance.DF = NaN(numberOfWindows, 1);
            for windowIndex = 1:numberOfWindows
                means = windowValues(windowIndex, :);
                discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
                [label, score] = predict(discriminantClassifier, means(:));
            
                [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(self.FileTypes, score(:, 2) - score(:, 1), 'N');
                performance.DF(windowIndex) = AUC;
            end
            
            % F-Wave amplitude
            windowValues = [];
            windowLength = 10;
            windowShift = 10;
            for fileIndex = 1:numel(self.FileData)
                v1Index = self.FileData{fileIndex}.FWave.settings.v1Index;
%                 result = FWavePanel.ComputeStaticWindowedAmplitudes(self.FileData{fileIndex}.ecgData,...
%                     self.FileData{fileIndex}.FWave.fWaveAmplitudes{v1Index},...
%                     self.FileData{fileIndex}.FWave.fWaveIndices{v1Index},...
%                     self.FileData{fileIndex}.FWave.settings.windowLength,...
%                     self.FileData{fileIndex}.FWave.settings.windowShift);
                
                result = FWavePanel.ComputeStaticWindowedAmplitudes(self.FileData{fileIndex}.ecgData,...
                    self.FileData{fileIndex}.FWave.fWaveAmplitudes{v1Index},...
                    self.FileData{fileIndex}.FWave.fWaveIndices{v1Index},...
                    windowLength, windowShift);
                
                windowValues = [windowValues, result.runningWindowMean()]; %#ok<AGROW>
            end
            
            numberOfWindows = size(windowValues, 1);
            performance.FWave = NaN(numberOfWindows, 1);
            for windowIndex = 1:numberOfWindows
                means = windowValues(windowIndex, :);
                discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
                [label, score] = predict(discriminantClassifier, means(:));
            
                [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(self.FileTypes, score(:, 2) - score(:, 1), 'N');
                performance.FWave(windowIndex) = AUC;
            end
            
        end
        
        function [result, parameterLabels, patientIDs] = ComputeNonInvasiveParameters(self, folderName, filenameList)
            if nargin < 3
                filenameList = {};
            end
            
            if isempty(filenameList)
                self.FolderName = folderName;
                listing = dir(fullfile(folderName, '*_QRST.mat'));
                filenameList = fullfile(foldername, {listing.name}');
            end
            numberOfFiles = numel(filenameList);
            
            channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            
            DFLabels = strcat('Dominant_Frequency', '_', channelLabels);
            OILabels = strcat('Organization_Index', '_', channelLabels);
            SELabels = strcat('Spectral_Entropy', '_', channelLabels);
            SampEnLabels = strcat('Sample_Entropy', '_', channelLabels);
            FWALabels = strcat('F_wave_Amplitude', '_', channelLabels);
            parameterLabels = horzcat(DFLabels, OILabels, SELabels, SampEnLabels, FWALabels,...
                'RR_Mean', 'RR_SD');
            
            result = NaN(numberOfFiles, numel(parameterLabels));
            patientIDs = cell(numberOfFiles, 1);
            
            parfor fileIndex = 1:numberOfFiles,
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                % Highpass filter of 3Hz
                nyquistFrequency = ecgData.SamplingFrequency / 2;
                [b, a] = cheby2(3, 20,...
                    3 / nyquistFrequency, 'high');
%                 [b, a] = butter(3,...
%                     3 / nyquistFrequency, 'high');
                ecgData.Data = filtfilt(b, a, ecgData.Data);
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time <= (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                               
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                complexityCalculator.DFParameters = struct(...
                    'NumberOfFFTPoints', 2^10,...
                    'WindowLength', ecgData.SamplingFrequency * 4,...
                    'FFTOverlap', 0.5,...
                    'DFRange', [3, 12],...
                    'NumberOfPeaks', 2,...
                    'PeakRange', 1);
                
                complexityCalculator.SampleEntropyParameters = struct(...
                    'samples', 3,...
                    'tolerance', 0.35);
                
                complexityCalculator.FWaveDistanceThreshold = 100;
                
                complexityCalculator.SegmentLength = 10;
                complexityCalculator.CNVThreshold = 0.95;
                
                % Spectral parameters
                spectralParameters = complexityCalculator.ComputeSpectralParameters(ecgData);
                
                % Sample entropy
                [~, dfSampleEntropy] = complexityCalculator.ComputeSampleEntropy(ecgData,...
                    1:ecgData.GetNumberOfChannels, spectralParameters.DominantFrequency, 250);
                               
                % F-wave amplitude
                fWaveAmplitudes = complexityCalculator.ComputeFWaveAmplitudes(ecgData);
                fWaveMedian = cellfun(@median, fWaveAmplitudes);
                
                % R-R intervals
                rrIntervals = diff(sort(fileData.rWaveIndices));
                rrIntervalMean = mean(rrIntervals ./ ecgData.SamplingFrequency);
                rrIntervalSD = std(rrIntervals ./ ecgData.SamplingFrequency);
                
                % Concatenate parameters
                result(fileIndex, :) = [...
                    spectralParameters.DominantFrequency;...
                    spectralParameters.OrganizationIndex;...
                    spectralParameters.SpectralEntropy;...
                    dfSampleEntropy;...
                    fWaveMedian;...
                    rrIntervalMean;...
                    rrIntervalSD]';
            end
        end
        
        function result = ComputeBSPMParameters(self, folderName, filenameList)
            if nargin < 3
                filenameList = {};
            end
            
            if isempty(filenameList)
                self.FolderName = folderName;
                listing = dir(fullfile(folderName, '*_QRST.mat'));
                filenameList = fullfile(foldername, {listing.name}');
            end
            numberOfFiles = numel(filenameList);
            
            result(numberOfFiles) = struct(...
                'Name', [],...
                'ElectrodeLabels', [],...
                'DF', [], 'OI', [], 'SE', [],...
                'SampEn', [], 'FWA', []);
            
            parfor fileIndex = 1:numberOfFiles,
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                result(fileIndex).Name = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                result(fileIndex).ElectrodeLabels = ecgData.ElectrodeLabels;
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time <= (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                               
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                complexityCalculator.DFParameters = struct(...
                    'NumberOfFFTPoints', 2^12,...
                    'WindowLength', 2^12,...
                    'FFTOverlap', 0.5,...
                    'DFRange', [3, 12],...
                    'NumberOfPeaks', 2,...
                    'PeakRange', 1);
                
                complexityCalculator.SampleEntropyParameters = struct(...
                    'samples', 3,...
                    'tolerance', 0.35);
                
                complexityCalculator.FWaveDistanceThreshold = 100;
                
                complexityCalculator.SegmentLength = 10;
                complexityCalculator.CNVThreshold = 0.95;
                
                % Spectral parameters
                spectralParameters = complexityCalculator.ComputeSpectralParameters(ecgData);
                
                % Sample entropy
                [~, dfSampleEntropy] = complexityCalculator.ComputeSampleEntropy(ecgData,...
                    1:ecgData.GetNumberOfChannels, spectralParameters.DominantFrequency, 250);
                               
                % F-wave amplitude
                fWaveAmplitudes = complexityCalculator.ComputeFWaveAmplitudes(ecgData);
                fWaveMedian = cellfun(@median, fWaveAmplitudes);
                
                % store parameters
                result(fileIndex).DF = spectralParameters.DominantFrequency';
                result(fileIndex).OI = spectralParameters.OrganizationIndex';
                result(fileIndex).SE = spectralParameters.SpectralEntropy';
                result(fileIndex).SampEn = dfSampleEntropy';
                result(fileIndex).FWA = fWaveMedian';
            end
        end
        
        function [result, parameterLabels, patientIDs] = ComputePairedSpectralParameters(self, folderName, filenameList)
            if nargin < 3
                filenameList = {};
            end

            if isempty(filenameList)
                self.FolderName = folderName;
                listing = dir(fullfile(folderName, '*_QRST.mat'));
                filenameList = fullfile(foldername, {listing.name}');
            end
            numberOfFiles = numel(filenameList);
            
            channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            channelCombinationLabels = cell(numel(channelLabels) - 1, 1);
            channelCombinations = cell(numel(channelLabels) - 1, 1);
            for channelIndex = 1:(numel(channelLabels) - 1)
                combinationRange = (channelIndex + 1):numel(channelLabels);
                channelCombinations{channelIndex} =...
                    [ones(numel(combinationRange), 1) * channelIndex, combinationRange'];
                channelCombinationLabels{channelIndex} = strcat(channelLabels{channelIndex},...
                    '_', channelLabels(combinationRange));
            end
            channelCombinationLabels = horzcat(channelCombinationLabels{:});
            channelCombinations = vertcat(channelCombinations{:});
            
            DFLabels = strcat('Dominant_Frequency', '_(', channelCombinationLabels, ')');
            OILabels = strcat('Organization_Index', '_(', channelCombinationLabels, ')');
            SELabels = strcat('Spectral_Entropy', '_(', channelCombinationLabels, ')');
            parameterLabels = horzcat(DFLabels, OILabels, SELabels);
            
            result = NaN(numberOfFiles, numel(parameterLabels));
            patientIDs = cell(numberOfFiles, 1);
            
           parfor fileIndex = 1:numberOfFiles
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                complexityCalculator.DFParameters = struct(...
                    'NumberOfFFTPoints', 2^10,...
                    'FFTOverlap', 0.5,...
                    'DFRange', [3, 12],...
                    'NumberOfPeaks', 2,...
                    'PeakRange', 1);
                
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time <= (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                
                % Spectral parameters
                spectralParameters = complexityCalculator.ComputeMultivariateSpectralParameters(ecgData, channelCombinations);
                
                % Concatenate parameters
                result(fileIndex, :) = [...
                    spectralParameters.DominantFrequency;...
                    spectralParameters.OrganizationIndex;...
                    spectralParameters.SpectralEntropy]';
            end
        end
        
        function [result, parameterLabels, patientIDs] = ComputeCombinedSpectralParameters(self, folderName, filenameList)
            if nargin < 3
                filenameList = {};
            end

            if isempty(filenameList)
                self.FolderName = folderName;
                listing = dir(fullfile(folderName, '*_QRST.mat'));
                filenameList = fullfile(foldername, {listing.name}');
            end
            numberOfFiles = numel(filenameList);
            
            channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
            numberOfLeads = numel(channelLabels);
            leadCombinations = cell(numberOfLeads, 1);
            leadCombinationsLabels = cell(numberOfLeads, 1);
            for numberOfCombinedLeads = 1:numberOfLeads
                currentLeadCombinations = combnk(1:numberOfLeads, numberOfCombinedLeads);
                numberOfCurrentCombinations = size(currentLeadCombinations, 1);
                leadCombinations{numberOfCombinedLeads} = mat2cell(currentLeadCombinations,...
                    ones(numberOfCurrentCombinations, 1), size(currentLeadCombinations, 2));
                currentLeadCombinationsLabels = cell(1, numberOfCurrentCombinations);
                for combinationIndex = 1:numberOfCurrentCombinations
                    currentLeadCombinationsLabels{combinationIndex} =...
                        strjoin(channelLabels(currentLeadCombinations(combinationIndex, :)), '_');
                end
                leadCombinationsLabels{numberOfCombinedLeads} = currentLeadCombinationsLabels;
            end
            leadCombinationsLabels = horzcat(leadCombinationsLabels{:});
            leadCombinations = vertcat(leadCombinations{:});
            
            DFLabels = strcat('Dominant_Frequency', '_(', leadCombinationsLabels, ')');
            OILabels = strcat('Organization_Index', '_(', leadCombinationsLabels, ')');
            SELabels = strcat('Spectral_Entropy', '_(', leadCombinationsLabels, ')');
            parameterLabels = horzcat(DFLabels, OILabels, SELabels);
            
            result = NaN(numberOfFiles, numel(parameterLabels));
            patientIDs = cell(numberOfFiles, 1);
            
           parfor fileIndex = 1:numberOfFiles
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                complexityCalculator.DFParameters = struct(...
                    'NumberOfFFTPoints', 2^10,...
                    'FFTOverlap', 0.5,...
                    'DFRange', [3, 12],...
                    'NumberOfPeaks', 2,...
                    'PeakRange', 1);
                
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                % Highpass filter of 3Hz
                nyquistFrequency = ecgData.SamplingFrequency / 2;
                [b, a] = cheby2(3, 20,...
                    3 / nyquistFrequency, 'high');
                ecgData.Data = filtfilt(b, a, ecgData.Data);
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time <= (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                
                % Spectral parameters
                spectralParameters = complexityCalculator.ComputeMultivariateSpectralParameters(ecgData, leadCombinations);
                
                % Concatenate parameters
                result(fileIndex, :) = [...
                    spectralParameters.DominantFrequency;...
                    spectralParameters.OrganizationIndex;...
                    spectralParameters.SpectralEntropy]';
            end
        end
        
        function ShowNonInvasiveParameters(self, dataMatrix, parameterLabels, outcome)
            labels = parameterLabels(:);
            
            screenSize = get(0,'Screensize');
            figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 3 * screenSize(3) / 5, 3 * screenSize(4) / 5];

            figureHandle = figure('Name', 'Noninvasive Analysis Results',...
                'NumberTitle', 'off', 'position', figurePosition);
            tabGroup = uitabgroup(...
                'parent', figureHandle,...
                'position', [0 .025 1 .975]);
            
            % Descriptive statistics
            parameterDistributionTab = uitab(tabGroup, 'title','Parameter distribution');
            self.ParameterDistributionPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', parameterDistributionTab);
            self.ShowDescriptiveStatistics(dataMatrix, labels, outcome, self.ParameterDistributionPanel);
            
            % Single significant parameter ROC
            if ~isempty(self.SignificantParameterIndices)
                singleParameterROCTab = uitab(tabGroup, 'title','Single parameter ROC');
                self.SingleParameterROCPanel = uipanel(...
                    'units', 'normalized',...
                    'position', [0 0 1 1],...
                    'parent', singleParameterROCTab);
                self.ShowSingleParameterROC(dataMatrix(:, self.SignificantParameterIndices), labels(self.SignificantParameterIndices), outcome, self.SingleParameterROCPanel);
            end
            
            % PCA
            pcaTab = uitab(tabGroup, 'title','PCA');
            self.PCAPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', pcaTab);
            try
                self.ShowPCA(dataMatrix, labels, outcome);
            end
            
            % Logistic regression
            logisticRegressionTab = uitab(tabGroup, 'title','Logistic regression');
            self.LogisticRegressionPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', logisticRegressionTab);
            self.ShowLogisticRegression(dataMatrix, labels, outcome);
            
            % support vectors
            supportVectorTab = uitab(tabGroup, 'title','Support vector machine');
            self.SVMPanel = uipanel(...
                'units', 'normalized',...
                'position', [0 0 1 1],...
                'parent', supportVectorTab);
%             self.ShowSVM(dataMatrix, labels, outcome);
%             
%             combinedParameterROCTab = uitab(tabGroup, 'title','Combined parameter ROC');
%             self.CombinedParameterROCPanel = uipanel(...
%                 'units', 'normalized',...
%                 'position', [0 0 1 1],...
%                 'parent', combinedParameterROCTab);
        end
        
        function ComputeFWaveAmplitudeMaximumAUC(self, filenameList, outcome, distanceThreshold)
            
            numberOfFiles = numel(filenameList);
            results(numberOfFiles) = struct(...
                'median', [],...
                'mean', [],...
                'SD', [],...
                'max', []);
            
            parfor fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time < (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                
                results(fileIndex).median = NaN(numel(distanceThreshold), ecgData.GetNumberOfChannels);
                results(fileIndex).mean = NaN(numel(distanceThreshold), ecgData.GetNumberOfChannels);
                results(fileIndex).SD = NaN(numel(distanceThreshold), ecgData.GetNumberOfChannels);
                results(fileIndex).max = NaN(numel(distanceThreshold), ecgData.GetNumberOfChannels);
                
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                for thresholdIndex = 1:numel(distanceThreshold)
                    complexityCalculator.FWaveDistanceThreshold = distanceThreshold(thresholdIndex);
                    fWaveAmplitudes = complexityCalculator.ComputeFWaveAmplitudes(ecgData);
                    results(fileIndex).median(thresholdIndex, :) = cellfun(@median, fWaveAmplitudes);
                    results(fileIndex).mean(thresholdIndex, :) = cellfun(@mean, fWaveAmplitudes);
                    results(fileIndex).SD(thresholdIndex, :) = cellfun(@std, fWaveAmplitudes);
                    results(fileIndex).max(thresholdIndex, :) = cellfun(@max, fWaveAmplitudes); 
                end 
            end
            
            medianData = vertcat(results.median);
            meanData = vertcat(results.mean);
            SDData = vertcat(results.SD);
            maxData = vertcat(results.max);
            
            numberOfChannels = size(medianData, 2);
            
            medianAUC = NaN(numel(distanceThreshold), numberOfChannels);
            meanAUC = NaN(numel(distanceThreshold), numberOfChannels);
            SDAUC = NaN(numel(distanceThreshold), numberOfChannels);
            maxAUC = NaN(numel(distanceThreshold), numberOfChannels);
            
            meanMedianAUC = NaN(numel(distanceThreshold), 1);
            maxMedianAUC = NaN(numel(distanceThreshold), 1);
            meanMeanAUC = NaN(numel(distanceThreshold), 1);
            maxMeanAUC = NaN(numel(distanceThreshold), 1);
            meanMaxAUC = NaN(numel(distanceThreshold), 1);
            maxMaxAUC = NaN(numel(distanceThreshold), 1);
            
            for thresholdIndex = 1:numel(distanceThreshold)
                for channelIndex = 1:numberOfChannels
                    data = medianData(thresholdIndex:numel(distanceThreshold):end, channelIndex);
                    [FPR, TPR, thresholds, medianAUC(thresholdIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, true);
                    
                    data = meanData(thresholdIndex:numel(distanceThreshold):end, channelIndex);
                    [FPR, TPR, thresholds, meanAUC(thresholdIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, true);
                    
                    data = SDData(thresholdIndex:numel(distanceThreshold):end, channelIndex);
                    [FPR, TPR, thresholds, SDAUC(thresholdIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, true);
                    
                    data = maxData(thresholdIndex:numel(distanceThreshold):end, channelIndex);
                    [FPR, TPR, thresholds, maxAUC(thresholdIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, true);
                end
                
                [FPR, TPR, thresholds, meanMedianAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    mean(medianData(thresholdIndex:numel(distanceThreshold):end, :), 2), true);
                
                [FPR, TPR, thresholds, maxMedianAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    max(medianData(thresholdIndex:numel(distanceThreshold):end, :), [], 2), true);
                
                [FPR, TPR, thresholds, meanMeanAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    mean(meanData(thresholdIndex:numel(distanceThreshold):end, :), 2), true);
                
                [FPR, TPR, thresholds, maxMeanAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    max(meanData(thresholdIndex:numel(distanceThreshold):end, :), [], 2), true);
                
                [FPR, TPR, thresholds, meanMaxAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    mean(maxData(thresholdIndex:numel(distanceThreshold):end, :), 2), true);
                
                [FPR, TPR, thresholds, maxMaxAUC(thresholdIndex) optimalThreshold] = perfcurve(outcome,...
                    max(maxData(thresholdIndex:numel(distanceThreshold):end, :), [], 2), true);
            end
        end
        
        function ComputeDFMaxAUC(self, filenameList, outcome, nFFT)
            disp('Dominant Frequency AUC optimization');
            numberOfFiles = numel(filenameList);
            numberOfChannels = 12;
            maxChannelAUC = NaN(numel(nFFT), 2);
            minChannelAUC = NaN(numel(nFFT), 2);
            maxCombinationAUC = NaN(numel(nFFT), 1);
            maxCombinationMembers = false(numel(nFFT), numberOfChannels);
            
            for fftIndex = 1:numel(nFFT)
                disp(['Number of FFT points: ', num2str(nFFT(fftIndex))]);
                DF = NaN(numberOfFiles, numberOfChannels);
                parfor fileIndex = 1:numberOfFiles
                    fileData = load(filenameList{fileIndex});
                    ecgData = fileData.cancelledEcgData;
                    % remove first and last second
                    time = ecgData.GetTimeRange();
                    validTime = time >= 1 & time < (time(end) - 1);
                    ecgData.Data = ecgData.Data(validTime, :);
                    
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    complexityCalculator.DFParameters.NumberOfFFTPoints = nFFT(fftIndex);
                    spectra = complexityCalculator.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                    DF(fileIndex, :) = complexityCalculator.ComputeOrganizationIndex(spectra)';
                end
                
                channelAUC = NaN(1, numberOfChannels);
                for channelIndex = 1:numberOfChannels
                    data = DF(:, channelIndex);
                    [~, ~, ~, channelAUC(channelIndex)] = perfcurve(outcome, data, true);
                end
                [maxChannelAUC(fftIndex, 1), maxChannelAUC(fftIndex, 2)] = max(channelAUC);
                [minChannelAUC(fftIndex, 1), minChannelAUC(fftIndex, 2)] = min(channelAUC);
                
                glModel = GeneralizedLinearModel.stepwise(DF, outcome,...
                    'linear', 'lower', 'constant', 'upper', 'linear', 'distr', 'binomial');
                predictions = predict(glModel, DF);
                misClassificationCost = [0 1; 2 0];
                [~, ~, ~, maxCombinationAUC(fftIndex)] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
                maxCombinationMembers(fftIndex, :) = glModel.VariableInfo.InModel(2:end)';
            end
        end
        
        function ComputeOIMaxAUC(self, filenameList, outcome, nPeaks)
            disp('Organization Index AUC optimization');
            numberOfFiles = numel(filenameList);
            numberOfChannels = 12;
            maxChannelAUC = NaN(numel(nPeaks), 2);
            minChannelAUC = NaN(numel(nPeaks), 2);
            maxCombinationAUC = NaN(numel(nPeaks), 1);
            maxCombinationMembers = false(numel(nPeaks), numberOfChannels);
            
            for peakIndex = 1:numel(nPeaks)
                disp(['Number of OI peaks: ', num2str(nPeaks(peakIndex))]);
                OI = NaN(numberOfFiles, numberOfChannels);
                parfor fileIndex = 1:numberOfFiles
                    fileData = load(filenameList{fileIndex});
                    ecgData = fileData.cancelledEcgData;
                    % remove first and last second
                    time = ecgData.GetTimeRange();
                    validTime = time >= 1 & time < (time(end) - 1);
                    ecgData.Data = ecgData.Data(validTime, :);
                    
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    complexityCalculator.DFParameters.NumberOfFFTPoints = 2^10;
                    complexityCalculator.DFParameters.NumberOfPeaks = nPeaks(peakIndex);
                    spectra = complexityCalculator.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                    [~, fileOI] = complexityCalculator.ComputeOrganizationIndex(spectra);
                    OI(fileIndex, :) = fileOI';
                end
                
                channelAUC = NaN(1, numberOfChannels);
                for channelIndex = 1:numberOfChannels
                    data = OI(:, channelIndex);
                    [~, ~, ~, channelAUC(channelIndex)] = perfcurve(outcome, data, true);
                end
                [maxChannelAUC(peakIndex, 1), maxChannelAUC(peakIndex, 2)] = max(channelAUC);
                [minChannelAUC(peakIndex, 1), minChannelAUC(peakIndex, 2)] = min(channelAUC);
                
                glModel = GeneralizedLinearModel.stepwise(OI, outcome,...
                    'linear', 'lower', 'constant', 'upper', 'linear', 'distr', 'binomial');
                predictions = predict(glModel, OI);
                misClassificationCost = [0 1; 2 0];
                [~, ~, ~, maxCombinationAUC(peakIndex)] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
                maxCombinationMembers(peakIndex, :) = glModel.VariableInfo.InModel(2:end)';
            end
        end
        
        function ComputeSEMaxAUC(self, filenameList, outcome, nFFT)
            disp('Spectral Entropy AUC optimization');
            numberOfFiles = numel(filenameList);
            numberOfChannels = 12;
            maxChannelAUC = NaN(numel(nFFT), 2);
            minChannelAUC = NaN(numel(nFFT), 2);
            maxCombinationAUC = NaN(numel(nFFT), 1);
            maxCombinationMembers = false(numel(nFFT), numberOfChannels);
            
            for fftIndex = 1:numel(nFFT)
                disp(['Number of FFT points: ', num2str(nFFT(fftIndex))]);
                SE = NaN(numberOfFiles, numberOfChannels);
                parfor fileIndex = 1:numberOfFiles
                    fileData = load(filenameList{fileIndex});
                    ecgData = fileData.cancelledEcgData;
                    % remove first and last second
                    time = ecgData.GetTimeRange();
                    validTime = time >= 1 & time < (time(end) - 1);
                    ecgData.Data = ecgData.Data(validTime, :);
                    
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    complexityCalculator.DFParameters.NumberOfFFTPoints = nFFT(fftIndex);
                    spectra = complexityCalculator.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                    SE(fileIndex, :) = complexityCalculator.ComputeSpectralEntropy(spectra)';
                end
                
                channelAUC = NaN(1, numberOfChannels);
                for channelIndex = 1:numberOfChannels
                    data = SE(:, channelIndex);
                    [~, ~, ~, channelAUC(channelIndex)] = perfcurve(outcome, data, true);
                end
                [maxChannelAUC(fftIndex, 1), maxChannelAUC(fftIndex, 2)] = max(channelAUC);
                [minChannelAUC(fftIndex, 1), minChannelAUC(fftIndex, 2)] = min(channelAUC);
                
                glModel = GeneralizedLinearModel.stepwise(SE, outcome,...
                    'linear', 'lower', 'constant', 'upper', 'linear', 'distr', 'binomial');
                predictions = predict(glModel, SE);
                misClassificationCost = [0 1; 2 0];
                [~, ~, ~, maxCombinationAUC(fftIndex)] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
                maxCombinationMembers(fftIndex, :) = glModel.VariableInfo.InModel(2:end)';
            end
        end
        
        function ComputePCAMaxAUC(self, filenameList, outcome, CNVThresholds)
            disp('PCA AUC optimization');
            numberOfFiles = numel(filenameList);
            numberOfChannels = 12;
            maxChannelAUC = NaN(numel(CNVThresholds), 2);
            minChannelAUC = NaN(numel(CNVThresholds), 2);
            maxCombinationAUC = NaN(numel(CNVThresholds), 1);
            maxCombinationMembers = false(numel(CNVThresholds), numberOfChannels);
            v1Index = 7;
            
            for cnvIndex = 1:numel(CNVThresholds)
                disp(['CNV threshold: ', num2str(CNVThresholds(cnvIndex))]);
                PCA = NaN(numberOfFiles, 1);
                for fileIndex = 1:numberOfFiles
                    fileData = load(filenameList{fileIndex});
                    ecgData = fileData.cancelledEcgData;
                    % remove first and last second
                    time = ecgData.GetTimeRange();
                    validTime = time >= 1 & time < (time(end) - 1);
                    ecgData.Data = ecgData.Data(validTime, :);
                    
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    complexityCalculator.CNVThreshold = CNVThresholds(cnvIndex);
                    rIndices = fileData.rWaveIndices;
                    rIndices = rIndices - find(validTime, 1, 'first') + 1;
                    rIndices = rIndices(rIndices > 0);
                    complexityCalculator.QRInterval = [40 80];
                    [segmentSpatialComplexities, segmentTemporalComplexity] =...
                        complexityCalculator.ComputePCAComplexity(ecgData, rIndices, v1Index);
                    PCA(fileIndex) = mean(segmentSpatialComplexities);
                end
                
                [~, ~, ~, maxChannelAUC(cnvIndex, 1)] = perfcurve(outcome, PCA, true);
                
                glModel = GeneralizedLinearModel.stepwise(PCA, outcome,...
                    'linear', 'lower', 'constant', 'upper', 'linear', 'distr', 'binomial');
                predictions = predict(glModel, PCA);
                misClassificationCost = [0 1; 2 0];
                [~, ~, ~, maxCombinationAUC(cnvIndex)] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
                maxCombinationMembers(cnvIndex, :) = glModel.VariableInfo.InModel(2:end)';
            end
            
        end
        
        function ComputeWEMaxAUC(self, filenameList, outcome, nLevels)
            disp('Wavelet Entropy AUC optimization');
            numberOfFiles = numel(filenameList);
            numberOfChannels = 12;
            maxChannelAUC = NaN(numel(nLevels), 2);
            minChannelAUC = NaN(numel(nLevels), 2);
            maxCombinationAUC = NaN(numel(nLevels), 1);
            maxCombinationMembers = false(numel(nLevels), numberOfChannels);
            waveletName = 'db5';
            
            for levelIndex = 1:numel(nLevels)
                maxLevel = nLevels(levelIndex);
                disp(['Maximum wavelet scale: ', num2str(maxLevel)]);
                WE = NaN(numberOfFiles, numberOfChannels);
                parfor fileIndex = 1:numberOfFiles
                    fileData = load(filenameList{fileIndex});
                    ecgData = fileData.cancelledEcgData;
                    % remove first and last second
                    time = ecgData.GetTimeRange();
                    validTime = time >= 1 & time < (time(end) - 1);
                    ecgData.Data = ecgData.Data(validTime, :);
                    
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    WE(fileIndex, :) = complexityCalculator.ComputeWaveletEntropy(ecgData, maxLevel, waveletName);
                end
                 
                channelAUC = NaN(1, numberOfChannels);
                for channelIndex = 1:numberOfChannels
                    data = WE(:, channelIndex);
                    [~, ~, ~, channelAUC(channelIndex)] = perfcurve(outcome, data, true);
                end
                [maxChannelAUC(levelIndex, 1), maxChannelAUC(levelIndex, 2)] = max(channelAUC);
                [minChannelAUC(levelIndex, 1), minChannelAUC(levelIndex, 2)] = min(channelAUC);
                
                glModel = GeneralizedLinearModel.stepwise(WE, outcome,...
                    'linear', 'lower', 'constant', 'upper', 'linear', 'distr', 'binomial');
                predictions = predict(glModel, WE);
                misClassificationCost = [0 1; 2 0];
                [~, ~, ~, maxCombinationAUC(levelIndex)] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
                maxCombinationMembers(levelIndex, :) = glModel.VariableInfo.InModel(2:end)';
            end
        end
        
        function ComputeSpectralParametersMaximumAUC(self, filenameList, outcome, nFFT, nPeaks, peakRange)
                        
            numberOfFiles = numel(filenameList);
            results(numberOfFiles) = struct(...
                'DF', [],...
                'OI', [],...
                'SE', []);
            
            parfor fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                patientIDs{fileIndex} = shortFilename;
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time < (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                
                results(fileIndex).DF = NaN(numel(nFFT), ecgData.GetNumberOfChannels);
                results(fileIndex).SE = NaN(numel(nFFT), ecgData.GetNumberOfChannels);
                results(fileIndex).OI = NaN(numel(nFFT), numel(nPeaks), numel(peakRange), ecgData.GetNumberOfChannels);
                
                complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                for fftIndex = 1:numel(nFFT)
                    complexityCalculator.DFParameters.NumberOfFFTPoints = nFFT(fftIndex);
                    spectra = complexityCalculator.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                    
                    [DF, ~] = complexityCalculator.ComputeOrganizationIndex(spectra);
                    results(fileIndex).DF(fftIndex, :) = DF;
                    
                    SE = complexityCalculator.ComputeSpectralEntropy(spectra);
                    results(fileIndex).SE(fftIndex, :) = SE;
                    
                    for peakIndex = 1:numel(nPeaks)
                        complexityCalculator.DFParameters.NumberOfPeaks = nPeaks(peakIndex);
                        for rangeIndex = 1:numel(peakRange)
                            complexityCalculator.DFParameters.PeakRange = peakRange(rangeIndex);
                            
                            [~, OI] = complexityCalculator.ComputeOrganizationIndex(spectra);
                            results(fileIndex).OI(fftIndex, peakIndex, rangeIndex, :) = OI;
                        end
                    end
                end 
            end
            
            DFData = vertcat(results.DF);
            SEData = vertcat(results.SE);
            
            numberOfChannels = size(DFData, 2);
            
            DFAUC = NaN(numel(nFFT), numberOfChannels);
            SEAUC = NaN(numel(nFFT), numberOfChannels);
            OIAUC = NaN(numel(nFFT), numel(nPeaks), numel(peakRange), numberOfChannels);
            
            meanDFAUC = NaN(numel(nFFT), 1);
            medianDFAUC = NaN(numel(nFFT), 1);
            maxDFAUC = NaN(numel(nFFT), 1);
            minDFAUC = NaN(numel(nFFT), 1);
            
            meanSEAUC = NaN(numel(nFFT), 1);
            medianSEAUC = NaN(numel(nFFT), 1);
            maxSEAUC = NaN(numel(nFFT), 1);
            minSEAUC = NaN(numel(nFFT), 1);
            
            meanOIAUC = NaN(numel(nFFT), numel(nPeaks), numel(peakRange));
            medianOIAUC = NaN(numel(nFFT), numel(nPeaks), numel(peakRange));
            maxOIAUC = NaN(numel(nFFT), numel(nPeaks), numel(peakRange));
            minOIAUC = NaN(numel(nFFT), numel(nPeaks), numel(peakRange));
            
            for fftIndex = 1:numel(nFFT)
                for channelIndex = 1:numberOfChannels
                    data = DFData(fftIndex:numel(nFFT):end, channelIndex);
                    [FPR, TPR, thresholds, DFAUC(fftIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, false);
                    
                    data = SEData(fftIndex:numel(nFFT):end, channelIndex);
                    [FPR, TPR, thresholds, SEAUC(fftIndex, channelIndex) optimalThreshold] = perfcurve(outcome, data, false);
                end
                
                [FPR, TPR, thresholds, meanDFAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    mean(DFData(fftIndex:numel(nFFT):end, :), 2), true);
                [FPR, TPR, thresholds, medianDFAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    median(DFData(fftIndex:numel(nFFT):end, :), 2), true);
                [FPR, TPR, thresholds, maxDFAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    max(DFData(fftIndex:numel(nFFT):end, :), [], 2), true);
                [FPR, TPR, thresholds, minDFAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    min(DFData(fftIndex:numel(nFFT):end, :), [], 2), true);
                
                [FPR, TPR, thresholds, meanSEAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    mean(SEData(fftIndex:numel(nFFT):end, :), 2), true);
                [FPR, TPR, thresholds, medianSEAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    median(SEData(fftIndex:numel(nFFT):end, :), 2), true);
                [FPR, TPR, thresholds, maxSEAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    max(SEData(fftIndex:numel(nFFT):end, :), [], 2), true);
                [FPR, TPR, thresholds, minSEAUC(fftIndex) optimalThreshold] = perfcurve(outcome,...
                    min(SEData(fftIndex:numel(nFFT):end, :), [], 2), true);
                
                
                for peakIndex = 1:numel(nPeaks)
                    OIRangeData = cell(numel(peakRange), 1);
                    for fileIndex = 1:numberOfFiles
                        OIRangeData{fileIndex} = squeeze(results(fileIndex).OI(fftIndex, peakIndex, :, :));
                    end
                    OIRangeData = vertcat(OIRangeData{:});
                    
                    for rangeIndex = 1:numel(peakRange)
                        for channelIndex = 1:numberOfChannels
                            data = OIRangeData(rangeIndex:numel(peakRange):end, channelIndex);
                            [FPR, TPR, thresholds, OIAUC(fftIndex, peakIndex, rangeIndex, channelIndex), optimalThreshold] = perfcurve(outcome, data, true);
                        end
                        
                        [FPR, TPR, thresholds, meanOIAUC(fftIndex, peakIndex, rangeIndex) optimalThreshold] = perfcurve(outcome,...
                            mean(OIRangeData(rangeIndex:numel(peakRange):end, :), 2), true);
                        [FPR, TPR, thresholds, medianOIAUC(fftIndex, peakIndex, rangeIndex) optimalThreshold] = perfcurve(outcome,...
                            median(OIRangeData(rangeIndex:numel(peakRange):end, :), 2), true);
                        [FPR, TPR, thresholds, maxOIAUC(fftIndex, peakIndex, rangeIndex) optimalThreshold] = perfcurve(outcome,...
                            max(OIRangeData(rangeIndex:numel(peakRange):end, :), [], 2), true);
                        [FPR, TPR, thresholds, minOIAUC(fftIndex, peakIndex, rangeIndex) optimalThreshold] = perfcurve(outcome,...
                            min(OIRangeData(rangeIndex:numel(peakRange):end, :), [], 2), true);
                    end
                end
            end
        end
        
        function ComputeSampleEntropyMaximumAUC(self, filenameList, outcome, nFFT, nSamples, tolerance)
            numberOfFiles = numel(filenameList);
            results(numberOfFiles) = struct(...
                'SampEn', [],...
                'SampEnDF', []);
            
            parfor fileIndex = 1:numberOfFiles
                filename = filenameList{fileIndex};
                [~, shortFilename, ~] = fileparts(filename);
                fileData = load(filename);
                ecgData = fileData.cancelledEcgData;
                
                disp(['File: ', shortFilename, ' (', num2str(fileIndex), ' of ', num2str(numberOfFiles), ')']);
                
                % remove first and last second
                time = ecgData.GetTimeRange();
                validTime = time >= 1 & time < (time(end) - 1);
                ecgData.Data = ecgData.Data(validTime, :);
                
                results(fileIndex).SampEn = NaN(numel(nFFT), numel(nSamples), numel(tolerance), ecgData.GetNumberOfChannels);
                results(fileIndex).SampEnDF = NaN(numel(nFFT), numel(nSamples), numel(tolerance), ecgData.GetNumberOfChannels);
                
                for fftIndex = 1:numel(nFFT)
                    complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
                    complexityCalculator.DFParameters.NumberOfFFTPoints = nFFT(fftIndex);
                    spectra = complexityCalculator.ComputeECGSpectrum(ecgData.Data, ecgData.SamplingFrequency);
                    
                    [DF, ~] = complexityCalculator.ComputeOrganizationIndex(spectra);
                    
                    for sampleIndex = 1:numel(nSamples)
                        complexityCalculator.SampleEntropyParameters.samples = nSamples(sampleIndex);
                        for toleranceIndex = 1:numel(tolerance)
                            complexityCalculator.SampleEntropyParameters.tolerance = tolerance(toleranceIndex);
                            [sampleEntropy, dfSampleEntropy] = complexityCalculator.ComputeSampleEntropy(ecgData,...
                                1:ecgData.GetNumberOfChannels, DF);
                            results(fileIndex).SampEn(fftIndex, sampleIndex, toleranceIndex, :) = sampleEntropy;
                            results(fileIndex).SampEnDF(fftIndex, sampleIndex, toleranceIndex, :) = dfSampleEntropy;
                        end
                    end
                end 
            end
            
            numberOfChannels = size(results(1).SampEn, 4);
            SampEnAUC = NaN(numel(nFFT), numel(nSamples), numel(tolerance), numberOfChannels);
            
            meanSampEnAUC = NaN(numel(nFFT), numel(nSamples), numel(tolerance));
            medianSampEnAUC = NaN(numel(nFFT), numel(nSamples), numel(tolerance));
            maxSampEnAUC = NaN(numel(nFFT), numel(nSamples), numel(tolerance));
            minSampEnAUC = NaN(numel(nFFT), numel(nSamples), numel(tolerance));
            
            for fftIndex = 1:numel(nFFT)
                for sampleIndex = 1:numel(nSamples)
                    SampEnToleranceData = cell(numel(nSamples), 1);
                    for fileIndex = 1:numberOfFiles
                        SampEnToleranceData{fileIndex} = squeeze(results(fileIndex).SampEnDF(fftIndex, sampleIndex, :, :));
                    end
                    SampEnToleranceData = vertcat(SampEnToleranceData{:});
                    
                    for toleranceIndex = 1:numel(tolerance)
                        for channelIndex = 1:numberOfChannels
                            data = SampEnToleranceData(toleranceIndex:numel(tolerance):end, channelIndex);
                            [FPR, TPR, thresholds, SampEnAUC(fftIndex, sampleIndex, toleranceIndex, channelIndex), optimalThreshold] = perfcurve(outcome, data, true);
                        end
                        
                        [FPR, TPR, thresholds, meanSampEnAUC(fftIndex, sampleIndex, toleranceIndex) optimalThreshold] = perfcurve(outcome,...
                            mean(SampEnToleranceData(toleranceIndex:numel(tolerance):end, :), 2), true);
                        [FPR, TPR, thresholds, medianSampEnAUC(fftIndex, sampleIndex, toleranceIndex) optimalThreshold] = perfcurve(outcome,...
                            median(SampEnToleranceData(toleranceIndex:numel(tolerance):end, :), 2), true);
                        [FPR, TPR, thresholds, maxSampEnAUC(fftIndex, sampleIndex, toleranceIndex) optimalThreshold] = perfcurve(outcome,...
                            max(SampEnToleranceData(toleranceIndex:numel(tolerance):end, :), [], 2), true);
                        [FPR, TPR, thresholds, minSampEnAUC(fftIndex, sampleIndex, toleranceIndex) optimalThreshold] = perfcurve(outcome,...
                            min(SampEnToleranceData(toleranceIndex:numel(tolerance):end, :), [], 2), true);
                    end
                end
            end
        end
        
        function ShowDescriptiveStatistics(self, dataMatrix, parameterLabels, outcome, figureHandle)
            if nargin < 5
                figureHandle = figure('name', 'Descriptive statistics');
            end
            
            numberOfParameters = size(dataMatrix, 2);
            pValues = NaN(numberOfParameters, 1);
            for parameterIndex = 1:numberOfParameters
                nonNormality1 = lillietest(dataMatrix(outcome, parameterIndex));
                nonNormality2 = lillietest(dataMatrix(~outcome, parameterIndex));
                
                if nonNormality1 || nonNormality2
                    pValues(parameterIndex) = ranksum(...
                        dataMatrix(outcome, parameterIndex),...
                        dataMatrix(~outcome, parameterIndex));
                else
                    [tf, pValues(parameterIndex)] = ttest2(...
                        dataMatrix(outcome, parameterIndex),...
                        dataMatrix(~outcome, parameterIndex));
                end
            end
            significantDifferences = pValues < 0.05;
            significantIndices = find(significantDifferences);
            self.SignificantParameterIndices = significantIndices;
            
            numberOfColumns = 3;
            numberOfRows = ceil((numel(significantIndices) + 1) / numberOfColumns);
            
            % Y vs N
            subplot(numberOfRows, numberOfColumns, 1, 'parent', figureHandle);
            bar([1 2], [numel(find(outcome)), numel(find(~outcome))]);
            set(gca, 'xTick', [1 2], 'xTickLabel', {'Y', 'N'});
            title('Cardioversion');
            
            for parameterIndex = 1:numel(significantIndices)
                subplot(numberOfRows, numberOfColumns, parameterIndex + 1, 'parent', figureHandle);
                boxplot(dataMatrix(:, significantIndices(parameterIndex)), outcome);
                title([parameterLabels{significantIndices(parameterIndex)}, ', p-value: ', num2str(pValues(significantIndices(parameterIndex)))]);
            end
        end
    end
    
    methods (Access = public)
        function ShowPCA(self, dataMatrix, parameterLabels, outcome)
            [pc, zscores] = princomp(zscore(dataMatrix));
            subplot(1,1,1, 'parent', self.PCAPanel);
            scatter(zscores(outcome, 1),zscores(outcome, 2), 'g');
            hold on;
            scatter(zscores(~outcome, 1),zscores(~outcome, 2), 'r');
            
        end
        
        function ShowSingleParameterROC(self, dataMatrix, parameterLabels, outcome, figureHandle)
            if nargin < 5
                figureHandle = figure('name', 'Parameter ROC');
            end
            numberOfParameters = numel(parameterLabels);
            
            numberOfColumns = 3;
            numberOfRows = ceil((numberOfParameters + 1) / numberOfColumns);
            
            for parameterIndex = 1:numberOfParameters
                discriminantClassifier = ClassificationDiscriminant.fit(dataMatrix(:, parameterIndex), outcome);
                [label, score] = predict(discriminantClassifier, dataMatrix(:, parameterIndex));
                measure = score(:, 2) - score(:, 1);

                plotHandle = subplot(numberOfRows, numberOfColumns, parameterIndex, 'parent', figureHandle);
                [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(outcome, measure, true);
                line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                    'displayName', [parameterLabels{parameterIndex},' (AUC: ' num2str(AUC) ')']);
                
                title(plotHandle, ['ROC ', parameterLabels{parameterIndex}]);
                xlabel(plotHandle, '1 - specificity');
                ylabel(plotHandle, 'sensitivity');
                legend(plotHandle, 'show');
            end
        end
        
        function ShowLogisticRegression(self, dataMatrix, parameterLabels, outcome)
            % logistic regression
            disp('Logistic regression');
            [coefficients, deviance, stats] = glmfit(dataMatrix, outcome, 'binomial');
            predictions = glmval(coefficients, dataMatrix, 'logit');
            subplot(1,3,1, 'parent', self.LogisticRegressionPanel);
            hist(outcome - predictions);
            xlim([-1, 1]);
            title('Logistic regression residuals - all variables');
            
            % feature selection (forward subset selection)
            maxdev = chi2inv(.95, 1);     
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs');
            [inmodel history] = sequentialfs(@NonInvasiveBatchAnalyser.LogisticFit, dataMatrix, outcome,...
                                   'cv','none',...
                                   'nullmodel', true,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', []);
           [coefficients, deviance, stats] = glmfit(dataMatrix(:, inmodel), outcome, 'binomial');
           predictions = glmval(coefficients, dataMatrix(:, inmodel), 'logit');
           subplot(1,3,2, 'parent', self.LogisticRegressionPanel);
           hist(outcome - predictions);
           xlim([-1, 1]);
           title(['Logistic regression residuals - subset (', num2str(find(inmodel)), ')']);
           disp('Forward subset selection parameters:');
           disp(parameterLabels(inmodel));
           
           % feature selection (forward subset selection (discriminant analysis))
%             maxdev = chi2inv(.95, 1);     
            disp('Discriminant analysis');
            maxdev = 0.01;  
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs');
            [inmodel history] = sequentialfs(@NonInvasiveBatchAnalyser.DiscriminantFit, dataMatrix, outcome,...
                                   'cv', 'none',...
                                   'nullmodel', false,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', []);
           discriminantClassifier = ClassificationDiscriminant.fit(dataMatrix(:, inmodel), outcome, 'DiscrimType', 'linear');
           cvmodel = crossval(discriminantClassifier, 'leaveout', 'on');
           loss = kfoldLoss(cvmodel);
           [label, score] = predict(discriminantClassifier, dataMatrix(:, inmodel));
            measure = score(:, 2) - score(:, 1);
            
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(outcome, measure, true);
           
           plotHandle = subplot(1,3,3, 'parent', self.LogisticRegressionPanel);
           line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                    'displayName', ['AUC: ' num2str(AUC) ', leave-out loss: ', num2str(loss)]);
           title(['Discriminant analysis - subset (', num2str(find(inmodel)), ')']);
           disp('Forward subset selection parameters (discriminant analysis):');
           disp(parameterLabels(inmodel));
           xlabel(plotHandle, '1 - specificity');
           ylabel(plotHandle, 'sensitivity');
           legend(plotHandle, 'show');
           
           optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
           predictions = measure >= optimalThresholdValue;
           error = sum(abs(outcome - predictions)) / numel(outcome);
        end
        
        function ShowSVM(self, dataMatrix, parameterLabels, outcome)
            disp('SVM');
            
            maxdev = 0.01;
            opt = statset('display','iter',...
                'TolFun', maxdev,...
                'TolTypeFun','abs');
            [inmodel history] = sequentialfs(@NonInvasiveBatchAnalyser.ComputeSVM, dataMatrix, outcome,...
                'cv', 'none',...
                'nullmodel', false,...
                'options', opt,...
                'direction','forward',...
                'nfeatures', 2);
            
            subplot(1,1,1, 'parent', self.SVMPanel);
            svmStruct = svmtrain(dataMatrix(:, inmodel), outcome,...
                'Kernel_Function', 'rbf', 'boxconstraint', 1,...
                'showPlot', true);
            
            newClasses = svmclassify(svmStruct, dataMatrix(:, inmodel));
            error = sum(abs(outcome - newClasses)) / numel(outcome);
            disp(parameterLabels(inmodel));
        end
        
        function ShowParameterDistributions(self)
            subplot(3,2,1, 'parent', self.ParameterDistributionPanel);
            boxplot(self.DFData.mean, self.FileTypes);
            title('Dominant frequency (mean)');
            ylabel('Hz');
            
            subplot(3,2,2, 'parent', self.ParameterDistributionPanel);
            boxplot(self.DFData.std, self.FileTypes);
            title('Dominant frequency (std)');
            ylabel('Hz');
            
            subplot(3,2,3, 'parent', self.ParameterDistributionPanel);
            boxplot(self.FWaveData.mean, self.FileTypes);
            title('F-wave amplitude (mean)');
            ylabel('mV');
            
            subplot(3,2,4, 'parent', self.ParameterDistributionPanel);
            boxplot(self.FWaveData.std, self.FileTypes);
            title('F-wave amplitude (std)');
            ylabel('mV');
            
            subplot(3,2,5, 'parent', self.ParameterDistributionPanel);
            boxplot(self.SEData.mean, self.FileTypes);
            title('Sample entropy (mean)');
            ylabel('Entropy');
            
            subplot(3,2,6, 'parent', self.ParameterDistributionPanel);
            boxplot(self.SEData.std, self.FileTypes);
            title('Sample entropy (std)');
            ylabel('Entropy');
        end
        
        function ShowROCCurves(self)
            uniqueFileTypes = unique(self.FileTypes);
            numberOfTypes = numel(uniqueFileTypes);
            
            for typeIndex = 1:numberOfTypes
                plotHandle = subplot(1, numberOfTypes, typeIndex, 'parent', self.SingleParameterROCPanel);
                PlotTypeROC(self, plotHandle, uniqueFileTypes{typeIndex});
            end
        end
        
        function ShowCombinedROCCurves(self)
            uniqueFileTypes = unique(self.FileTypes);
            numberOfTypes = numel(uniqueFileTypes);
            
            for typeIndex = 1:numberOfTypes
                plotHandle = subplot(1, numberOfTypes, typeIndex, 'parent', self.CombinedParameterROCPanel);
                PlotCombinedTypeROC(self, plotHandle, uniqueFileTypes{typeIndex});
            end
        end
        
        function ComputeParameterDistributions(self)
           dataStruct = struct(...
                'mean', NaN(size(self.FileData)),...
                'std', NaN(size(self.FileData)));
            self.DFData = dataStruct;
            self.FWaveData = dataStruct;
            self.SEData = dataStruct;
            for fileIndex = 1:numel(self.FileData)
                resultData = self.FileData{fileIndex};
                
                % Dominant frequency
                self.DFData.mean(fileIndex) = resultData.DF.frequencies(resultData.DF.dfIndex);
                
                % F-wave amplitude
                v1Index = resultData.FWave.settings.v1Index;
                self.FWaveData.mean(fileIndex) = mean(resultData.FWave.fWaveAmplitudes{v1Index});
                self.FWaveData.std(fileIndex) = std(resultData.FWave.fWaveAmplitudes{v1Index});
                
                % Sample entropy
                self.SEData.mean(fileIndex) = mean(resultData.SE.result.runningWindowMean);
                self.SEData.std(fileIndex) = std(resultData.SE.result.runningWindowMean);
            end 
        end
        
        function PlotTypeROC(self, plotHandle, type)
            types = false(size(self.FileTypes));  
            types(strcmp(type, self.FileTypes)) = true;
            
        % Dominant frequency
            means = self.DFData.mean;
            discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
            [label, score] = predict(discriminantClassifier, means(:));
            measure = score(:, 2) - score(:, 1);
                
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                'displayName', ['Dominant frequency (AUC: ' num2str(AUC) ')']);
            
            % F-wave amplitude
            means = self.FWaveData.mean;
            discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
            [label, score] = predict(discriminantClassifier, means(:));
            measure = score(:, 2) - score(:, 1);
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [0 1 0], 'parent', plotHandle,...
                'displayName', ['F-wave amplitude (AUC: ' num2str(AUC) ')']);
            
            % Sample entropy
            means = self.SEData.mean;
            discriminantClassifier = ClassificationDiscriminant.fit(means(:), types);
            [label, score] = predict(discriminantClassifier, means(:));
            measure = score(:, 2) - score(:, 1);
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [0 0 1], 'parent', plotHandle,...
                'displayName', ['Sample entropy (AUC: ' num2str(AUC) ')']);
            
            title(plotHandle, ['ROC type ' type]);
            xlabel(plotHandle, '1 - specificity');
            ylabel(plotHandle, 'sensitivity');
            legend(plotHandle, 'show');
        end
        
        function PlotCombinedTypeROC(self, plotHandle, type)
            types = false(size(self.FileTypes));  
            types(strcmp(type, self.FileTypes)) = true;
            
            dfMeans = self.DFData.mean;
            seMeans = self.SEData.mean;
            fWaveMeans = self.FWaveData.mean;
            
            % DF & F-wave
            means = [dfMeans(:) fWaveMeans(:)];
            discriminantClassifier = ClassificationDiscriminant.fit(means, types);
            [label, score] = predict(discriminantClassifier, means);
            measure = score(:, 2) - score(:, 1);
            
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                'displayName', ['DF & F-Wave (AUC: ' num2str(AUC) ')']);
            
            % DF & SE
            means = [dfMeans(:), seMeans(:)];
            discriminantClassifier = ClassificationDiscriminant.fit(means, types);
            [label, score] = predict(discriminantClassifier, means);
            measure = score(:, 2) - score(:, 1);
            
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [0 1 0], 'parent', plotHandle,...
                'displayName', ['DF & SE (AUC: ' num2str(AUC) ')']);
            
            % F-Wave & SE           
            means = [fWaveMeans(:) seMeans(:)];
            discriminantClassifier = ClassificationDiscriminant.fit(means, types);
            [label, score] = predict(discriminantClassifier, means);
            measure = score(:, 2) - score(:, 1);
            
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [0 0 1], 'parent', plotHandle,...
                'displayName', ['F-Wave & SE (AUC: ' num2str(AUC) ')']);
            
            % DF, F-Wave & SE           
            means = [dfMeans(:), fWaveMeans(:) seMeans(:)];
            discriminantClassifier = ClassificationDiscriminant.fit(means, types);
            [label, score] = predict(discriminantClassifier, means);
            measure = score(:, 2) - score(:, 1);
            
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(types, measure, true);
            line('xData', FPR, 'yData', TPR, 'color', [0 1 1], 'parent', plotHandle,...
                'displayName', ['DF, F-Wave & SE (AUC: ' num2str(AUC) ')']);
            
            title(plotHandle, ['ROC type ' type]);
            xlabel(plotHandle, '1 - specificity');
            ylabel(plotHandle, 'sensitivity');
            legend(plotHandle, 'show');
        end
    end
    
    methods (Static)
        function error = LogisticFit(data, outcome, testData, testOutcome)
            if nargin < 3
                testData = data;
                testOutcome = outcome;
            end
            
            [b, deviance] = glmfit(data, outcome, 'binomial');
            prediction = (round(glmval(b, testData, 'logit')));
            
            confusion = confusionmat(testOutcome, prediction, 'order', [0, 1]);
            
            misclassification = sum(confusion(:)) - sum(diag(confusion));
            
            error = misclassification;
        end
        
        function error = DiscriminantFit(data, outcome, testData, testOutcome)
            if nargin < 3
                testData = data;
                testOutcome = outcome;
            end
            
            discriminantClassifier = ClassificationDiscriminant.fit(data, outcome, 'DiscrimType', 'quadratic');
%             discriminantClassifier.Prior = [1 3];
            
            [testPrediction score] = predict(discriminantClassifier, testData);
            
            error = sum(find(testPrediction ~= testOutcome));
        end
        
        function error = SVMFit(data, outcome, testData, testOutcome)
            if nargin < 3
                testData = data;
                testOutcome = outcome;
            end
            svmStruct = svmtrain(data, outcome, 'Kernel_Function', 'rbf', 'boxconstraint', 100);
            
            newClasses = svmclassify(svmStruct, testData);
            confusion = confusionmat(testOutcome, newClasses);
            
            % misclassification
            misclassification = sum(confusion(:)) - sum(diag(confusion));
            
            error = misclassification;
        end
        
        function error = SVMOptimizedFit(data, outcome, testData, testOutcome)
            if nargin < 3
                testData = data;
                testOutcome = outcome;
            end
            
            c = cvpartition(numel(outcome), 'kfold', 10);
            
            opts = statset('UseParallel', true);
            minfn = @(z) crossval('mcr', data, outcome,...
                'predfun', @(xtrain, ytrain, xtest) crossfun(xtrain, ytrain,...
                xtest, exp(z(1)), exp(z(2))), 'partition', c, 'options', opts);
            
            opts = optimset('TolX', 5e-4, 'TolFun', 5e-4);
            [searchmin, fval] = fminsearch(minfn, randn(2, 1), opts);
            
            svmStruct = svmtrain(data, outcome, 'Kernel_Function', 'rbf',...
                'rbf_sigma', exp(searchmin(1)), 'boxconstraint', exp(searchmin(2)),...
                'method', 'QP');
            
            newClasses = svmclassify(svmStruct, testData);
            confusion = confusionmat(testOutcome, newClasses);
            
            % misclassification
            misclassification = sum(confusion(:)) - sum(diag(confusion));
            
            error = misclassification;
            
            function yfit = crossfun(xtrain, ytrain, xtest, rbf_sigma, boxconstraint)
                % Train the model on xtrain, ytrain,
                % and get predictions of class of xtest
                svmStruct = svmtrain(xtrain, ytrain, 'Kernel_Function','rbf',...
                    'rbf_sigma', rbf_sigma, 'boxconstraint', boxconstraint, 'method', 'QP');
                yfit = svmclassify(svmStruct, xtest);
            end
        end
        
        function ComputeDecisionTree(dataMatrix, parameterLabels, outcome)
            disp('Decision tree');
            
            ctree = ClassificationTree.fit(dataMatrix, outcome,...
                'predictorNames', parameterLabels, 'crossval', 'on');
            view(ctree);
%             view(ctree,'mode','graph')
            
            [label, score] = predict(ctree, dataMatrix);
           confusion = confusionmat(outcome, label);
           
           disp('Forward subset selection parameters (Decision Tree):');
           disp('Confusion matrix:');
           disp(confusion);
           disp(['Resubstitution loss: ', num2str(resubLoss(ctree))]);
           
           leafs = 5:20;
           N = numel(leafs);
           err = zeros(N, 1);
           for n=1:N
               t = ClassificationTree.fit(dataMatrix, outcome, 'crossval', 'on',...
                   'minleaf', leafs(n));
               err(n) = kfoldLoss(t);
           end
           figure;
           plot(leafs, err);
           xlabel('Min Leaf Size');
           ylabel('cross-validated error');
           
           [minValue, minPos] = min(err);
           disp(['minimal number in leaf: ', num2str(leafs(minPos))]);
           optimalTree = ClassificationTree.fit(dataMatrix, outcome,...
                'predictorNames', parameterLabels,...
                'minleaf', leafs(minPos),...
                'crossval', 'on');
            view(optimalTree,'mode','graph')
            view(optimalTree);
            
            resubOpt = resubLoss(optimalTree);
            lossOpt = kfoldLoss(crossval(optimalTree));
            resubDefault = resubLoss(ctree);
            lossDefault = kfoldLoss(crossval(ctree));
            
            [label, score] = predict(optimalTree, dataMatrix);
           confusion = confusionmat(outcome, label);
           
           disp('Confusion matrix optimal tree:');
           disp(confusion);
        end
        
        function ComputeLogisticRegression(dataMatrix, parameterLabels, outcome)
            dataMatrix = bsxfun(@minus, dataMatrix, min(dataMatrix));
            dataMatrix = bsxfun(@rdivide, dataMatrix, max(dataMatrix));
            
            % logistic regression
            disp('Logistic regression');

            % feature selection (forward subset selection)
            c = cvpartition(numel(outcome), 'leaveout');
            maxdev = 0.01;     
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs');
            [inmodel, history] = sequentialfs(@NonInvasiveBatchAnalyser.LogisticFit, dataMatrix, outcome,...
                                   'cv', c,...
                                   'nullmodel', true,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', []);
           [coefficients, deviance, stats] = glmfit(dataMatrix(:, inmodel), outcome, 'binomial');
           predictions = glmval(coefficients, dataMatrix(:, inmodel), 'logit');
           
           misClassificationCost = [0 1; 2 0];
           [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(outcome, predictions, 1, 'Cost', misClassificationCost);
           plotHandle = subplot(1,1,1);
           line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                    'displayName', ['AUC: ' num2str(AUC)]);
           title(['Logistic regression - subset (', num2str(find(inmodel)), ')']);
           disp('Forward subset selection parameters (Logistic regression):');
           disp(parameterLabels(inmodel));
           xlabel(plotHandle, '1 - specificity');
           ylabel(plotHandle, 'sensitivity');
           legend(plotHandle, 'show');
           
           optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
           predictions = predictions >= optimalThresholdValue;
           confusion = confusionmat(outcome, double(predictions));
           disp('Confusion matrix:');
           disp(confusion);
           disp(['AUC: ', num2str(AUC), ' Specificity: ', num2str(1 - optimalThreshold(1)),...
               ' Sensitivity: ', num2str(optimalThreshold(2))]);
        end
        
        function [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = ComputeOptimalLogisticRegression(ds)
            %            parameterMean = mean(dataMatrix, 2);
            %            parameterMedian = median(dataMatrix, 2);
            %            parameterSD = std(dataMatrix, 0, 2);
            %            parameterMax = max(dataMatrix, [], 2);
            %            parameterMin = min(dataMatrix, [], 2);
            %            dataMatrix = [dataMatrix, [parameterMean, parameterMedian, parameterSD, parameterMax, parameterMin]];
            %            parameterLabels = [parameterLabels; {'Mean'; 'Median'; 'SD'; 'Max'; 'Min'}];
            
            
            numberOfParameters = size(ds, 2) - 1;
            % linear terms
            terms = [zeros(1, numberOfParameters + 1);...
                [eye(numberOfParameters), zeros(numberOfParameters, 1)]];
            % interactions
%             for parameterIndex = 1:(numberOfParameters - 1)
%                 interaction = zeros(1, numberOfParameters + 1);
%                 interaction(parameterIndex:(parameterIndex + 1)) = 1;
%                 terms = [terms;...
%                     interaction]; %#ok<AGROW>
%             end
            
            glModel = GeneralizedLinearModel.stepwise(ds,...
                'constant', 'lower', 'constant', 'upper', 'linear',...
                'distr', 'binomial', 'criterion', 'deviance');
            
            predictions = predict(glModel, ds(:, 1:(end-1)));
            outcome = ds.Outcome;
            
            misClassificationCost = [0 1; 2 0];
            [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
        end
        
        function [confusion, aucValues, TPR, FPR, glModel, sensitivity, specificity] = ComputeCVLogisticRegression(ds, initialModel)          
            numberOfParameters = size(ds, 2) - 1;
            if nargin < 2
                initialModel = 'constant';
            else    
                initialModel = [zeros(1, numberOfParameters + 1);...
                [diag(double(initialModel)), zeros(numberOfParameters, 1)]];
                [~, i] = unique(initialModel, 'rows', 'first');
                if length(i) < size(initialModel, 1)
                    initialModel = initialModel(sort(i),:);
                end
            end
                       
            glModel = GeneralizedLinearModel.stepwise(ds,...
                initialModel, 'lower', 'constant', 'upper', 'linear',...
                'distr', 'binomial', 'criterion', 'deviance', 'ResponseVar', 'Outcome');
            
            predictions = predict(glModel, ds(:, 1:(end-1)));
            outcome = ds.Outcome;
            
            misClassificationCost = [0 3; 7 0];
            [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
            
            functionHandle = @(trainingSet, testSet)(TrainAndTestModel(glModel,trainingSet, testSet));
            partitionFold = 10;
            dataPartition = cvpartition(ds.Outcome, 'kfold', partitionFold);
            crossvalOptions = statset('useParallel', true);
            aucValues = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', 10, 'options', crossvalOptions);
            
            function testAUC = TrainAndTestModel(model, trainingData, testData)
                inModel = model.VariableInfo.InModel;
                fittedModel = GeneralizedLinearModel.fit(trainingData, 'PredictorVars',  inModel,...
                    'ResponseVar', 'Outcome', 'distr', 'binomial');
                predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
                
                actualOutcome = testData.Outcome;
            
                
                [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true', 'Cost', misClassificationCost);
            end
        end
        
        function [confusion, aucValues, TPR, FPR, glModel, sensitivity, specificity, fullSetAUC] = ComputeFixedCVLogisticRegression(ds)          
            glModel = fitglm(ds,...
                'distr', 'binomial', 'ResponseVar', 'Outcome');
            
            predictions = predict(glModel, ds(:, 1:(end-1)));
            outcome = ds.Outcome;
            
            misClassificationCost = [0 3; 7 0];
%             [FPR, TPR, thresholds, fullSetAUC, optimalThreshold] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
            [FPR, TPR, thresholds, fullSetAUC, optimalThreshold] = perfcurve(outcome, predictions, 'true');
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
            
            functionHandle = @(trainingSet, testSet)(TrainAndTestModel(glModel,trainingSet, testSet));
            partitionFold = 10;
            dataPartition = cvpartition(ds.Outcome, 'kfold', partitionFold);
            crossvalOptions = statset('useParallel', true);
            aucValues = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', 10, 'options', crossvalOptions);
            
            function testAUC = TrainAndTestModel(model, trainingData, testData)
                inModel = model.VariableInfo.InModel;
                fittedModel = fitglm(trainingData, 'PredictorVars',  inModel,...
                    'ResponseVar', 'Outcome', 'distr', 'binomial');
                predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
                
                actualOutcome = testData.Outcome;
            
                [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true', 'Cost', misClassificationCost);
            end
        end
                
        function [confusion, aucValues, TPR, FPR, glModel, sensitivity, specificity] = ComputeCVParameterSelection(ds)
            numberOfParameters = size(ds, 2) - 1;
            terms = [zeros(1, numberOfParameters + 1);...
                [eye(numberOfParameters), zeros(numberOfParameters, 1)]];
            partitionFold = 10;
            repartitionNumber = 2;
            dataPartition = cvpartition(ds.Outcome, 'kfold', partitionFold);
            
            selectedInModel = false(numberOfParameters + 1, dataPartition.NumTestSets * repartitionNumber);
            for repartitionIndex = 1:repartitionNumber
                for cvIndex = 1:dataPartition.NumTestSets
                    cvModel = GeneralizedLinearModel.stepwise(ds(training(dataPartition, cvIndex), :),...
                        'constant', 'lower', 'constant', 'upper', 'linear',...
                        'distr', 'binomial', 'criterion', 'deviance');
                    resultIndex = cvIndex  + dataPartition.NumTestSets * (repartitionIndex - 1);
                    selectedInModel(:, resultIndex) = cvModel.VariableInfo.InModel;
                end
                dataPartition = repartition(dataPartition);
            end
            
            selectedInModel = sum(selectedInModel, 2) / (dataPartition.NumTestSets * repartitionNumber);
            parametersInModel = selectedInModel >= 0.5;
            
            glModel = GeneralizedLinearModel.fit(ds, 'PredictorVars',  parametersInModel, 'ResponseVar', 'Outcome');
            
            predictions = predict(glModel, ds(:, 1:(end-1)));
            outcome = ds.Outcome;
            
            misClassificationCost = [0 1; 2 0];
            [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(outcome, predictions, 'true', 'Cost', misClassificationCost);
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
            
            functionHandle = @(trainingSet, testSet)(TrainAndTestModel(glModel,trainingSet, testSet));
            
            aucValues = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', 10);
            
            function testAUC = TrainAndTestModel(model, trainingData, testData)
                inModel = model.VariableInfo.InModel;
                fittedModel = GeneralizedLinearModel.fit(trainingData, 'PredictorVars',  inModel,...
                    'ResponseVar', 'Outcome', 'distr', 'binomial');
                predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
                
                actualOutcome = testData.Outcome;
            
                
                [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true', 'Cost', misClassificationCost);
            end
        end
        
        function [parameterInModel, aucValues] = ComputeCVModelSelection(ds)
            numberOfParameters = size(ds, 2) - 1;
            
            numberOfParameterCombinations = 2^numberOfParameters - 1;
            parameterInModel = false(numberOfParameters, numberOfParameterCombinations);
            
            partitionFold = 10;
            mcRepetitions = 10;
            dataPartition = cvpartition(ds.Outcome, 'kfold', partitionFold);
            aucValues = NaN(partitionFold * mcRepetitions, numberOfParameterCombinations);
            
            combinationCounter = 1;
            reverseStr = '';
            for modelSizeIndex = 1:numberOfParameters
                msg = sprintf('Number of model parameters: %d', modelSizeIndex);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
                currentParameterCombinations = combnk(1:numberOfParameters, modelSizeIndex);
                for combinationIndex = 1:size(currentParameterCombinations, 1)
                    parameterInModel(currentParameterCombinations(combinationIndex, :), combinationCounter) = true;
                    combinationCounter = combinationCounter + 1;
                end
            end
            fprintf('\n');
            
            disp('Computing all model combinations...');
            parfor combinationIndex = 1:numberOfParameterCombinations
                glModel = GeneralizedLinearModel.fit(ds,...
                    'PredictorVars',  [parameterInModel(:, combinationIndex); false], 'ResponseVar', 'Outcome');
                
                functionHandle = @(trainingSet, testSet)(NonInvasiveBatchAnalyser.TrainAndTestModel(glModel, trainingSet, testSet));
                aucValues(:, combinationIndex) = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', mcRepetitions);
            end
            disp('Done');
        end
        
        function testAUC = TrainAndTestModel(model, trainingData, testData)
            inModel = model.VariableInfo.InModel;
            fittedModel = GeneralizedLinearModel.fit(trainingData, 'PredictorVars',  inModel,...
                'ResponseVar', 'Outcome', 'distr', 'binomial');
            predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
            
            actualOutcome = testData.Outcome;
            
            [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true');
        end
        
        function setSizeAUC = ComputeCVSetSizeEffect(ds, setSizes)
            numberOfSubSets = 100;
            setSizeAUC = NaN(numberOfSubSets, numel(setSizes));
                    
            parfor sizeIndex = 1:numel(setSizes)
                warning('off', 'stats:glmfit:PerfectSeparation');
                warning('off', 'stats:glmfit:IterationLimit');
                disp(['Sample size: ', num2str(setSizes(sizeIndex))]);
                for subsetIndex = 1:numberOfSubSets
                    subSet = datasample(ds, setSizes(sizeIndex));                    
                    cvModel = GeneralizedLinearModel.stepwise(subSet,...
                        'constant', 'lower', 'constant', 'upper', 'linear',...
                        'distr', 'binomial', 'criterion', 'deviance');
                    predictions = predict(cvModel, subSet(:, 1:(end-1)));
                    [FPR, TPR, thresholds, AUC, optimalThreshold] =...
                        perfcurve(subSet.Outcome, predictions, 'true');
                    setSizeAUC(subsetIndex, sizeIndex) = AUC;
                end
            end
            
%             functionHandle = @(trainingSet, testSet)(TrainAndTestModel(glModel,trainingSet, testSet));
%             
%             aucValues = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', 10);
%             
%             function testAUC = TrainAndTestModel(model, trainingData, testData)
%                 inModel = model.VariableInfo.InModel;
%                 fittedModel = GeneralizedLinearModel.fit(trainingData, 'PredictorVars',  inModel,...
%                     'ResponseVar', 'Outcome', 'distr', 'binomial');
%                 predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
%                 
%                 actualOutcome = testData.Outcome;
%             
%                 
%                 [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true', 'Cost', misClassificationCost);
%             end
        end
        
        function parameterScore = BootstrappedLogisticRegression(ds, maxNumberOfParameters, iterations)

            numberOfFeatures = size(ds, 2) - 1;
            
            numberOfWorkers = 12;
            iterationsPerWorker = round(iterations / numberOfWorkers);
            
            parameterScore = zeros(numberOfWorkers, numberOfFeatures);
            parameterSelected = zeros(numberOfWorkers, numberOfFeatures);
            
            requiredBlanks = ceil(log10(iterations)) + 1;
            fprintf(1,['Bootstrap iteration:  ' blanks(requiredBlanks)]);
            backspaceString = '';
            for n = 1:requiredBlanks
                backspaceString = strcat(backspaceString, '\b');
            end
            formatString = [backspaceString '%-' num2str(requiredBlanks) 'd'];
            
            parfor workerIndex = 1:numberOfWorkers
                workerParameterScore = zeros(iterationsPerWorker, numberOfFeatures);
                workerParameterSelected = zeros(iterationsPerWorker, numberOfFeatures);
                numberOfParameters = floor(rand(iterationsPerWorker, 1) * maxNumberOfParameters + 1);
                for iterationIndex = 1:iterationsPerWorker
                    % select a random amount of random parameters 
                    parameterOrder = randperm(numberOfFeatures);
                    selectedParameters = parameterOrder(1:numberOfParameters(iterationIndex));

                    % fit a logistic regression model
                    glModel = GeneralizedLinearModel.fit(ds(:, [selectedParameters, end]),...
                    'linear', 'distr', 'binomial');

                    predictions = predict(glModel, ds(:, 1:(end-1)));
                    outcome = ds.Outcome;

                    [~, ~, ~, AUC] = perfcurve(outcome, predictions, 'true');
                    currentScore = zeros(1, numberOfFeatures)
                    currentScore(selectedParameters) = AUC;
                    workerParameterScore(iterationIndex, :) = currentScore;

                    currentSelection = zeros(1, numberOfFeatures);
                    currentSelection(selectedParameters) = 1;
                    workerParameterSelected(iterationIndex, :) = currentSelection;

    %                 fprintf(1, formatString, iterationIndex);
                end
                workerParameterScore = sum(workerParameterScore) ./ sum(workerParameterSelected);
                parameterScore(workerIndex, :) = workerParameterScore;
                parameterSelected(workerIndex, :) = sum(workerParameterSelected);
            end
            fprintf('\n')
            disp('done');
            
            parameterScore = sum(parameterScore .* parameterSelected) ./ sum(parameterSelected);
        end
        
        function [confusion, AUC, FPR, TPR, qdaClassifier, sensitivity, specificity] = ComputeOptimalLDA(dataMatrix, parameterLabels, outcome)
            %            parameterMean = mean(dataMatrix, 2);
            %            parameterMedian = median(dataMatrix, 2);
            %            parameterSD = std(dataMatrix, 0, 2);
            %            parameterMax = max(dataMatrix, [], 2);
            %            parameterMin = min(dataMatrix, [], 2);
            %            dataMatrix = [dataMatrix, [parameterMean, parameterMedian, parameterSD, parameterMax, parameterMin]];
            %            parameterLabels = [parameterLabels; {'Mean'; 'Median'; 'SD'; 'Max'; 'Min'}];
            
            misClassificationCost = [0 1; 2 0];
%             ldaClassifier = ClassificationDiscriminant.fit(dataMatrix, outcome, 'DiscrimType', 'linear', 'predictorNames', parameterLabels);
%             ldaClassifier.Cost = misClassificationCost;
            
            qdaClassifier = ClassificationDiscriminant.fit(dataMatrix, outcome, 'DiscrimType', 'pseudoQuadratic',  'predictorNames', parameterLabels);
            qdaClassifier.Cost = misClassificationCost;
            
            [predictions, score] = predict(qdaClassifier, dataMatrix);
            measure = score(:, 2) - score(:, 1); 
            
            [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(double(outcome), measure, 1, 'Cost', misClassificationCost);
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = measure >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
            
%             [errorRate, gamma, delta, numberOfPredictors] = cvshrink(qdaClassifier,...
%                 'NumGamma', 29, 'NumDelta', 29, 'Verbose', 1);
%             
%             figure;
%             plot(errorRate, numberOfPredictors, 'k.');
%             xlabel('Error rate');
%             ylabel('Number of predictors');
%             
%             minimalErrorRate = min(min(errorRate));
%             [p, q] = find(errorRate == minimalErrorRate);
%             
%             indx = repmat(1:size(delta, 2),size(delta, 1),1);
%             figure
%             subplot(1,2,1)
%             imagesc(errorRate);
%             colorbar;
%             title('Classification error');
%             xlabel('Delta index');
%             ylabel('Gamma index');
%             
%             subplot(1,2,2)
%             imagesc(numberOfPredictors);
%             colorbar;
%             title('Number of predictors in the model');
%             xlabel('Delta index');
%             ylabel('Gamma index');
% 
%             SigmaQ = qdaClassifier.Sigma;
%             SigmaL = ldaClassifier.Sigma;
%             N = ldaClassifier.NObservations;
%             K = numel(ldaClassifier.ClassNames);
%             D = size(dataMatrix, 2);
%             Nclass = [numel(find(~outcome)), numel(find(outcome))];
%             logV = (N - K) * log(det(SigmaL));
%             for k = 1:K
%                 logV = logV - (Nclass(k) - 1) * log(det(SigmaQ(: , : , k)));
%             end
%             nu = (K - 1) * D * (D + 1) / 2;
%             pval = 1 - chi2cdf(logV, nu);

%             mahQ = mahal(qdaClassifier, qdaClassifier.X, 'ClassLabels', qdaClassifier.Y);
%             expQ = chi2inv(((1:N) - 0.5) / N, D);
%             [mahQ, sorted] = sort(mahQ);
%             figure;
%             gscatter(expQ, mahQ, qdaClassifier.Y(sorted),'bgr', [], [], 'off');
%             legend('no CV', 'CV', 'Location', 'NW');
%             xlabel('Expected quantile');
%             ylabel('Observed quantile for QDA');
%             line([0 100],[0 100], 'color', 'k');
%             
%             expKurt = D * (D + 2);
%             varKurt = 8 * D * (D + 2) / N;
%             obsKurt = mean(mahQ.^2);
%             [~,pval] = ztest(obsKurt, expKurt, sqrt(varKurt));
        end
            
        function ComputeDiscriminantAnalysis(dataMatrix, parameterLabels, outcome)
            disp('Discriminant analysis');
            maxdev = 0.01;
            c = cvpartition(numel(outcome), 'k', 10);
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs',...
                          'UseParallel', 'always');
            [inmodel history] = sequentialfs(@NonInvasiveBatchAnalyser.DiscriminantFit, dataMatrix, outcome,...
                                   'cv', c,...
                                   'nullmodel', false,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', []);
           discriminantClassifier = ClassificationDiscriminant.fit(dataMatrix(:, inmodel), outcome, 'DiscrimType', 'quadratic');
%            discriminantClassifier.Prior = [1 3];
           
           % cross validation loss
           cvmodel = crossval(discriminantClassifier, 'leaveout', 'on');
           loss = kfoldLoss(cvmodel);
           disp(['Cross validation loss (leave-one-out): ', num2str(loss)])
           
           % prediction
           [modelPredictions, score] = predict(discriminantClassifier, dataMatrix(:, inmodel));
           
           % model confusion
           confusion = confusionmat(outcome, modelPredictions);
           disp('Confusion matrix (model):');
           disp(confusion);
           
           % confusion based on optimal Se/Sp
           measure = score(:, 2) - score(:, 1); 
           [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(outcome, measure, true);
           
           plotHandle = subplot(1,1,1);
           line('xData', FPR, 'yData', TPR, 'color', [1 0 0], 'parent', plotHandle,...
                    'displayName', ['AUC: ' num2str(AUC) ', leave-out loss: ', num2str(loss)]);
           title(['Discriminant analysis - subset (', num2str(find(inmodel)), ')']);
           disp('Forward subset selection parameters (discriminant analysis):');
           disp(parameterLabels(inmodel));
           xlabel(plotHandle, '1 - specificity');
           ylabel(plotHandle, 'sensitivity');
           legend(plotHandle, 'show');
           
           optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
           predictions = measure >= optimalThresholdValue;
           
           confusionOptimalThreshold = confusionmat(outcome, predictions);
           disp('Confusion matrix (AUC):');
           disp(confusionOptimalThreshold);
        end
        
        function [confusion, svmStruct] = ComputeOptimizedSVM(dataMatrix, parameterLabels, outcome)
            disp('SVM');
            dataMatrix = bsxfun(@minus, dataMatrix, min(dataMatrix));
            dataMatrix = bsxfun(@rdivide, dataMatrix, max(dataMatrix));
            numberOfFeatures = 3;
            
            c = cvpartition(numel(outcome), 'k', 10);
            maxdev = 1e-6;  
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs',...
                          'UseParallel', true);
            [inmodel, history] = sequentialfs(@NonInvasiveBatchAnalyser.SVMOptimizedFit, dataMatrix, outcome,...
                                   'cv', c,...
                                   'nullmodel', false,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', [],...
                                   'mcreps', 10);
                               
           % optimize svm parameters
           c = cvpartition(numel(outcome), 'kfold', 10);
            
            opts = statset('UseParallel', true);
            minfn = @(z) crossval('mcr', dataMatrix(:, inmodel), outcome,...
                'predfun', @(xtrain, ytrain, xtest) crossfun(xtrain, ytrain,...
                xtest, exp(z(1)), exp(z(2))), 'partition',c, 'options', opts);
            
            opts = optimset('TolX', 5e-4,'TolFun', 5e-4);
            
            numberOfStartingPoints = 5;
            bestValues = [0, 0];
            bestCriterion = Inf;
            for pointIndex = 1:numberOfStartingPoints
                [searchmin, fVal] = fminsearch(minfn, randn(2, 1), opts);
                if fVal < bestCriterion
                    bestValues = searchmin;
                    bestCriterion = fVal;
                end
            end
            
            svmStruct = svmtrain(dataMatrix(:, inmodel), outcome, 'Kernel_Function', 'rbf',...
                'rbf_sigma', exp(bestValues(1)), 'boxconstraint', exp(bestValues(2)),...
                'method', 'QP');
            
            newClasses = svmclassify(svmStruct, dataMatrix(:, inmodel));
            confusion = confusionmat(outcome, newClasses);
            error = 1 - sum(diag(confusion)) / sum(confusion(:));
            
            disp('Forward subset selection parameters (SVM):');
            disp(parameterLabels(inmodel));
            disp('Confusion matrix:');
            disp(confusion);
            disp(['Misclassification rate: ', num2str(error)]);
            sensitivity = confusion(2, 2) / sum(confusion(:, 2));
            specificity = confusion(1, 1) / sum(confusion(:, 1));
            disp(['Sensitivity: ', num2str(sensitivity)])
            disp(['Specificity: ', num2str(specificity)])
            
            function yfit = crossfun(xtrain, ytrain, xtest, rbf_sigma, boxconstraint)
                % Train the model on xtrain, ytrain,
                % and get predictions of class of xtest
                svmStruct = svmtrain(xtrain, ytrain, 'Kernel_Function','rbf',...
                    'rbf_sigma', rbf_sigma, 'boxconstraint', boxconstraint, 'method', 'QP');
                yfit = svmclassify(svmStruct, xtest);
            end
        end
        
        function [confusion, svmStruct] = ComputeSVM(dataMatrix, parameterLabels, outcome)
            disp('SVM');
            dataMatrix = bsxfun(@minus, dataMatrix, min(dataMatrix));
            dataMatrix = bsxfun(@rdivide, dataMatrix, max(dataMatrix));
            numberOfFeatures = 3;
            
            c = cvpartition(numel(outcome), 'k', 2);
            maxdev = 1e-6;  
            opt = statset('display','iter',...
                          'TolFun', maxdev,...
                          'TolTypeFun','abs',...
                          'UseParallel', true);
            [inmodel, history] = sequentialfs(@NonInvasiveBatchAnalyser.SVMFit, dataMatrix, outcome,...
                                   'cv', c,...
                                   'nullmodel', false,...
                                   'options', opt,...
                                   'direction','forward',...
                                   'nfeatures', [],...
                                   'mcreps', 1);
            
            svmStruct = svmtrain(dataMatrix(:, inmodel), outcome,...
                'Kernel_Function', 'rbf', 'boxconstraint', 100,...
                'showPlot', false);
            
            newClasses = svmclassify(svmStruct, dataMatrix(:, inmodel));
            confusion = confusionmat(outcome, newClasses);
            error = 1 - sum(diag(confusion)) / sum(confusion(:));
            
            disp('Forward subset selection parameters (SVM):');
            disp(parameterLabels(inmodel));
            disp('Confusion matrix:');
            disp(confusion);
            disp(['Misclassification rate: ', num2str(error)]);
            sensitivity = confusion(2, 2) / sum(confusion(:, 2));
            specificity = confusion(1, 1) / sum(confusion(:, 1));
            disp(['Sensitivity: ', num2str(sensitivity)])
            disp(['Specificity: ', num2str(specificity)])
        end
        
        function [confusion, AUC] = ComputeGLM(ds)
            numberOfParameters = size(ds, 2) - 1;
            % linear terms
            terms = [zeros(1, numberOfParameters + 1);...
                [eye(numberOfParameters), zeros(numberOfParameters, 1)]];
            % interactions
            %             for parameterIndex = 1:(numberOfParameters - 1)
            %                 interaction = zeros(1, numberOfParameters + 1);
            %                 interaction(parameterIndex:(parameterIndex + 1)) = 1;
            %                 terms = [terms;...
            %                     interaction]; %#ok<AGROW>
            %             end
            
            glModel = GeneralizedLinearModel.fit(ds, terms, 'distr', 'binomial');
            
            predictions = predict(glModel, ds(:, 1:(end-1)));
            outcome = ds.Cardioversion;
            
            misClassificationCost = [0 1; 3 0];
            [FPR, TPR, thresholds, AUC optimalThreshold] = perfcurve(outcome, predictions, true, 'Cost', misClassificationCost);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
        end
        
        function [confusion, aucValues, TPR, FPR, glModel, sensitivity, specificity] = ComputeLassoGLM(dataMatrix, parameterLabels, outcome)
            options = statset('useParallel', true);
            [coefficients, fitInfo] = lassoglm(dataMatrix, outcome, 'binomial',...
                'numLambda', 100, 'cv', 10, 'alpha', 0.9, 'options', options);
            
            lassoPlot(coefficients, fitInfo,'PlotType','CV');
            lassoPlot(coefficients, fitInfo,'PlotType','Lambda','XScale','log');
            indx = fitInfo.Index1SE;
%             indx = fitInfo.IndexMinDeviance;
            coefficients0 = coefficients(:, indx);
            predictors = coefficients0 ~= 0;
            glModel = GeneralizedLinearModel.fit(dataMatrix, outcome,...
                'linear', 'distr', 'binomial',... 
                'predictorVars', [predictors; false],...
                'responseVar', 'Outcome',...
                'VarNames', [parameterLabels; 'Outcome']);
            
            predictions = predict(glModel, dataMatrix);
            misClassificationCost = [0 1; 2 0];
            [FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(outcome, predictions, true, 'Cost', misClassificationCost);
            specificity = 1 - optimalThreshold(1);
            sensitivity = optimalThreshold(2);
            optimalThresholdValue = thresholds(FPR == optimalThreshold(1) & TPR == optimalThreshold(2));
            predictions = predictions >= optimalThresholdValue;
            confusion = confusionmat(outcome, predictions);
            
            functionHandle = @(trainingSet, testSet)(TrainAndTestModel(glModel,trainingSet, testSet));
            partitionFold = 10;
            dataPartition = cvpartition(outcome, 'kfold', partitionFold);
            ds = mat2dataset([dataMatrix, outcome], 'VarNames', [labels; 'Outcome']);
            ds.Outcome = logical(ds.Outcome);
            aucValues = crossval(functionHandle, ds, 'partition', dataPartition, 'mcreps', 10);
            
            function testAUC = TrainAndTestModel(model, trainingData, testData)
                inModel = model.VariableInfo.InModel;
                fittedModel = GeneralizedLinearModel.fit(trainingData, 'PredictorVars',  inModel, 'ResponseVar', 'Outcome');
                predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
                
                actualOutcome = testData.Outcome;
            
                
                [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true', 'Cost', misClassificationCost);
            end
        end
        
        function obj = ComputeLDAShrink(dataMatrix, parameterLabels, outcome)
            obj = ClassificationDiscriminant.fit(dataMatrix, outcome,...
                'SaveMemory','on','FillCoeffs','off');
            
            [err,gamma,delta,numpred] = cvshrink(obj,...
                'NumGamma',29,'NumDelta',29,'Verbose',1);
            
            minerr = min(min(err));
            [p,q] = find(err == minerr);
            
            obj.Gamma = gamma(p(1));
            obj.Delta = delta(p(1), q(1));
            
            figure;
            plot(err,numpred,'k.')
            xlabel('Error rate');
            ylabel('Number of predictors');
            
            indx = repmat(1:size(delta,2),size(delta,1),1);
            figure
            subplot(1,2,1)
            imagesc(err);
            colorbar;
            title('Classification error');
            xlabel('Delta index');
            ylabel('Gamma index');
            
            subplot(1,2,2)
            imagesc(numpred);
            colorbar;
            title('Number of predictors in the model');
            xlabel('Delta index');
            ylabel('Gamma index');
        end
    end
end