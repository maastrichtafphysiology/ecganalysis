%% Load result data
rootFolder = '/Volumes/DKE/Pharmacological Cardioversion/Analysis files';
load(fullfile(rootFolder, 'FLec_SingleLead_FFT1024_2014_02_20.mat'));

dataMatrix = result;
labels = parameterLabels';

%% Stepwise logistic regression
validParameterIndices = 1:60;
validParameterIndices = validParameterIndices(:);

ds = mat2dataset([dataMatrix(:, validParameterIndices), validOutcomes], 'VarNames', [labels(validParameterIndices); 'Outcome']);
ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));

[AUC, TPR, FPR, thresholds, glModel] = CVPredictionModel.ComputeStepwiseLogisticRegression(ds);
disp(glModel);
disp(['AUC: ', num2str(AUC)]);

%% Crossvalidated stepwise logistic regression
validParameterIndices = 1:12;
validParameterIndices = validParameterIndices(:);

ds = mat2dataset([dataMatrix(:, validParameterIndices), validOutcomes], 'VarNames', [labels(validParameterIndices); 'Outcome']);
ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));

[crossvalidationAUCValues, glModel] = CVPredictionModel.ComputeCrossValidatedLogisticRegression(ds);
disp(glModel);
disp(['AUC: ', num2str(mean(crossvalidationAUCValues)), '�', num2str(std(crossvalidationAUCValues))]);
figure('name', 'Prediction model AUC distribution');
hist(crossvalidationAUCValues);

%% Lasso / Elastic net logistic regression (Slow!)
validParameterIndices = 1:60;
validParameterIndices = validParameterIndices(:);

[aucValues, glModel] = CVPredictionModel.ComputeLassoGLM(dataMatrix(:, validParameterIndices), labels(validParameterIndices), validOutcomes);
disp(glModel);
disp(['AUC: ', num2str(mean(aucValues)), '�', num2str(std(aucValues))]);
figure('name', 'Prediction model AUC distribution');
hist(aucValues);