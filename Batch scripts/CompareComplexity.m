%% FlecCV
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Flecainide CV/Data/ClinicalDB_2008_2012.mat');
FlecainidECGcardioversion = FlecainidECGcardioversion(1:721, :);
outcome = FlecainidECGcardioversion.Succes;
outcomeIDs = (1:numel(outcome))';
validOutcomes = ~isnan(outcome);


rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Flecainide CV/Data';
load(fullfile(rootFolder, 'analysisResults_20130913.mat'));

dataMatrix = result;
labels = parameterLabels';

PhCVEcgData = mat2dataset([dataMatrix, validOutcomes], 'VarNames', [labels; 'Outcome']);
PhCVEcgData.Outcome = logical(PhCVEcgData.Outcome);
PhCVEcgData = PhCVEcgData(:, [1:48, 61:end]);
PhCVAFDuration = 2 * ones(size(PhCVEcgData, 1), 1) * (12 / 365);

%% ECV
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECV database_2013_11_27.mat');
outcome = ComputeECVRecurrence(ElectricalCardioversion, 70, 40);
outcomeIDs = (1:numel(outcome))';

rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/QRST Cancellation 2013_11_27';
% load(fullfile(rootFolder, 'analysisResults_2013_11_27.mat'));
load(fullfile(rootFolder, 'analysisResults_DF_2013_12_09.mat'));

load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECGQuality_2013_11_27.mat');
qualityECGIDs = ECGQuality.FileID(ECGQuality.ECG > 1 & ECGQuality.QRST > 1);
[qualityECG, ~] = ismember(ecgIdList, qualityECGIDs);

dataMatrix = result(qualityECG, :);
labels = parameterLabels';

% select valid outcome
validOutcome = outcome(~isnan(outcome));
validOutcomeIDs = outcomeIDs(~isnan(outcome));

% matchs ECG IDs to valid outcome IDs
[availableOutcomeECG, ecgIndex] = ismember(validOutcomeIDs, ecgIdList(qualityECG));
validECGOutcome = validOutcome(availableOutcomeECG);
validECGOutcomeIDs = validOutcomeIDs(availableOutcomeECG);

ECVEcgData = mat2dataset([dataMatrix(ecgIndex(ecgIndex > 0), :), validECGOutcome], 'VarNames', [labels; 'Outcome']);
ECVEcgData.Outcome = logical(ECVEcgData.Outcome);
ECVEcgData = ECVEcgData(:, [1:48, 61:end]);

ECVAFDuration = ElectricalCardioversion.Episode;
ECVAFDuration = ECVAFDuration(ismember(ecgIdList(qualityECG), validOutcomeIDs)) * (12 / 365);

%% Ablation
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/Clinical DB 20131030.mat');
outcome = BordeauxAblation.Termination_AF;
outcomeIDs = BordeauxAblation.Number;

rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
load(fullfile(rootFolder, 'analysisResults_20131119.mat'));
% load(fullfile(rootFolder, 'analysisResults_DF_2013_12_09.mat'));

dataMatrix = result;
labels = parameterLabels';

AblationEcgData = mat2dataset([dataMatrix, validOutcomes], 'VarNames', [labels; 'Outcome']);
AblationEcgData.Outcome = logical(AblationEcgData.Outcome);
AblationEcgData = AblationEcgData(:, [1:48, 61:end]);

[availableOutcomeECG, fileIDIndex] = ismember(validOutcomeIDs, outcomeIDs);
AblationAFDuration = BordeauxAblation.Current_AF(fileIDIndex(fileIDIndex > 0));

%% Compare a parameter
parameter = [PhCVEcgData.Dominant_Frequency_V1 ./ (PhCVEcgData.F_wave_Amplitude_AVL / 100);...
    ECVEcgData.Dominant_Frequency_V1 ./ (ECVEcgData.F_wave_Amplitude_AVL / 100);...
    AblationEcgData.Dominant_Frequency_V1 ./ (AblationEcgData.F_wave_Amplitude_AVL / 10000)];
groupPhCV = ones(size(PhCVEcgData, 1), 1);
groupPhCV(~PhCVEcgData.Outcome) = 2;

groupECV = 3 * ones(size(ECVEcgData, 1), 1);
groupECV(ECVEcgData.Outcome) = 4;

groupAblation = 5 * ones(size(AblationEcgData, 1), 1);
groupAblation(~AblationEcgData.Outcome) = 6;

group = [groupPhCV; groupECV; groupAblation];
figure;
boxplot(parameter, group, 'labels',...
    {'CV success', 'CV no success', 'ECV no recurrence',...
    'ECV recurrence', 'Ablation succes', 'Ablation no success'});
set(gcf, 'position', [200, 200, 1200, 900]);
set(gca, 'FontSize', 16, 'FontName', 'Arial');

%% Compare AF duration to a parameter
figure
markerSize = 50;
columnIndex = 56;
scatter(PhCVAFDuration, PhCVEcgData(:, columnIndex), markerSize,...
    'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[0 0 1]);
hold on;
scatter(ECVAFDuration, ECVEcgData(:, columnIndex), markerSize,...
    'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[1 0 0]);
scatter(AblationAFDuration, double(AblationEcgData(:, columnIndex)) / 100, markerSize,...
    'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[0 1 0]);
xlim(gca, [0, 120]);
set(gcf, 'position', [200, 200, 1200, 900]);
set(gca, 'FontSize', 16, 'FontName', 'Arial');

%% AF complexity score?
figure
markerSize = 70;
columnIndex = 53;

ecvParameter = double(ECVEcgData(:, 7)) ./ double(ECVEcgData(:, columnIndex));
scatter(log(ECVAFDuration(ECVAFDuration > 0)), ecvParameter(ECVAFDuration > 0) ,...
    markerSize, 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor',[1 0 0]);
hold on;

ablationParameter = double(AblationEcgData(:, 7)) ./ (double(AblationEcgData(:, columnIndex)) / 100);
scatter(log(AblationAFDuration), ablationParameter,...
    markerSize, 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor',[0 1 0]);
set(gcf, 'position', [200, 200, 1200, 900]);
set(gca, 'FontSize', 16, 'FontName', 'Arial');

AFDuration = [ECVAFDuration; AblationAFDuration];
parameterValues = [ecvParameter; ablationParameter];
validValues = AFDuration > 0;
mdl = LinearModel.fit(log(AFDuration(validValues)), parameterValues(validValues), 'RobustOpts', 'on');
disp(mdl);

modelData = linspace(min(log(AFDuration(validValues))),max(log(AFDuration(validValues))))';
line(modelData, feval(mdl, modelData), 'color', [0 0 0], 'lineSmoothing', 'on', 'lineWidth', 2);

group = [zeros(numel(ecvParameter), 1); ones(numel(ablationParameter), 1)]; 
[partialRho,partialPValue] = partialcorr([parameterValues(validValues), log(AFDuration(validValues))], group(validValues));
[rho,pValue] = corrcoef(parameterValues(validValues), log(AFDuration(validValues)));

%% Linear regression of parameters on AF duration
% combinedDataSet = [PhCVEcgData(:, 1:(end-1)); ECVEcgData(:, 1:(end-1)); AblationEcgData(:, 1:(end-1))];
% combinedDataSet = [combinedDataSet,   mat2dataset([PhCVAFDuration; ECVAFDuration; AblationAFDuration])];
complexityData = [ECVEcgData(:, 1:(end-1)); AblationEcgData(:, 1:(end-1))];
AFDuration = [ECVAFDuration; AblationAFDuration];
validValues = AFDuration >= 0 & AFDuration < 120;

combinedDataSet = complexityData(validValues, :);
combinedDataSet = [combinedDataSet,   mat2dataset(AFDuration(validValues))];
mdl = GeneralizedLinearModel.stepwise(combinedDataSet,...
    'constant', 'lower', 'constant', 'upper', 'interactions',...
    'Link', 'identity');

% mdl = GeneralizedLinearModel.fit(combinedDataSet,...
%     'Link', 'identity');

predictedDuration = predict(mdl, combinedDataSet);
figure;
scatter(AFDuration(validValues), predictedDuration, markerSize,...
    'MarkerEdgeColor',[0 0 0], 'MarkerFaceColor',[0 1 0]);

%% Lasso regression
complexityData = dataset2cell([ECVEcgData(:, 1:(end-1)); AblationEcgData(:, 1:(end-1))]);
complexityData = cell2mat(complexityData(2:end, :));
AFDuration = [ECVAFDuration; AblationAFDuration];
validValues = AFDuration >= 0 & AFDuration < 120;
complexityData = complexityData(validValues, :);
AFDuration = AFDuration(validValues);

options = statset('useParallel', true);
[coefficients, fitInfo] = lassoglm(complexityData, AFDuration, 'normal',...
    'numLambda', 100, 'cv', 10, 'alpha', 0.9, 'options', options);

lassoPlot(coefficients, fitInfo,'PlotType','CV');
lassoPlot(coefficients, fitInfo,'PlotType','Lambda','XScale','log');
indx = fitInfo.Index1SE;
coefficients0 = coefficients(:, indx);
predictors = coefficients0 ~= 0;
glModel = GeneralizedLinearModel.fit(complexityData, AFDuration,...
    'linear', 'distr', 'normal',...
    'predictorVars', [predictors; false],...
    'responseVar', 'Outcome',...
    'VarNames', [labels; 'Outcome']);

%% Correlation between complexity parameters
range = 12:12:60;
complexityData = dataset2cell([ECVEcgData(:, range); PhCVEcgData(:, range)]);
complexityData = cell2mat(complexityData(2:end, :));

[correlation, pValues] = corrcoef(complexityData);
imagesc(correlation);

figure;
numberOfParameters = numel(range);
for rowIndex = 1:numberOfParameters
    parameterValues = complexityData(:, rowIndex);
    for columnIndex = 1:numberOfParameters
        plotIndex = (rowIndex - 1) * numberOfParameters + columnIndex;
        subplot(numberOfParameters, numberOfParameters, plotIndex);
        scatter(parameterValues, complexityData(:, columnIndex));
    end
end
