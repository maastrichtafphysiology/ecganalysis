%% load clinical database
load('/Volumes/Macintosh HD 2/Data/Human/Bordeaux/AblationOutcomeData.mat');
outcome = AblationOutcomeData.SuccesSingleProcedure;
outcomeIDs = (1:numel(outcome))';
validOutcomes = ~isnan(outcome);
disp(['Patients with outcome: ', num2str(numel(find(validOutcomes)))]);

outcome = outcome(validOutcomes);
outcomeIDs = outcomeIDs(validOutcomes);

%% locate available ECGs
rootFolder = '/Volumes/Macintosh HD 2/Data/Human/Bordeaux/Bordeaux data';
directoryListing = dir(fullfile(rootFolder, '*_QRST.mat'));
filenameList = cell(numel(directoryListing), 1);
idList = NaN(numel(directoryListing), 1);
for listingIndex = 1:numel(directoryListing)
    filename = directoryListing(listingIndex).name;
    filenameList{listingIndex} = fullfile(rootFolder, filename);
    parts = strsplit(filename, {' ', '_'});
    idList(listingIndex) = str2double(parts{4});
end
validFiles = ~isnan(idList);
validFilenameList = filenameList(validFiles);
validIDList = idList(validFiles);

%% Match ECG's to outcome
[availableOutcomeECG, fileIDIndex] = ismember(outcomeIDs, validIDList);
validOutcomes = outcome(availableOutcomeECG);
validOutcomeIDs = outcomeIDs(availableOutcomeECG);
validFilenameList = validFilenameList(fileIDIndex(availableOutcomeECG));

%% Optimize individual parameter settings
analyser = NonInvasiveBatchAnalyser();
% analyser.ComputeDFMaxAUC(validFilenameList, validOutcomes > 0, [2^7, 2^8, 2^9, 2^10, 2^11, 2^12]);
% analyser.ComputeOIMaxAUC(validFilenameList, validOutcomes > 0, 1:10);
% analyser.ComputeSEMaxAUC(validFilenameList, validOutcomes > 0, [2^7, 2^8, 2^9, 2^10, 2^11, 2^12]);
% analyser.ComputePCAMaxAUC(validFilenameList, validOutcomes > 0, 0.75:0.05:0.95);
% analyser.ComputeFWaveAmplitudeMaximumAUC(validFilenameList, validOutcomes > 0, 10:5:200);
% analyser.ComputeSampleEntropyMaximumAUC(validFilenameList, validOutcomes > 0,...
%     [2^9 2^10 2^11], 2:10, 0.1:0.05:1);
% analyser.ComputeWEMaxAUC(validFilenameList, validOutcomes > 0, 4:12);

%% Compute noninvasive parameters
try
    matlabpool close;
end
matlabpool;

analyser = NonInvasiveBatchAnalyser();
[result, parameterLabels, patientIDs] = analyser.ComputeNonInvasiveParameters('', validFilenameList);
% [result, parameterLabels, patientIDs] = analyser.ComputeMultivariateParameters('', validFilenameList);
save(fullfile(rootFolder, 'analysisResults.mat'), 'result', 'parameterLabels',...
    'validOutcomes', 'validOutcomeIDs', 'validFilenameList');

matlabpool close;

% %% Analysis
% load(fullfile(rootFolder, 'analysisResults.mat'));
% 
% channelLabels = {'I', 'II', 'III', 'AVR', 'AVL', 'AVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6'};
% 
% analyser.ShowNonInvasiveParameters(result, parameterLabels, channelLabels, validOutcomes > 0);


%% Load result data
rootFolder = '/Volumes/Macintosh HD 2/Data/Human/Bordeaux/Bordeaux data';
load(fullfile(rootFolder, 'analysisResults.mat'));

dataMatrix = result;
labels = parameterLabels';

%% Logistic regression
% validParameterIndices = bsxfun(@plus, (7:9)', 12 * (0:4));
validParameterIndices = 61:72;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);
p = randperm(numel(validParameterIndices));
validParameterIndices = validParameterIndices(p);

ds = mat2dataset([dataMatrix(patientRange, validParameterIndices), validOutcomes(patientRange)], 'VarNames', [labels(validParameterIndices); 'Cardioversion']);
ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));

[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(ds);
% sensitivity = confusion(2, 2) / sum(confusion(2, :));
% specificity = confusion(1, 1) / sum(confusion(1, :));
ROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);


%% Lasso LR (Cross validation)
validParameterIndices = 1:12;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = ...
    NonInvasiveBatchAnalyser.ComputeLassoGLM(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

parameterPerformance = [sensitivity, specificity, AUC];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% LDA (CVShrink)
validParameterIndices = 52:66;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, AUC, TPR, FPR, ldaClassifier, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeOptimalLDA(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

parameterPerformance = [sensitivity, specificity, AUC];
disp(ldaClassifier);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% SVM
validParameterIndices = 1:12;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

[confusion, svmStruct] = ...
    NonInvasiveBatchAnalyser.ComputeOptimizedSVM(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

disp(svmStruct);
%% Decision Tree
validParameterIndices = 1:198;
validParameterIndices = validParameterIndices(:);
patientRange = 1:numel(validOutcomes);

NonInvasiveBatchAnalyser.ComputeDecisionTree(dataMatrix(patientRange, validParameterIndices), labels(validParameterIndices), logical(validOutcomes(patientRange)));

%% Parameter per lead
leadResults = NaN(numel(labels), 3);
patientRange = 1:numel(validOutcomes);
for validParameterIndex = 1:numel(labels);
    ds = mat2dataset([dataMatrix(patientRange, validParameterIndex), validOutcomes(patientRange)], 'VarNames', [labels(validParameterIndex); 'Cardioversion']);
        ds.(ds.Properties.VarNames{end}) = logical(ds.(ds.Properties.VarNames{end}));
     [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] = NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(ds);
    
    leadResults(validParameterIndex, 1) = sensitivity;
    leadResults(validParameterIndex, 2) = specificity;
    leadResults(validParameterIndex, 3) = AUC;
end