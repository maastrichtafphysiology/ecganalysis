% Body surface map complexity paramater analysis
%% load clinical DB
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV BSPM/ECV_BSPM_Database_2014_04_14.mat');

outcome = ElectricalCardioversionBSPM.Recurrence_6_weeks;
% outcome = ElectricalCardioversionBSPM.Succes;
outcomeIDs = ElectricalCardioversionBSPM.CRF_Number;

% AAD
amiodaron = ElectricalCardioversionBSPM.Amiodaron;
sotalol = ElectricalCardioversionBSPM.Sotalol;
flecainide = ElectricalCardioversionBSPM.Flecainide;
noAAD = (amiodaron + sotalol + flecainide) == 0;

% validOutcome = ~isnan(outcome);
validOutcome = ~isnan(outcome) & noAAD;
outcome = outcome(validOutcome);
outcomeIDs = outcomeIDs(validOutcome);


%% load BSPMs and determine ID
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV BSPM/BSPM Data (WCT)';
directoryListing = dir(fullfile(rootFolder, '*_QRST.mat'));
filenameList = cell(numel(directoryListing), 1);
bspmIDList = NaN(numel(directoryListing), 1);
for listingIndex = 1:numel(directoryListing)
    filenameList{listingIndex} = fullfile(rootFolder, directoryListing(listingIndex).name);
    stringParts = strsplit(directoryListing(listingIndex).name, {' ', '_'});
    bspmIDList(listingIndex) = str2double(stringParts{3});
end

%% Compute noninvasive parameters
try %#ok<TRYNC>
    matlabpool close;
end
matlabpool;

analyser = NonInvasiveBatchAnalyser();
result = analyser.ComputeBSPMParameters('', filenameList);
save(fullfile(rootFolder, 'analysisResults_BSPM_2014_04_14.mat'), 'result', 'bspmIDList', 'filenameList');

matlabpool close;

%% Create AUC map for complexity parameter
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV BSPM/BSPM Data (WCT)';
load(fullfile(rootFolder, 'analysisResults_BSPM_2014_04_14.mat'));

invalidElectrodeData = load(fullfile(rootFolder, 'invalidElectrodes.mat'));
invalidElectrodes = invalidElectrodeData.invalidElectrodes;
invalidElectrodeFileIDs = invalidElectrodeData.fileIDs;
[idFound, indexOfFileIDs] = ismember(bspmIDList, invalidElectrodeFileIDs);
bspmIDList = bspmIDList(idFound);
invalidElectrodes = invalidElectrodes(indexOfFileIDs(indexOfFileIDs ~= 0));

[bspmIDsWithOutcome, indexOfOutcome] = ismember(bspmIDList, outcomeIDs);
bspmOutcome = outcome(indexOfOutcome(indexOfOutcome ~= 0));

results = result(bspmIDsWithOutcome);
numberOfFiles = numel(results);

mapData = load('/Users/Zeemering/Software/AFSoftware/Noninvasive Analysis/Data/BSPMMaps/ABCD_EF_map.mat');
map = mapData.map;

mapLabels = [map.Front, cell(size(map.Front, 1), 2), fliplr(map.Back)];
mapLabels = mapLabels(~cellfun(@isempty, mapLabels));

% collect values for each electrode
parameterName = 'SE';
mapValues = NaN(numberOfFiles, numel(mapLabels));
for labelIndex = 1:numel(mapLabels);
    channelData = NaN(numberOfFiles, 1);
    for fileIndex = 1:numberOfFiles
        if any(strcmpi(mapLabels{labelIndex}, invalidElectrodes{fileIndex})),
            continue;
        end
        
        electrodeLabels = result(fileIndex).ElectrodeLabels;
        data = result(fileIndex).(parameterName);
        
        channelPosition = find(strcmpi(mapLabels{labelIndex}, electrodeLabels), 1, 'first');
        if isempty(channelPosition),continue; end
        
        channelData(fileIndex) = data(channelPosition);
    end
    mapValues(:, labelIndex) = channelData;
end

% compute AUC for each electrode
mapAUC = NaN(size(mapLabels));
mapN = zeros(size(mapLabels));
for labelIndex = 1:numel(mapLabels);
    channelData = mapValues(:, labelIndex);
    validData = ~isnan(channelData);
    
    if ~any(validData)
        continue;
    end
    
%     b = glmfit(channelData, bspmOutcome, 'binomial');
%     prediction = NaN(size(bspmOutcome));
%     prediction(validData) = glmval(b, channelData(validData), 'logit');
%     [X,Y,T,AUC] = perfcurve(bspmOutcome, prediction, 1,...
%         'ProcessNaN', 'addtofalse');
    [X,Y,T,AUC] = perfcurve(bspmOutcome, channelData, 0,...
        'ProcessNaN', 'addtofalse');
    mapAUC(labelIndex) = AUC;
    mapN(labelIndex) = numel(find(validData));
end

mapAUC(mapAUC < 0.5) = 0.5;
% PlotBSPMPropertyMaps(mapAUC, mapLabels, map, false, true,...
%     false, true, [], 1);


allMeans = [nanmedian(mapValues);...
    nanmedian(mapValues(bspmOutcome == 0, :));...
    nanmedian(mapValues(bspmOutcome == 1, :))];
scaleLimits = [0.025, 0.0815];
   
PlotBSPMPropertyMaps(allMeans(1, :)',...
    mapLabels, map, false, true, false, true, scaleLimits, 1);
set(gcf, 'name', 'All patients');

PlotBSPMPropertyMaps(allMeans(2, :)',...
    mapLabels, map, false, true, false, true, scaleLimits, 1);
set(gcf, 'name', 'No recurrence');

PlotBSPMPropertyMaps(allMeans(3, :)',...
    mapLabels, map, false, true, false, true, scaleLimits, 1);
set(gcf, 'name', '6wk recurrence');