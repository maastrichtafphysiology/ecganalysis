%% load clinical database
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECV database_2013_11_27.mat');
% outcome = ElectricalCardioversion.FU_4_8_weeks;
outcome = ComputeECVRecurrence(ElectricalCardioversion, 70, 40);
outcomeIDs = (1:numel(outcome))';
outcome(outcome == 9) = NaN;

InDatabase = ElectricalCardioversion.In_database;

gender = ElectricalCardioversion.Geslacht;
sex = cell(size(gender));
sex(gender == 1) = {'M'};
sex(gender == 2) = {'F'};
age = ElectricalCardioversion.Age;
hypertension = ElectricalCardioversion.VGhypertensie;
CAD = ElectricalCardioversion.VGCAD;
diabetes = ElectricalCardioversion.VGDM;
COPD = ElectricalCardioversion.VGCOPD;
hyperCholesterolemia = ElectricalCardioversion.VGhyperchol;
AAD = ElectricalCardioversion.MedAADtotaal;
% AAD(AAD == 1) = NaN;
episodeDuration = ElectricalCardioversion.Episode;
LAVolume = ElectricalCardioversion.Echo1LAvol;
RAVolume = ElectricalCardioversion.Echo1RAvol;
LVEF = ElectricalCardioversion.Echo1LVEF;
BMI = ElectricalCardioversion.BMI_new;

validOutcomes = ~(any(isnan([InDatabase == 0, outcome,...
    age, gender, hypertension, CAD, diabetes, COPD, hyperCholesterolemia,...
    AAD, episodeDuration, LAVolume, RAVolume, LVEF, BMI]), 2));


clinicalOutcome = outcome(validOutcomes);
clinicalOutcomeIDs = outcomeIDs(validOutcomes);

clinicalData = dataset(age, sex, hypertension, CAD, diabetes, COPD, hyperCholesterolemia,...
    AAD, episodeDuration, LAVolume, RAVolume, LVEF, BMI, outcome,...
    'VarNames', {...
    'Age', 'Sex', 'Hypertension', 'CAD', 'Diabetes', 'COPD',...
    'HyperCholesterolemia', 'AAD', 'EpisodeDuration',...
    'LAVolume,', 'RAVolume', 'LVEF', 'BMI',...
    'Outcome'});
clinicalData = clinicalData(validOutcomes, :);

clinicalData.Sex = nominal(clinicalData.Sex, {'M', 'F'});
clinicalData.Hypertension = logical(clinicalData.Hypertension);
clinicalData.CAD = logical(clinicalData.CAD);
clinicalData.Diabetes = logical(clinicalData.Diabetes);
clinicalData.COPD = logical(clinicalData.COPD);
clinicalData.HyperCholesterolemia = logical(clinicalData.HyperCholesterolemia);
clinicalData.AAD = logical(clinicalData.AAD);

clinicalData.Outcome = logical(clinicalData.Outcome);

disp(['Patients with valid clinical parameters: ', num2str(numel(find(validOutcomes)))]);

%% load available ECGs
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/QRST Cancellation 2013_11_27';
directoryListing = dir(fullfile(rootFolder, '*_QRST.mat'));
filenameList = cell(numel(directoryListing), 1);
ecgIdList = NaN(numel(directoryListing), 1);
for listingIndex = 1:numel(directoryListing)
    filenameList{listingIndex} = fullfile(rootFolder, directoryListing(listingIndex).name);
    ecgIdList(listingIndex) = str2double(directoryListing(listingIndex).name(5:(end-9)));
end

% %% Match ECG's to outcome (NO)
% [availableOutcomeECG, fileIDIndex] = ismember(outcomeIDs, idList);
% validOutcomes = outcome(availableOutcomeECG);
% validOutcomeIDs = outcomeIDs(availableOutcomeECG);
% validFilenameList = filenameList(fileIDIndex(availableOutcomeECG));

%% Compute noninvasive parameters
try %#ok<TRYNC>
    matlabpool close;
end
matlabpool;

analyser = NonInvasiveBatchAnalyser();
[result, parameterLabels, patientIDs] = analyser.ComputeNonInvasiveParameters('', filenameList);
% [result, parameterLabels, patientIDs] = analyser.ComputeMultivariateParameters('', filenameList);
save(fullfile(rootFolder, 'analysisResults_2015_04_01.mat'), 'result', 'parameterLabels', 'ecgIdList', 'filenameList');

matlabpool close;

%% Optimize individual parameter settings
% select valid outcome
validOutcome = outcome(~isnan(outcome));
validOutcomeIDs = outcomeIDs(~isnan(outcome));

% matchs ECG IDs to valid outcome IDs
[availableOutcomeECG, ecgIndex] = ismember(validOutcomeIDs, ecgIdList);
validECGOutcome = validOutcome(availableOutcomeECG);

analyser = NonInvasiveBatchAnalyser();
% analyser.ComputeDFMaxAUC(filenameList(ecgIndex(ecgIndex > 0)), validECGOutcome > 0, [2^10, 2^11, 2^12]);
% analyser.ComputeOIMaxAUC(validFilenameList, validOutcomes > 0, 1:10);
% analyser.ComputeSEMaxAUC(validFilenameList, validOutcomes > 0, [2^7, 2^8, 2^9, 2^10, 2^11, 2^12]);
% analyser.ComputePCAMaxAUC(validFilenameList, validOutcomes > 0, 0.75:0.05:0.95);
analyser.ComputeFWaveAmplitudeMaximumAUC(filenameList(ecgIndex(ecgIndex > 0)), validECGOutcome == 0, 10:5:200);
% analyser.ComputeSampleEntropyMaximumAUC(validFilenameList, validOutcomes > 0,...
%     [2^9 2^10 2^11], 2:10, 0.1:0.05:1);

%% Load ECG complexity parameter data
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/QRST Cancellation 2013_11_27';
load(fullfile(rootFolder, 'analysisResults_2014_10_30.mat'));

load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Electrical CV/ECGQuality_2013_11_27.mat');
qualityECGIDs = ECGQuality.FileID(ECGQuality.ECG > 1 & ECGQuality.QRST > 1);
[qualityECG, ~] = ismember(ecgIdList, qualityECGIDs);

% qualityECG = true(size(ecgIdList));

dataMatrix = result(qualityECG, :);
labels = parameterLabels';

% select valid outcome
validOutcome = outcome(~isnan(outcome));
validOutcomeIDs = outcomeIDs(~isnan(outcome));

% matchs ECG IDs to valid outcome IDs
[availableOutcomeECG, ecgIndex] = ismember(validOutcomeIDs, ecgIdList(qualityECG));
validECGOutcome = validOutcome(availableOutcomeECG);
validECGOutcomeIDs = validOutcomeIDs(availableOutcomeECG);

ecgData = mat2dataset([dataMatrix(ecgIndex(ecgIndex > 0), :), validECGOutcome], 'VarNames', [labels; 'Outcome']);
ecgData.Outcome = logical(ecgData.Outcome);
ecgData = ecgData(:, [1:48, 61:end]);

%% Match ECG's to outcome
[availableOutcomeECG, fileIDIndex] = ismember(validECGOutcomeIDs, clinicalOutcomeIDs);

clinicalData = clinicalData(fileIDIndex(fileIDIndex > 0), :);
ecgData = ecgData(availableOutcomeECG, :);
ecgData.Outcome = clinicalData.Outcome;

%% Clinical parameters
parameterRange = 1:13;
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(clinicalData(:, [parameterRange, end]));
ROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, mean(AUC)];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(mean(AUC)), '?', num2str(std(AUC))]);

%% ECG parameters
[crossvalidationAUCValues, stepwiseModel, fullSetAUC] = CVPredictionModel.ComputeCrossValidatedStepWiseLogisticRegression(ecgData);
stepwiseModelData = [mean(crossvalidationAUCValues), std(crossvalidationAUCValues), fullSetAUC];
stepwiseCoefficientData = [stepwiseModel.Coefficients.Estimate(2:end), stepwiseModel.Coefficients.pValue(2:end)]; 

%% All parameters
fullDataSet = [ecgData(:, 1:60), clinicalData];
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(fullDataSet);
ROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, mean(AUC)];
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(mean(AUC)), '?', num2str(std(AUC))]);

%% Test sample size effect
parameterRange = 1:12;
setSizes = 50:25:200;
setSizeAUC = NonInvasiveBatchAnalyser.ComputeCVSetSizeEffect(...
    clinicalData,...
    setSizes);