%% load clinical database
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Flecainide CV/Data/ClinicalDB_2008_2012.mat');
FlecainidECGcardioversion = FlecainidECGcardioversion(1:721, :);
outcome = FlecainidECGcardioversion.Succes;
outcomeIDs = (1:numel(outcome))';

clinicalParameter = FlecainidECGcardioversion.Echo1LAvol;

validOutcomes = ~isnan(clinicalParameter);

clinicalOutcome = outcome(validOutcomes);
clinicalOutcomeIDs = outcomeIDs(validOutcomes);
clinicalParameter = clinicalParameter(validOutcomes);

%% Load ECG complexity parameter data
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Flecainide CV/Data';
load(fullfile(rootFolder, 'analysisResults.mat'));

dataMatrix = result;
labels = parameterLabels';

%% Correlate parameters
[availableOutcomeECG, fileIDIndex] = ismember(validOutcomeIDs, clinicalOutcomeIDs);

clinicalParameter = clinicalParameter(fileIDIndex(fileIDIndex > 0), :);
dataMatrix = dataMatrix(availableOutcomeECG, :);

[r, p] = corrcoef([dataMatrix, clinicalParameter]);

r = r - eye(size(r, 1));
imagesc(r);