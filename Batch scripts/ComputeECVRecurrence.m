function [patientRecurrence, daysToRecurrence] = ComputeECVRecurrence(ElectricalCardioversion, recurrenceThreshold, minimumFU)

rhythm = dataset2cell(ElectricalCardioversion(:, 154:3:190));
rhythm = cell2mat(rhythm(2:end, :));
days = dataset2cell(ElectricalCardioversion(:, 156:3:192));
days = cell2mat(days(2:end, :));

SR = rhythm == 1 | rhythm == 4 | rhythm == 5 | rhythm == 6;
AF = rhythm == 2 | rhythm ==3;
rhythm(SR) = 0;
rhythm(AF) = 1;

numberOfPatients = size(rhythm, 1);
patientRecurrence = NaN(numberOfPatients, 1);
daysToRecurrence = NaN(numberOfPatients, 1);
for patientIndex = 1:numberOfPatients
    recurrenceIndex = rhythm(patientIndex, :) == 1 &...
        days(patientIndex, :) <= recurrenceThreshold;
    if any(recurrenceIndex)
        patientRecurrence(patientIndex) = 1;
        daysToRecurrence(patientIndex) = days(patientIndex, find(recurrenceIndex, 1, 'first'));
    else
        srFU = rhythm(patientIndex, :) == 0 &...
            days(patientIndex, :) >= minimumFU & days(patientIndex, :) <= recurrenceThreshold;
        if any(srFU)
            patientRecurrence(patientIndex) = 0;
            daysToRecurrence(patientIndex) = days(patientIndex, find(srFU, 1, 'last'));
        else
            firstFUAfterThreshold = find(~isnan(rhythm(patientIndex, :)) & days(patientIndex, :) > recurrenceThreshold, 1, 'first');
            if rhythm(patientIndex, firstFUAfterThreshold) == 0
                patientRecurrence(patientIndex) = 0;
                daysToRecurrence(patientIndex) = days(patientIndex, firstFUAfterThreshold);
            end
        end
    end
end