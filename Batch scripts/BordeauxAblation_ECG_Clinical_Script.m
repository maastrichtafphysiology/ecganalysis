%% load clinical database
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/Clinical DB 20131030.mat');
outcomeIDs = BordeauxAblation.Number;
disp(['Patients with outcome: ', num2str(numel(outcomeIDs))]);

age = BordeauxAblation.Age;
encodedSex = BordeauxAblation.Gender;
sex = cell(size(encodedSex));
sex(encodedSex == 0) = {'M'};
sex(encodedSex == 1) = {'F'};
totalMonthsOfAF = BordeauxAblation.Total_AF;
currentMonthsOfAF = BordeauxAblation.Current_AF;
LVEF = BordeauxAblation.LVEF;
LAD = BordeauxAblation.LA_para;
LAA_CL = BordeauxAblation.LAA_CL_base;
acuteOutcome = BordeauxAblation.Termination_AF;
multiOutcome = BordeauxAblation.Success_mult;

% % Acute outcome
% outcome = BordeauxAblation.Termination_AF;
% validOutcomes = ~(any(isnan([...
%     outcome,...
%     age,...
%     totalMonthsOfAF,...
%     currentMonthsOfAF,...
%     LVEF,...
%     LAD,...
%     LAA_CL]), 2));
% clinicalData = dataset(sex, age, totalMonthsOfAF, currentMonthsOfAF, LVEF, LAD, LAA_CL, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'currentMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'Outcome'});


% % Acute outcome, without current episode duration
% outcome = BordeauxAblation.Termination_AF;
% validOutcomes = ~(any(isnan([...
%     outcome,...
%     age,...
%     totalMonthsOfAF,...
%     LVEF,...
%     LAD,...
%     LAA_CL]), 2));
% clinicalData = dataset(sex, age, totalMonthsOfAF, LVEF, LAD, LAA_CL, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'Outcome'});


% Multiple procedure outcome
outcome = BordeauxAblation.Success_mult;
validOutcomes = ~(any(isnan([...
    outcome,...
    age,...
    totalMonthsOfAF,...
    currentMonthsOfAF,...
    LVEF,...
    LAD,...
    LAA_CL,...
    acuteOutcome]), 2));
clinicalData = dataset(sex, age, totalMonthsOfAF, currentMonthsOfAF, LVEF, LAD, LAA_CL, acuteOutcome, outcome,...
    'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'currentMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'AcuteOutcome', 'Outcome'});
clinicalData.AcuteOutcome = logical(clinicalData.AcuteOutcome);


% % Recurrence outcome
% outcome = BordeauxAblation.Recurrence;
% validOutcomes = ~(any(isnan([...
%     outcome,...
%     age,...
%     totalMonthsOfAF,...
%     currentMonthsOfAF,...
%     LVEF,...
%     LAD,...
%     LAA_CL,...
%     acuteOutcome]), 2));
% clinicalData = dataset(sex, age, totalMonthsOfAF, currentMonthsOfAF, LVEF, LAD, LAA_CL, acuteOutcome, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'currentMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'AcuteOutcome', 'Outcome'});
% clinicalData.AcuteOutcome = logical(clinicalData.AcuteOutcome);


% % Only acute outcome
% outcome = BordeauxAblation.Termination_AF;
% validOutcomes = ~isnan(outcome);
% clinicalData = dataset(sex, age, totalMonthsOfAF, LVEF, LAD, LAA_CL, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'Outcome'});

% % Only multiple procedure outcome
% outcome = BordeauxAblation.Success_mult;
% validOutcomes = ~isnan(outcome);
% clinicalData = dataset(sex, age, totalMonthsOfAF, LVEF, LAD, LAA_CL, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'Outcome'});

% % Only recurrence outcome
% outcome = BordeauxAblation.Recurrence;
% validOutcomes = ~isnan(outcome);
% clinicalData = dataset(sex, age, totalMonthsOfAF, LVEF, LAD, LAA_CL, outcome,...
%     'VarNames', {'Sex', 'Age', 'totalMonthsOfAF', 'LVEF', 'LAD', 'LAA_CL', 'Outcome'});

clinicalOutcome = outcome(validOutcomes);
clinicalOutcomeIDs = outcomeIDs(validOutcomes);
clinicalData = clinicalData(validOutcomes, :);
clinicalData.Sex = nominal(clinicalData.Sex, {'M', 'F'});
clinicalData.Outcome = logical(clinicalData.Outcome);

disp(['Patients with valid clinical parameters: ', num2str(numel(find(validOutcomes)))]);

%% Load ECG complexity parameter data
% rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
% load(fullfile(rootFolder, 'analysisResults_20131119.mat'));

rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/FU';
load(fullfile(rootFolder, 'analysisResults_2014_04_23_3Hz.mat'));
% load(fullfile(rootFolder, 'Ablation_SingleLead_1Hz_PWELCH_default_2014_05_23.mat'));
% load(fullfile(rootFolder, 'analysisResults_20130913.mat'));


dataMatrix = result;
labels = parameterLabels';

load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/QRST cancellatie 2014_02_17/ExcludedECGs.mat');
% validIDs = ~ismember(lower(validOutcomeIDs), lower(excludedIDs));
% validOutcomes = validOutcomes(validIDs);
% validOutcomeIDs = validOutcomeIDs(validIDs);
% dataMatrix = dataMatrix(validIDs, :);

ecgData = mat2dataset([dataMatrix, validOutcomes], 'VarNames', [labels; 'Outcome']);
ecgData.Outcome = logical(ecgData.Outcome);
% ecgData = ecgData(:, [1:48, 61:end]);

%% Match ECG's to outcome
[availableOutcomeECG, fileIDIndex] = ismember(validOutcomeIDs, clinicalOutcomeIDs);

clinicalData = clinicalData(fileIDIndex(fileIDIndex > 0), :);
ecgData = ecgData(availableOutcomeECG, :);
ecgData.Outcome = clinicalData.Outcome;

%% Clinical parameters
% parameterRange = 1:7;
% [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
%     NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(clinicalData(:, [parameterRange, end]));
% ROCData = [TPR, FPR];
% parameterPerformance = [sensitivity, specificity, mean(AUC)];
% disp(glModel);
% disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
% disp(['Specificity: ', num2str(parameterPerformance(2))]);
% disp(['AUC: ', num2str(mean(AUC)), '?', num2str(std(AUC))]);

parameterRange = 1:8;
[crossvalidationAUCValues, glModel, fullSetAUC, fullSetFPR, fullSetTPR] = CVPredictionModel.ComputeCrossValidatedStepWiseLogisticRegression(clinicalData(:, [parameterRange, end]));
modelData = [mean(crossvalidationAUCValues), std(crossvalidationAUCValues), fullSetAUC, glModel.Coefficients.Estimate(2:end)', glModel.Coefficients.pValue(2:end)'];
clinicalROCData = [fullSetTPR, fullSetFPR];
disp(glModel);

%% ECG parameters
% parameterRange = 1:12;
% [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
%     NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(ecgData(:, [parameterRange, end]));
% ROCData = [TPR, FPR];
% parameterPerformance = [sensitivity, specificity, mean(AUC)];
% disp(glModel);
% disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
% disp(['Specificity: ', num2str(parameterPerformance(2))]);
% disp(['AUC: ', num2str(mean(AUC)), '?', num2str(std(AUC))]);

parameterRange = 1:60;
[crossvalidationAUCValues, glModel, fullSetAUC, fullSetFPR, fullSetTPR] = CVPredictionModel.ComputeCrossValidatedStepWiseLogisticRegression(ecgData(:, [parameterRange, end]));
modelData = [mean(crossvalidationAUCValues), std(crossvalidationAUCValues), fullSetAUC, glModel.Coefficients.Estimate(2:end)', glModel.Coefficients.pValue(2:end)'];
ecgROCData = [fullSetTPR, fullSetFPR];
disp(glModel);

%% All parameters
fullDataSet = [ecgData(:,1:60), clinicalData];
% [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
%     NonInvasiveBatchAnalyser.ComputeCVLogisticRegression(fullDataSet);
% ROCData = [TPR, FPR];
% parameterPerformance = [sensitivity, specificity, mean(AUC)];
% disp(glModel);
% disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
% disp(['Specificity: ', num2str(parameterPerformance(2))]);
% disp(['AUC: ', num2str(mean(AUC)), '?', num2str(std(AUC))]);

[crossvalidationAUCValues, glModel, fullSetAUC, fullSetFPR, fullSetTPR] = CVPredictionModel.ComputeCrossValidatedStepWiseLogisticRegression(fullDataSet);
modelData = [mean(crossvalidationAUCValues), std(crossvalidationAUCValues), fullSetAUC, glModel.Coefficients.Estimate(2:end)', glModel.Coefficients.pValue(2:end)'];
combinedROCData = [fullSetTPR, fullSetFPR];
disp(glModel);

%% Single parameter performance
singleParameterAUC = NaN(numel(labels), 5);
for validParameterIndex = 1:numel(labels);
    disp(labels{validParameterIndex});
    [confusion, AUC, TPR, FPR, glModel, sensitivity, specificity, fullSetAUC] =...
    NonInvasiveBatchAnalyser.ComputeFixedCVLogisticRegression(ecgData(:, [validParameterIndex, end]));
    
    singleParameterAUC(validParameterIndex, 1) = mean(AUC);
    singleParameterAUC(validParameterIndex, 2) = std(AUC);
    singleParameterAUC(validParameterIndex, 3) = fullSetAUC;
    singleParameterAUC(validParameterIndex, 4) = glModel.Coefficients.Estimate(2);
    singleParameterAUC(validParameterIndex, 5) = glModel.Coefficients.pValue(2);
end

%% Single parameter differences
parameterDifferences = NaN(numel(labels), 12);
succesfulCV = ecgData.Outcome;
for validParameterIndex = 1:numel(labels);
    disp(labels{validParameterIndex});
    
    successfulMean = double(ecgData(succesfulCV, validParameterIndex));
    isNormalSuccessful = lillietest(successfulMean) == 0;
    successfulData = [mean(successfulMean), std(successfulMean), median(successfulMean), iqr(successfulMean), isNormalSuccessful];
    
    failedValues = double(ecgData(~succesfulCV, validParameterIndex));
    isNormalFailed = lillietest(failedValues) == 0;
    failedData = [mean(failedValues), std(failedValues), median(failedValues), iqr(failedValues), isNormalFailed];
    
    if isNormalSuccessful && isNormalFailed
        [~, pValue] = ttest2(successfulMean, failedValues);
        difference = mean(successfulMean) - mean(failedValues);
    else
        pValue = ranksum(successfulMean, failedValues);
        difference = median(successfulMean) - median(failedValues);
    end
    
    parameterDifferences(validParameterIndex, :) = [successfulData, failedData, difference, pValue];
end

%% Episode duration vs AF complexity
load('/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/Clinical DB 20131030.mat');
currentMonthsOfAF = BordeauxAblation.Current_AF;
outcome = BordeauxAblation.Termination_AF(~isnan(currentMonthsOfAF));
outcomeIDs = BordeauxAblation.Number(~isnan(currentMonthsOfAF));
currentMonthsOfAF = currentMonthsOfAF(~isnan(currentMonthsOfAF));

rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
load(fullfile(rootFolder, 'analysisResults_20131030.mat'));
[availableOutcomeECG, fileIDIndex] = ismember(validOutcomeIDs, outcomeIDs);
parameterIndex = 1:12;

currentMonthsOfAF = currentMonthsOfAF(fileIDIndex(fileIDIndex > 0));
parameterValues = result(availableOutcomeECG, parameterIndex);
plot(currentMonthsOfAF, parameterValues, '.', 'markerSize', 10);

%% Visualize a parameter distribution on 12 leads
figure('name', 'Parameter distribution')
parameterRange = 1:12;
for parameterIndex = 1:12,
    subplot(4, 3, parameterIndex);
    parameterData = double(ecgData(:, parameterRange(parameterIndex)));
    [binCount, binCenters] = hist(parameterData);
    
    successBinCount = hist(parameterData(ecgData.Outcome), binCenters);
    failureBinCount = hist(parameterData(~ecgData.Outcome), binCenters);
    
    bar(binCenters', [successBinCount; failureBinCount]');
    title(parameterLabels(parameterRange(parameterIndex)));
end

%% Plot ROC curve
screenSize = get(0, 'screensize');
figurePosition = [screenSize(3) / 5, screenSize(4) / 5, 2 * screenSize(3) / 5, 2 * screenSize(3) / 5];
figureHandle = figure('name', 'ROC Curve', 'numberTitle', 'off',...
    'color', [1 1 1], 'outerPosition', figurePosition);

lineWidth = 3;
plot(clinicalROCData(:, 4), clinicalROCData(:, 1),...
    'k', 'lineSmoothing', 'off', 'lineWidth', lineWidth);
hold on;
plot(ecgROCData(:, 4), ecgROCData(:, 1),...
    'b', 'lineSmoothing', 'off', 'lineWidth', lineWidth);
plot(combinedROCData(:, 4), combinedROCData(:, 1),...
    'r', 'lineSmoothing', 'off', 'lineWidth', lineWidth);
hold off;

% shadedErrorBar(clinicalROCData(:, 4), clinicalROCData(:, 1), [clinicalROCData(:, 1) - clinicalROCData(:, 2), clinicalROCData(:, 3) - clinicalROCData(:, 1)],...
%     '-k', 1);
% hold on;
% shadedErrorBar(ecgROCData(:, 4), ecgROCData(:, 1), [ecgROCData(:, 1) - ecgROCData(:, 2), ecgROCData(:, 3) - ecgROCData(:, 1)],...
%     '-b', 1);
% shadedErrorBar(combinedROCData(:, 4), combinedROCData(:, 1), [combinedROCData(:, 1) - combinedROCData(:, 2), combinedROCData(:, 3) - combinedROCData(:, 1)],...
%     '-r', 1);
% hold off;

axis equal;
xlim([-0.01, 1.01]);
ylim([-0.01, 1.01]);