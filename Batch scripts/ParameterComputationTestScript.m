foldername = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
shortFilename = 'Base ECG nummer 12_QRST.mat';

filename = fullfile(foldername, shortFilename);

complexityCalculator = AlgorithmPkg.AFComplexityCalculator();
complexityCalculator.DFParameters = struct(...
    'NumberOfFFTPoints', 2^12,...
    'FFTOverlap', 0.5,...
    'DFRange', [3, 12],...
    'NumberOfPeaks', 2,...
    'PeakRange', 1);

complexityCalculator.SampleEntropyParameters = struct(...
    'samples', 2,...
    'tolerance', 0.35);

complexityCalculator.FWaveDistanceThreshold = 100;

complexityCalculator.SegmentLength = 10;
complexityCalculator.CNVThreshold = 0.95;

fileData = load(filename);
ecgData = fileData.cancelledEcgData;

% remove first and last second
time = ecgData.GetTimeRange();
validTime = time >= 1 & time < (time(end) - 1);
ecgData.Data = ecgData.Data(validTime, :);

% Spectral parameters
spectralParameters = complexityCalculator.ComputeSpectralParameters(ecgData);