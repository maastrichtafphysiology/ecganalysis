%% Load Ablation ECG complexity parameter data
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Bordeaux Ablation/FollowUp5Years';
load(fullfile(rootFolder, 'analysisResults_20131030.mat'));

dataMatrix = result;
labels = parameterLabels';

ecgData = mat2dataset([dataMatrix, validOutcomes], 'VarNames', [labels; 'Outcome']);
ecgData.Outcome = logical(ecgData.Outcome);
ablationEcgData = ecgData(:, [1:48, 61:end]);

%% Load CV ECG complexity parameter data
rootFolder = '/Users/Zeemering/Documents/Physiology/Articles (Personal)/Cardioversion/Flecainide CV/Data';
load(fullfile(rootFolder, 'analysisResults.mat'));

dataMatrix = result;
labels = parameterLabels';

ecgData = mat2dataset([dataMatrix, validOutcomes], 'VarNames', [labels; 'Outcome']);
ecgData.Outcome = logical(ecgData.Outcome);
cardioversionEcgData = ecgData(:, [1:48, 61:end]);

%% Train on CV, test on Ablation
parameterRange = 1:60;
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(cardioversionEcgData(:, [parameterRange, end]));
cardioversionROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Training on CV');
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

predictions = predict(glModel, ablationEcgData(:, 1:(end-1)));
misClassificationCost = [0 1; 2 0];
[FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(ablationEcgData.Outcome, predictions, 'true', 'Cost', misClassificationCost);
specificity = 1 - optimalThreshold(1);
sensitivity = optimalThreshold(2);
AblationROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Testing on Ablation');
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% Train on Ablation, test on CV
parameterRange = 1:60;
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(ablationEcgData(:, [parameterRange, end]));
AblationROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Training on Ablation');
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

predictions = predict(glModel, cardioversionEcgData(:, 1:(end-1)));
misClassificationCost = [0 1; 2 0];
[FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(cardioversionEcgData.Outcome, predictions, 'true', 'Cost', misClassificationCost);
specificity = 1 - optimalThreshold(1);
sensitivity = optimalThreshold(2);
cardioversionROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Testing on Cardioversion');
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

%% Train on both
combinedDataSet = [cardioversionEcgData; ablationEcgData];
parameterRange = 1:60;
[confusion, AUC, TPR, FPR, glModel, sensitivity, specificity] =...
    NonInvasiveBatchAnalyser.ComputeOptimalLogisticRegression(combinedDataSet(:, [parameterRange, end]));
combinedROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Training on Cardioversion and Ablation');
disp(glModel);
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

predictions = predict(glModel, cardioversionEcgData(:, 1:(end-1)));
misClassificationCost = [0 1; 2 0];
[FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(cardioversionEcgData.Outcome, predictions, 'true', 'Cost', misClassificationCost);
specificity = 1 - optimalThreshold(1);
sensitivity = optimalThreshold(2);
cardioversionROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Testing on Cardioversion');
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

predictions = predict(glModel, ablationEcgData(:, 1:(end-1)));
misClassificationCost = [0 1; 2 0];
[FPR, TPR, thresholds, AUC, optimalThreshold] = perfcurve(ablationEcgData.Outcome, predictions, 'true', 'Cost', misClassificationCost);
specificity = 1 - optimalThreshold(1);
sensitivity = optimalThreshold(2);
AblationROCData = [TPR, FPR];
parameterPerformance = [sensitivity, specificity, AUC];
disp('Testing on Ablation');
disp(['Sensitivity: ', num2str(parameterPerformance(1))]);
disp(['Specificity: ', num2str(parameterPerformance(2))]);
disp(['AUC: ', num2str(parameterPerformance(3))]);

