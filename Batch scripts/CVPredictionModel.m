classdef CVPredictionModel
    properties
    end
    
    methods
        function self = CVPredictionModel()
        end
    end
    
    methods (Static)
        function [crossvalidationAUCValues, glModel] = ComputeCrossValidatedLogisticRegressionFit(parameterSet)
            glModel = fitglm(parameterSet,...
                'distr', 'binomial', 'ResponseVar', 'Outcome');
            
            % Estimate AUC distribution using k-fold cross-validation
            functionHandle = @(trainingSet, testSet)(CVPredictionModel.TrainAndTestModel(glModel, trainingSet, testSet));
            partitionFold = 10;
            dataPartition = cvpartition(parameterSet.Outcome, 'kfold', partitionFold);
            options = statset('UseParallel', true);
            crossvalidationAUCValues = crossval(functionHandle, parameterSet, 'partition', dataPartition,...
                'mcreps', 10, 'options', options);
        end
        
        function [parameterInModel, aucValues] = ComputeCrossValidatedModelSelection(parameterSet, maxModelSize)
            numberOfParameters = size(parameterSet, 2) - 1;
            if nargin < 2
                maxModelSize = numberOfParameters;
                numberOfParameterCombinations = 2^numberOfParameters - 1;
            else
                numberOfParameterCombinations = 0;
                for modelSizeIndex = 1:maxModelSize
                   numberOfParameterCombinations = numberOfParameterCombinations + size(combnk(1:numberOfParameters, modelSizeIndex), 1);
                end
            end     
            
            parameterInModel = false(numberOfParameters, numberOfParameterCombinations);
            
            partitionFold = 5;
            mcRepetitions = 10;
            dataPartition = cvpartition(parameterSet.Outcome, 'kfold', partitionFold);
            aucValues = NaN(partitionFold * mcRepetitions, numberOfParameterCombinations);
            
            combinationCounter = 1;
            reverseStr = '';
            for modelSizeIndex = 1:maxModelSize
                msg = sprintf('Number of model parameters: %d', modelSizeIndex);
                fprintf([reverseStr, msg]);
                reverseStr = repmat(sprintf('\b'), 1, length(msg));
                
                currentParameterCombinations = combnk(1:numberOfParameters, modelSizeIndex);
                for combinationIndex = 1:size(currentParameterCombinations, 1)
                    parameterInModel(currentParameterCombinations(combinationIndex, :), combinationCounter) = true;
                    combinationCounter = combinationCounter + 1;
                end
            end
            fprintf('\n');
            
            disp('Computing all model combinations...');
            parfor combinationIndex = 1:numberOfParameterCombinations
                disp(['Combination index: ', num2str(combinationIndex)]);
                glModel = fitglm(parameterSet,...
                    'PredictorVars',  [parameterInModel(:, combinationIndex); false], 'ResponseVar', 'Outcome');
                
                functionHandle = @(trainingSet, testSet)(CVPredictionModel.TrainAndTestModel(glModel, trainingSet, testSet));
                aucValues(:, combinationIndex) = crossval(functionHandle, parameterSet, 'partition', dataPartition, 'mcreps', mcRepetitions);
            end
            disp('Done');
        end
        
        function [AUC, TPR, FPR, thresholds, glModel] = ComputeStepwiseLogisticRegression(parameterSet, initialModel)
            % Stepwise logistic regression model:
            % - binomial response
            % - constant initial and lower model
            % - linear upper model
            % - criterion: deviance
            % - initial model: logical vector with parameter in the initial model (default:
            % constant model)
            
            numberOfParameters = size(parameterSet, 2) - 1;
            if nargin < 2
                initialModel = 'constant';
            else
                initialModel = [zeros(1, numberOfParameters + 1);...
                    [diag(double(initialModel)), zeros(numberOfParameters, 1)]];
                [~, i] = unique(initialModel, 'rows', 'first');
                if length(i) < size(initialModel, 1)
                    initialModel = initialModel(sort(i),:);
                end
            end
            
            glModel = GeneralizedLinearModel.stepwise(parameterSet,...
                initialModel, 'lower', 'constant', 'upper', 'linear',...
                'distr', 'binomial', 'criterion', 'deviance', 'ResponseVar', 'Outcome');
            
            % last column of dataset contains outcome
            predictions = predict(glModel, parameterSet(:, 1:(end-1)));
            outcome = parameterSet.Outcome;
            
            % computation of AUC
            [FPR, TPR, thresholds, AUC] = perfcurve(outcome, predictions, 'true', 'NBoot', 100);
        end
        
        function [crossvalidationAUCValues, glModel, fullSetAUC, fullSetFPR, fullSetTPR] = ComputeCrossValidatedStepWiseLogisticRegression(parameterSet, initialModel)
            % Crossvalidated stepwise logistic regression model:
            % - binomial response
            % - constant initial and lower model
            % - linear upper model
            % - criterion: deviance
            % - initial model: logical vector with parameter in the initial model (default:
            % constant model)
            numberOfParameters = size(parameterSet, 2) - 1;
            if nargin < 2
                initialModel = false(numberOfParameters, 1);
            end
            
            % Determine significant parameters on full dataset
            [fullSetAUC, fullSetTPR, fullSetFPR, ~, glModel] = CVPredictionModel.ComputeStepwiseLogisticRegression(parameterSet, initialModel);
            
            % Estimate AUC distribution using k-fold cross-validation
            functionHandle = @(trainingSet, testSet)(CVPredictionModel.TrainAndTestModel(glModel,trainingSet, testSet));
            partitionFold = 10;
            dataPartition = cvpartition(parameterSet.Outcome, 'kfold', partitionFold);
            crossvalidationAUCValues = crossval(functionHandle, parameterSet, 'partition', dataPartition, 'mcreps', 10);
        end
        
        function [aucValues, glModel, lassoAUC, lassoCoefficients, fitInfo] = ComputeLassoGLM(dataMatrix, parameterLabels, outcome, alpha, numberOfLambdas, cvFold, mcRepetitions, DFMax)
            if nargin < 8
                DFMax = Inf;
                if nargin < 7
                    mcRepetitions = 10;
                    if nargin < 6
                        cvFold = 10;
                        if nargin < 5
                            numberOfLambdas = 100;
                            if nargin < 4
                                alpha = 0.9;
                            end
                        end
                    end
                end
            end
            
            % Determine best settings for lambda for Lasso minimization
            % - alpha = 1: lasso regression, alpha < 1: elastic net, alpha = 0: ridge regression
            options = statset('useParallel', true);
            [lassoCoefficients, fitInfo] = lassoglm(dataMatrix, outcome, 'binomial',...
                'numLambda', numberOfLambdas, 'cv', cvFold, 'MCReps', mcRepetitions,...
                'alpha', alpha, 'LambdaRatio',1e-4, 'standardize', true, 'DFMax', DFMax, 'options', options);
            
            % plot the results
            if strcmpi(cvFold, 'resubstitution')
                lassoPlot(lassoCoefficients, fitInfo,'PlotType','L1');
            else
                lassoPlot(lassoCoefficients, fitInfo,'PlotType','CV');
            end
            lassoPlot(lassoCoefficients, fitInfo,'PlotType', 'Lambda', 'XScale', 'log');
            
            % select model with minimum deviance - 1 SE
            selectedLambdaIndex = fitInfo.Index1SE;
            %             selectedLambdaIndex = fitInfo.IndexMinDeviance;
            intercept = fitInfo.Intercept(selectedLambdaIndex);
            coefficients0 = lassoCoefficients(:, selectedLambdaIndex);
            predictors = coefficients0 ~= 0;
            regressionCoefficients = [intercept; coefficients0];
            lassoPredictions = glmval(regressionCoefficients, dataMatrix, 'logit');
            [~, ~, ~, lassoAUC] = perfcurve(outcome, lassoPredictions, 'true', 'NBoot', mcRepetitions);
            lassoCoefficients = coefficients0(predictors);
            
            % fit the model using standard logistic regression
            glModel = GeneralizedLinearModel.fit(dataMatrix, outcome,...
                'linear', 'distr', 'binomial',...
                'predictorVars', [predictors; false],...
                'responseVar', 'Outcome',...
                'VarNames', [parameterLabels; 'Outcome']);
            
            % cross-validation
            parameterSet = mat2dataset([dataMatrix, outcome], 'VarNames', [parameterLabels; 'Outcome']);
            parameterSet.Outcome = logical(parameterSet.Outcome);
            
            functionHandle = @(trainingSet, testSet)(CVPredictionModel.TrainAndTestModel(glModel,trainingSet, testSet));
            partitionFold = cvFold;
            dataPartition = cvpartition(outcome, 'kfold', partitionFold);
            aucValues = crossval(functionHandle, parameterSet, 'partition', dataPartition, 'mcreps', mcRepetitions);
        end
        
        function [fitInfos, coeffients] = ComputeLassoGLMAlpha(dataMatrix, outcome, alphaValues, numberOfLambdas, cvFold, mcRepetitions)
            if nargin < 6
                mcRepetitions = 10;
                if nargin < 5
                    cvFold = 10;
                    if nargin < 4
                        numberOfLambdas = 100;
                    end
                end
            end
            numberOfAlphaValues = numel(alphaValues);
            fitInfos = cell(numberOfAlphaValues, 1);
            coeffients = cell(numberOfAlphaValues, 1);
            
            for alphaIndex = 1:numberOfAlphaValues
                disp(['Alpha = ', num2str(alphaValues(alphaIndex))]);
                options = statset('useParallel', true);
                [coeffients{alphaIndex}, fitInfos{alphaIndex}] = lassoglm(dataMatrix, outcome, 'binomial',...
                    'numLambda', numberOfLambdas, 'cv', cvFold, 'MCReps', mcRepetitions,...
                    'alpha', alphaValues(alphaIndex), 'standardize', true, 'options', options);
            end
            
            fitInfos = vertcat(fitInfos{:});
        end
        
        function testAUC = TrainAndTestModel(model, trainingData, testData)
            inModel = model.VariableInfo.InModel;
            fittedModel = fitglm(trainingData,...
                'PredictorVars', inModel, 'ResponseVar', 'Outcome');
            
            predictedOutcome = predict(fittedModel, testData(:, 1:(end-1)));
            actualOutcome = testData.Outcome;
            [~, ~, ~, testAUC, ~] = perfcurve(actualOutcome, predictedOutcome, 'true');
        end
        
        function [pval, actualval] = BootStrapTwoSampleTest(x, y, alpha, statistics)
            % This function investigate if particular statistics (mean, median, std, etc.)
            % of two sample are significantly different. The null hypothesis is that
            % there is no difference, the two samples have been drawn from the same
            % population. The analysis is carried on in a non-parametric way by means
            % of a boot strapping procedure.
            % inputs: x = sample 1
            %         y = sample 2
            %         alpha = confidence level of the nonparametric bootstrap test
            %         (default = 5%)
            %         statistics = type of statistics to compare. Possible types are:
            %         mean, median, and std.
            % output: pval = p-value of the difference of the statistics under
            %         investigation.
            
            if nargin < 3 || isempty(alpha)
                alpha = 0.05;
            end
            if nargin < 4 || isempty(statistics)
                statistics = 'median';
            end
            
            [m1, n1] = size(x);
            [m2, n2] = size(y);
            
            if m1<n1
                x = x';
            end
            if m2<n2
                y = y';
            end
            
            % compute the metric on the actual data
            switch statistics
                case 'mean'
                    actualval = std([mean(x) mean(y)]);
                case 'median'
                    actualval = std([median(x) median(y)]);
                case 'std'
                    actualval = std([std(x) std(y)]);
            end
            
            % the null hypothesis is that all two groups actually reflect
            % the same underlying probability distribution. so, let's
            % aggregate all the values together.
            alldata = [x;y];
            % in our randomization test, we will randomly divide our data
            % into two groups and compute the metric.
            vals = zeros(1,10000); % initialize vector of results
            for p=1:10000
                % randomly shuffle the data
                datashuffle = alldata(randperm(length(alldata)));
                % reshape into two groups
                indicesx = randperm(length(datashuffle),length(x));
                datashuffle_x = datashuffle(indicesx);
                indicesy = setdiff((1:length(datashuffle)),indicesx);
                datashuffle_y = datashuffle(indicesy);
                
                % compute the metric and record the result
                switch statistics
                    case 'mean'
                        mx = mean(datashuffle_x);
                        my = mean(datashuffle_y);
                        vals(p) = std([mx my]);
                    case 'median'
                        mx = median(datashuffle_x);
                        my = median(datashuffle_y);
                        vals(p) = std([mx my]);
                    case 'std'
                        mx = std(datashuffle_x);
                        my = std(datashuffle_y);
                        vals(p) = std([mx my]);
                end
            end
            % in what fraction of simulations are values observed
            % that are more extreme than the actual value?
            % pval = sum(vals > actualval) / 10000;
            pval = sum(abs(vals) > abs(actualval)) / 10000;
            % report the result
            if pval > alpha
                sprintf('[p-value = %f, not significant]', pval)
            elseif pval < alpha
                sprintf('[p-value = %f, significant]', pval)
            end
        end
    end
end