classdef ECGQRSAndTCancellator < ECGQRSTCancellator
    properties
        TRange
        TWaveClusterSensitivity
    end
    
    methods
        function self = ECGQRSAndTCancellator()
            self.TRange = [0.2 0.05];
            self.TWaveClusterSensitivity = 0.9;
        end
               
        function cancelledEcgData = CancelQRST(self, ecgData, rWaveIndices, tqIndices)
            if nargin < 4
                cancelledEcgData = CancelQRST@ECGQRSTCancellator(self, ecgData, rWaveIndices);
                return;
            end
            
            cancelledEcgData = self.CancelQRSAndT(ecgData, rWaveIndices, tqIndices);
        end       
    end
    
    methods (Access = protected)
        function cancelledEcgData = CancelQRSAndT(self, ecgData, rWaveIndices, tqIndices)
            self.SamplingFrequency = ecgData.SamplingFrequency;
            data = ecgData.Data;
            referenceChannel = ecgData.ReferenceData(:, self.ReferenceChannelIndex);
            
            rWaveIndices = unique(rWaveIndices);
            tWaveIndices = unique(tqIndices.tIndices);
            
            % interpolation
            originalRWaveIndices = rWaveIndices;
            interpolationFactor = 4;
            if interpolationFactor > 1
                self.SamplingFrequency = ecgData.SamplingFrequency * interpolationFactor;
                data = ecgData.Data;
                time = ecgData.GetTimeRange();
                interpolatedTime = time(1):(1 / self.SamplingFrequency):time(end);
                data = spline(time, data', interpolatedTime)';
                referenceChannel = spline(time, referenceChannel', interpolatedTime)';
                rWaveIndices = (rWaveIndices - 1) * interpolationFactor + 1;
                tWaveIndices = (tWaveIndices - 1) * interpolationFactor + 1;
            end
            % end interpolation
            
            rrMin = min(diff(rWaveIndices));
            windowStart= -round(rrMin * self.QRSTRange(1));
            windowStop = round(rrMin * self.QRSTRange(2));
            windowIndices = windowStart:windowStop;
%             windowRange = round(self.SamplingFrequency * self.QRSRange);
%             windowIndices = (-windowRange(1)):windowRange(2);
            rWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(rWaveIndices)),...
                rWaveIndices(:)');
            
            windowRange = round(self.SamplingFrequency * self.TRange);
            windowIndices = (-windowRange(1)):windowRange(2);
            tWaveWindowIndices = bsxfun(@plus,...
                repmat(windowIndices(:), 1, numel(tWaveIndices)),...
                tWaveIndices(:)');
                        
            QRSClusterSensitivity = self.ClusterSensitivity;
            self.ClusterSensitivity = self.TWaveClusterSensitivity;
            data = self.CancelQRSTMethod(data, ecgData, tWaveWindowIndices, referenceChannel);
            
            self.ClusterSensitivity = QRSClusterSensitivity;
            data = self.CancelQRSTMethod(data, ecgData, rWaveWindowIndices, referenceChannel);
            data = self.FilterCancelledSignal(data, self.SamplingFrequency / 2);
            
            % downsample
            cancelledEcgData = ecgData.Copy();
            cancelledEcgData.Data = downsample(data, interpolationFactor);
            
            self.SamplingFrequency = ecgData.SamplingFrequency;
            
            if self.QRSResidueReduction
                cancelledEcgData = self.ReduceQRSResidue(cancelledEcgData, originalRWaveIndices);
            end
        end
    end
end