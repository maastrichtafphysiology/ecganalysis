classdef PWaveDurationPanel < UserInterfacePkg.CustomPanel
    properties
        EcgData
        
        RWaveIndices
        PWaveIntervals
        BaselineCorrectedData
        ValidChannels
        
        PWaveMatching
        OnsetOffset
        ElectrodeOnsetOffset
        ElectrodeTerminalForce
        
        minSlope
        fitThreshold
        minAngle
        checkOutliers
        errorCodes
        
        Map
    end
    
    properties (Access = private)
        ControlPanel
        MapDropDown
        
        VisualizationPanel
        AxesHandles
        AxesChannelIndices
    end
    
    properties (Constant)
        AvailableMaps = PWaveMatcher.LoadAvailableBSPMMaps();
    end
    
    events
        PWaveDurationChecked
        NewPWaveMatchingLoaded
    end
    
    methods
        function self = PWaveDurationPanel(position)
            self = self@UserInterfacePkg.CustomPanel(position);
            self.AxesHandles = PWaveDurationAxis.empty(0);
            self.Map = self.AvailableMaps(1);
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomPanel(self, parentHandle);
            
            self.CreateControlPanel();
            
            self.VisualizationPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, 0, 1, .9],...
                'borderType', 'none',...
                'backgroundColor', [1,1,1]);
        end
        
        function SetPWaveDurationData(self, pWaveData)
            self.SetPWaveMatchingData(pWaveData);
        end
        
        function delete(self)
            delete@UserInterfacePkg.CustomPanel(self);
            validHandles = isvalid(self.AxesHandles);
            if any(validHandles)
                delete(self.AxesHandles(validHandles));
            end
        end
    end
    
    methods (Access = protected)
        function CreateControlPanel(self)
            self.ControlPanel = uipanel('parent', self.ControlHandle,...
                'units', 'normalized', ...
                'position', [0, .9, 1, .1]);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.0 .35 .05 .3],...
                'style', 'text',...
                'HorizontalAlignment', 'left',...
                'string', 'Lead set');
            maps = PWaveAnalysisPanel.AvailableMaps;
            self.MapDropDown = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.05 .2 .15 .5],...
                'style', 'popup',...
                'string', {maps.name}',...
                'callback', @self.SetBSPMMap);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.2 .2 .12 .6],...
                'style', 'pushbutton',...
                'string', '(Re)Load analysis',...
                'callback', @self.LoadPWaveMatching);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.32 .2 .12 .6],...
                'style', 'pushbutton',...
                'string', 'Load new analysis',...
                'callback', @self.LoadNewPWaveMatching);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.44 .2 .12 .6],...
                'style', 'pushbutton',...
                'string', 'Save analysis',...
                'callback', @self.SavePWaveMatching);

            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.85 0.22 .8 .5],...
                'style', 'text',...
                'string', {'b = onset'; 'e = offset'},...
                'horizontalAlignment', 'left',...
                'fontSize', 10);
            
            uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.92 0.22 .8 .5],...
                'style', 'text',...
                'string', {'t = terminal force'; 'c = clear'},...
                'horizontalAlignment', 'left',...
                'fontSize', 10);
        end
        
        function ShowPWaveDurationMap(self)
            parentHandle = self.VisualizationPanel;
            
            if isempty(self.Map.Back)
                mapLabels = self.Map.Front;
            else
                mapLabels = [self.Map.Front, cell(size(self.Map.Front, 1), 2), fliplr(self.Map.Back)];
            end
            
%             switch self.Map.name
%                 case 'ECG_YourRhythmicsData'
%                     self.EcgData.Data=self.EcgData.Data*2e3;
%                     self.BaselineCorrectedData=self.BaselineCorrectedData*2e3;
%             end
            
            dataLabels = self.EcgData.ElectrodeLabels;
            
            pWaveData = self.PWaveMatching.GetPWaveData(self.EcgData, self.BaselineCorrectedData,...
                self.PWaveIntervals, self.OnsetOffset, self.ElectrodeOnsetOffset, self.ValidChannels);
            
            averagePWaves = pWaveData.AveragePWaves;
            validChannelIndices = pWaveData.ValidChannels;
            validChannels = false(size(dataLabels));
            validChannels(validChannelIndices) = true;
            yLimits = [min(averagePWaves(:)), max(averagePWaves(:))];
            pWaveTime = pWaveData.AveragingInterval;
            xLimits = [pWaveTime(1), pWaveTime(end)];
            
            numberOfMapRows = size(mapLabels, 1);
            numberOfMapColumns = size(mapLabels, 2);
            validHandles = isvalid(self.AxesHandles);
            if any(validHandles(:))
                delete(self.AxesHandles(validHandles));
            end
            self.AxesHandles = PWaveDurationAxis.empty(0,0);
            self.AxesHandles(numberOfMapRows, numberOfMapColumns) = PWaveDurationAxis([0,0,0,0]);
            self.AxesChannelIndices = NaN(numberOfMapRows, numberOfMapColumns);
            
            axesMagnification = 1.5;
            axesSize = axesMagnification * [1.3 / numberOfMapColumns, 1 / numberOfMapRows];
            axesSeparation = [1 / numberOfMapColumns, 1 / numberOfMapRows] - (axesMagnification - 1) .* [1 / numberOfMapColumns, 1 / numberOfMapRows].^2;
            
            for mapRow = 1:numberOfMapRows
                for mapColumn = 1:numberOfMapColumns
                    channelPosition = find(strcmpi(mapLabels{mapRow, mapColumn}, dataLabels), 1, 'first');
                    if isempty(channelPosition), continue; end
                    
                    axesPosition = [0.025 + (mapColumn - 1) * axesSeparation(1) * 0.9,...
                        0.025 + (numberOfMapRows - mapRow) * axesSeparation(2) * 0.9,...
                        axesSize];
%                     electrodeOnsetOffset = self.ElectrodeOnsetOffset(channelPosition, :);
                    electrodeOnsetOffset = self.OnsetOffset(1, 1:2);
                    electrodeTerminalForce = self.ElectrodeTerminalForce(channelPosition);
                    self.AxesHandles(mapRow, mapColumn) = PWaveDurationAxis(axesPosition,...
                        electrodeOnsetOffset, electrodeTerminalForce);
                    self.AxesChannelIndices(mapRow, mapColumn) = channelPosition;
                    
                    currentAxis = self.AxesHandles(mapRow, mapColumn);
                    currentAxis.Create(parentHandle);
                    currentAxis.Draw(averagePWaves(:, channelPosition), pWaveTime)
                    
                    currentAxis.Title = mapLabels{mapRow, mapColumn};
                    currentAxis.XLimits = xLimits;
                    currentAxis.YLimits = yLimits;
                    currentAxis.Grid = 'on';
                    
                    if mapRow < numberOfMapRows
                        currentAxis.XTickLabels = {};
                    else
                        currentAxis.XLabel = 'time (ms)';
                    end
                    if mapColumn > 1
                        currentAxis.YTickLabels = {};
                    else
                        if range(yLimits)<10
                            currentAxis.YLabel = 'voltage (mV)';
                        elseif range(yLimits)>=10
                            currentAxis.YLabel = 'voltage (muV)';
                        end
                    end
                    
                    currentAxis.Show();
                end
            end
        end
        
        function LoadPWaveMatching(self, varargin)
            if isempty(self.EcgData)
                [filename, pathname] = HelperFunctions.customUigetfile('*_PWaveMatching.mat',...
                    'Select P-wave matching file');
                if isequal(filename, 0) || isequal(pathname, 0), return; end
                filename = fullfile(pathname, filename);
                
                self.LoadData(filename);
                notify(self, 'NewPWaveMatchingLoaded');
            else
                [pathString, nameString] = fileparts(self.EcgData.Filename);
                filename = [pathString, filesep, nameString '_PWaveMatching.mat'];
                
                self.LoadData(filename);
            end 
        end
        
        function LoadNewPWaveMatching(self, varargin)
            [filename, pathname] = HelperFunctions.customUigetfile('*_PWaveMatching.mat',...
                'Select P-wave matching file');
            if isequal(filename, 0) || isequal(pathname, 0), return; end
            filename = fullfile(pathname, filename);
            
            self.LoadData(filename);
            
            notify(self, 'NewPWaveMatchingLoaded');
        end
        
        function SetPWaveMatchingData(self, pWaveMatchingData)
            self.EcgData = pWaveMatchingData.ecgData;
            
            self.RWaveIndices = pWaveMatchingData.rWaveIndices;
            self.PWaveIntervals = pWaveMatchingData.pWaveIntervals;
            self.ValidChannels = pWaveMatchingData.validChannels;
            self.BaselineCorrectedData = pWaveMatchingData.baselineCorrectedData;
            
            self.PWaveMatching = pWaveMatchingData.pWaveMatching;
            
            self.OnsetOffset = pWaveMatchingData.onsetOffset;
            self.ElectrodeOnsetOffset = pWaveMatchingData.electrodeOnsetOffset;
            
            if isfield(pWaveMatchingData, 'electrodeTerminalForce')
                self.ElectrodeTerminalForce = pWaveMatchingData.electrodeTerminalForce;
            else
                self.ElectrodeTerminalForce = NaN(self.EcgData.GetNumberOfChannels(), 1);
            end
            
            self.SetECGMap();
            self.ShowPWaveDurationMap();
        end
        
        function ecgData = LoadData(self, filename)
            pWaveData = load(filename);
            
            [pathString, nameString] = fileparts(filename);
            nameString = nameString(1:(end - 14));
            if ~isempty(pWaveData.ecgData)
                pWaveData.ecgData.Filename = fullfile(pathString, nameString);
            end
            
            self.SetPWaveMatchingData(pWaveData);
            ecgData = self.EcgData;
        end
        
        function SavePWaveMatching(self, varargin)
            % collect on- and offset from axes
            selectedChannels = ~isnan(self.AxesChannelIndices);
            selectedChannelIndices = self.AxesChannelIndices(selectedChannels);
            selectedAxes = self.AxesHandles(selectedChannels);
            for channelIndex = 1:numel(selectedAxes)
                onsetOffset = selectedAxes(channelIndex).OnsetOffset;
                terminalForce = selectedAxes(channelIndex).TerminalForce;
                self.ElectrodeOnsetOffset(selectedChannelIndices(channelIndex), :) = onsetOffset;
                self.ElectrodeTerminalForce(selectedChannelIndices(channelIndex), :) = terminalForce;
            end
            
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_PWaveMatching'];
            onsetOffset = self.OnsetOffset;%#ok<NASGU>
            electrodeOnsetOffset = self.ElectrodeOnsetOffset;%#ok<NASGU>
            electrodeTerminalForce = self.ElectrodeTerminalForce;%#ok<NASGU>
            save(filename,...
                'electrodeOnsetOffset',...
                'electrodeTerminalForce', '-append');
        end
        
        function SetBSPMMap(self, source, varargin)
            mapIndex = get(source, 'value');
            self.Map = PWaveAnalysisPanel.AvailableMaps(mapIndex);
            self.ShowPWaveDurationMap();
        end
        
        function SetECGMap(self)
            if isempty(self.EcgData), return; end
            
            electrodeLabels = self.EcgData.ElectrodeLabels;
            [detectedMap, detectedMapIndex] = PWaveMatcher.DetectECGMap(electrodeLabels);
            
            self.Map = detectedMap;
            set(self.MapDropDown, 'value', detectedMapIndex);
        end
        
    end
end