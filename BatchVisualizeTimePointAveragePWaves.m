%% Settings

% Folder name that contains the _PWaveMatching,mat files
foldername = '/Users/stefzeemering/surfdrive/Projects/AFAB/PWaveMatching';

% Time points to show (set to true (default) or false)
showT1 = true;
showT2 = true;
showT3 = true;

% Leads to show in the visualization
% example: selectedLeads = {'II', 'V1', 'A2'};
selectedLeads = {'II', 'V1', 'A2'};

% Method to align time points
% - onset: align the onset of T2 and T3 to the onset of T1
% - correlation: align to the maximum correlation with T1
% default: alignmentMethod = 'onset';
alignmentMethod = 'correlation';

% if the alignment method = correlation, set the maximum shift
% default: maximumLag = 20;
maximumLag = 20;

%% Visualization code
fileListing = dir(fullfile(foldername, '*_PWaveMatching.mat'));
filenames = {fileListing.name};

% determine the unique patient IDs
filePatientStrings = cellfun(@(x) strsplit(x, '#'), filenames, 'uniformOutput', false);
filePatientID = cellfun(@(x) x{1}, filePatientStrings, 'uniformOutput', false);

patientID = unique(filePatientID);
numberOfPatients = numel(patientID);

T1Files = find(cellfun(@(x) contains(x, 'T1'), filenames));
T2Files = find(cellfun(@(x) contains(x, 'T2'), filenames));
T3Files = find(cellfun(@(x) contains(x, 'T3'), filenames));

timepointColors = lines(3);
numberOfSelectedLeads = numel(selectedLeads);

for patientIndex = 1:numberOfPatients
    t1Index = ismember(filePatientID(T1Files), patientID(patientIndex));
    t2Index = ismember(filePatientID(T2Files), patientID(patientIndex));
    t3Index = ismember(filePatientID(T3Files), patientID(patientIndex));
    
    if any(t1Index)
        pWaveDataT1 = LoadPWaveMatchingData(fullfile(foldername, filenames{T1Files(t1Index)}));
    else
        pWaveDataT1 = [];
    end
    if any(t2Index)
        pWaveDataT2 = LoadPWaveMatchingData(fullfile(foldername, filenames{T2Files(t2Index)}));
    else
        pWaveDataT2 = [];
    end
    if any(t3Index)
        pWaveDataT3 = LoadPWaveMatchingData(fullfile(foldername, filenames{T3Files(t3Index)}));
    else
        pWaveDataT3 = [];
    end
    
    figureHandle = figure('Name', ['Average P-waves - ', patientID{patientIndex}],...
        'position',[200, 200, 300 * numberOfSelectedLeads, 600],...
        'color', [1, 1, 1]);
    
    pWavePlotHandles = gobjects(numberOfSelectedLeads, 1);
    pWaveDifferencesPlotHandles = gobjects(numberOfSelectedLeads, 1);
    for leadIndex = 1:numberOfSelectedLeads
        pWavePlotHandles(leadIndex) = subplot(2, numberOfSelectedLeads, leadIndex);
        
        selectedLead = selectedLeads{leadIndex};
        if showT1 && ~isempty(pWaveDataT1)
            [pWaveT1, onsetOffsetT1, intervalTimeT1] = ExtractPWave(pWaveDataT1, selectedLead);
            line('xData', intervalTimeT1, 'yData', pWaveT1,...
                'displayName', 'T1', 'color', timepointColors(1, :));
        else
            pWaveT1 = [];
        end
        
        if showT2 && ~isempty(pWaveDataT2)
            [pWaveT2, onsetOffsetT2, intervalTimeT2] = ExtractPWave(pWaveDataT2, selectedLead);
        else
            pWaveT2 = [];
        end
        
        if showT3 && ~isempty(pWaveDataT3)
            [pWaveT3, onsetOffsetT3, intervalTimeT3] = ExtractPWave(pWaveDataT3, selectedLead);
        else
            pWaveT3 = [];
        end
        
        axis(pWavePlotHandles(leadIndex), 'tight');
        
%         xlabel('time (ms)');
        legend('show');
        xlimits = get(gca, 'xLim');
        title(selectedLeads{leadIndex});
        if leadIndex == 1
            ylabel('voltage (muV)');
        end
        
        pWaveDifferencesPlotHandles(leadIndex) = subplot(2, numberOfSelectedLeads, numberOfSelectedLeads + leadIndex);
        if showT1 && showT2 && ~(isempty(pWaveT1) || isempty(pWaveT2))
            switch alignmentMethod
                case 'onset'
                    sampleShiftT2 = AlignPWavesOnOnset(onsetOffsetT1, onsetOffsetT2, intervalTimeT2);
                case 'correlation'
                    sampleShiftT2 = AlignPWaves(pWaveT1, onsetOffsetT1, intervalTimeT1,...
                        pWaveT2, onsetOffsetT2, intervalTimeT2, maximumLag);
            end
            intervalT2Step = mean(diff(intervalTimeT2));
            alignedIntervalTimeT2 = intervalTimeT2 + sampleShiftT2 * intervalT2Step;
            
            line('xData', alignedIntervalTimeT2, 'yData', pWaveT2,...
                'displayName', 'T2', 'color', timepointColors(2, :),...
                'parent', pWavePlotHandles(leadIndex));
            
            overlappingTime1 = intervalTimeT1 >= min(alignedIntervalTimeT2) & intervalTimeT1 <= max(alignedIntervalTimeT2);
            overlappingTime2 = alignedIntervalTimeT2 >= min(intervalTimeT1) & alignedIntervalTimeT2 <= max(intervalTimeT1);
            differenceT1T2 = pWaveT2(overlappingTime2) - pWaveT1(overlappingTime1);
            
            line('xData', intervalTimeT1(overlappingTime1), 'yData', differenceT1T2,...
                'displayName', 'T2-T1', 'color', timepointColors(1, :));
        end
        
        if showT1 && showT3 && ~(isempty(pWaveT1) || isempty(pWaveT3))
            switch alignmentMethod
                case 'onset'
                    sampleShiftT3 = AlignPWavesOnOnset(onsetOffsetT1, onsetOffsetT3, intervalTimeT3);
                case 'correlation'
                    sampleShiftT3 = AlignPWaves(pWaveT1, onsetOffsetT1, intervalTimeT1,...
                        pWaveT3, onsetOffsetT3, intervalTimeT3, maximumLag);
            end
            
            intervalT3Step = mean(diff(intervalTimeT3));
            alignedIntervalTimeT3 = intervalTimeT3 + sampleShiftT3 * intervalT3Step;
            
            line('xData', alignedIntervalTimeT3, 'yData', pWaveT3,...
                'displayName', 'T3', 'color', timepointColors(3, :),...
                'parent', pWavePlotHandles(leadIndex));
            
            overlappingTime1 = intervalTimeT1 >= min(alignedIntervalTimeT3) & intervalTimeT1 <= max(alignedIntervalTimeT3);
            overlappingTime3 = alignedIntervalTimeT3 >= min(intervalTimeT1) & alignedIntervalTimeT3 <= max(intervalTimeT1);
            differenceT1T3 = pWaveT3(overlappingTime3) - pWaveT1(overlappingTime1);
            
            line('xData', intervalTimeT1(overlappingTime1), 'yData', differenceT1T3,...
                'displayName', 'T3-T1', 'color', timepointColors(2, :));
        end
        
        if showT1 && showT2 && showT3 && ~(isempty(pWaveT1) || isempty(pWaveT2) || isempty(pWaveT3))
            overlappingTime2 = alignedIntervalTimeT2 >= min(alignedIntervalTimeT3) & alignedIntervalTimeT2 <= max(alignedIntervalTimeT3);
            overlappingTime3 = alignedIntervalTimeT3 >= min(alignedIntervalTimeT2) & alignedIntervalTimeT3 <= max(alignedIntervalTimeT2);
            differenceT2T3 = pWaveT3(overlappingTime3) - pWaveT2(overlappingTime2);
            
            line('xData', alignedIntervalTimeT2(overlappingTime2), 'yData', differenceT2T3,...
                'displayName', 'T3-T2', 'color', timepointColors(3, :));
        end
        
        if (~showT1 || isempty(pWaveT1)) && (showT2 && showT3) && ~(isempty(pWaveT2) || isempty(pWaveT3))
            switch alignmentMethod
                case 'onset'
                    sampleShiftT3 = AlignPWavesOnOnset(onsetOffsetT2, onsetOffsetT3, intervalTimeT3);
                case 'correlation'
                    sampleShiftT3 = AlignPWaves(pWaveT2, onsetOffsetT2, intervalTimeT2,...
                        pWaveT3, onsetOffsetT3, intervalTimeT3, maximumLag);
            end
            
            intervalT3Step = mean(diff(intervalTimeT3));
            alignedIntervalTimeT3 = intervalTimeT3 + sampleShiftT3 * intervalT3Step;
            
            line('xData', alignedIntervalTimeT3, 'yData', pWaveT3,...
                'displayName', 'T3', 'color', timepointColors(3, :));
            
            overlappingTime2 = intervalTimeT2 >= min(alignedIntervalTimeT3) & intervalTimeT2 <= max(alignedIntervalTimeT3);
            overlappingTime3 = alignedIntervalTimeT3 >= min(intervalTimeT2) & alignedIntervalTimeT3 <= max(intervalTimeT2);
            differenceT1T3 = pWaveT3(overlappingTime3) - pWaveT2(overlappingTime2);
            
            line('xData', intervalTimeT2(overlappingTime2), 'yData', differenceT1T3,...
                'displayName', 'T3-T2', 'color', timepointColors(3, :),...
                'parent', pWavePlotHandles(leadIndex));
        end
        
        axis(pWaveDifferencesPlotHandles(leadIndex), 'tight');
        xlabel('time (ms)');
        xlim(xlimits);
        legend('show');
        title('Differences');
        if leadIndex == 1
            ylabel('voltage (muV)');
        end
    end
    
    linkaxes(pWavePlotHandles, 'y');
    linkaxes(pWaveDifferencesPlotHandles, 'y');
    
%     figureHandle.PaperPositionMode = 'auto';
%     figurePosition = figureHandle.PaperPosition;
%     figureHandle.PaperSize = [figurePosition(3) figurePosition(4)];
    
    figureFilename = [patientID{patientIndex}, '_TimePointDifferences'];
    
    savefig(figureHandle, fullfile(foldername, figureFilename), 'compact');
    print(figureHandle, fullfile(foldername, figureFilename), '-dpng');
    
    delete(figureHandle);
end

function pWaveData = LoadPWaveMatchingData(filename)
data = load(filename);
pWaveMatcher = data.pWaveMatching;
pWaveData = pWaveMatcher.GetPWaveData(data.ecgData, data.baselineCorrectedData,...
    data.pWaveIntervals, data.onsetOffset, data.electrodeOnsetOffset, data.validChannels,...
    data.electrodeTerminalForce);
end

function [pWave, onsetOffset, intervalTime] = ExtractPWave(pWaveData, leadLabel)
electrodeIndex = ismember(pWaveData.ElectrodeLabels, leadLabel);
if electrodeIndex == 0
    pWave = [];
    onsetOffset = [];
    intervalTime = [];
    return;
end
onsetOffset = pWaveData.ElectrodeOnsetOffset;
onsetOffset(isnan(onsetOffset(:, 1)), 1) = pWaveData.OnsetOffset(1);
onsetOffset(isnan(onsetOffset(:, 2)), 2) = pWaveData.OnsetOffset(2);
intervalTime = pWaveData.AveragingInterval;
pWave = pWaveData.AveragePWaves(:, electrodeIndex);
onsetOffset = onsetOffset(electrodeIndex, :);
end

function sampleShift = AlignPWaves(extendedPWave1, onsetOffset1, intervalTime1,...
        extendedPWave2, onsetOffset2, intervalTime2, maximumLag)
    
    onSetSampleShift = AlignPWavesOnOnset(onsetOffset1, onsetOffset2, intervalTime2);
        
    startIndex1 = find(intervalTime1 >= onsetOffset1(1), 1, 'first');
    endIndex1 = find(intervalTime1 <= onsetOffset1(2), 1, 'last');
    pWave1 = extendedPWave1(startIndex1:endIndex1);
    
    startIndex2 = find(intervalTime2 >= onsetOffset2(1), 1, 'first');
    endIndex2 = find(intervalTime2 <= onsetOffset2(2), 1, 'last');
    pWave2 = extendedPWave2(startIndex2:endIndex2);
    
    [crossCorrelation, lags] = xcorr(pWave1, pWave2, maximumLag);
    [~, maxLagIndex] = max(crossCorrelation);
    
    sampleShift = lags(maxLagIndex) + onSetSampleShift;
end

function sampleShift = AlignPWavesOnOnset(onsetOffset1, onsetOffset2, intervalTime2)
orginalStartIndex2 = find(intervalTime2 >= onsetOffset2(1), 1, 'first');
shiftedStartIndex2 = find(intervalTime2 >= onsetOffset1(1), 1, 'first');
sampleShift = shiftedStartIndex2 - orginalStartIndex2;
end