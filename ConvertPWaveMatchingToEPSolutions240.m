% input filename
% filename = '/Users/stefzeemering/Documents/Projects/AFAB/BSPM/Boumans data/export-Boumans, Suzanne, 10220, pre-ablation1_PWaveMatching.mat';
% originalFilename = '/Users/stefzeemering/Documents/Projects/AFAB/BSPM/Boumans data/export-Boumans, Suzanne, 10220, pre-ablation1.240';

filename = '/Users/stefzeemering/Documents/Projects/AFAB/BSPM/10229/export-10229, Hautvast, Pierre, pre-ablation1_PWaveMatching.mat';
originalFilename = '/Users/stefzeemering/Documents/Projects/AFAB/BSPM/10229/export-10229, Hautvast, Pierre, pre-ablation1.240';

% copy header from original recording
fileID = fopen(originalFilename);
header = fread(fileID, 256, 'char');

% default channel labels
channelLabels = horzcat({'I', 'II', 'III', 'aVR', 'aVL', 'aVF',...
    'V1', 'V2', 'V3', 'V4', 'V5', 'V6',...
    'STD_ECG1', 'STD_ECG2', 'STD_ECG3', 'STD_ECG4'}, strcat('BSPM', cellstr(num2str((1:240)'))'));
fileIdentifier = '_PWaveMatching.mat';

% compute p-wave data
data = load(filename);
pWaveMatcher = data.pWaveMatching;

% extend intervals to cover (roughly) one beat
medianRRInterval = median(diff(data.rWaveIndices));
pWaveIntervalLength = mean(data.pWaveIntervals(:, 2) - data.pWaveIntervals(:, 1));
pWaveIntervalExtension = medianRRInterval - pWaveIntervalLength;
extendedIntervals = [...
    data.pWaveIntervals(:, 1)  -  round(0.2 * pWaveIntervalExtension),...
    data.pWaveIntervals(:, 2)  +  round(0.8 * pWaveIntervalExtension)];
validExtendedIntervals = extendedIntervals(:, 1) > 0 & extendedIntervals(:, 2) <= data.ecgData.GetNumberOfSamples;
extendedIntervals = extendedIntervals(validExtendedIntervals, :);

pWaveData = pWaveMatcher.GetPWaveData(data.ecgData, data.baselineCorrectedData,...
    extendedIntervals, data.onsetOffset, data.electrodeOnsetOffset, data.validChannels,...
    data.electrodeTerminalForce);

averagePWaves = pWaveData.AveragePWaves;
averagePWaveLabels = pWaveData.ElectrodeLabels;
numberOfPWaveSamples = size(averagePWaves, 1);
[labelsFound, labelPosition] = ismember(averagePWaveLabels, channelLabels);

exportedPWaves = NaN(numel(channelLabels), numberOfPWaveSamples);
exportedPWaves(labelPosition(labelsFound), :) = averagePWaves(:, labelsFound)';

epSolutionsFilename = strrep(filename, fileIdentifier, '_AveragedPWave.240');
Save240File(epSolutionsFilename, exportedPWaves, header);


function Save240File(filename, channelData, header)
if size(channelData, 1)~=256
    fprintf('Number of rows in the ECG matrix must be 256')
    return
end

% if no header is provided
if nargin < 3
    header = zeros(256, 1);
end

% cut header if its length is more than 256 characters
if length(header) > 256
    header = header(1:256);
end

scale = 1e+3; % convert millivolts to microvolts

fileID = fopen(filename, 'w');
fwrite(fileID, header, 'uchar'); % write header
ecg2write = reshape(channelData, 1, []) * scale;
fwrite(fileID, ecg2write, 'int16');

fclose(fileID);
end