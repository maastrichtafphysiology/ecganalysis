classdef TEECGAnalysis < UserInterfacePkg.CustomFigure
    properties (Access = private)
        EcgData
        
        TabGroup
        QrstCancellationTab
        TEDeflectionTab
        ParameterOverviewTab
        
        ChannelSelectionView
        PreProcessingView
        TEPreProcessingView
        QRSTCancellationView
        QCView
        DFView
        FWaveView
        TEDissociationView
        OverView
        
        ApplicationSettings
        ParallelProcessing
        
        ZoomToggleButton
        PanToggleButton
        
        AnalysisStatusBar
        
        MappedFileTypeIndices
        
        EcgPreprocessingComplete
        TEEcgPreprocessingComplete
    end
    
    properties (Constant)
        FILETYPEFILTER = {...
            '*.DATA', 'YourRhythmics DATA (*.DATA)';...
            '*.txt', 'YourRhythmics data (*.txt)';...            
            '*.bdf', 'BioSemi TE-ECG Measurement (*.bdf)';};
    end
    
    methods
        function self = TEECGAnalysis(position)
            if nargin < 1
                screenSize = get(0,'Screensize');
                position = [screenSize(3) / 8, screenSize(4) / 8, 6 * screenSize(3) / 8, 6 * screenSize(4) / 8];
            end
            
            self = self@UserInterfacePkg.CustomFigure(position);
            self.Name = 'TE-ECG Analysis';
            self.ParallelProcessing = false;
            self.MappedFileTypeIndices = 1:size(TEECGAnalysis.FILETYPEFILTER, 1);
            self.EcgPreprocessingComplete = false;
            self.TEEcgPreprocessingComplete = false;
            
            durations = 10:10:50;
            applicationSettings = ApplicationSettings.Instance();
            applicationSettings.TemplateMatching.Durations = durations;
            
            if nargin < 1
                self.Create();
            end
        end
        
        function Create(self)
            Create@UserInterfacePkg.CustomFigure(self);
            
            set(self.ControlHandle,...
                'CloseRequestFcn', @self.Close);
            
            set(self.ControlHandle,...
                'colormap', flipud(gray(256)));
            
            self.Hide();
            
            self.InitializeApplicationSettings();
            
            self.CreateFileMenu();
            
            self.CreateSettingsMenu();
            
            self.CreateToolbar();
            
            self.CreateStatusBar();
            
            self.InitializeEvents();
            
            self.InitializeApplicationSettings();
            
            self.Show();
        end
        
        function delete(self)
        end
    end
    
    methods (Access = private)
        function InitializeTabs(self)
            self.RemoveTabs();
            if ishandle(self.TabGroup)
                delete(self.TabGroup);
            end
            
            self.InitializeECGTabs();
        end
        
        function ShowEmptyECGTabs(self, varargin)
            self.EcgData = ECGData('', [], NaN);
            self.InitializeTabs();
        end
        
        function InitializeECGTabs(self)
            self.TabGroup = uitabgroup(...
                'parent', self.ControlHandle,...
                'position', [0 .025 1 .975]);
            
            channelSelectionTab = uitab(self.TabGroup, 'title','Data');
%             self.ChannelSelectionView = TEECGVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView = TEECGPlotVisualizationPanel([0 0 1 1]);
            self.ChannelSelectionView.Create(channelSelectionTab);
            self.ChannelSelectionView.Show();
            
            addlistener(self.ChannelSelectionView, 'ValidChannelsSelectionCompleted',...
                @self.HandleChannelSelectionEvent);
            
            preProcessingTab = uitab(self.TabGroup, 'title','ECG Pre-processing');
            self.PreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.PreProcessingView.Create(preProcessingTab);
            self.PreProcessingView.Show();
            
            addlistener(self.PreProcessingView, 'PreProcessingCompleted',...
                @self.HandlePreProcessingEvent);
            
            tePreProcessingTab = uitab(self.TabGroup, 'title','TE-ECG Pre-processing');
            self.TEPreProcessingView = PreProcessingPanel([0 0 1 1]);
            self.TEPreProcessingView.Create(tePreProcessingTab);
            self.TEPreProcessingView.Show();
            
            addlistener(self.TEPreProcessingView, 'PreProcessingCompleted',...
                @self.HandleTEPreProcessingEvent);
            
            self.QrstCancellationTab = uitab(self.TabGroup, 'title','QRST Cancellation');
            self.QRSTCancellationView = ECGQRSTCancellationPanel([0 0 1 1]);
            self.QRSTCancellationView.Create(self.QrstCancellationTab);
            self.QRSTCancellationView.Show();
            
            addlistener(self.QRSTCancellationView, 'QRSTCancellationCompleted',...
                @self.HandleQRSTCancellationEvent);
            
            qcTab = uitab(self.TabGroup, 'title','Quality check');
            self.QCView = ECGQualityCheckPanel([0 0 1 1]);
            self.QCView.Create(qcTab);
            self.QCView.Show();
            addlistener(self.QCView, 'QualityCheckCompleted',...
                @self.HandleQualityCheckCompletedEvent);
            
            dfTab = uitab(self.TabGroup, 'title','Dominant Frequency');
            self.DFView = TEECGDFPanel([0 0 1 1]);
            self.DFView.Create(dfTab);
            self.DFView.Show();
            addlistener(self.DFView, 'AnalysisSaved',...
                @self.HandleDFSaveEvent);
            
            fWaveTab = uitab(self.TabGroup, 'title','F-waves');
            self.FWaveView = FWavePanel([0 0 1 1]);
            self.FWaveView.Create(fWaveTab);
            self.FWaveView.Show();
            addlistener(self.FWaveView, 'AnalysisSaved',...
                @self.HandleFWASaveEvent);
            
%             self.TEDeflectionTab = uitab(self.TabGroup, 'title', 'TE-ECG Dissociation');
%             self.TEDissociationView = TEECGDissociationPanel([0 0 1 1]);
%             self.TEDissociationView.Create(self.TEDeflectionTab);
%             self.TEDissociationView.Show();
            
            self.ParameterOverviewTab = uitab(self.TabGroup, 'title', 'Overview');
            self.OverView = ECGComplexityOverviewPanel([0 0 1 1]);
            self.OverView.Create(self.ParameterOverviewTab);
            self.OverView.Show();
        end
        
        function RemoveTabs(self)
            if isobject(self.ChannelSelectionView)
                delete(self.ChannelSelectionView);
            end
            
            if isobject(self.PreProcessingView)
                delete(self.PreProcessingView);
            end
            
            if isobject(self.QRSTCancellationView)
                delete(self.QRSTCancellationView);
            end
            
            if isobject(self.QCView)
                delete(self.QCView);
            end
            
            if isobject(self.DFView)
                delete(self.DFView);
            end
            
            if isobject(self.FWaveView)
                delete(self.FWaveView);
            end
            
            if isobject(self.TEDissociationView)
                delete(self.TEDissociationView);
            end
            
            if isobject(self.OverView)
                delete(self.OverView);
            end
        end
        
        function CreateFileMenu(self)
            fileMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'File');
            uimenu('parent', fileMenu,...
                'label', 'Load signal',...
                'callback', @self.LoadSignal);
            uimenu('parent', fileMenu,...
                'label', 'Load result file',...
                'callback', @self.LoadResult);
        end
        
        function CreateSettingsMenu(self)
            settingsMenu = uimenu(...
                'parent', self.ControlHandle,...
                'label', 'Settings');
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Algorithm settings',...
                'callback', @self.ShowApplicationSettings);
            uimenu(...
                'parent', settingsMenu,...
                'label', 'Parallel processing',...
                'callback', @self.SetParallelProcessing);
        end
        
        function CreateToolbar(self)
            toolbar = uitoolbar(...
                'parent', self.ControlHandle);
            toolbarIconData = load('toolbarIcons.mat');
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.opendoc,...
                'tooltipString', 'Load signal',...
                'clickedCallback', @self.LoadSignal);
            self.ZoomToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.zoomCData,...
                'offCallback', 'zoom off',...
                'onCallback', @self.ZoomOn,...
                'separator', 'on');
            self.PanToggleButton = uitoggletool(...
                'parent', toolbar,...
                'CData', toolbarIconData.panCData,...
                'offCallback', 'pan off',...
                'onCallback', @self.PanOn);
            uipushtool(...
                'parent', toolbar,...
                'CData', toolbarIconData.newDataCData,...
                'tooltipString', 'Show empty ECG tabs',...
                'clickedCallback', @self.ShowEmptyECGTabs);
        end
        
        function CreateStatusBar(self)
            self.AnalysisStatusBar = UserInterfacePkg.StatusBar([0 0 1 .025]);
            self.AddCustomControl(self.AnalysisStatusBar);
            self.AnalysisStatusBar.AddListener(UserInterfacePkg.StatusEventClass.Instance(), 'StatusChange');
            self.AnalysisStatusBar.Show();
        end
        
        function SetFilename(self)
            if isempty(self.EcgData), return; end
            
            self.Name = ['TE-ECG Analysis - ', self.EcgData.Filename];
        end
        
        function InitializeEvents(self)
            set(self.ControlHandle, 'KeyPressFcn', @self.NotifyKeyPress);
            set(self.ControlHandle, 'KeyReleaseFcn', @self.NotifyKeyRelease);
            
            set(self.ControlHandle, 'WindowButtonMotionFcn', @self.NotifyMouseMotion)
            set(self.ControlHandle, 'WindowButtonUpFcn', @self.NotifyMouseUp)
            set(self.ControlHandle, 'WindowButtonDownFcn', @self.NotifyMouseDown)
        end
        
        function NotifyKeyPress(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyPressed(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyKeyRelease(self, ~, eventData)
            UserInterfacePkg.KeyEventClass.Instance().NotifyKeyReleased(UserInterfacePkg.KeyPressedEventData(eventData));
        end
        
        function NotifyMouseMotion(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseMotion();
        end
        
        function NotifyMouseUp(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseUp();
        end
        
        function NotifyMouseDown(self, varargin)
            UserInterfacePkg.MouseEventClass.Instance().NotifyMouseDown();
        end
        
        function LoadSignal(self, varargin)
   
            fileTypeFilters = TEECGAnalysis.FILETYPEFILTER;
            mappedFiletypeFilters = fileTypeFilters(self.MappedFileTypeIndices, :);
            
            [fileName, pathName, filterIndex] = HelperFunctions.customUigetfile(...
                mappedFiletypeFilters,...
                'Select a signal file');
            if isequal(fileName, 0), return; end
            
            mappedFilterIndex = self.MappedFileTypeIndices(filterIndex);
            if filterIndex > 1
                fileTypeIndices = 1:size(AFECGAnalysis.FILETYPEFILTER, 1);
                fileTypeIndices(mappedFilterIndex) = [];
                self.MappedFileTypeIndices = [mappedFilterIndex, fileTypeIndices];
            end
                
            switch mappedFilterIndex
                case 1
                    ecgData = InputOutputPkg.ECGReader.ReadYourRhythmicsData(fullfile(pathName, fileName));
                case 2
                    ecgData = InputOutputPkg.ECGReader.ReadYRTXT(fullfile(pathName, fileName));
                case 3
                    ecgData = InputOutputPkg.ECGReader.ReadBDF(fullfile(pathName, fileName));
                otherwise
                    errordlg('File Error', 'Unkown file type');
                    return;
            end
                                    
            teEcgData = self.ConvertEcgDataToTEEcgData(ecgData);
            if isempty(teEcgData), return; end
            
            self.EcgData = teEcgData;
            self.SetFilename();
            self.InitializeTabs();
            
            self.EcgPreprocessingComplete = false;
            self.TEEcgPreprocessingComplete = false;
            
            self.ChannelSelectionView.SetECGData(self.EcgData);
            
%             switch mappedFilterIndex
%                 case {1, 2, 3}
%                     self.AutomaticUntilQRSTCancellation();
%                 otherwise
%                     return;
%             end
        end
        
        function AutomaticUntilQRSTCancellation(self)
            bandpassFrequencies = [0.5, 100];
            self.PreProcessingView.SetECGData(self.EcgData);
            preProcessedData = self.PreProcessingView.PreProcessData(bandpassFrequencies);
            self.QRSTCancellationView.SetECGData(preProcessedData);
            
            qrstDetectionElectrodeIndex = 7;
            qrstCancellator = ECGQRSTCancellator();
            qrstCancellator.OriginalPanTompkins = false;
            qrstCancellator.VentricularRefractoryPeriod = 0.3;
            qrstCancellator.IntegrationWindowLength = 0.2;
            qrstCancellator.SelectProximalPeak = true;
            qrstCancellator.SelectedPeakSign = 0;
            qrstCancellator.ShowDiagnostics = false;
            self.QRSTCancellationView.DetectRWavesOnLead(qrstDetectionElectrodeIndex,...
                qrstCancellator, 1:preProcessedData.GetNumberOfChannels());
            
            set(self.TabGroup, 'selectedTab', self.QrstCancellationTab);
        end
        
        function LoadResult(self, varargin)
            [fileName, pathName] = HelperFunctions.customUigetfile(...
                '*_Result.mat', 'Analysis result file (*_Result.mat)',...
                'Select a result file');
            if isequal(fileName, 0), return; end
            
            resultData = load(fullfile(pathName, fileName));
            resultData.ecgData.Filename = fullfile(pathName, fileName(1:(end - 11)));
            
            self.ShowEmptyECGTabs();
            
            fullFilename = fullfile(pathName, fileName);
            
            if isobject(self.OverView)
                self.OverView.SetECGData(resultData.ecgData);
                if isfield(resultData, 'ChannelsToAnalyze')
                    self.OverView.SetChannelsToAnalyze(resultData.ChannelsToAnalyze);
                end
            end
            
            if isfield(resultData, 'excludedIntervals')
                self.QCView.LoadAnalysis(fullFilename);
                self.HandleQualityCheckCompletedEvent();
            else
                self.QCView.SetECGData(resultData.ecgData);
                self.QCView.RWaveIndices = resultData.rWaveIndices;
            end
            
            if isfield(resultData, 'DF')
                self.DFView.LoadAnalysis(fullFilename);
                self.HandleDFSaveEvent();
            else
                self.DFView.SetECGData(resultData.ecgData, resultData.rWaveIndices);
            end
            
            if isfield(resultData, 'FWave')
                self.FWaveView.LoadAnalysis(fullFilename);
                self.HandleFWASaveEvent();
            else
                self.FWaveView.SetECGData(resultData.ecgData, resultData.rWaveIndices);
            end
            
            self.TabGroup.SelectedTab = self.ParameterOverviewTab;
        end
        
        function InitializeApplicationSettings(self)
            self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
            self.ApplicationSettings.Create();
            self.ApplicationSettings.Hide();
        end
        
        function ShowApplicationSettings(self, varargin)
            if ~isobject(self.ApplicationSettings)
                self.ApplicationSettings = ApplicationSettingsFigure(self.Position);
                self.ApplicationSettings.Create();
            end
            self.ApplicationSettings.Show();
        end
        
        function SetParallelProcessing(self, source, varargin)
            if self.ParallelProcessing
                self.ParallelProcessing = false;
                set(source, 'checked', 'off');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') > 0
                        matlabpool('close');
                    end
                else
                    delete(gcp('nocreate'));
                end
            else
                self.ParallelProcessing = true;
                set(source, 'checked', 'on');
                
                if verLessThan('matlab', '7.0.1')
                    if matlabpool('size') == 0
                        matlabpool('open');
                    end
                else
                    if isempty(gcp('nocreate'))
                        parpool;
                    end
                end
            end
        end
        
        function PanOn(self, varargin)
            set(self.ZoomToggleButton, 'state', 'off');
            pan on;
        end
        
        function ZoomOn(self, varargin)
            set(self.PanToggleButton, 'state', 'off');
            zoom on;
        end
        
        function HandleChannelSelectionEvent(self, varargin)
            disp('Channels selected');
            
            ecgData = self.ChannelSelectionView.GetECGDataToAnalyze();
            
            teEcgData = ecgData.Copy();
            teEcgData.Data = teEcgData.TEData;
            teEcgData.ElectrodeLabels = teEcgData.TELabels;
            
            teEcgData.TEData = [];
            teEcgData.TELabels = [];
            
            self.PreProcessingView.SetECGData(ecgData);
            self.TEPreProcessingView.SetECGData(teEcgData);
        end
        
        function HandlePreProcessingEvent(self, varargin)
            disp('ECG Pre-processing completed');
            
            self.EcgPreprocessingComplete = true;
            
            self.HandlePreprocessingCompleted();
        end
        
        function HandleTEPreProcessingEvent(self, varargin)
            disp('TE-ECG Pre-processing completed');
            
            self.TEEcgPreprocessingComplete = true;
            
            self.HandlePreprocessingCompleted();
         end
         
        function HandlePreprocessingCompleted(self)
             if self.EcgPreprocessingComplete && self.TEEcgPreprocessingComplete
                ecgData = self.PreProcessingView.GetPreProcessedECGData();
                TEEcgData = self.TEPreProcessingView.GetPreProcessedECGData();
                
                combinedEGGData = ecgData.Copy();
                combinedEGGData.Data = [ecgData.Data, TEEcgData.Data];
                combinedEGGData.ElectrodeLabels = [ecgData.ElectrodeLabels; TEEcgData.ElectrodeLabels];
                
                self.QRSTCancellationView.SetECGData(combinedEGGData);
            end
         end
        
        function HandleQRSTCancellationEvent(self, varargin)
            disp('QRST cancellation completed');
            
            rWaveIndices = self.QRSTCancellationView.RWaveIndices;
            ecgData = self.QRSTCancellationView.GetCancelledECGData();
%             ecgData = self.ConvertEcgDataToTEEcgData(ecgData);

            if isobject(self.QCView)
                self.QCView.SetECGData(ecgData);
                self.QCView.RWaveIndices = rWaveIndices;
            end
            
            if isobject(self.DFView)
                self.DFView.SetECGData(ecgData, rWaveIndices);
            end
            
            if isobject(self.FWaveView)
                self.FWaveView.SetECGData(ecgData, rWaveIndices);
            end
            
            if isobject(self.TEDissociationView)
                teEcgData = self.ConvertEcgDataToTEEcgData(ecgData);
                teEcgData.Data = teEcgData.TEData;
                teEcgData.ElectrodeLabels = teEcgData.TELabels;
                teEcgData.TEData = [];
                teEcgData.TELabels = [];
                self.TEDissociationView.SetECGData(teEcgData);
            end 
            
            if isobject(self.OverView)
                self.OverView.SetECGData(ecgData);
            end
        end
        
        function HandleQualityCheckCompletedEvent(self, varargin)
            disp('Quality check saved');
            
            excludedIntervals = self.QCView.GetExcludedIntervals();
            if isobject(self.OverView)
                self.OverView.SetQCData(excludedIntervals);
            end
        end
        
        function HandleDFSaveEvent(self, varargin)
            disp('DF analysis saved');
            
            dfData = self.DFView.GetAnalysisData();           
            if isobject(self.OverView)
                self.OverView.SetDFData(dfData);
            end
        end
        
        function HandleFWASaveEvent(self, varargin)
            disp('FWA analysis saved');
            
            fwaData = self.FWaveView.GetAnalysisData();
            if isobject(self.OverView)
                self.OverView.SetFWAData(fwaData);
            end
        end
        
        function Close(self, varargin)
            self.RemoveTabs();
            
            if isobject(self.EcgData)
                delete(self.EcgData)
            end
            
            delete(timerfindall);
            
            delete(self.AnalysisStatusBar);
            
            delete(self.ControlHandle);
        end
             
        function teEcgData = ConvertEcgDataToTEEcgData(self, ecgData)
            teECGLeads = contains(ecgData.ElectrodeLabels, 'UniP');
            if any(teECGLeads)
                ecgLeadData = ecgData.Data(:, ~teECGLeads);
                ecgLeadLabels = ecgData.ElectrodeLabels(~teECGLeads);
                teData = ecgData.Data(:, teECGLeads);
                teLabels = ecgData.ElectrodeLabels(teECGLeads);
            else
                errordlg('Format Error', 'No TE-ECG data');
                teEcgData = [];
                return;
            end
            
            teEcgData = TEECGData(ecgData.Filename, ecgLeadData, ecgData.SamplingFrequency,...
                            ecgLeadLabels);
            teEcgData.SetTEData(teData, teLabels);
        end
    end
end