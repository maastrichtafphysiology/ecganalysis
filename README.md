# README #

This README documents how to setup and run the ECG Analysis software

### What is this repository for? ###

* This repository contains the algorithms and user interface developed in MATLAB to import and analyze ECG data
* Version: 1.0

### How do I get set up? ###

* Dependencies: CustomMatlabPackages and EGMAnalysis (*add to MATLAB path*)
* Set current directory to ECGAnalysis folder
* Run: *AFECGAnalysis*,*PWaveAnalysis* or *TEECGAnalysis*

### Questions / contact ###

* [Stef Zeemering](mailto:s.zeemering@maastrichtuniversity.nl)
