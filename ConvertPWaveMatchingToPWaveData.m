% input folder name
foldername = '/Users/matthiaszink/Desktop/Maastricht/AFAB _ Maastricht/ECG Analysis export 171013';

fileIdentifier = 'PWaveMatching.mat';
fileListing = dir(fullfile(foldername, ['*', fileIdentifier]));
filenames = {fileListing.name};

% compute p-wave data
numberOfFiles = numel(filenames);
for fileIndex = 1:numberOfFiles
    data = load(fullfile(foldername, filename));
    pWaveMatcher = data.pWaveMatching;
    pWaveData = pWaveMatcher.GetPWaveData(data.ecgData, data.baselineCorrectedData,...
        data.pWaveIntervals, data.onsetOffset, data.electrodeOnsetOffset, data.validChannels,...
        data.electrodeTerminalForce);
    pWaveFilename = strrep(filename, fileIdentifier, '_PWaveData.mat');
    save(fullfile(foldername, pWaveFilename), 'pWaveData');
end
