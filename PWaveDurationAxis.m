classdef PWaveDurationAxis < UserInterfacePkg.CustomAxes
    properties
        OnsetOffset
        TerminalForce
    end
    
    properties (Access = private)
        MouseMotionListener
        KeyPressedListener
        
        PositionLine
        OnsetLine
        OffsetLine
        TerminalForceLine
    end
    
    methods
        function self = PWaveDurationAxis(position, onsetOffset, terminalForce)
            if nargin < 3
                terminalForce = NaN;
                if nargin < 2
                    onsetOffset = [NaN, NaN];
                    if nargin < 1
                        position = [0,0,0,0];
                    end
                end
            end
            self = self@UserInterfacePkg.CustomAxes(position);
            self.OnsetOffset = onsetOffset;
            self.TerminalForce = terminalForce;
        end
        
        function Create(self, parentHandle)
            Create@UserInterfacePkg.CustomAxes(self, parentHandle);
            
            set(self.ControlHandle, 'gridColor', [1,0,0]);
            
            self.InitializeLines();
            self.CreateListeners();
        end
        
        function Draw(self, pWaveSignal, pWaveTime)
            line('xData', pWaveTime, 'yData', zeros(size(pWaveTime)),...
                'color', [.7,.7,.7], 'parent', self.ControlHandle);
            pWaveLine = line('xData', pWaveTime, 'yData', pWaveSignal,...
                'color', [0,0,0], 'parent', self.ControlHandle,...
                'lineWidth', 2, 'lineSmoothing', 'on');
            
            yLimits = ylim(self.ControlHandle);
            
            set(self.OnsetLine,...
                'xData', [self.OnsetOffset(1), self.OnsetOffset(1)],...
                'yData', yLimits);
            set(self.OffsetLine,...
                'xData', [self.OnsetOffset(2), self.OnsetOffset(2)],...
                'yData', yLimits);
            set(self.TerminalForceLine,...
                'xData', [self.TerminalForce, self.TerminalForce],...
                'yData', yLimits);
        end
        
        function delete(self)
            delete@UserInterfacePkg.CustomAxes(self);
            delete(self.MouseMotionListener);
            delete(self.KeyPressedListener);
        end
    end
    
    methods (Access = private)
        function InitializeLines(self)
            self.PositionLine = line('xData', [],...
                'yData', [],...
                'color', [.7 .7 .7],...
                'lineStyle', '--',...
                'parent', self.ControlHandle,...
                'hitTest', 'off');
            
            self.OnsetLine =...
                line('xData', [],...
                'yData', [],...
                'color', [0 0 1],...
                'lineStyle', '--',...
                'lineWidth', 1,...
                'parent', self.ControlHandle,...
                'buttonDownFcn', @self.OnsetLineCallback);
            
             self.OffsetLine =...
                line('xData', [],...
                'yData', [],...
                'color', [1 0 0],...
                'lineStyle', '--',...
                'lineWidth', 1,...
                'parent', self.ControlHandle,...
                'buttonDownFcn', @self.OffsetLineCallback);
            
            self.TerminalForceLine = line('xData', [],...
                'yData', [],...
                'color', [0 1 0],...
                'lineStyle', '--',...
                'lineWidth', 1,...
                'parent', self.ControlHandle,...
                'buttonDownFcn', @self.TerminalForceLineCallback);
        end
        
        function CreateListeners(self)
            addlistener(self.ControlHandle, 'YLim', 'PostSet', @self.UpdateYLimits);
            
            self.MouseMotionListener = event.listener(UserInterfacePkg.MouseEventClass.Instance(),...
                'MouseMotion', @self.MouseMotionCallback);
            
            self.KeyPressedListener = event.listener(UserInterfacePkg.KeyEventClass.Instance(),...
                'KeyPressed', @self.HandleKeyPressedEvent);
        end
        
        function MouseMotionCallback(self, varargin)
%             persistent previousPointerShape;
            currentPoint = get(self.ControlHandle, 'currentPoint');
            if self.MouseInAxes(currentPoint)
                yLimits = ylim(self.ControlHandle);
                set(self.PositionLine, 'xData', [currentPoint(1, 1), currentPoint(1, 1)],...
                'yData', yLimits, 'visible', 'on');
            else
                set(self.PositionLine, 'visible', 'off');
            end
        end
        
        function HandleKeyPressedEvent(self, ~, eventData)
                currentPoint = get(self.ControlHandle, 'currentPoint');
                    if ~self.MouseInAxes(currentPoint), return; end
            switch eventData.Key
                case 'b'
                    % insert/change onset
                    self.OnsetOffset(1) = currentPoint(1, 1);
                    set(self.OnsetLine, 'xData', [currentPoint(1, 1), currentPoint(1, 1)]);
                case 'e'
                    % insert/change offset
                    self.OnsetOffset(2) = currentPoint(1, 1);
                    set(self.OffsetLine, 'xData', [currentPoint(1, 1), currentPoint(1, 1)]);
                case 't'
                    % insert/change terminal force point
                    self.TerminalForce = currentPoint(1, 1);
                    set(self.TerminalForceLine, 'xData', [currentPoint(1, 1), currentPoint(1, 1)]);
                case 'c'
                    % clear all lines
                    self.OnsetOffset = [NaN, NaN];
                    self.TerminalForce = NaN;
                    set(self.OnsetLine, 'xData', [NaN, NaN]);
                    set(self.OffsetLine, 'xData', [NaN, NaN]);
                    set(self.TerminalForceLine, 'xData', [NaN, NaN]);
                case {'rightarrow', 'leftarrow'}
                    switch eventData.Key
                        case 'rightarrow'
                            
                        case 'leftarrow'
                    end
            end
        end
        
        function OnsetLineCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
            end
        end
        
        function OffsetLineCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
            end
        end
        
        function TerminalForceLineCallback(~, source, varargin)
            lineWidth = get(source, 'lineWidth');
            if lineWidth == 1
                set(source, 'lineWidth', 2);
                uistack(source, 'top');
            else
                set(source, 'lineWidth', 1);
            end
        end
        
        function UpdateYLimits(self, varargin)
            yLimits = ylim(self.ControlHandle);
            set(self.OnsetLine, 'yData', yLimits);
            set(self.OffsetLine, 'yData', yLimits);
            set(self.TerminalForceLine, 'yData', yLimits);
        end
        
        function inAxes = MouseInAxes(self, currentPoint)
            inAxes = true;
            xLimits = xlim(self.ControlHandle);
            yLimits = ylim(self.ControlHandle);
            if (currentPoint(1, 1) < xLimits(1) || currentPoint(1, 1) > xLimits(2)) ||...
                    (currentPoint(1, 2) < yLimits(1) || currentPoint(1, 2) > yLimits(2))
                inAxes = false;
            end
        end
    end
end