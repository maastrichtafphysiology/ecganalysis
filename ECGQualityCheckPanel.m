classdef ECGQualityCheckPanel < ECGPlotChannelPanel
    properties
        ExcludedIntervalsTable
        RWaveIndices
    end
    
    properties (Access = protected)
        QCPanel
        ExcludedIntervalPatches
        SelectedTableRow
        ApplyQCButton
    end
    
    events
        QualityCheckCompleted
    end
    
    methods
        function self = ECGQualityCheckPanel(position)
            self = self@ECGPlotChannelPanel(position);
        end
        
        function SetECGData(self, ecgData)            
            SetECGData@ECGPlotChannelPanel(self, ecgData);
        end
        
        function excludedIntervals = GetExcludedIntervals(self)
            excludedIntervals = self.ExcludedIntervalsTable.Data;
        end
        
                function LoadAnalysis(self, filename)
            ecgData = load(filename, 'ecgData');
            ecgData = ecgData.ecgData;
            ecgData.Filename = filename(1:(end - 11));
            data = load(filename, 'excludedIntervals');
            
            self.ExcludedIntervalsTable.Data = data.excludedIntervals;
            
            rData = load(filename, 'rWaveIndices');
            rData = rData.rWaveIndices;
            self.RWaveIndices = rData;
            
            self.SetECGData(ecgData);
        end
    end
    
    methods (Access = protected)
        function CreateChannelSelection(self)
            CreateChannelSelection@ECGPlotChannelPanel(self);
            
            set(self.ChannelPanel, 'position', [0 .52 1 .47]);
            
            self.QCPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 .25 1 .25],...
                'title', 'Excluded intervals',...
                'titlePosition', 'centertop',...
                'borderType', 'none');
            self.ExcludedIntervalsTable = uitable(...
                'parent', self.QCPanel,...
                'units', 'normalized',...
                'position', [0 .2 1 .8],...
                'backgroundColor', [1 1 1],...
                'Data', cell(1,3),...
                'cellEditCallback', @self.ReDraw,...
                'cellSelectionCallback', @self.SetSelectedTableRow);
            
            self.ExcludedIntervalsTable.ColumnName = {'Start (s)','End (s)','Comments'};
            self.ExcludedIntervalsTable.ColumnFormat = {'numeric','numeric','char'};
            self.ExcludedIntervalsTable.ColumnEditable = true;
            
            uicontrol('parent', self.QCPanel,...
                'units', 'normalized',...
                'position', [0 0 .5 .2],...
                'style', 'pushbutton',...
                'string', 'Add',...
                'callback', @self.AddInterval);
             uicontrol('parent', self.QCPanel,...
                'units', 'normalized',...
                'position', [.5 0 .5 .2],...
                'style', 'pushbutton',...
                'string', 'Delete',...
                'callback', @self.DeleteInterval);
            
            self.ApplyQCButton = uicontrol('parent', self.ControlPanel,...
                'units', 'normalized',...
                'position', [.025 0 .95 .05],...
                'style', 'pushbutton',...
                'string', 'Apply quality check to analysis',...
                'callback', @self.SaveAnalysis);
        end
        
        function ShowECGData(self)
            ShowECGData@ECGPlotChannelPanel(self);
            
            self.SetValidTimeLines();
        end
        
        function ReDraw(self, varargin)
            ReDraw@ECGPlotChannelPanel(self, varargin{:});
            
            self.SetValidTimeLines();
        end
        
        function SetChannelList(self)
            SetChannelList@ECGPlotChannelPanel(self);
        end
        
        function SetValidTimeLines(self)
            validPatches = ishandle(self.ExcludedIntervalPatches);
            if any(validPatches)
                delete(self.ExcludedIntervalPatches(validPatches));
            end
            
            yLimits = get(self.MainAxes, 'yLim');
            
            tableData = self.ExcludedIntervalsTable.Data;
            numberOfRows = size(tableData, 1);
            
            self.ExcludedIntervalPatches = NaN(numberOfRows, 1);
            for intervalIndex = 1:numberOfRows
                intervalStart = tableData{intervalIndex, 1};
                intervalEnd = tableData{intervalIndex, 2};
                if isempty(intervalStart) || isempty(intervalEnd)
                    continue;
                end
                
                self.ExcludedIntervalPatches(intervalIndex) =...
                    patch('xData', [intervalStart, intervalStart,...
                    intervalEnd, intervalEnd],...
                    'yData', [yLimits(1), yLimits(2), yLimits(2), yLimits(1)],...
                    'faceColor', [.8 .8 .8],...
                    'faceAlpha',0.5,...
                    'edgeColor', 'none',...
                    'parent', self.MainAxes);
            end
        end
        
        function AddInterval(self, varargin)
            tableData = self.ExcludedIntervalsTable.Data;
            tableData = [tableData; cell(1, size(tableData, 2))];
            self.ExcludedIntervalsTable.Data = tableData;
        end
        
        function DeleteInterval(self, varargin)
            if isempty(self.SelectedTableRow)
                return
            end
            
            tableData = self.ExcludedIntervalsTable.Data;
            if self.SelectedTableRow > 0 && self.SelectedTableRow <= size(tableData, 1)
                tableData(self.SelectedTableRow, :) = [];
            end
            self.ExcludedIntervalsTable.Data = tableData;
            
            self.ReDraw();
        end
        
        function SetSelectedTableRow(self, source, eventData)
            if isempty(eventData.Indices)
                return
            end
            self.SelectedTableRow = eventData.Indices(1);
        end
        
        function SaveAnalysis(self, varargin)
            excludedIntervals = self.ExcludedIntervalsTable.Data;
            
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            if exist(filename, 'file')
                save(filename, 'excludedIntervals', '-append');
            else
                ecgData = self.EcgData; 
                rWaveIndices = self.RWaveIndices;
                save(filename, 'ecgData', 'rWaveIndices', 'excludedIntervals');
            end
            notify(self, 'QualityCheckCompleted');
        end
    end
end