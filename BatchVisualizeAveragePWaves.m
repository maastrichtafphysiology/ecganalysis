%% Set folder location containing PWaveMatching files
foldername = '/Users/stefzeemering/Data/AFAB/AutomatedCustom';

%% Read files from folder
fileListing = dir(fullfile(foldername, '*_PWaveMatching.mat'));
filenames = {fileListing.name}';
numberOfFiles = numel(filenames);

%% For each file, show SA P-waves and save to figure / image
figureHandle = figure('name', 'P-Wave averages & annotations',...
    'numberTitle', 'off', 'toolbar', 'none', 'menu', 'none',...
    'position', [100, 100, 1200, 800]);
set(figureHandle,'PaperOrientation','landscape');
set(figureHandle,'PaperUnits','normalized');
set(figureHandle,'PaperPosition', [0 0 1 1]);

pWaveDurationView = PWaveDurationPanel([0 0 1 1]);
pWaveDurationView.Create(figureHandle);
pWaveDurationView.Show();

for fileIndex = 1:numberOfFiles
    filename = fullfile(foldername, filenames{fileIndex});
    pWaveData = load(filename);
    
    pWaveDurationView.SetPWaveDurationData(pWaveData);
    
    figureFilename = [filename(1:(end-17)), 'PWaveAverages'];
    
    print(figureHandle, '-dpdf', [figureFilename, '.pdf']);
end

close(figureHandle);