classdef FWavePanel < WindowedAnalysisPanel
    properties (Access = private)
        TQIntervals
        QRSTRange
        
        FWaveAmplitudes
        FWaveIndices
        FWavePeakValleyIndices
        WindowedFWA
        
        FWavePeakLines
        FWaveValleyLines
        
        RWaveLines
        TQPatches
        
        FWAAxesHandles
        FWAHistogramPanel
        
        DistanceThresholdEdit
        TQIntervalCheckbox
        QRSTStartEdit
        QRSTEndEdit
    end
    
    methods
        function self = FWavePanel(position)
            self = self@WindowedAnalysisPanel(position);
            
            self.TQIntervals = true;
            self.QRSTRange = [0.05 0.05];
            
            self.WindowLength = 10;
            self.WindowShift = 5;
        end
        
        function LoadAnalysis(self, filename)
            ecgData = load(filename, 'ecgData');
            ecgData = ecgData.ecgData;
            ecgData.Filename = filename(1:(end - 11));
            fWaveData = load(filename, 'FWave');
            fWaveData = fWaveData.FWave;
            
            self.AFComplexityCalculator.FWaveDistanceThreshold = fWaveData.settings.parameters;
            
            self.V1Index = fWaveData.settings.v1Index;
            self.WindowLength = fWaveData.settings.windowLength;
            self.WindowShift = fWaveData.settings.windowShift;
            
            self.TQIntervals = fWaveData.settings.tqIntervals;
            self.QRSTRange = fWaveData.settings.qrstRange;
            
            rData = load(filename, 'rWaveIndices');
            rData = rData.rWaveIndices;
            self.RWaveIndices = rData;
            
            self.FWaveAmplitudes = fWaveData.fWaveAmplitudes;
            self.FWaveIndices = fWaveData.fWaveIndices;
            self.FWavePeakValleyIndices = fWaveData.fWavePeakValleyIndices;
            
            self.InitializeSettings();
            
            self.SetECGData(ecgData);
                        
            if isfield(fWaveData, 'windowedFWA')
                self.WindowedFWA = fWaveData.windowedFWA;
            else
                self.WindowedFWA = self.ComputeWindowedFWA();
            end
            
            self.ShowAnalysis();
        end
        
        function analysisData = GetAnalysisData(self)
            fwaSettings = struct(...
                'parameters', self.AFComplexityCalculator.FWaveDistanceThreshold,...
                'tqIntervals', self.TQIntervals,...
                'qrstRange', self.QRSTRange,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift,...
                'v1Index', self.V1Index);
            
            analysisData = struct(...
                'windowedFWA', self.WindowedFWA,...
                'settings', fwaSettings);
        end
    end
    
    methods (Access = protected)
        function ShowECGData(self)
            ShowECGData@WindowedAnalysisPanel(self);
            
            self.ShowTQIntervals()
            
            if isempty(self.FWaveIndices), return; end
            
            self.ShowFWaveDetection();
            
            self.ShowAnalysis();
        end
        
        function CreateAnalysisSettings(self)
            set(self.SettingsPanel, 'position', [0 .3 1 .15]);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .7 .4 .2],...
                'style', 'text',...
                'string', 'F-wave interval (ms)');
            self.DistanceThresholdEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .7 .4 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.FWaveDistanceThreshold),...
                'callback', @self.SetFWaveDistanceThreshold);
            
            % TQ intervals
            self.TQIntervalCheckbox = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .4 .4 .2],...
                'style', 'checkbox',...
                'string', 'TQ Intervals',...
                'value', self.TQIntervals,...
                'callback', @self.EnableTQIntervals);
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .4 .2 .2],...
                'style', 'text',...
                'string', '< R (ms)');
            self.QRSTStartEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .2 .2 .2],...
                'style', 'edit',...
                'string', num2str(1000 * self.QRSTRange(1)),...
                'callback', @self.SetQRSTRangeBefore);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .4 .2 .2],...
                'style', 'text',...
                'string', '> R (ms)');
            self.QRSTEndEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .2 .2 .2],...
                'style', 'edit',...
                'string', num2str(1000 * self.QRSTRange(2)),...
                'callback', @self.SetQRSTRangeAfter);
            
            self.FWAHistogramPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .3],...
                'title', 'FWA histogram');
        end
        
        function InitializeSettings(self)
            InitializeSettings@WindowedAnalysisPanel(self);
            
            set(self.DistanceThresholdEdit, 'string', num2str(self.AFComplexityCalculator.FWaveDistanceThreshold));
            set(self.TQIntervalCheckbox, 'value', self.TQIntervals);
            set(self.QRSTStartEdit, 'string',  num2str(1000 * self.QRSTRange(1)));
            set(self.QRSTEndEdit, 'string', num2str(1000 * self.QRSTRange(2)));
        end
        
        function ComputeAnalysis(self, varargin)
            [self.FWaveAmplitudes, self.FWaveIndices, self.FWavePeakValleyIndices] = ...
                self.AFComplexityCalculator.ComputeFWaveAmplitudes(self.EcgData);
            
            self.WindowedFWA = self.ComputeWindowedFWA();
            
            self.ShowFWaveDetection();
            
            self.ShowAnalysis();
        end
        
        function windowedFWA = ComputeWindowedFWA(self)
            numberOfLeads = numel(self.FWaveAmplitudes);
            windowedFWA(numberOfLeads) = struct(...
                'windowStart', NaN,...
                'windowEnd', NaN,...
                'windowCenter', NaN,...
                'runningWindowMean', NaN,...
                'runningWindowStd',NaN,...
                'expandingWindowMean', NaN,...
                'expandingWindowStd', NaN);
            
            for channelIndex = 1:numberOfLeads
                fWaveAmplitudes = self.FWaveAmplitudes{channelIndex};
                fWavePeakIndices = self.FWaveIndices{channelIndex};
                validFWaveIndices = self.GetValidFWaves(...
                    fWavePeakIndices, self.FWavePeakValleyIndices{channelIndex});
                
                windowedFWA(channelIndex) = self.ComputeWindowedAmplitudes(fWaveAmplitudes(validFWaveIndices),...
                    fWavePeakIndices(validFWaveIndices));
            end
        end
        
        function ShowAnalysis(self)
            if isempty(self.FWaveAmplitudes), return; end
            
            delete(get(self.ResultPanel, 'children'));
            
            time = self.EcgData.GetTimeRange();
            electrodeLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            self.FWAAxesHandles = ECGChannelPanel.TightSubplot(numel(electrodeLabels), 1, self.ResultPanel,...
                0.02, [0.05, 0.05], [0.025, 0.05]);
            
            if ~isempty(self.WindowedFWA)
                % Windowed FWA
                for lineIndex = 1:numel(electrodeLabels)
                    channelIndex = self.SelectedChannels(lineIndex);
                    %                 fWaveAmplitudes = self.FWaveAmplitudes{channelIndex};
                    %                 fWavePeakIndices = self.FWaveIndices{channelIndex};
                    %                 validFWaveIndices = self.GetValidFWaves(...
                    %                     fWavePeakIndices, self.FWavePeakValleyIndices{channelIndex});
                    
                    windowedFWAInfo = self.WindowedFWA(channelIndex);
                    %                 windowedFWAInfo = self.ComputeWindowedAmplitudes(fWaveAmplitudes(validFWaveIndices),...
                    %                     fWavePeakIndices(validFWaveIndices));
                    self.PlotWindowedData(self.FWAAxesHandles(lineIndex), windowedFWAInfo);
                    
                    text('parent', self.FWAAxesHandles(lineIndex), 'string', electrodeLabels{lineIndex},...
                        'fontWeight', 'demi',...
                        'units', 'normalized', 'position', [0, .5, 0],...
                        'horizontalAlignment', 'right');
                    set(self.FWAAxesHandles(lineIndex), 'xTick', [],...
                        'YAxisLocation', 'right', 'xLim', [time(1), time(end)]);
                    
                    set(self.FWAAxesHandles(lineIndex), 'buttonDownFcn',...
                        {@self.ShowFWAHistogram, self.SelectedChannels(lineIndex), windowedFWAInfo});
                    
                    self.ShowQuality(lineIndex, windowedFWAInfo);
                end
            end
            
            set(self.FWAAxesHandles(end),'xTickMode', 'auto');
            xlabel(self.FWAAxesHandles(end), 'time (seconds)');
            if numel(self.FWAAxesHandles) > 1
                if ishandle(self.FWAAxesHandles)
                    linkaxes([self.AxesHandles; self.FWAAxesHandles], 'x');
                end
            end
        end
        
        function ShowQuality(self, axesIndex, windowedFWAInfo)
            qualityResolution = 100;
            bias = 20;
            qualityColors = hot(qualityResolution + bias);
            qualityColors = qualityColors((bias + 1):end, :);
            qualityColors = flipud(qualityColors);
            time = self.EcgData.GetTimeRange();
            
            fwaValues = windowedFWAInfo;
            qualityMeasure = fwaValues.runningWindowStd ./ fwaValues.runningWindowMean;
            qualityMeasure = (qualityMeasure - min(qualityMeasure)) / (max(qualityMeasure) - min(qualityMeasure));
            
            numberOfWindows = numel(fwaValues.windowStart);
            windowColorIndex = round((qualityResolution - 1) * (qualityMeasure)) + 1;
            axisYLimit = get(self.AxesHandles(axesIndex), 'ylim');
            patchHandles = NaN(numberOfWindows, 1);
            for windowIndex = 1:numberOfWindows
                windowStartTime = time(fwaValues.windowStart(windowIndex));
                windowEndTime = time(fwaValues.windowEnd(windowIndex));
                patchHandles(windowIndex) = patch('xData', [windowStartTime, windowStartTime, windowEndTime, windowEndTime],...
                    'yData', [axisYLimit(1), axisYLimit(end), axisYLimit(end), axisYLimit(1)],...
                    'facecolor', qualityColors(windowColorIndex(windowIndex), :),...
                    'edgeColor', 'none',...
                    'parent', self.AxesHandles(axesIndex));
            end
            uistack(patchHandles, 'bottom');
        end
        
        function SaveAnalysis(self, varargin)
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            dfSettings = struct(...
                'parameters', self.AFComplexityCalculator.FWaveDistanceThreshold,...
                'tqIntervals', self.TQIntervals,...
                'qrstRange', self.QRSTRange,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift,...
                'v1Index', self.V1Index);
            FWave = struct(...
                'fWaveAmplitudes', {self.FWaveAmplitudes},...
                'fWaveIndices', {self.FWaveIndices},...
                'fWavePeakValleyIndices', {self.FWavePeakValleyIndices},...
                'windowedFWA', self.WindowedFWA,...
                'settings', dfSettings); %#ok<NASGU>
            
            if exist(filename, 'file')
                save(filename, 'FWave', '-append');
            else
                ecgData = self.EcgData; %#ok<NASGU>
                rWaveIndices = self.RWaveIndices; %#ok<NASGU>
                save(filename, 'ecgData', 'rWaveIndices', 'FWave');
            end
            
            notify(self, 'AnalysisSaved');
        end
    end
    
    methods (Access = private)        
        function SetFWaveDistanceThreshold(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 0
                self.AFComplexityCalculator.FWaveDistanceThreshold = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.FWaveDistanceThreshold));
        end
        
        function EnableTQIntervals(self, source, varargin)
            self.TQIntervals = get(source, 'value') > 0;
            
            self.ShowECGData();
        end
        
        function SetQRSTRangeBefore(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QRSTRange(1) = value / 1000;
            self.ShowTQIntervals();
        end
        
        function SetQRSTRangeAfter(self, source, varargin)
            value = str2double(get(source, 'string'));
            self.QRSTRange(2) = value / 1000;
            self.ShowTQIntervals();
        end
        
        function ShowTQIntervals(self)
            validRWaveLines = ishandle(self.RWaveLines);
            if any(validRWaveLines)
                delete(self.RWaveLines(validRWaveLines));
            end
            
            validTQPatches = ishandle(self.TQPatches);
            if any(validTQPatches(:))
                delete(self.TQPatches(validTQPatches));
            end
            
            if ~self.TQIntervals, return; end
            
            time = self.EcgData.GetTimeRange();
            qrstRangeSamples = round(self.QRSTRange * self.EcgData.SamplingFrequency);
            qtIntervals = [self.RWaveIndices - qrstRangeSamples(1), self.RWaveIndices +  qrstRangeSamples(2)];
            qtIntervals(qtIntervals < 1) = 1;
            qtIntervals(qtIntervals > self.EcgData.GetNumberOfSamples) = self.EcgData.GetNumberOfSamples;
            self.RWaveLines = NaN(numel(self.AxesHandles), numel(self.RWaveIndices));
            self.TQPatches = NaN(numel(self.AxesHandles), numel(self.RWaveIndices));
            
            for axesIndex = 1:numel(self.SelectedChannels)
                yLimits = get(self.AxesHandles(axesIndex), 'yLim');
                for rIndex = 1:numel(self.RWaveIndices)
                    self.RWaveLines(rIndex) =...
                        line('xData', [time(self.RWaveIndices(rIndex)) time(self.RWaveIndices(rIndex))],...
                        'yData', yLimits,...
                        'color', [1 0 0],...
                        'parent', self.AxesHandles(axesIndex));
                    
                    try %#ok<TRYNC>
                        self.TQPatches(rIndex) =...
                            patch('xData', [time(qtIntervals(rIndex, 1)), time(qtIntervals(rIndex, 1)),...
                            time(qtIntervals(rIndex, 2)), time(qtIntervals(rIndex, 2))],...
                            'yData', [yLimits(1), yLimits(2), yLimits(2), yLimits(1)],...
                            'faceColor', [.8 .8 .8],...
                            'faceAlpha',0.5,...
                            'edgeColor', 'none',...
                            'parent', self.AxesHandles(axesIndex));
                    end
                end
            end
        end
        
        function ShowFWaveDetection(self)
            validHandles = ishandle(self.FWavePeakLines);
            if any(validHandles)
                delete(self.FWavePeakLines(validHandles));
            end
            
            validHandles = ishandle(self.FWaveValleyLines);
            if any(validHandles)
                delete(self.FWaveValleyLines(validHandles));
            end
            
            lineData = self.EcgData.Data(:, self.SelectedChannels);
            time = self.EcgData.GetTimeRange();
            
            fWavePeakIndices = self.FWaveIndices(self.SelectedChannels);
            fWaveValleyIndices = self.FWavePeakValleyIndices(self.SelectedChannels);
            peakLineColor = [1 0 0];
            
            fWavePeakValleyIndices = self.FWavePeakValleyIndices(self.SelectedChannels);
            valleyLineColor = [0 0 1];
            
            numberOfSelectedChannels = size(lineData, 2);
            self.FWavePeakLines = NaN(numberOfSelectedChannels, 1);
            self.FWaveValleyLines = NaN(numberOfSelectedChannels, 1);
            
            for lineIndex = 1:numberOfSelectedChannels
                currentFWavePeakIndices = fWavePeakIndices{lineIndex};
                currentFWaveValleyIndices = fWaveValleyIndices{lineIndex};
                validPeakIndices = self.GetValidFWaves(currentFWavePeakIndices, currentFWaveValleyIndices);
            
                self.FWavePeakLines(lineIndex) = line('xData', time(currentFWavePeakIndices(validPeakIndices)),...
                    'yData', lineData(currentFWavePeakIndices(validPeakIndices), lineIndex),...
                    'color', peakLineColor,...
                    'lineSmoothing', 'on',...
                    'parent', self.AxesHandles(lineIndex));
                
                currentFWavePeakValleyIndices = fWavePeakValleyIndices{lineIndex};
                self.FWaveValleyLines(lineIndex) = line('xData', time(currentFWavePeakValleyIndices(validPeakIndices)),...
                    'yData', lineData(currentFWavePeakValleyIndices(validPeakIndices), lineIndex),...
                    'color', valleyLineColor,...
                    'lineSmoothing', 'on',...
                    'parent', self.AxesHandles(lineIndex));
            end
        end
        
        function validFWaveIndices = GetValidFWaves(self, fWavePeakIndices, fWaveValleyIndices)
            if self.TQIntervals
                qrstRangeSamples = round(self.QRSTRange * self.EcgData.SamplingFrequency);
                qtIntervals = [self.RWaveIndices - qrstRangeSamples(1), self.RWaveIndices +  qrstRangeSamples(2)];
                
                validPeakIndices = ~any(bsxfun(@ge, fWavePeakIndices, qtIntervals(:, 1)') &...
                    bsxfun(@le, fWavePeakIndices, qtIntervals(:, 2)'), 2);
                
                validValleyIndices = ~any(bsxfun(@ge, fWaveValleyIndices, qtIntervals(:, 1)') &...
                    bsxfun(@le, fWaveValleyIndices, qtIntervals(:, 2)'), 2);
                
                validFWaveIndices = validPeakIndices & validValleyIndices;
            else
                validFWaveIndices = true(size(fWavePeakIndices));
            end
        end
        
        function result = ComputeWindowedAmplitudes(self, fWaveAmplitudes, fWavePeakIndices)
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            result = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            for windowIndex = 1:numberOfWindows
                % running window
                validFWavePeakIndices = fWavePeakIndices >= windowStart(windowIndex) &...
                    fWavePeakIndices <= windowEnd(windowIndex);
                result.runningWindowMean(windowIndex) = mean(fWaveAmplitudes(validFWavePeakIndices));
                result.runningWindowStd(windowIndex) = std(fWaveAmplitudes(validFWavePeakIndices));
                
                %expanding window
                validFWavePeakIndices = fWavePeakIndices <= windowEnd(windowIndex);
                result.expandingWindowMean(windowIndex) = mean(fWaveAmplitudes(validFWavePeakIndices));
                result.expandingWindowStd(windowIndex) = std(fWaveAmplitudes(validFWavePeakIndices));
            end
        end
        
        function ShowFWAHistogram(self, source, ~, channelIndex, windowInfo)
            delete(get(self.FWAHistogramPanel, 'children'));
            
            windowCenter = windowInfo.windowCenter;
            currentPoint = get(source, 'currentPoint');
            selectedTime = currentPoint(1);
            time = self.EcgData.GetTimeRange();
            centerTimes = time(windowCenter);
            [~, selectedWindowIndex] = min(abs(centerTimes - selectedTime));
            
            fWaveAmplitudes = self.FWaveAmplitudes{channelIndex};
            fWavePeakIndices = self.FWaveIndices{channelIndex};
            validFWaveIndices = self.GetValidFWaves(...
                fWavePeakIndices, self.FWavePeakValleyIndices{channelIndex});
            selectedFWaves = fWavePeakIndices >= windowInfo.windowStart(selectedWindowIndex) &...
                fWavePeakIndices <= windowInfo.windowEnd(selectedWindowIndex) &...
                validFWaveIndices;
            
            if ~any(selectedFWaves), return; end
                       
            plotHandle = subplot(1,1,1, 'parent', self.FWAHistogramPanel);
            histogram(plotHandle, fWaveAmplitudes(selectedFWaves));
            electrodeLabel = self.EcgData.ElectrodeLabels{self.V1Index};
            title(plotHandle, ['F-wave amplitude (', electrodeLabel, ')']);
            xlabel(plotHandle, 'amplitude');
            ylabel(plotHandle, 'count');
            title(plotHandle, [self.EcgData.ElectrodeLabels{channelIndex}, ' at t=', num2str(centerTimes(selectedWindowIndex)), 's']);
            
            infoText = {...
                ['median FWA: ', num2str(median(fWaveAmplitudes(selectedFWaves)), '%.3f'), 'mV'];...
                ['mean FWA: ', num2str(mean(fWaveAmplitudes(selectedFWaves)), '%.3f'), 'mV'];...
                ['CV: ', num2str(std(fWaveAmplitudes(selectedFWaves)) / mean(fWaveAmplitudes(selectedFWaves)), '%.2f')]};
            
            text(1, 1, 1, infoText, 'units', 'normalized',...
                'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'right',...
                'verticalAlignment', 'top', 'parent', plotHandle);
        end
    end
    
    methods (Static)
        function result = ComputeStaticWindowedAmplitudes(ecgData, fWaveAmplitudes, fWavePeakIndices, windowLength, windowShift)
            windowSampleLength = round(windowLength * ecgData.SamplingFrequency);
            windowSampleShift = round(windowShift * ecgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:ecgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength + 1):windowSampleShift:ecgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            result = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            for windowIndex = 1:numberOfWindows
                % running window
                validFWavePeakIndices = fWavePeakIndices >= windowStart(windowIndex) &...
                    fWavePeakIndices <= windowEnd(windowIndex);
                result.runningWindowMean(windowIndex) = mean(fWaveAmplitudes(validFWavePeakIndices));
                result.runningWindowStd(windowIndex) = std(fWaveAmplitudes(validFWavePeakIndices));
                
                %expanding window
                validFWavePeakIndices = fWavePeakIndices <= windowEnd(windowIndex);
                result.expandingWindowMean(windowIndex) = mean(fWaveAmplitudes(validFWavePeakIndices));
                result.expandingWindowStd(windowIndex) = std(fWaveAmplitudes(validFWavePeakIndices));
            end
        end
    end
end