classdef TEECGDFPanel < WindowedAnalysisPlotPanel    
    properties (Access = private)
        FFTPointsEdit
        DFRangeStartEdit
        DFRangeEndEdit
        
        SpectrumRangeStartEdit
        SpectrumRangeEndEdit
        SpectrumNormalizeCheck
        
        NormalizeSpectrum
        
        SpectrumRange
        SpectrumHandle
        SpectrumPanel
        
        Frequencies
        Power
        WindowedOI
        WindowFrequencyPower
        ExpandingWindowFrequencyPower
        WindowedDF
        WindowedSC
        WindowedRR
        DFIndex
        
        DFAxesHandles
        AverageSpectraLineHandles
    end
    
    methods
        function self = TEECGDFPanel(position)
            self = self@WindowedAnalysisPlotPanel(position);
            
            self.WindowLength = 10;
            self.WindowShift = 5;
            
            self.SpectrumRange = [0, 20];
            self.NormalizeSpectrum = false;
        end
        
        function SetECGData(self, ecgData, rWaveIndices)
            SetECGData@WindowedAnalysisPlotPanel(self, ecgData, rWaveIndices);
            set(self.SettingsPanel, 'title', ['Settings (Fs=', num2str(self.EcgData.SamplingFrequency), 'Hz)']);
            
            % set number of FFT points
            nFFT = 2^nextpow2(self.EcgData.SamplingFrequency * self.WindowLength);
            self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints = nFFT;
            self.AFComplexityCalculator.DFParameters.WindowLength = nFFT;
            
            self.InitializeSettings();
        end
        
        function LoadAnalysis(self, filename)
            ecgData = load(filename, 'ecgData');
            ecgData = ecgData.ecgData;
            ecgData.Filename = filename(1:(end - 11));
            dfData = load(filename, 'DF');
            dfData = dfData.DF;
            rWaveIndices = load(filename, 'rWaveIndices');
            rWaveIndices = rWaveIndices.rWaveIndices;
            
            self.Frequencies = dfData.frequencies;
            self.Power = dfData.powers;
            self.DFIndex = dfData.dfIndex;
            self.WindowedDF = dfData.windowedDF;
            self.WindowedOI = dfData.windowedOI;
            if isfield(dfData, 'windowFrequencyPower')
                self.WindowFrequencyPower = dfData.windowFrequencyPower;
            end
            if isfield(dfData, 'windowedRR')
                self.WindowedRR = dfData.windowedRR;
            end
            
            self.AFComplexityCalculator.DFParameters = dfData.settings.parameters;
            
            self.WindowLength = dfData.settings.windowLength;
            self.WindowShift = dfData.settings.windowShift;
            
            self.SetECGData(ecgData, rWaveIndices);
            
            self.InitializeSettings();
            
            self.SelectedWindowIndex = 1;
            self.SelectedChannelIndex = self.SelectedChannels(1);
            self.ShowAnalysis();
        end
        
        function analysisData = GetAnalysisData(self)
            dfSettings = struct(...
                'parameters', self.AFComplexityCalculator.DFParameters,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift);
            
            analysisData = struct(...
                'frequencies', self.Frequencies,...
                'powers', self.Power,...
                'dfIndex', self.DFIndex,...
                'windowedDF', self.WindowedDF,...
                'windowedOI', self.WindowedOI,...
                'windowedSC', self.WindowedSC,...
                'windowFrequencyPower', {self.WindowFrequencyPower},...
                'windowedRR', self.WindowedRR,...
                'settings', dfSettings);
        end
    end
    
    methods (Access = protected)
        function CreateAnalysisSettings(self)
            set(self.SettingsPanel, 'position', [0 .3 1 .25]);
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .75 .4 .2],...
                'style', 'text',...
                'string', 'FFT points');
            self.FFTPointsEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .75 .4 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints),...
                'callback', @self.SetNumberOfFFTPoints);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .5 .4 .2],...
                'style', 'text',...
                'string', 'DF range (Hz)');
            self.DFRangeStartEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .5 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)),...
                'callback', @self.SetDFRangeStart);
            self.DFRangeEndEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .5 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)),...
                'callback', @self.SetDFRangeEnd);
            
            uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .25 .4 .2],...
                'style', 'text',...
                'string', 'Spectrum range (Hz)');
            self.SpectrumRangeStartEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.5 .25 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.SpectrumRange(1)),...
                'callback', @self.SetSpectrumRangeStart);
            self.SpectrumRangeEndEdit = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.7 .25 .2 .2],...
                'style', 'edit',...
                'string', num2str(self.SpectrumRange(2)),...
                'callback', @self.SetSpectrumRangeEnd);
            
            self.SpectrumNormalizeCheck = uicontrol('parent', self.SettingsPanel,...
                'units', 'normalized',...
                'position', [.05 .05 .9 .2],...
                'style', 'checkbox',...
                'string', 'Normalize spectrum',...
                'value', self.NormalizeSpectrum,...
                'callback', @self.SetNormalizeSpectrum);
            
            self.SpectrumPanel = uipanel('parent', self.ControlPanel,...
                'units', 'normalized', ...
                'position', [0 0 1 .3],...
                'title', 'Spectrum');
        end
        
        function InitializeSettings(self)
            InitializeSettings@WindowedAnalysisPlotPanel(self);
            
            set(self.FFTPointsEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints));
            set(self.DFRangeStartEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)));
            set(self.DFRangeEndEdit, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)));
        end
        
        function ShowECGData(self)
            ShowECGData@WindowedAnalysisPlotPanel(self);
            self.ShowAnalysis();
        end
        
        function ComputeAnalysis(self, varargin)
            [self.DFIndex, self.Power, self.Frequencies] =...
                self.AFComplexityCalculator.ComputeDominantFrequency(self.EcgData);
            
            [self.WindowedDF, self.WindowedOI, self.WindowedSC] = self.ComputeWindowedDominantFrequency();
            
            self.WindowedRR = self.ComputeWindowedRRInterval();
            
            self.SelectedWindowIndex = 1;
            self.SelectedChannelIndex = self.SelectedChannels(1);
            self.ShowAnalysis();
        end
        
        function ShowAnalysis(self, varargin)
            if isempty(self.Frequencies), return; end
            
            time = self.EcgData.GetTimeRange();
            windowedDFInfo = self.WindowedDF(self.SelectedChannels);
            electrodeLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            
            % Overal spectrum & DF
            delete(get(self.ResultOverallPanel, 'children'));
            if ~isempty(self.Power)
                axesHandle = axes(self.ResultOverallPanel,...
                    'outerposition', [0,0,1,1], 'units', 'normalized');
                spectrumPower = self.Power;
                if self.NormalizeSpectrum
                    spectrumPower = bsxfun(@rdivide, spectrumPower, sum(spectrumPower, 1));
                end
                frequencies = self.Frequencies(:, 1);
                self.AverageSpectraLineHandles = plot(frequencies, spectrumPower(:, self.SelectedChannels),...
                    'parent', axesHandle);                
                xlabel('Frequency (Hz)');
                if self.NormalizeSpectrum
                    ylabel('Normalized power');
                else
                    ylabel('Power');
                end
                set(axesHandle, 'xlim', self.SpectrumRange);
                title(axesHandle, 'Average windowed spectrum');
                legend(electrodeLabels);
            end
            
            % Window spectrum & DF
            if ~isempty(self.SelectedWindowIndex) && ~isempty(self.SelectedChannelIndex)
                self.ShowSelectedWindowSpectra(self.SelectedWindowIndex, self.SelectedChannelIndex);
            end
            
            % Windowed DF
%             colormap(bones);
            delete(get(self.ResultPanel, 'children'));
            self.DFAxesHandles = ECGChannelPanel.TightSubplot(numel(electrodeLabels) + 1, 1, self.ResultPanel,...
                0.02, [0.05, 0.05], [0.025, 0.05]);
            set(self.DFAxesHandles, 'hitTest', 'on');
            validFrequencies = self.Frequencies(:, 1) >= self.SpectrumRange(1) &...
                self.Frequencies(:, 1) <= self.SpectrumRange(end);
            spectrumPower = cat(3, self.WindowFrequencyPower{:});
            if self.NormalizeSpectrum
                spectrumPower = bsxfun(@rdivide, spectrumPower, sum(spectrumPower, 1));
            end
            spectrumPower = spectrumPower(validFrequencies, self.SelectedChannels, :);
            for lineIndex = 1:numel(electrodeLabels)
                channelSpectrumPower = squeeze(spectrumPower(:, lineIndex, :));
                xData = windowedDFInfo(lineIndex).windowCenter;
                xData = time([xData(1), xData(end)]);
                yData = self.Frequencies(validFrequencies, 1);
                yData = [yData(1), yData(end)];
                imagesc('xData', xData, 'yData', yData,...
                    'CData', channelSpectrumPower,...
                    'parent', self.DFAxesHandles(lineIndex), 'hitTest', 'off');
                self.PlotWindowedData(self.DFAxesHandles(lineIndex), windowedDFInfo(lineIndex));
                ylabel(self.DFAxesHandles(lineIndex), 'Frequency (Hz)');
            
                text('parent', self.DFAxesHandles(lineIndex), 'string', electrodeLabels{lineIndex},...
                    'fontWeight', 'demi',...
                    'units', 'normalized', 'position', [0, .5, 0],...
                    'horizontalAlignment', 'right');
                set(self.DFAxesHandles(lineIndex), 'xTick', [],...
                    'YAxisLocation', 'right', 'xLim', get(self.MainAxes, 'xLim'));
                
                set(self.DFAxesHandles(lineIndex), 'ylim', self.SpectrumRange, 'box', 'on');
                set(self.DFAxesHandles(lineIndex), 'buttonDownFcn',...
                    {@self.ShowPowerSpectrum, self.SelectedChannels(lineIndex), windowedDFInfo(lineIndex).windowCenter});
                
                contextMenu = uicontextmenu('Parent', ancestor(self.DFAxesHandles(lineIndex), 'figure'));
                uimenu(contextMenu, 'Label', 'Copy DF-OI data to clipboard', 'callback', {@self.CopyWindowedDataToClipboard, self.SelectedChannels(lineIndex)});
                set(self.DFAxesHandles(lineIndex), 'UIContextMenu', contextMenu);
            end
            
            % RR interval
            self.PlotWindowedData(self.DFAxesHandles(end), self.WindowedRR);
            ylabel(self.DFAxesHandles(end), 'Interval (ms)');
            
            text('parent', self.DFAxesHandles(end), 'string', 'RR',...
                'fontWeight', 'bold',...
                'units', 'normalized', 'position', [0, .5, 0],...
                'horizontalAlignment', 'right');
            set(self.DFAxesHandles(end), 'xTick', [],...
                'YAxisLocation', 'right', 'xLim', [time(1), time(end)],...
                'box', 'on');
            contextMenu = uicontextmenu(ancestor(self.DFAxesHandles(end), 'figure'));
            uimenu(contextMenu, 'Label', 'Copy RR data to clipboard', 'callback', @self.CopyWindowedRRDataToClipboard);
            set(self.DFAxesHandles(end), 'UIContextMenu', contextMenu);
            
            set(self.DFAxesHandles(end),'xTickMode', 'auto');
            set(self.DFAxesHandles(end), 'xLim', get(self.MainAxes, 'xLim'));
            xlabel(self.DFAxesHandles(end), 'time (seconds)');
            if numel(self.DFAxesHandles) > 1
                if ishandle(self.DFAxesHandles)
                    linkaxes([self.MainAxes; self.DFAxesHandles], 'x');
                end
            end
        end
        
        function SaveAnalysis(self, varargin)
            data = [self.Frequencies(:), self.Power(:)];
            WindowedAnalysisPanel.CopyToClipboard(data);
            
            [pathString, nameString] = fileparts(self.EcgData.Filename);
            filename = [pathString, filesep, nameString '_Result.mat'];
            
            dfSettings = struct(...
                'parameters', self.AFComplexityCalculator.DFParameters,...
                'windowLength', self.WindowLength,...
                'windowShift', self.WindowShift);
            DF = struct(...
                'frequencies', self.Frequencies,...
                'powers', self.Power,...
                'dfIndex', self.DFIndex,...
                'windowedDF', self.WindowedDF,...
                'windowedOI', self.WindowedOI,...
                'windowedSC', self.WindowedSC,...
                'windowFrequencyPower', {self.WindowFrequencyPower},...
                'windowedRR', self.WindowedRR,...
                'settings', dfSettings); %#ok<NASGU>
            
            if exist(filename, 'file')
                save(filename, 'DF', '-append');
            else
                ecgData = self.EcgData; %#ok<NASGU>
                rWaveIndices = self.RWaveIndices; %#ok<NASGU>
                save(filename, 'ecgData', 'rWaveIndices', 'DF');
            end
            
            notify(self, 'AnalysisSaved');
        end
        
        function SetWindowLength(self, source, varargin)
            SetWindowLength@WindowedAnalysisPlotPanel(self, source, varargin{:});
             
            % set number of FFT points
            nFFT = 2^nextpow2(self.EcgData.SamplingFrequency * self.WindowLength);
            self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints = nFFT;
            self.AFComplexityCalculator.DFParameters.WindowLength = nFFT;
            
            self.InitializeSettings();
        end
    end
    
    methods (Access = private)
        function [dfResult, oiResult, scResult, windowedFrequencyPower] = ComputeWindowedDominantFrequency(self)
            samplingFrequency = self.EcgData.SamplingFrequency;
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            bandwidth = 1;
            
            numberOfChannels = self.EcgData.GetNumberOfChannels();
            
            dfResult(numberOfChannels) = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            oiResult = dfResult;
            scResult = dfResult;
            windowedFrequencyPower = cell(numberOfWindows, 1);
            
            for channelIndex = 1:(numberOfChannels - 1)
                dfResult(channelIndex) = dfResult(numberOfChannels);
                oiResult(channelIndex) = oiResult(numberOfChannels);
                scResult(channelIndex) = scResult(numberOfChannels);
            end
            
            originalData = self.EcgData.Data();
            complexityCalculator = self.AFComplexityCalculator;
            complexityCalculator.DFParameters.FundamentalFrequency = true;
            complexityCalculator.DFParameters.Threshold = 0.5;
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing windowed dominant frequency...', 0, self));
            
            windowDF = NaN(numberOfWindows, numberOfChannels);
            expandingWindowDF = NaN(numberOfWindows, numberOfChannels);
            windowOI = NaN(numberOfWindows, numberOfChannels);
            expandingWindowOI = NaN(numberOfWindows, numberOfChannels);
            windowSC = NaN(numberOfWindows, numberOfChannels);
            expandingWindowSC = NaN(numberOfWindows, numberOfChannels);
            for windowIndex = 1:numberOfWindows
                windowedData = originalData(windowStart(windowIndex):windowEnd(windowIndex), :);
                
                % running window
                [~, frequencies, ~, dominantFrequencyValues, ~, spectra] = ...
                    complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
                windowDF(windowIndex, :) = dominantFrequencyValues;
                windowedFrequencyPower{windowIndex} = frequencies;
                
                regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
                windowOI(windowIndex, :) = regularityIndices;
                
                spectralConcentrations = complexityCalculator.ComputeSignalSpectralConcentration(spectra);
                windowSC(windowIndex, :) = spectralConcentrations;
                
                %expanding window
                windowedData = originalData(1:windowEnd(windowIndex), :);
                [~, ~, ~, dominantFrequencyValues, ~, spectra] = ...
                    complexityCalculator.ComputeDominantSignalFrequency(windowedData, samplingFrequency);
                expandingWindowDF(windowIndex, :) = dominantFrequencyValues;
                
                regularityIndices = complexityCalculator.ComputeRegularityIndex(spectra, dominantFrequencyValues, bandwidth);
                expandingWindowOI(windowIndex, :) = regularityIndices;
                
                spectralConcentrations = complexityCalculator.ComputeSignalSpectralConcentration(spectra);
                expandingWindowSC(windowIndex, :) = spectralConcentrations;
            end
            
            self.WindowFrequencyPower = windowedFrequencyPower;
            
            for channelIndex = 1:(numberOfChannels)
                dfResult(channelIndex).runningWindowMean = windowDF(:, channelIndex);
                dfResult(channelIndex).expandingWindowMean = expandingWindowDF(:, channelIndex);
                
                oiResult(channelIndex).runningWindowMean = windowOI(:, channelIndex);
                oiResult(channelIndex).expandingWindowMean = expandingWindowOI(:, channelIndex);
                
                scResult(channelIndex).runningWindowMean = windowSC(:, channelIndex);
                scResult(channelIndex).expandingWindowMean = expandingWindowSC(:, channelIndex);
            end
            
            UserInterfacePkg.StatusEventClass.Instance().NotifyStatusChange(...
                UserInterfacePkg.StatusChangeEventData('Computing windowed dominant frequency...Done', 1, self));
        end
        
        function rrResult = ComputeWindowedRRInterval(self)
            windowSampleLength = round(self.WindowLength * self.EcgData.SamplingFrequency);
            windowSampleShift = round(self.WindowShift * self.EcgData.SamplingFrequency);
            
            windowStart = 1:windowSampleShift:self.EcgData.GetNumberOfSamples;
            windowEnd = (windowSampleLength):windowSampleShift:self.EcgData.GetNumberOfSamples;
            numberOfWindows = numel(windowEnd);
            windowStart = windowStart(1:numberOfWindows);
            
            rrResult = struct(...
                'windowStart', windowStart,...
                'windowEnd', windowEnd,...
                'windowCenter', round(windowStart + (windowEnd - windowStart) / 2),...
                'runningWindowMean', NaN(numberOfWindows, 1),...
                'runningWindowStd',NaN(numberOfWindows, 1),...
                'expandingWindowMean', NaN(numberOfWindows, 1),...
                'expandingWindowStd', NaN(numberOfWindows, 1));
            
            time = self.EcgData.GetTimeRange();
            rWaveTimes = time(self.RWaveIndices);
            for windowIndex = 1:numberOfWindows
                % running window
                validRRtimes = rWaveTimes(self.RWaveIndices >= windowStart(windowIndex) &...
                    self.RWaveIndices < windowEnd(windowIndex));
                if numel(validRRtimes) > 1
                    rrResult.runningWindowMean(windowIndex) = mean(diff(validRRtimes));
                    rrResult.runningWindowStd(windowIndex) = std(diff(validRRtimes));
                end
                
                % expanding window
                validRRtimes = rWaveTimes(self.RWaveIndices < windowEnd(windowIndex));
                if numel(validRRtimes) > 1
                    rrResult.expandingWindowMean(windowIndex) = mean(diff(validRRtimes));
                    rrResult.expandingWindowStd(windowIndex) = std(diff(validRRtimes));
                end
            end
        end
        
        function SetNumberOfFFTPoints(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value > 0
                self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints = value;
                self.AFComplexityCalculator.DFParameters.WindowLength = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.NumberOfFFTPoints));
        end
        
        function SetDFRangeStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.AFComplexityCalculator.DFParameters.DFRange(1) = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(1)));
        end
        
        function SetDFRangeEnd(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.AFComplexityCalculator.DFParameters.DFRange(2) = value;
            end
            set(source, 'string', num2str(self.AFComplexityCalculator.DFParameters.DFRange(2)));
        end
        
        function SetSpectrumRangeStart(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.SpectrumRange(1) = value;
            end
            set(source, 'string', num2str(self.SpectrumRange(1)));
            
            self.ShowAnalysis();
        end
        
        function SetSpectrumRangeEnd(self, source, varargin)
            value = str2double(get(source, 'string'));
            if value >= 0
                self.SpectrumRange(2) = value;
            end
            set(source, 'string', num2str(self.SpectrumRange(2)));
            
            self.ShowAnalysis();
        end
        
        function SetNormalizeSpectrum(self, source, varargin)
            value = get(source, 'value');
            if value > 0
                self.NormalizeSpectrum = true;
            else
                self.NormalizeSpectrum = false;
            end
            
            self.ShowAnalysis();
        end
        
        function ShowSelectedWindowSpectra(self, windowIndex, channelIndex)
            spectrumPower = self.WindowFrequencyPower{windowIndex};
            if self.NormalizeSpectrum
                spectrumPower = bsxfun(@rdivide, spectrumPower, sum(spectrumPower, 1));
            end
            % Spectra of selected channels
            delete(get(self.ResultTemporalPanel, 'children'));
            axesHandle = axes(self.ResultTemporalPanel,...
                'outerposition', [0,0,1,1], 'units', 'normalized');
            frequencies = self.Frequencies(:, 1);
            lineHandles = plot(frequencies, spectrumPower(:, self.SelectedChannels),...
                'parent', axesHandle);
            lineIndex = self.SelectedChannels == channelIndex;
            set(lineHandles(lineIndex), 'lineWidth', 1.5);
            if ~isempty(self.AverageSpectraLineHandles)
                set(self.AverageSpectraLineHandles, 'lineWidth', 0.5);
                set(self.AverageSpectraLineHandles(lineIndex), 'lineWidth', 1.5);
            end
            xlabel('Frequency (Hz)');
            if self.NormalizeSpectrum
                ylabel('Normalized power');
            else
                ylabel('Power');
            end
            set(axesHandle, 'xlim', self.SpectrumRange);
            electrodeLabels = self.EcgData.ElectrodeLabels(self.SelectedChannels);
            legend(electrodeLabels);
                
            % Spectrum + information on single channel
            dfData = self.WindowedDF(channelIndex);
            powerData = spectrumPower(:, channelIndex);
            time = self.EcgData.GetTimeRange();
            windowTime = time(dfData.windowCenter(windowIndex));
            selectedDF = dfData.runningWindowMean(windowIndex);
            oiData = self.WindowedOI(channelIndex);
            selectedOI = oiData.runningWindowMean(windowIndex);
            if ~isempty(self.WindowedSC)
                scData = self.WindowedSC(channelIndex);
                selectedSC = scData.runningWindowMean(windowIndex);
            else
                selectedSC = NaN;
            end
            
            delete(get(self.SpectrumPanel, 'children'));
            plotHandle = subplot(1,1,1, 'parent', self.SpectrumPanel);
            line('xData', [selectedDF, selectedDF],...
                'yData', [0, powerData(self.Frequencies(:, channelIndex)==selectedDF)],...
                'color', [1,0,0], 'lineStyle', '-', 'lineWidth', 1.5);
            line('xData', self.Frequencies(:, channelIndex), 'yData', powerData,...
                'color', [0,0,1], 'lineSmoothing', 'on', 'parent', plotHandle);
            set(plotHandle, 'xlim', self.SpectrumRange);
            xlabel(plotHandle, 'Frequency (Hz)');
            ylabel(plotHandle, 'Power');
            title(plotHandle, [self.EcgData.ElectrodeLabels{channelIndex},...
                ' at t=', num2str(windowTime), 's']);
            title(axesHandle, ['Window spectra at t=', num2str(windowTime), 's']);
            
            infoText = {...
                ['DF: ', num2str(selectedDF, '%.2f'), 'Hz'];...
                ['OI: ', num2str(100 * selectedOI, '%.0f'), '%'];...
                ['SC: ', num2str(100 * selectedSC, '%.0f'), '%']};
            
            text(1, 1, 1, infoText, 'units', 'normalized',...
                'backgroundColor', [.7 .7 .7], 'horizontalAlignment', 'right',...
                'verticalAlignment', 'top', 'parent', plotHandle);
        end
        
        function ShowPowerSpectrum(self, source, ~, channelIndex, windowCenter)
            delete(get(self.SpectrumPanel, 'children'));
            
            currentPoint = get(source, 'currentPoint');
            selectedTime = currentPoint(1);
            time = self.EcgData.GetTimeRange();
            centerTimes = time(windowCenter);
            [~, selectedWindowIndex] = min(abs(centerTimes - selectedTime));
            self.SelectedChannelIndex = channelIndex;
            self.SelectedWindowIndex = self.SelectedWindowIndex;
            self.ShowSelectedWindowSpectra(selectedWindowIndex, channelIndex)
        end
        
        function CopyWindowedDataToClipboard(self, source, ~, channelIndex)
            dfData = self.WindowedDF(channelIndex);
            oiData = self.WindowedOI(channelIndex);
            
            time = self.EcgData.GetTimeRange();
            centerTimes = time(dfData.windowCenter);
            
            UtilityPkg.num2clip([centerTimes(:), dfData.runningWindowMean(:), oiData.runningWindowMean(:)]);
        end
        
        function CopyWindowedRRDataToClipboard(self, ~, ~)
            rrData = self.WindowedRR;
            time = self.EcgData.GetTimeRange();
            centerTimes = time(rrData.windowCenter);
            
            UtilityPkg.num2clip([centerTimes(:), rrData.runningWindowMean(:), rrData.runningWindowStd(:)]);
        end
    end
end